#!/bin/bash
set -x
envsubst < /usr/share/nginx/html/assets/js/env.template.js > /usr/share/nginx/html/assets/js/env.js
nginx -g 'daemon off;'
