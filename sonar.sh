#!/bin/bash
set -x

export TAGS="mw"  # comma separated, go nuts...

# KL: tried using volumn-mount method as described by sonar-documentation; but doesn't seem to work with this Jenkins instance. so the next best method is to bake source directly into an intermediate image
docker build --rm -f Dockerfile.sonar -t sonarsrc:$BUILD_TAG .
docker run --rm -t sonarsrc:$BUILD_TAG -Dsonar.scm.disabled=true -Dsonar.projectKey=$REPO_NAME -Dsonar.projectName=$REPO_NAME -Dsonar.projectVersion=$BUILD_TAG -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.login=$SONAR_AUTH_TOKEN
sleep 1

docker rmi sonarsrc:$BUILD_TAG
curl -X POST -v -u $SONAR_AUTH_TOKEN: "$SONAR_HOST_URL/api/project_tags/set?project=$REPO_NAME&tags=$TAGS"
