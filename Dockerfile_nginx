FROM klee310/cfh-node-e2e:latest as BUILD

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .
# RUN npm run build
RUN npm run build:prod

FROM nginx:1.19.6 as NGINX
COPY --from=BUILD /usr/src/app/dist/cathay-vn-channel-mobile-web /usr/share/nginx/html
COPY --from=BUILD /usr/src/app/default.conf /etc/nginx/conf.d/default.conf
COPY --from=BUILD /usr/src/app/start.sh /
CMD ["/start.sh"]

# Local testing usage:
# $ docker build --rm -t channel/mobileweb . && docker run -it -p 8080:80 channel/mobileweb
