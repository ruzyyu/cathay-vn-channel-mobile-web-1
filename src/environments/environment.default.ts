export const defaultEnvironment = {
  production: false,
  /** getway */
  apiUrl: window['env']['apiUrl'] || 'http://10.20.30.210:8086/',

  /** APP版號 */
  appVersion: require('../../package.json').version + '-dev',

  /** 不清楚功能 */
  ipAddressUrl: '//api.ipify.org/',
  successCode: '0000',

  /** sso位置 */
  cathaylifebaseurl:window['env']['cathaylifebaseurl']  ||
    'https://sbcs.cathaylife.com.vn/CPWeb/servlet/HttpDispatcher/CPZ0_0130/SSO',
  cathaylifesso: window['env']['cathaylifesso'] || 'https://sbcs.cathaylife.com.vn/',

  /** 支付變數 */
  pay_account: window['env']['vtc_account'] || '0963465816',
  websitId: window['env']['vtc_websitid'] || '182421',
  pay_pwd: window['env']['vtc_paypwd'] || '@QAZ2wsx3edc4rfv',
  VNPay_Url:
    window['env']['vtc_payurl'] ||
    'http://alpha1.vtcpay.vn/portalgateway/checkout.html',
  baseUrl: 'http://127.0.0.1:4200',

  /** 加解密開關 */
  ActiveEnc: false,

  /** Google相關變數 */
  google: {
    GAID: 'G-LFFW5GMWXP',
    AuthID:
      '586186969803-n48g4l0fmofb5rpicaet2n4mjed0237o.apps.googleusercontent.com',
  },
  /** FB相關變數 */
  fb: {
    AuthID: '217922036623039',
  },
};
