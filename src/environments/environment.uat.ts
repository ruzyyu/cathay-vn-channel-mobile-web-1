import { defaultEnvironment } from './environment.default';

export const environment = {
  ...defaultEnvironment,
  apiUrl: window['env']['apiUrl'] || 'https://api-uat.cathay-ins.com.vn:9527/',
  baseurl: 'https://uat.cathay-ins.com.vn',
  production: true,
};
