import { PublicService } from 'src/app/core/services/api/public/public.service';
import { MetaService } from './core/services/meta.service';
import { CommonService } from 'src/app/core/services/common.service';
import { versionInfo } from './../version-info';
import { AfterViewInit, Component, HostListener, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { Router, NavigationStart, ActivationEnd } from '@angular/router';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { IRouteData } from 'src/app/core/interface';
import { DatapoolService } from './core/services/datapool/datapool.service';
import { isScullyGenerated } from '@scullyio/ng-lib';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  currentApplicationVersion = environment.appVersion;

  version: string;

  slides = [
    { img: 'http://placehold.it/350x150/000000' },
    { img: 'http://placehold.it/350x150/111111' },
    { img: 'http://placehold.it/350x150/333333' },
    { img: 'http://placehold.it/350x150/666666' },
  ];

  slideConfig = { slidesToShow: 4, slidesToScroll: 2 };

  mlang: 'vi-VN' | 'zh-TW' | 'en-US';

  originalSize: number;

  addSlide() {
    this.slides.push({ img: 'http://placehold.it/350x150/777777' });
  }

  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }

  constructor(
    private metaService: Meta,
    private router: Router,
    private cs: CommonService, // 依賴注入必要，請勿刪除
    metaData: MetaService,
    cds: DatapoolService,
    private pb: PublicService
  ) {
    this.mlang = cds.gLangType;

    // 前路由
    router.events
      .pipe(
        distinctUntilChanged((previous: any, current: any) => {
          if (current instanceof NavigationStart) {
            return false;
          }
          return true;
        })
      )
      .subscribe((x: any) => {
        /** seo */
        metaData.addTags(x.url);
      });

    // 後路由
    this.router.events
      .pipe(
        filter(
          (event) =>
            event instanceof ActivationEnd &&
            event.snapshot.children.length === 0
        )
      )
      .subscribe((e: ActivationEnd) => {
        this.removeModalBackdrops();
        this.setMetaAndTitle(
          this.getTitleFromData(e.snapshot.data as IRouteData)
        );
      });

    /** 處理執行中才需要加載的東西 */
    if (isScullyGenerated()) {
      (function (w, d, l, i) {
        w[l] = w[l] || [];
        w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });
        var f = d.getElementsByTagName('script')[0],
          j = d.createElement('script'),
          dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'dataLayer', 'GTM-57R7FCB');

      (function (d, url) {
        var f = d.getElementsByTagName('script')[0];
        var j = d.createElement('script');
        j.async = true;
        j.src = url;
        f.parentNode.insertBefore(j, f);
      })(document, 'https://www.google.com/recaptcha/api.js');
    }
  }

  ngAfterViewInit(): void {
    document.body.className = this.mlang;
    this.closeScalableIOS();
    // 動態預載入資料
    this.pb.gAirPortList();
  }

  /** 關閉ios延伸，避免日期放大錯誤 */
  closeScalableIOS() {
    if (CommonService.isIOS()) {
      const meta: any = document.getElementsByTagName('meta');
      meta.viewport.content =
        'width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.5, user-scalable=0';
    }
  }

  /** 移除 Bootstrap 背景遮罩 */
  removeModalBackdrops() {
    const body = document.querySelector('body.modal-open');
    if (body) {
      const backdrops = body.querySelectorAll('.modal-backdrop');
      if (backdrops.length > 1) {
        body.removeChild(backdrops[0]);
      } else if (backdrops.length === 1) {
        body.removeChild(backdrops[0]);
        body.classList.remove('modal-open');
      }
    }
  }

  setMetaAndTitle(data: string) {
    // super.setTitle(data);
    this.metaService.updateTag({
      name: 'twitter:title',
      content: data,
    });
    this.metaService.updateTag({
      property: 'og:title',
      content: data,
    });
  }

  /** 取得 IP */
  // getIp() {
  //   if (!StorageService.getItem(StorageKeys.ip)) {
  //     this.http
  //       .get(environment.ipAddressUrl, { responseType: 'text' })
  //       .subscribe((x) => StorageService.setItem(StorageKeys.ip, x));
  //   }
  // }

  // Android Input 被鍵盤擋住修正
  @HostListener('window:resize')
  onResize() {
    setTimeout(() => {
      // 鍵盤是否開啟
      if ($(window).width() + $(window).height() != this.originalSize) {
        /** 固定在鍵盤上方的元素 */
        const fixBottomElement = $('.total-content.fix-bottom');
        /** Header */
        const header = $('.fixed-top');

        // 如果鍵盤上方有固定元素且擋到 focus 的 Input，重新計算滾動位置
        if (fixBottomElement.css('position') === 'fixed') {
          const focusInput = $('input:focus');

          if (
            focusInput.length &&
            focusInput.offset().top + focusInput.outerHeight() >
              fixBottomElement.offset().top
          ) {
            $('html, body').animate(
              {
                scrollTop:
                  focusInput.offset().top +
                  header.outerHeight() +
                  fixBottomElement.outerHeight() -
                  $(window).height() +
                  10,
              },
              200
            );
          }
        }
      }
    }, 50);
  }

  async ngOnInit(): Promise<void> {
    this.originalSize = $(window).width() + $(window).height();

    this.generateVer();
    /** 導頁前，統一事件處理 */
    this.router.events
      .pipe(
        distinctUntilChanged((previous: any, current: any) => {
          if (current instanceof NavigationStart) {
            return false;
          }
          return true;
        })
      )
      .subscribe((x: any) => {
        /** 移除未關閉的日期窗 */
        $('.daterangepicker').remove();
      });
  }

  generateVer() {
    this.version = `${environment.appVersion}@${versionInfo.hash}`;
  }

  getTitleFromData(item: IRouteData) {
    if (item.hideTitle) {
      return '';
    }

    switch (this.mlang) {
      case 'zh-TW':
        return item.breadcrumb;
      case 'en-US':
        return item.enbreadcrumb;
      default:
        return item.vnbreadcrumb;
    }
  }
}
