import { RouterFilterService } from './core/services/utils/router-filter.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { MainLayoutComponent } from './shared/layout/app-layout/main-layout/main-layout.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      /** 首頁 */
      {
        path: '',
        loadChildren: () =>
          import('./features/index/index.module').then((m) => m.IndexModule),
        data: {
          breadcrumb: '首頁',
          vnbreadcrumb: 'Trang Chủ',
          enbreadcrumb: 'Homepage',
          hideTitle: true,
        },
      },
      /** 關於國泰 */
      {
        path: 'about',
        data: {
          breadcrumb: '公司介紹',
          vnbreadcrumb: 'Giới Thiệu Công Ty',
          enbreadcrumb: 'Company Profile',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./features/about/about.module').then(
                (m) => m.AboutModule
              ),
          },
          {
            path: 'opening',
            children: [
              {
                path: '',
                loadChildren: () =>
                  import(
                    'src/app/features/job-opening/opening/opening.module'
                  ).then((m) => m.OpeningModule),
                data: {
                  breadcrumb: '職缺總覽',
                  vnbreadcrumb: 'Tuyển Dụng',
                  enbreadcrumb: 'Job overview',
                },
              },
              {
                path: 'opening-detail',
                loadChildren: () =>
                  import(
                    'src/app/features/job-opening/opening-detail/opening-detail.module'
                  ).then((m) => m.OpeningDetailModule),
                data: {
                  breadcrumb: '新消息公告',
                  vnbreadcrumb: 'Tin Mới',
                  enbreadcrumb: 'New',
                },
              },
            ],
            data: {
              breadcrumb: '職缺總覽',
              vnbreadcrumb: 'Tuyển Dụng',
              enbreadcrumb: 'Job overview',
            },
          },
        ],
      },
      /** 幫助中心 */
      {
        path: 'help',
        loadChildren: () =>
          import('./features/help/help.module').then((m) => m.HelpModule),
        data: {
          breadcrumb: '幫助中心',
          vnbreadcrumb: 'Hỗ Trợ',
          enbreadcrumb: 'Help Center',
        },
      },
      /** 最新消息 */
      {
        path: 'news',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('src/app/features/news/news/news.module').then(
                (m) => m.NewsModule
              ),
            data: {
              breadcrumb: '最新消息',
              vnbreadcrumb: 'Tin Tức',
              enbreadcrumb: 'News',
            },
          },
          {
            path: 'news-detail',
            loadChildren: () =>
              import(
                'src/app/features/news/news-detail/news-detail.module'
              ).then((m) => m.NewsDetailModule),
            data: {
              breadcrumb: '新消息公告',
              vnbreadcrumb: 'Tin Mới',
              enbreadcrumb: 'New',
            },
          },
        ],
        data: {
          breadcrumb: '最新消息',
          vnbreadcrumb: 'Tin Tức',
          enbreadcrumb: 'News',
        },
      },
      /** 保戶服務中心 */
      {
        path: 'member',
        data: {
          breadcrumb: '保戶服務中心',
          vnbreadcrumb: 'Trung Tâm Khách Hàng',
          enbreadcrumb: 'Customer Services Center',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                'src/app/features/member/member-center/member-center.module'
              ).then((m) => m.MemberCenterModule),
            data: {
              breadcrumb: '保戶服務中心',
              vnbreadcrumb: 'Trung Tâm Khách Hàng',
              enbreadcrumb: 'Customer Services Center',
            },
            // canActivate: [RouterFilterService],
          },
          /** 強制險查詢 */
          {
            path: 'insurance-query',
            loadChildren: () =>
              import(
                'src/app/features/member/insurance-query/insurance-query.module'
              ).then((m) => m.InsuranceQueryModule),
            data: {
              breadcrumb: '強制險查詢',
              vnbreadcrumb: 'Tra Cứu Bảo Hiểm Bắt Buộc TNDS',
              enbreadcrumb: 'Mandatory insurance query',
            },
          },
          /** 我的訂單 */
          {
            path: 'my-order',
            data: {
              breadcrumb: '我的訂單',
              vnbreadcrumb: 'Đơn Hàng',
              enbreadcrumb: 'My Orders',
            },
            children: [
              {
                path: '',
                loadChildren: () =>
                  import(
                    'src/app/features/member/my-order/my-order.module'
                  ).then((m) => m.MyOrderModule),
                data: {
                  breadcrumb: '我的訂單',
                  vnbreadcrumb: 'Đơn Hàng',
                  enbreadcrumb: 'My Orders',
                },
                canActivate: [RouterFilterService],
              },
              {
                path: 'detail',
                loadChildren: () =>
                  import(
                    './features/member/my-order/order-detail/order-detail.module'
                  ).then((m) => m.OrderDetailModule),
                data: {
                  breadcrumb: '訂單資訊',
                  vnbreadcrumb: 'Thông Tin Đơn',
                  enbreadcrumb: 'Order Information',
                },
                canActivate: [RouterFilterService],
              },
              {
                path: 'result',
                loadChildren: () =>
                  import(
                    './features/member/my-order/order-result/order-result.module'
                  ).then((m) => m.OrderResultModule),
                data: {
                  breadcrumb: '訂單資訊',
                  vnbreadcrumb: 'Thông Tin Đơn',
                  enbreadcrumb: 'Order Information',
                },
                canActivate: [RouterFilterService],
              },
            ],
          },
          /** 理賠 */
          {
            path: 'claim',
            data: {
              breadcrumb: '我要理賠',
              vnbreadcrumb: 'Yêu Cầu Bồi Thường',
              enbreadcrumb: 'Make a Claim',
            },
            children: [
              {
                path: '',
                loadChildren: () =>
                  import(
                    './features/member/claim/insurance/insurance.module'
                  ).then((m) => m.InsuranceModule),
                data: {
                  breadcrumb: '我要理賠',
                  vnbreadcrumb: 'Yêu Cầu Bồi Thường',
                  enbreadcrumb: 'Make a Claim',
                },
                // canActivate: [RouterFilterService],
              },
              {
                path: 'introduction',
                loadChildren: () =>
                  import(
                    './features/member/claim/Introduction/introduction.module'
                  ).then((m) => m.IntroductionModule),
                data: {
                  // 動態轉換麵包屑
                  dynamic(item, data) {
                    return switchUrl(item, data);
                  },
                },
              },
              {
                path: 'fast-claim',
                loadChildren: () =>
                  import(
                    './features/member/claim/fast-claim/fast-claim.module'
                  ).then((m) => m.FastClaimModule),
                data: {
                  breadcrumb: '選擇理賠保單',
                  vnbreadcrumb: 'Xin Chọn Hợp Đồng Bồi Thường',
                  enbreadcrumb: 'Select policy for claiming',
                },
              },
              {
                path: 'claim-apply',
                loadChildren: () =>
                  import(
                    './features/member/claim/claim-apply/claim-apply.module'
                  ).then((m) => m.ClaimApplyModule),
                data: {
                  breadcrumb: '線上理賠申請',
                  vnbreadcrumb: 'Yêu Cầu Bồi Thường Trực Tuyến',
                  enbreadcrumb: 'Online Claims',
                },
              },
              {
                path: 'claim-result',
                loadChildren: () =>
                  import(
                    './features/member/claim/claim-result/claim-result.module'
                  ).then((m) => m.ClaimResultModule),
                data: {
                  breadcrumb: '線上理賠申請',
                  vnbreadcrumb: 'Yêu Cầu Bồi Thường Trực Tuyến',
                  enbreadcrumb: 'Online Claims',
                },
              },
              {
                path: 'search',
                loadChildren: () =>
                  import('./features/member/claim/search/search.module').then(
                    (m) => m.SearchModule
                  ),
                data: {
                  breadcrumb: '理賠進度查詢',
                  vnbreadcrumb: 'Tiến Độ Bồi Thường',
                  enbreadcrumb: 'Claims Progress Enquiry',
                },
                canActivate: [RouterFilterService],
              },
            ],
          },
          /** 我的保單 */
          {
            path: 'my-policy',
            data: {
              breadcrumb: '我的保單',
              vnbreadcrumb: 'Hợp Đồng Bảo Hiểm',
              enbreadcrumb: 'My Policy',
            },
            children: [
              {
                path: '',
                loadChildren: () =>
                  import(
                    'src/app/features/member/my-policy/my-policy.module'
                  ).then((m) => m.MyPolicyModule),
                data: {
                  breadcrumb: '我的保單',
                  vnbreadcrumb: 'Hợp Đồng Bảo Hiểm',
                  enbreadcrumb: 'My Policy',
                },
                canActivate: [RouterFilterService],
              },
              {
                path: 'detail',
                loadChildren: () =>
                  import(
                    'src/app/features/member/my-policy/policy-detail/policy-detail.module'
                  ).then((m) => m.PolicyDetailModule),
                data: {
                  breadcrumb: '保單資訊',
                  vnbreadcrumb: 'Thông Tin Hợp Đồng',
                  enbreadcrumb: 'Policy Information',
                },
                canActivate: [RouterFilterService],
              },
              {
                path: 'insurance-changing',
                loadChildren: () =>
                  import(
                    './features/member/my-policy/insurance-changing/insurance-changing.module'
                  ).then((m) => m.InsuranceChangingModule),
                data: {
                  breadcrumb: '保單變更',
                  vnbreadcrumb: 'Thay đổi hợp đồng',
                  enbreadcrumb: 'Insurance Endorsement',
                },
                canActivate: [RouterFilterService],
              },
            ],
          },
          /** 我的保單下載 */
          {
            path: 'my-policy-download',
            loadChildren: () =>
              import(
                'src/app/features/member/my-policy-download/my-policy-download.module'
              ).then((m) => m.MyPolicyDownloadModule),
          },
          /** 會員維護 */
          {
            path: 'maintain',
            loadChildren: () =>
              import('src/app/features/member/maintain/maintain.module').then(
                (m) => m.MaintainModule
              ),
            data: {
              breadcrumb: '保戶資料維護',
              vnbreadcrumb: 'Thông Tin Hội Viên',
              enbreadcrumb: 'Customer Information Maintenance',
            },
            canActivate: [RouterFilterService],
          },
          {
            path: 'important-notice',
            loadChildren: () =>
              import(
                'src/app/features/member/important-notice/important-notice.module'
              ).then((m) => m.ImportantNoticeModule),
            data: {
              breadcrumb: '重要通知',
              vnbreadcrumb: 'Thông Báo Quan Trọng',
              enbreadcrumb: 'important Notice',
            },
          },
          {
            path: 'verify',
            loadChildren: () =>
              import('./features/member/verify/verify.module').then(
                (m) => m.VerifyModule
              ),
            data: {
              breadcrumb: '開通線下保單資訊',
              vnbreadcrumb: 'Xem Đơn Bảo Hiểm Mua Từ Đại Lý',
              enbreadcrumb: 'Activate Offline Policy Information',
            },
            canActivate: [RouterFilterService],
          },
        ],
      },
      /** 註冊 */
      {
        path: 'login',
        data: {
          breadcrumb: '保戶服務中心',
          vnbreadcrumb: 'Trung Tâm Khách Hàng',
          enbreadcrumb: 'Customer Services Center',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import('src/app/features/login/login/login.module').then(
                (m) => m.MemberLoginModule
              ),
            data: {
              breadcrumb: '會員登入',
              vnbreadcrumb: 'Đăng Nhập Hội Viên',
              enbreadcrumb: 'Member Login',
            },
          },
          {
            path: 'register-id',
            loadChildren: () =>
              import('./features/login/register-id/register-id.module').then(
                (m) => m.MemberRegisteridModule
              ),
            data: {
              breadcrumb: '註冊',
              vnbreadcrumb: 'Đăng Ký',
              enbreadcrumb: 'Registration',
            },
          },
          {
            path: 'register-data',
            loadChildren: () =>
              import(
                './features/login/register-data/register-data.module'
              ).then((m) => m.RegisterDataModule),
            data: {
              breadcrumb: '註冊',
              vnbreadcrumb: 'Đăng Ký',
              enbreadcrumb: 'Registration',
            },
          },
          {
            path: 'otp-verification',
            loadChildren: () =>
              import(
                './features/login/otp-verification/otp-verification.module'
              ).then((m) => m.OtpVerificationModule),
            data: {
              breadcrumb: 'OTP驗證',
              vnbreadcrumb: 'Mã Xác Thực',
              enbreadcrumb: 'OTP Verification',
            },
          },
          {
            path: 'change-password',
            loadChildren: () =>
              import(
                './features/login/change-password/change-password.module'
              ).then((m) => m.ChangePasswordModule),
            data: {
              breadcrumb: '密碼變更',
              vnbreadcrumb: 'Thay  Đổi Mật Khẩu',
              enbreadcrumb: 'Change Password',
            },
          },
          {
            path: 'find-account',
            loadChildren: () =>
              import('./features/login/find-account/find-account.module').then(
                (m) => m.FindAccountModule
              ),
            data: {
              breadcrumb: '找回密碼',
              vnbreadcrumb: 'Tìm Lại Mật Khẩu ',
              enbreadcrumb: 'Recover password',
            },
          },
          {
            path: 'id-verification',
            loadChildren: () =>
              import(
                './features/login/id-verification/id-verification.module'
              ).then((m) => m.IdVerificationModule),
            data: {
              breadcrumb: '身分驗證',
              vnbreadcrumb: 'Xác Thực Danh Tính',
              enbreadcrumb: 'ID verification',
            },
          },
          {
            path: 'bind-account',
            loadChildren: () =>
              import('./features/login/bind-account/bind-account.module').then(
                (m) => m.BindAccountModule
              ),
            data: {
              breadcrumb: '帳號綁定',
              vnbreadcrumb: 'Liên Kết Tài Khoản',
              enbreadcrumb: 'Confirm',
            },
          },
        ],
      },
      /** 網投中心 */
      {
        path: 'online-insurance',
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                'src/app/features/online-insurance/online-insurance-index/online-insurance-index.module'
              ).then((m) => m.OnlineInsuranceIndexModule),
          },
          {
            path: 'travel-insurance',
            loadChildren: () =>
              import(
                'src/app/features/online-insurance/travel-insurance/travel-insurance.module'
              ).then((m) => m.TravelInsuranceModule),
            data: {
              breadcrumb: '旅遊險',
              vnbreadcrumb: 'Bảo Hiểm Du Lịch',
              enbreadcrumb: 'Travel Insurance',
            },
          },
          {
            path: 'car-insurance',
            loadChildren: () =>
              import(
                './features/online-insurance/car-insurance/car-insurance.module'
              ).then((m) => m.CarInsuranceModule),
            data: {
              breadcrumb: '汽車險',
              vnbreadcrumb: 'Bảo Hiểm Xe Ô tô',
              enbreadcrumb: 'Car Insurance',
            },
          },
          {
            path: 'motor-insurance',
            loadChildren: () =>
              import(
                './features/online-insurance/motor-insurance/motor-insurance.module'
              ).then((m) => m.MotorInsuranceModule),
            data: {
              breadcrumb: '機車險',
              vnbreadcrumb: 'Bảo Hiểm Xe Máy',
              enbreadcrumb: 'Motorcycle Insurance',
            },
          },
          {
            path: 'property-insurance',
            loadChildren: () =>
              import(
                './features/online-insurance/property-insurance/property-insurance.module'
              ).then((m) => m.PropertyInsuranceModule),
            data: {
              breadcrumb: '財產保險',
              vnbreadcrumb: 'Bảo Hiểm Tài Sản',
              enbreadcrumb: 'Property Insurance',
            },
          },
          {
            path: 'cargo-insurance',
            loadChildren: () =>
              import(
                './features/online-insurance/cargo-insurance/cargo-insurance.module'
              ).then((m) => m.CargoInsuranceModule),
            data: {
              breadcrumb: '運輸保險',
              vnbreadcrumb: 'Bảo Hiểm Hàng Hóa Vận Chuyển',
              enbreadcrumb: 'Transportation Insurance',
            },
          },
          {
            path: 'injury-insurance',
            loadChildren: () =>
              import(
                './features/online-insurance/injury-insurance/injury-insurance.module'
              ).then((m) => m.InjuryInsuranceModule),
            data: {
              breadcrumb: '意外保險',
              vnbreadcrumb: 'Bảo Hiểm Tai Nạn Con Người',
              enbreadcrumb: 'Accident Insurance',
            },
          },
          {
            path: 'house-insurance',
            loadChildren: () =>
              import(
                './features/online-insurance/house-insurance/house-insurance.module'
              ).then((m) => m.HouseInsuranceModule),
            data: {
              breadcrumb: '房屋保險',
              vnbreadcrumb: 'Bảo Hiểm Nhà Tư Nhân',
              enbreadcrumb: 'Home Insurance',
            },
          },
          {
            path: 'liability-insurance',
            loadChildren: () =>
              import(
                './features/online-insurance/liability-insurance/liability-insurance.module'
              ).then((m) => m.LiabilityInsuranceModule),
            data: {
              breadcrumb: '責任保險',
              vnbreadcrumb: 'Bảo Hiểm Trách Nhiệm',
              enbreadcrumb: 'Liability Insurance',
            },
          },
          {
            path: 'engineering-insurance',
            loadChildren: () =>
              import(
                './features/online-insurance/engineering-insurance/engineering-insurance.module'
              ).then((m) => m.EngineeringInsuranceModule),
            data: {
              breadcrumb: '工程保險',
              vnbreadcrumb: 'Bảo Hiểm Công Trình',
              enbreadcrumb: 'Construction Insurance',
            },
          },
          {
            path: 'project-trial-motor/:step',
            loadChildren: () =>
              import(
                './features/online-insurance/project-trial-motor/project-trial-motor.module'
              ).then((m) => m.ProjectTrialMotorModule),
            data: {
              breadcrumb: '機車險',
              vnbreadcrumb: 'Bảo Hiểm Xe Máy',
              enbreadcrumb: 'Motorcycle Insurance',
            },
          },
          {
            path: 'project-trial-car/:step',
            loadChildren: () =>
              import(
                './features/online-insurance/project-trial-car/project-trial-car.module'
              ).then((m) => m.ProjectTrialCarModule),
            data: {
              breadcrumb: '汽車險',
              vnbreadcrumb: 'Bảo Hiểm Xe Ô tô',
              enbreadcrumb: 'Car Insurance',
            },
          },
          {
            path: 'project-trial-travel/:step',
            loadChildren: () =>
              import(
                './features/online-insurance/project-trial-travel/project-trial-travel.module'
              ).then((m) => m.ProjectTrialTravelModule),
            data: {
              breadcrumb: '旅遊險',
              vnbreadcrumb: 'Bảo Hiểm Du Lịch',
              enbreadcrumb: 'Travel Insurance',
            },
          },
        ],
        data: {
          breadcrumb: '網路投保',
          vnbreadcrumb: 'Bảo Hiểm Trực Tuyến',
          enbreadcrumb: 'Buy Online',
        },
      },
      /** 網站地圖 */
      {
        path: 'sitemap',
        loadChildren: () =>
          import('./features/sitemap/sitemap.module').then(
            (m) => m.SitemapModule
          ),
        data: {
          breadcrumb: '網站地圖',
          vnbreadcrumb: 'Sơ Đồ Trang Web',
          enbreadcrumb: 'Sitemap',
        },
      },
      {
        path: 'search',
        loadChildren: () =>
          import('./features/search/search.module').then((m) => m.SearchModule),
        data: {
          breadcrumb: '全站搜尋',
          vnbreadcrumb: 'Tìm Kiếm Trang',
          enbreadcrumb: 'page search',
        },
      },
      // {
      //   path: 'payment',
      //   loadChildren: () =>
      //     import('./features/payment/payment.module').then(
      //       (m) => m.PaymentModule
      //     ),
      //   data: {
      //     breadcrumb: '付款模組',
      //     vnbreadcrumb: '付款模組',
      //     enbreadcrumb: '付款模組',
      //   },
      // },
      /** 404 */
      {
        path: '404',
        loadChildren: () =>
          import(
            './features/common/pagenotfound/pagenotfound-routing.module'
          ).then((m) => m.PagenotfoundRoutingModule),
      },
      { path: '**', redirectTo: '/404' },
    ],
  },
];

function switchUrl(url, data) {
  const urlResult = url.split('%2F').pop().split('.')[0];
  switch (urlResult) {
    // 車險
    case 'contract01':
      return {
        breadcrumb: '車險',
        vnbreadcrumb: 'Bảo Hiểm Xe',
        enbreadcrumb: 'Automobile Insurance',
      };
    // 旅遊險
    case 'contract02':
      return {
        breadcrumb: '旅遊險',
        vnbreadcrumb: 'Bảo Hiểm Du Lịch',
        enbreadcrumb: 'Travel Insurance',
      };
    // 傷害險
    case 'contract03':
      return {
        breadcrumb: '意外保險',
        vnbreadcrumb: 'Bảo Hiểm Tai Nạn Con Người',
        enbreadcrumb: 'Accident Insurance',
      };
    // 住火險
    case 'contract04':
      return {
        breadcrumb: '房屋保險',
        vnbreadcrumb: 'Bảo Hiểm Nhà Tư Nhân',
        enbreadcrumb: 'Home Insurance',
      };
    // 工程險
    case 'contract05':
      return {
        breadcrumb: '工程保險',
        vnbreadcrumb: 'Bảo Hiểm Công Trình',
        enbreadcrumb: 'Construction Insurance',
      };
    // 責任險
    case 'contract06':
      return {
        breadcrumb: '責任保險',
        vnbreadcrumb: 'Bảo Hiểm Trách Nhiệm',
        enbreadcrumb: 'Liability Insurance',
      };
    // 財產險
    case 'contract07':
      return {
        breadcrumb: '財產保險',
        vnbreadcrumb: 'Bảo Hiểm Tài Sản',
        enbreadcrumb: 'Property Insurance',
      };
    // 貨運險
    case 'contract08':
      return {
        breadcrumb: '運輸保險',
        vnbreadcrumb: 'Bảo Hiểm Hàng Hóa Vận Chuyển',
        enbreadcrumb: 'Transportation Insurance',
      };
  }
}
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // preloadingStrategy: PreloadAllModules,
      scrollPositionRestoration: 'top',
      // 重導
      onSameUrlNavigation: 'reload',
      anchorScrolling: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
