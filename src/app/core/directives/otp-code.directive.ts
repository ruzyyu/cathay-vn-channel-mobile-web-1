import { Directive, Input, ElementRef, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';

@Directive({
  selector: '[appOtpCode]'
})
export class OtpCodeDirective implements OnChanges {
  @Input() formControl: FormControl;
  maxLength = 7;
  separateIndex = 3;

  constructor(private el: ElementRef) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.formControl.currentValue) {
      this.registerValueChange();
    }
  }

  registerValueChange(): void {
    const elRef = this.el.nativeElement;
    this.formControl.valueChanges.subscribe((val: string) => {
      if (val) {
        val = val.toString().split('-').join('') as string;
        const lastVal = elRef.getAttribute('data-last-value') || '';
        const isValid = new RegExp('^[0-9]+$').test(val) && val.length <= this.maxLength;
        const isAdd = val?.length > lastVal.length;
        const limit = isAdd ? this.separateIndex - 1 : this.separateIndex;
        val = isValid ? val : lastVal;

        elRef.value = (val?.length > limit) ? `${val.substr(0, this.separateIndex)}-${val.substr(this.separateIndex)}` : val;
        elRef.setAttribute('data-last-value', val);

        // update module without emitting event and without changing view model
        this.formControl.setValue(elRef.value, {
          emitEvent: false,
          emitModelToViewChange: false,
        });
      }
    });
  }
}
