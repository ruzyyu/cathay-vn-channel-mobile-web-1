import { Directive, OnInit, Input, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';

@Directive({
  selector: '[appCardNo]'
})
export class CardNoDirective implements OnChanges {
  @Input() formControl: FormControl;
  maxLength = 16;

  constructor(private el: ElementRef) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.formControl.currentValue) {
      this.registerValueChange();
    }
  }

  registerValueChange() {
    const elRef = this.el.nativeElement;
    this.formControl.valueChanges.subscribe((val: string) => {
      if (val) {
        val = val.toString().split('-').join('') as string;
        const lastVal = elRef.getAttribute('data-last-value') || '';

        const isValid = new RegExp('^[0-9]+$').test(val) && val.length <= this.maxLength;
        const isAdd = lastVal.length < val.length;
        val = isValid ? val : lastVal;

        const arr = [];
        let temp = val;
        while (temp.length >= 4) {
          arr.push(temp.substr(0, 4));
          temp = temp.substr(4);
        }
        if (temp.length) { arr.push(temp); }
        // 新增＋長度小於四節＋最後一節全部填滿
        if (isAdd && arr.length < 4 && (arr[arr.length - 1].length === 4 )) {
          temp = arr.join('-') + '-';
        } else {
          temp = arr.join('-');
        }
        elRef.value = temp;
        elRef.setAttribute('data-last-value', val);

        // update module without emitting event and without changing view model
        this.formControl.setValue(val, {
          emitEvent: false,
          emitModelToViewChange: false,
        });
      }
    });
  }
}
