import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appLimitInput]',
})
export class LimitInputDirective {
  /**
   * 可以是字串或正則
   */
  @Input() replace: string | RegExp;

  // 限制輸入的正則
  _regexp: RegExp;

  isComposite = false;

  /**
   * 複雜業務傳入方法
   */
  @Input() replaceFn: Function;

  constructor(private el: ElementRef, private control: NgControl) {}

  /**
   * 選詞輸入開始(中文選字的問題)
   */
  @HostListener('compositionstart') onCompositionStart() {
    this.isComposite = true;
  }

  /**
   * 選詞輸入結束(中文選字的問題)
   */
  @HostListener('compositionend', ['$event']) onCompositionEnd($event) {
    this.isComposite = false;
    this.limitInput($event);
  }

  @HostListener('change') onChange() {
    this.control.control.setValue(this.el.nativeElement.value);
  }

  /**
   * blur & change 需重新給予值，解決最末碼的異常
   * @param $event
   */
  @HostListener('blur', ['$event']) onBlur($event) {
    this.control.control.setValue(this.el.nativeElement.value);
  }

  /**
   * 正常輸入
   */
  @HostListener('input', ['$event']) onInput(event) {
    this.limitInput(event);
  }

  private limitInput($event) {
    if (!this.el.nativeElement.value || this.isComposite) {
      return;
    }
    if (this.replaceFn) {
      this.replaceFn(this.el);
      return;
    } else {
      this._regexp = this.getRegexp(this.replace);
      this.el.nativeElement.value = this.el.nativeElement.value.replace(
        this._regexp,
        ''
      );
    }
  }

  private getRegexp(str) {
    if (typeof str === 'object') {
      return str;
    }
    let regexp;
    switch (str) {
      case 'en':
        regexp = /(^\s|[\u4e00-\u9fa5])/g;
        break;
      case 'vn':
        regexp = /[^A-Za-zàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ\s]/g;
        break;
      case 'number':
        regexp = /[^0-9]/g;
        break;
      case 'number|letter':
        regexp = /[^\d|\w]/g;
        break;
      case 'float':
        this.replace = /[^\d|\.]/gi;
        break;
      case 'enNumber':
        regexp = /[^0-9A-Za-z]/g;
        break;
    }
    return regexp;
  }
}
