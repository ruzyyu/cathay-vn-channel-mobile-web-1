import { StorageTypes } from '../enums/storage-type.enum';

export interface StorageItem {
  type: StorageTypes;
  key: string;
  value: string;
  length?: number;
}
