import { ValidationErrors, FormGroup } from '@angular/forms';

/** 跨欄位驗證 */
export class FormValidators {

  /** 兩個欄位需相等 */
  static isSame = (prop1: string, prop2: string) => {
    return (form: FormGroup): null | ValidationErrors => {
      const field1 = form.value[prop1];
      const field2 = form.value[prop2];

      if (field1.length === 0 || field2.length === 0) { return; }
      if (field1 !== field2) {
        return { isSame: true };
      }
      return null;
    };

  }

  /** 兩個欄位需不相等 */
  static notTheSame = (prop1: string, prop2: string) => {
    return (form: FormGroup): null | ValidationErrors => {
      const field1 = form.value[prop1];
      const field2 = form.value[prop2];

      if (field1.length === 0 || field2.length === 0) { return; }
      if (field1 === field2) {
        return { notTheSame: true };
      }
      return null;
    };

  }

  /** 前項欄位值不可包含後項欄位值 */
  static notInclude = (toTest: string, term: string) => {
    return (form: FormGroup): null | ValidationErrors => {
      const toTestVal = '' + form.value[toTest];
      const termVal = '' + form.value[term];

      if (toTestVal?.length === 0 || termVal?.length === 0) { return null; }
      if (toTestVal.includes(termVal)) {
        return { notInclude: { term: termVal } };
      }

      return null;
    };
  }
}