import { ValidationErrors } from '@angular/forms';

export const MultiErrorParser = (config: any, name: string, errors: ValidationErrors) => {
  if (!errors) { return; }
  return Object.keys(errors)
    .map(k => {
      if (config[name] && config[name][k]) {
        return config[name][k](errors[k]);
      }
    }).filter(x => !!x).join('<br/>');
};