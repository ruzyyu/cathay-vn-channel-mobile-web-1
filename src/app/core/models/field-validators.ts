import { AbstractControl, ValidationErrors } from '@angular/forms';

export class FieldValidators {

  /** 身份證字號檢核 */
  static personalId(control: AbstractControl): null | ValidationErrors {
    const pid = control.value;
    const tab = 'ABCDEFGHJKLMNPQRSTUVXYWZIO';
    const A1 = new Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3);
    const A2 = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5);
    const Mx = new Array(9, 8, 7, 6, 5, 4, 3, 2, 1, 1);

    if (pid.length !== 10) { return null; }

    let i = tab.indexOf(pid.charAt(0));
    if (i === -1) { return { personalId: true }; }
    let sum = A1[i] + A2[i] * 9;

    for (i = 1; i < 10; i++) {
      const v = +pid.charAt(i);
      if (isNaN(v)) { return { personalId: true }; }
      sum = sum + v * Mx[i];
    }
    if (sum % 10 !== 0) { return { personalId: true }; }

    return null;
  }

  /** 欄位不可包含 term 字串 */
  static notInclude = (term: string) => {
    return (control: AbstractControl): null | ValidationErrors => {
      const val = '' + control.value;
      if (val?.length === 0 || term?.length === 0) { return null; }
      if (val.includes(term)) {
        return { notInclude: { term } };
      }
      return null;
    };
  }

  /** 欄位不可包含超過重複數量字元 */
  static duplicateMax = (max: number) => {
    return (control: AbstractControl): null | ValidationErrors => {
      const val = '' + control.value;
      if (val?.length === 0) { return null; }
      const chars = val.split('');
      let count = 0;
      const result = chars.map((c, i) => i === 0 ? 0 : chars[i - 1] === c ? ++count : count = 0)
        .sort((a, b) => b - a);

      if (result[0] + 1 >= max) {
        return { duplicateMax: { requiredLength: max, actualLength: result[0] } };
      }
      return null;
    };
  }

  /** 欄位不可包含連續數字 */
  static continuousNum = (max: number) => {
    return (control: AbstractControl): null | ValidationErrors => {
      const val = '' + control.value;
      if (val?.length === 0) { return null; }

      const chars = val.split('');
      let count = 0;
      const result = chars.map((c, i) => {
        if (i < chars.length - 1) {
          return +c - +chars[i + 1];
        }
      }).map((x, i, arr) => Math.abs(x) === 1 && arr[i - 1] === x ? ++count : count = 0).sort((a, b) => b - a);

      if (result[0] + 2 >= max) {
        return { continuousNum: { requiredLength: max, actualLength: result[0] + 1 } };
      }
      return null;
    };
  }

  /** 欄位不可包含連續字母 */
  static continuousChar = (max: number) => {
    return (control: AbstractControl): null | ValidationErrors => {
      const val = '' + control.value;
      if (val?.length === 0) { return null; }

      const chars = val.split('');
      let count = 0;
      const result = chars.map((c, i) => {
        if (i < chars.length - 1) {
          return c.charCodeAt(0) - chars[i + 1].charCodeAt(0);
        }
      }).map((x, i, arr) => Math.abs(x) === 1 && arr[i - 1] === x ? ++count : count = 0).sort((a, b) => b - a);

      if (result[0] + 2 >= max) {
        return { continuousNum: { requiredLength: max, actualLength: result[0] + 1 } };
      }
      return null;
    };
  }

  /** 地址檢核 */
  static address(control: AbstractControl): null | ValidationErrors {
    const address: string = control.value;
    if (!address?.length) { return null; }
    if (address.indexOf('村') >= 0 || address.indexOf('里') >= 0 || address.indexOf('鄰') >= 0
      || address.indexOf('大道') >= 0 || address.indexOf('路') >= 0 || address.indexOf('街') >= 0
      || address.indexOf('號') >= 0 || address.indexOf('巷') >= 0 || address.indexOf('樓') >= 0
      || address.indexOf('室') >= 0) {
      return null;
    } else { return { address: true }; }
  }
}
