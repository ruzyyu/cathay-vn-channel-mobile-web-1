import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StyleHelperService {
  /** 初始化元素高度調整 */
  initAdjustCardHeight(selector: string) {
    this.setPiceItemHeight(selector);
    $(window).resize(() => {
      this.setPiceItemHeight(selector);
    });
  }

  setPiceItemHeight(selector) {
    const titles = $(selector).find('.compare-title');
    const prices = $(selector).find('.compare-price');

    $(prices).each(function () {
      const priceItems = $(this).find('tbody').children();

      for (let i = 0; i < priceItems.length; i++) {
        const titleElement = $(titles[i]);
        const priceElement = $(priceItems[i]).children().first();

        const titleOuterHeight = titleElement.outerHeight();
        const priceOuterHeight = $(priceElement).outerHeight();

        if (titleOuterHeight > priceOuterHeight) {
          let height =
            titleOuterHeight - (priceOuterHeight - priceElement.height());

          if (priceElement.parent().hasClass('compare-price-second')) {
            height--;
          }

          priceElement.height(height);
        }

        if (titleOuterHeight < priceOuterHeight) {
          let height =
            priceOuterHeight - (titleOuterHeight - titleElement.height());

          if (priceElement.parent().hasClass('compare-price-second')) {
            height++;
          }

          titleElement.height(height);
        }
      }
    });
  }
}
