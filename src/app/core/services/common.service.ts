import { UserInfo } from './../interface/index';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StorageService } from './storage.service';
import { StorageKeys } from '../enums/storage-key.enum';
import { formatDate } from '@angular/common';
import { DatapoolService } from './datapool/datapool.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import * as crypto from 'crypto-js';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  static get accountToken() {
    return StorageService.getItem(StorageKeys.accountToken) ?? '';
  }

  static get ip() {
    return StorageService.getItem(StorageKeys.ip) ?? '10.175.2.29';
  }

  static get isLoginAsText() {
    return CommonService.isLoginSub$.value ? 'Y' : 'N';
  }

  static get lang() {
    return StorageService.getItem(StorageKeys.lang) ?? 'zh-TW';
  }

  static get sessionId() {
    return '';
  }

  static get celebrusCookieId() {
    return StorageService.getItem(StorageKeys.ckId) ?? 'CK1';
  }

  static get clientDevice() {
    return CommonService.isMobile() ? 'M' : 'P';
  }

  static get cookiesAlert() {
    return StorageService.getItem(StorageKeys.cookiesAlertfotter) ?? 'open';
  }

  constructor(private cds: DatapoolService, private router: Router) {
    CommonService.FLang = this.cds.gLang;
    CommonService.gLangType = this.cds.gLangType;
    // 不要問他們為什麼長得如此之醜...
    CommonService.giv = this.cds.iwdiiv;
    CommonService.gkey = this.cds.k_00e0y;
    CommonService.router = this.router;
  }
  static isLoginSub$ = new BehaviorSubject(
    !!StorageService.getItem(StorageKeys.accountToken)
  );
  static FLang: any;
  static router: Router;
  static gLangType: string;
  static giv: string;
  static gkey: string;

  user: any;
  loggedIn: boolean;

  static isMobile() {
    return !!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    );
  }

  /** 顯示登入彈窗 */
  static showLoginModal() {}

  /**  顯示資料載入屏蔽 */
  static showLoading() {}

  /**  關閉資料載入屏蔽 */
  static closeLoading() {}

  /** utf8 base64 decode */
  static base64Decode(str: string) {
    return decodeURIComponent(escape(atob(str)));
  }

  /** utf8 base64 decode */
  static base64Encode(str: string) {
    return window.btoa(unescape(encodeURIComponent(str)));
  }

  /** 檔案下載錯誤處理 */
  // static isBlobDownloadSuccess(res: HttpResponse<Blob>) {
  //   if (!!res?.body?.type && res.body.type.includes('application/json')) {
  //     const reader = new FileReader();
  //     reader.onload = () => {
  //       const json = JSON.parse(reader.result.toString());
  //       return CommonService.isHttpRespSuccess(json);
  //     };
  //     reader.readAsText(res.body);
  //   } else {
  //     return res;
  //   }
  // }

  /** 壓縮圖片 */
  static compressImg(dataURL: string, fileExtension: string): Promise<string> {
    return new Promise((resolve) => {
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');
      const img = new Image();
      img.src = dataURL;
      img.onload = () => {
        let width = img.width;
        let height = img.height;
        if (img.width > 2048 && img.width > img.height) {
          width = 2048;
          height = Math.round(img.height * (2048 / img.width));
        }
        if (img.height > 2048 && img.height > img.width) {
          width = Math.round(img.width * (2048 / img.height));
          height = 2048;
        }

        canvas.width = width;
        canvas.height = height;
        ctx.clearRect(0, 0, width, height);
        ctx.drawImage(img, 0, 0, width, height);
        resolve(canvas.toDataURL(fileExtension, 0.8));
      };
    });
  }

  /** Base64ToBlob */
  static base64ToFile(dataurl: string, fileName: string): Promise<File> {
    return new Promise((resolve) => {
      const arr = dataurl.split(',');
      const mime = arr[0].match(/:(.*?);/)[1];
      const bstr = atob(arr[1]);
      let n = bstr.length;
      const u8arr = new Uint8Array(n);

      while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
      }
      resolve(new File([u8arr], fileName, { type: mime }));
    });
  }

  /** Base64 to blob url */
  static base64ToBlobUrl(base64, type: 'application/pdf') {
    const byteArray = new Uint8Array(
      atob(base64)
        .split('')
        .map((char) => char.charCodeAt(0))
    );

    return URL.createObjectURL(new Blob([byteArray], { type }));
  }

  /**
   * @description 畫面轉換日期
   */
  static doDateFormat(item: string) {
    // 假設是 yyyymmdd 重組成 mmddyyyy 231
    return item.replace(/(\d{4})(\d{2})(\d{2})/, '$2$3$1');
  }

  /**
   * @description 取得機車名稱
   * VEHC_KD | CAR_PWR_KD  | CN |  VN | EN
   * 11	     |  C1         |	兩輪機車排氣50cc以下 |	 Dưới 50cc
   * 11	     |  C2         |	兩輪機車排氣超過50cc |	 Trên 50cc
   * 12	     |  C3         |	三輪機車 	           | Xe 3 bánh
   */
  static getMotoAlias(item: string) {
    return item.replace(/(\d{4})(\d{2})(\d{2})/, '$2$3$1');
  }

  /**
   * @description 取得汽車名稱
   */
  static getCarAlias(item: string) {
    return item.replace(/(\d{4})(\d{2})(\d{2})/, '$2$3$1');
  }

  /**
   * @description c# formatDatetime
   */
  static formatDatetime(date: string, format: string) {
    // 假設是 yyyymmdd 重組成 mmddyyyy 231
    return formatDate(date, format, 'en-US', '+0800');
  }

  static formatDate(item: string) {
    // 假設是 yyyymmdd 重組成 mmddyyyy 231
    return item.replace(/(\d{2})[- /.](\d{2})[- /.](\d{4})/, '$3-$2-$1');
  }

  /** 把userinfo存起來 */
  static SaveUsertoSession(item: UserInfo) {
    StorageService.setItem(StorageKeys.memberinfo, JSON.stringify(item));
  }

  /** 左側補0 */
  static paddingLeft(str, lenght) {
    if (String(str).length >= lenght) {
      return str;
    } else {
      return this.paddingLeft('0' + str, lenght);
    }
  }

  static topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  /**
   * @description 物件加密
   * @param ObjectData 資料
   */
  static ObjEncryption(ObjectData) {
    const ObjEncryption = window.btoa(
      encodeURIComponent(JSON.stringify(ObjectData))
    );
    return ObjEncryption;
  }
  /** 物件加密 End */
  /** 物件解密 */
  static ObjDecode(ObjectData) {
    const ObjDecode = JSON.parse(decodeURIComponent(window.atob(ObjectData)));
    return ObjDecode;
  }
  /** 物件解密 End*/

  /** 開啟 slick */
  static bindSlick(init = 0) {
    $('.slickcard')
      .not('.slick-initialized')
      .slick({
        dots: true,
        infinite: false,
        variableWidth: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        initialSlide: init,
        autoplaySpeed: 1000,
        responsive: [
          {
            breakpoint: 1440.98,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 991.98,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 767.98,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              centerMode: true,
            },
          },
        ],
      });
  }

  /** 刷新 slick */
  static refreshSlick(selector?: string) {
    $(selector || '.slickcard').slick('refresh');
  }

  /** 清除登入資訊 */
  static clearMemberinfo() {
    StorageService.setItem(StorageKeys.mobileToken, '');
    StorageService.setItem(StorageKeys.memberinfo, '');
  }

  /** 下瞄點  */
  static scroll(el: HTMLElement) {
    setTimeout(() => {
      el.scrollIntoView();
      if ($(document.body).width() >= 991) {
        $(window).scrollTop($(window).scrollTop() - 50);
      }
    }, 200);
  }

  static scrollto(name, offset = 0) {
    $('html,body').animate(
      { scrollTop: $(`#${name}`).offset().top - offset },
      'show'
    );
  }

  /**
   * @description 年齡規則判斷
   * @param xPlan [int] 方案
   * @param birthday [int] 生日 yyyy-mm-dd
   * @param startday [int] 生日 yyyy-mm-dd
   * */
  static checkAge(xPlan: number, xbirthday, xstartday) {
    const birthday = new Date(xbirthday);
    // 使用保額起始日
    const d = new Date(xstartday);

    // 判斷年數為70歲 80歲以下
    const age =
      d.getFullYear() -
      birthday.getFullYear() -
      (d.getMonth() < birthday.getMonth() ||
      (d.getMonth() == birthday.getMonth() && d.getDate() <= birthday.getDate())
        ? 1
        : 0);
    // 判斷年數為18歲以上
    const firstAge =
      d.getFullYear() -
      birthday.getFullYear() -
      (d.getMonth() < birthday.getMonth() ||
      (d.getMonth() == birthday.getMonth() && d.getDate() < birthday.getDate())
        ? 1
        : 0);

    // 計算相差天數
    const dayDiff = (d.getTime() - birthday.getTime()) / 1000 / 60 / 60 / 24;

    //  FPlan // 國內 銀1 金2 鑽石3    //國外 銀4 金5 鑽石6
    //  FAbroad  //國內 true  //國外 false
    /** 除了國外鑽石之外都判斷6周~80歲 */

    if (xPlan !== 6) {
      if (age > 79) {
        return CommonService.FLang.C02.insured.L0010;
      } else {
        if (age === 0) {
          if (dayDiff > 42) {
            return undefined;
          } else {
            return CommonService.FLang.C02.insured.L0010;
          }
        }
      }
    }
    /** 只有國外鑽石判斷18~70歲 */
    if (xPlan === 6) {
      if (firstAge < 18 || age > 69) {
        return CommonService.FLang.C02.insured.L0014;
      } else {
        return undefined;
      }
    }
  }

  /**
   *  @description 判斷閏年增加1天
   * @param date 起始日期
   * @param format  輸出格式ㄋ
   * @returns
   */
  static addOneYear(date: string, format: string) {
    const mmdd = moment(date).format('MMDD');
    let FData = moment(date).add(1, 'years');
    if (mmdd === '0229') {
      FData = FData.add(1, 'day');
    }
    return FData.format(format);
  }

  /** 多國語系文字連結點擊事件 */
  static contentClickHandler(event) {
    event.preventDefault();

    const href = event.target.getAttribute('href');

    if (href) {
      const info = this.getRouterData(href);
      this.router.navigate([info.href], info.extras);
    }
  }

  /** 解析 href 資訊 */
  static getRouterData(href: string) {
    const lang = CommonService.gLangType;
    const newHref = href.replace('language_variable', lang);
    const hrefList = newHref.split('#');
    const list = hrefList[0].split('?');
    const queryParams =
      list[1]?.split('&').reduce((a, b) => {
        const [key, value] = b.split('=');
        return { ...a, [key]: value };
      }, {}) || {};

    return {
      href: list[0],
      extras: {
        queryParams,
        fragment: hrefList[1],
      },
    };
  }

  /** 是否為iPhone或者QQHD瀏覽器 */
  static isIOS() {
    const u = navigator.userAgent;
    return (
      u.indexOf('iPhone') > -1 ||
      u.indexOf('Mac') > -1 ||
      u.indexOf('iPad') > -1
    );
  }
  verifyId(id, birthday = '') {
    const year = birthday.split('-')[0];
    // 生日為2020後，身分證須為12碼
    if (year > '2019') {
      if (id.split('').length === 12) {
        // 身分證為12碼時,檢查身分證5,6碼是否與年份後兩碼相符
        if (
          id.split('')[4] + id.split('')[5] ===
          year.split('')[2] + year.split('')[3]
        ) {
          return '';
        } else {
          return '身分證與年份後兩碼不符';
        }
      } else {
        return '身分證長度須為12碼';
      }
    }
  }

  /** AES CBC PK7 IV & sec */
  static AESDecrypt(word) {
    let encryptedHexStr = crypto.enc.Hex.parse(word);
    let srcs = crypto.enc.Base64.stringify(encryptedHexStr);
    let decrypt = crypto.AES.decrypt(
      srcs,
      crypto.enc.Utf8.parse(CommonService.gkey),
      {
        iv: crypto.enc.Utf8.parse(CommonService.giv),
        mode: crypto.mode.CBC,
        padding: crypto.pad.Pkcs7,
      }
    );
    let decryptedStr = decrypt.toString(crypto.enc.Utf8);
    return decryptedStr.toString();
  }

  static autoScrollToAccordionHeader(
    selector: string,
    getTargetId?: (target: Event['target'] & { id: string }) => string
  ) {
    $(selector).on('shown.bs.collapse', (event) => {
      /** Header */
      const headerMainElement = $('.header-main');
      /** Header 上方區塊 */
      const headerTopElement = $('.header-top');
      /** Header 總高度 */
      const headerMainHeight = headerMainElement?.height() || 0;
      /** Header 上方區塊高度 */
      const headerTopHeight = headerTopElement?.height() || 0;
      /** 跑馬燈高度 */
      const newsMessageHeight = $('.news-message')?.height() || 0;

      /** Header 計算後的高度 */
      const headerHeight = headerMainElement.hasClass('hideTop')
        ? headerMainHeight - headerTopHeight + newsMessageHeight
        : headerMainHeight + newsMessageHeight;

      // const headerMainHeight = $('.header-main')?.height() || 0;
      const targetSelectorId = getTargetId
        ? getTargetId(event.target)
        : event.target.id;
      $('html, body').animate(
        {
          scrollTop:
            $(`div[data-target='#${targetSelectorId}']`).offset().top -
            headerHeight -
            20,
        },
        200
      );
    });

    $(selector).on('hidden.bs.collapse', ({ target }) => {
      $(target).find('.collapse.show').collapse('hide');
    });
  }
}
