import {
  ICalcInsurance,
  ICalcTravelInsurance,
  insuranceQueryList,
} from './../../interface/index';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  UserInfo,
  UserInfoTemp,
  OtpCode,
  ICountry,
  ICounty,
  IVechicleKd,
  IVehicleUse,
  INation,
  IPublicCommon,
  motorUserInfo,
  IPayInfo,
} from '../../interface/index';

@Injectable({
  providedIn: 'root',
})
export class DatapoolService {
  /** 真實會員資料 */
  userInfo: UserInfo = {
    // 會員流水號
    seq_no: null,
    // 姓名
    customer_name: '',
    certificate_number_9: '',
    certificate_number_12: '',
    birthday: '',
    email: '',
    email_bk: '',
    sex: '',
    mobile: '', // 越南手機070開頭
    option: '',
    optiontype: null,
    optiontypeDetail: '',
    county: '',
    province: '',
    addr: '',
    is_link_id: '',
    fb_account: '',
    fb_title: '',
    google_account: '',
    google_title: '',
    is_link_sns: '',
    is_set_pw: '',
    kor: '',
    captchaCode: '',
  };
  /** 緩存會員資料 */
  userInfoTemp: UserInfoTemp = {
    // 會員流水號
    seq_no: undefined,
    // 姓名
    customer_name: undefined,
    certificate_number_9: undefined,
    certificate_number_12: undefined,
    birthday: undefined,
    email: undefined,
    email_bk: undefined,
    sex: undefined,
    mobile: undefined, // 越南手機070開頭
    county: undefined,
    province: undefined,
    addr: undefined,
    is_link_id: undefined,
    fb_account: undefined,
    fb_title: undefined,
    google_account: undefined,
    google_title: undefined,
    is_link_sns: undefined,
    is_set_pw: undefined,
    kor: undefined,
    account: undefined,
  };

  /** 付款資料緩存 */
  gBeforPayinfo: IPayInfo;

  otpCode: OtpCode = {
    otpOne: undefined,
    otpTwo: undefined,
    otpThr: undefined,
    otpFou: undefined,
    otpFiv: undefined,
    otpSix: undefined,
  };

  motorUserInfo: motorUserInfo = {
    /** 商品細類別 */
    prod_Polcode: 'CF02002',
    /** 車輛種類代號 */
    vehcKd: '',
    /** 車輛保險類型 */
    carpwrkd: '',
    /** 保單生效日 */
    procDate: '',
    /** 保單結束日 */
    procDate_end: '',
    /** 座位數: 機車固定 2 */
    maxCapl: 2,
    /** 車主姓名 */
    vehcOwnr: '',
    /** 車牌號碼 */
    rgstNo: '',
    /** 車身號碼 */
    plate: '',
  };

  /** 強制險查詢 */
  insuranceQueryList: insuranceQueryList = {
    // 保單號碼
    pol_no: '',
    // 種類
    option: '1',
    // 車牌號碼
    rgst_no: '',
    // 車身號碼
    fram_no: '',
    // 引擎號碼
    engn_no: '',
  };

  /** 汽機車網路試算資料 */
  gTrafficicinsurance: ICalcInsurance = {
    order_no: null,
    order_status: '',
    aply_no: '',
    type_pol: '',
    prod_pol_code: '',
    prod_pol_code_nm: '',
    pol_dt: '',
    pol_due_dt: '',
    certificate_type: '',
    certificate_number: '',
    customer_name: '',
    birthday: '',
    addr: '',
    mobile: '',
    email: '',
    national_id: '',
    engn_no: '',
    fram_no: '',
    rgst_no: '',
    vehc_ownr: '',
    vehc_kd: '',
    car_pwr_kd: '',
    max_capl: 0,
    comp_prem: 0,
    comp_premvat: 0,
    driver_amt: 0,
    driver_cnt: 0,
    driver_prem: 0,
    passenger_amt: 0,
    passenger_cnt: 0,
    passenger_prem: 0,
    tot_prem_with_vat: 0,
    tot_prem: 0,
    print_type: '',
    pol_dt_kind: '',
  };
  /** 旅遊 */
  gTrafficTravelInsurance: ICalcTravelInsurance = null;
  /** 我要續保狀態 */
  gTrafficType = false;

  gCountry: ICountry;
  /** 縣市 */
  gCounty: ICounty;
  gCarlist: IVechicleKd;
  gCarUselist: IVehicleUse;
  gNation: INation;

  /** 全域變數 */
  gPubCommon: IPublicCommon;

  /** 保單傳值 */
  gPolicyCommon: any;

  /** 多國語 */
  gLang: any;

  /** 語言別 */
  gLangType: 'vi-VN' | 'zh-TW' | 'en-US';

  todos$: Observable<any[]>;

  /** 登入後取得 */
  /**
   * @description
   * 會員 token 固定
   */
  gMobileToken: string;

  /**
   * @description
   * 一般用戶 token 固定
   */
  gNormalToken: string;

  /**
   * @description
   * 核心 token 固定
   *
   */
  gToken: string;

  /** 設定初始值 */
  constructor() {
    this.gToken =
      'NlXEt66fejIrQvIGampcPnLVdmIYVo0wdZf8T5H5xkM5lpVpvuhDn9qSaAQDSkbkOdUjxvXufQpyH0WZWvQThw==';
    this.gNormalToken = '';
    this.gMobileToken = '';
    this.k_00e0y = 'cathay0000000000';
    this.iwdiiv = '0000000000cathay';
  }

  /** 保單變更資料暫存 */
  xinsuranceChangingData;
  /** 保單變更清單資訊 */
  gEndRsmntCard;
  /** 續保清單資訊 */
  gDueSoonCard;

  // 是否是快速理賠
  gIsFastClaim = true;

  // 是否可以保險所有項目
  gNoClaims = false;

  /** 手動 . 可理賠項目 */
  gClamKindList: any = [];

  /** 理賠案件資料 */
  gClaimData: any = {};

  /** 理賠航班清單 */
  gFlightDataList: any[] = [];

  /** 已選擇的理賠項目航班清單 */
  gSelectedFlightDataList: any[] = [];

  /** 快速理賠時間 */
  gClaimDate: string;

  /** 頁面設置 */
  gPage: any = {
    page: '',
    prevPage: '',
  };

  /** router專用 */
  gPre_url: string;

  /** 我要續保登入後換頁用 */
  gWhocall: string = undefined;

  /** 試算緩存，共用 */
  gTCalc_to_save: {
    type: 'motor' | 'car' | 'travel' | 'none';
    userinfo: UserInfo;
    saveorder;
    paytype;
  };

  /** 日期緩存recno */
  gDatapicker: any = {};

  /** 汽機車商品代號識別 */
  gMotorPluNoList = ['CF02001', 'CF01001', 'CF01003'];
  gCarPluNoList = ['CF02002', 'CF01002', 'CF01007'];

  gNoticeDetail;

  /** 付款彈窗 */
  gWindows;

  /** 全域變數-支付變數 */
  pay_pwd: string;
  pay_account: string;
  websitId: string;
  VNPay_Url: string;
  k_00e0y: string;
  iwdiiv: string;
  /** 全域變數-sso cp5 cp6*/
  cathaylifebaseurl: string;
  cathaylifesso: string;

  /** 緩存會員資料是否來自於壽險 */
  gIsSSO: boolean;
}

// map屬性 ....
// pub:{
//   payment :{
//     pay_pwd: string;
//     pay_account: string;
//     websitId: string;
//     VNPay_Url: string;
//   },
//   sso:{
//     cathaylifebaseurl:

//   }
// }
