import { Injectable } from '@angular/core';
import { StorageItem } from '../models/storage-item';
import { StorageTypes } from '../enums/storage-type.enum';
import StorageDefines from '../models/storage-defines';
import { StorageKeys } from '../enums/storage-key.enum';
// import * as Cookies from 'js-cookie';
import Cookies from 'js-cookie';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor() {}
  getItem;

  /** 設定儲存值 */
  static setItem(key: StorageKeys, value: string, expires?: number | Date) {
    switch (StorageDefines[key]) {
      case StorageTypes.local:
        localStorage.setItem(key, value);
        break;
      case StorageTypes.session:
        sessionStorage.setItem(key, value);
        break;
      case StorageTypes.cookies:
        Cookies.set(key, value, { expires });
        break;
    }
  }
  /** 取得儲存值 */
  static getItem(key: StorageKeys): string | null {
    switch (StorageDefines[key]) {
      case StorageTypes.local:
        return localStorage.getItem(key);
      case StorageTypes.session:
        return sessionStorage.getItem(key);
      case StorageTypes.cookies:
        return Cookies.get(key);
    }
  }

  /** 移除儲存值 */
  static removeItem(key: StorageKeys) {
    switch (StorageDefines[key]) {
      case StorageTypes.local:
        localStorage.removeItem(key);
        break;
      case StorageTypes.session:
        sessionStorage.removeItem(key);
        break;
      case StorageTypes.cookies:
        Cookies.remove(key);
        break;
    }
  }

  /** 全部清除 */
  static clearAll() {
    localStorage.clear();
    sessionStorage.clear();
    Object.keys(Cookies.getJSON()).forEach((k) => Cookies.remove(k));
  }

  /** 取得全部 */
  static getAll(): StorageItem[] {
    const locals: StorageItem[] = Object.keys(localStorage).map((k) => ({
      key: k,
      value: localStorage.getItem(k),
      type: StorageTypes.local,
    }));

    const sessions: StorageItem[] = Object.keys(sessionStorage).map((k) => ({
      key: k,
      value: sessionStorage.getItem(k),
      type: StorageTypes.session,
    }));

    const cookies: StorageItem[] = Object.keys(Cookies.getJSON()).map((k) => ({
      key: k,
      value: Cookies.get(k),
      type: StorageTypes.cookies,
    }));

    return locals
      .concat(sessions)
      .concat(cookies)
      .map((x) => ({ ...x, length: x.value ? x.value.length : 0 }));
  }
}
