import { Injectable } from '@angular/core';
import { SocialAuthService } from 'angularx-social-login';
import {
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialUser,
} from 'angularx-social-login';

@Injectable({
  providedIn: 'root',
})
export class SocialService {
  /** 存放登入後的狀態資料 */
  user: SocialUser;
  loggedIn: boolean;
  constructor(private authService: SocialAuthService) {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = user != null;
    });
  }

  /** Google登入 */
  async signInWithGoogle() {
    // await this.signOut();
    const result = await this.authService.signIn(
      GoogleLoginProvider.PROVIDER_ID
    );
    await this.signOut();
    return result;
  }
  /** Google登出 */
  async signOutWithGoogle() {
    return await this.authService.signOut(true);
  }

  /** FB登入 */
  async signInWithFB() {
    // await this.signOut();
    const result = await this.authService.signIn(
      FacebookLoginProvider.PROVIDER_ID
    );
    await this.signOut();
    return result;
  }
  /** 登出 */
  async signOut() {
    return await this.authService.signOut(true);
  }
}
