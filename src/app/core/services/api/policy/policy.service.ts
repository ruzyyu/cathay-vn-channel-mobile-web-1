import { DatapoolService } from './../../datapool/datapool.service';
import { ApiHelperService } from './../../utils/api-helper.service';
import { Injectable } from '@angular/core';
import { IResponse } from 'src/app/core/interface';
@Injectable({
  providedIn: 'root',
})
export class PolicyService {
  constructor(public api: ApiHelperService, private cds: DatapoolService) {}

  /**
   * @description 取得保單清單
   * @param  CF02001 機車強制險 CF01001 機車任意(強制加保駕駛乘客傷害) CF02002 汽車強制險 CF01007 汽車任意(強制加保駕駛乘客傷害) AC01001 旅綜險(T1) AC01005 旅綜險(T3) AC01013 旅綜險(T4)
   *
   */
  getPolicyList(payload) {
    return this.api.post('Policy/Compulsory/QueryList', payload, {
      mask: true,
      alert: false,
    }) as IResponse;
    // return {
    //   status: 200,
    //   message: '成功',
    //   success: true,
    //   total: 1,
    //   data: {
    //     TravelQueryList: [
    //       {
    //         is_group: 'N',
    //         is_insr: 'N',
    //         cntr_no: '201200001988',
    //         pol_no: 'C220T1000936',
    //         proc_no: 'CIA6T1067302',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01001',
    //         prod_pol_name: '世界逍遙遊 （World)',
    //         pol_dt: '2021-01-26 11:00:00.0',
    //         pol_due_dt: '2021-01-31 11:00:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'Y',
    //         is_insr: 'N',
    //         cntr_no: '201200002317',
    //         pol_no: 'CI20T3000398',
    //         proc_no: 'CIA6T3045658',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01005',
    //         prod_pol_name: '越南自由行（越南）',
    //         pol_dt: '2020-12-07 14:30:00.0',
    //         pol_due_dt: '2020-12-12 14:30:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry與其他 3 人(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'N',
    //         cntr_no: '201200002436',
    //         pol_no: 'C220T6000034',
    //         proc_no: 'CIA6T6000315',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01015',
    //         prod_pol_name: '國外旅綜險',
    //         pol_dt: '2020-12-08 14:30:00.0',
    //         pol_due_dt: '2020-12-20 14:30:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'N',
    //         cntr_no: '201200002478',
    //         pol_no: 'CI20T1002010',
    //         proc_no: 'CIA6T1067571',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01001',
    //         prod_pol_name: '世界逍遙遊 （World)',
    //         pol_dt: '2021-05-09 11:30:00.0',
    //         pol_due_dt: '2021-05-14 11:30:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'Y',
    //         is_insr: 'N',
    //         cntr_no: '201200002479',
    //         pol_no: 'CI20T3000423',
    //         proc_no: 'CIA6T3045726',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01005',
    //         prod_pol_name: '越南自由行（越南）',
    //         pol_dt: '2020-12-09 11:30:00.0',
    //         pol_due_dt: '2020-12-24 11:30:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry與其他 2 人(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'N',
    //         cntr_no: '201200002480',
    //         pol_no: 'CI20T4000803',
    //         proc_no: 'CIA6T4008838',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01013',
    //         prod_pol_name: '國內旅綜險',
    //         pol_dt: '2021-05-09 11:30:00.0',
    //         pol_due_dt: '2021-05-14 11:30:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '1',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'N',
    //         cntr_no: '201200002494',
    //         pol_no: 'CI20T6000136',
    //         proc_no: 'CIA6T6000348',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01015',
    //         prod_pol_name: '國外旅綜險',
    //         pol_dt: '2021-05-09 11:30:00.0',
    //         pol_due_dt: '2021-05-14 11:30:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '1',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'N',
    //         cntr_no: '201200002509',
    //         pol_no: 'CI20T1002027',
    //         proc_no: 'CIA6T1067582',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01001',
    //         prod_pol_name: '世界逍遙遊 （World)',
    //         pol_dt: '2020-12-09 13:30:00.0',
    //         pol_due_dt: '2020-12-14 13:30:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'N',
    //         cntr_no: '201200002696',
    //         pol_no: 'C220T1001124',
    //         proc_no: 'CIA6T1067683',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01001',
    //         prod_pol_name: '世界逍遙遊 （World)',
    //         pol_dt: '2021-05-11 19:00:00.0',
    //         pol_due_dt: '2021-05-16 19:00:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'James(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'N',
    //         cntr_no: '201200002972',
    //         pol_no: 'C220T3001151',
    //         proc_no: 'CIA6T3045928',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01005',
    //         prod_pol_name: '越南自由行（越南）',
    //         pol_dt: '2020-12-16 22:30:00.0',
    //         pol_due_dt: '2020-12-26 22:30:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'James(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'Y',
    //         is_insr: 'N',
    //         cntr_no: '210200005251',
    //         pol_no: 'CI21T1000123',
    //         proc_no: 'CIA6T1069102',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01001',
    //         prod_pol_name: '世界逍遙遊 （World)',
    //         pol_dt: '2021-03-22 02:00:00.0',
    //         pol_due_dt: '2021-03-30 02:00:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry與其他 3 人(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'N',
    //         cntr_no: '210200005256',
    //         pol_no: 'CI21T1000152',
    //         proc_no: 'CIA6T1069135',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01001',
    //         prod_pol_name: '世界逍遙遊 （World)',
    //         pol_dt: '2021-02-08 03:00:00.0',
    //         pol_due_dt: '2021-02-11 03:00:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'N',
    //         cntr_no: '210200005336',
    //         pol_no: 'CI21T4000118',
    //         proc_no: 'CIA6T4009435',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01013',
    //         prod_pol_name: '國內旅綜險',
    //         pol_dt: '2021-02-11 01:00:00.0',
    //         pol_due_dt: '2021-02-12 01:00:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'N',
    //         cntr_no: '210200005342',
    //         pol_no: 'CI21T4000185',
    //         proc_no: 'CIA6T4009479',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01013',
    //         prod_pol_name: '國內旅綜險',
    //         pol_dt: '2021-02-15 11:00:00.0',
    //         pol_due_dt: '2021-02-21 11:00:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '1',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'Y',
    //         cntr_no: '210200005354',
    //         pol_no: 'CI21T1000221',
    //         proc_no: 'CIA6T1069427',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01001',
    //         prod_pol_name: '世界逍遙遊 （World)',
    //         pol_dt: '2021-02-18 00:00:00.0',
    //         pol_due_dt: '2021-02-19 00:00:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'Y',
    //         cntr_no: '210200005355',
    //         pol_no: 'CI21T1000236',
    //         proc_no: 'CIA6T1069438',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01001',
    //         prod_pol_name: '世界逍遙遊 （World)',
    //         pol_dt: '2021-02-19 00:00:00.0',
    //         pol_due_dt: '2021-02-25 00:00:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //       {
    //         is_group: 'N',
    //         is_insr: 'Y',
    //         cntr_no: '210200005356',
    //         pol_no: 'CI21T1000249',
    //         proc_no: 'CIA6T1069449',
    //         prod_code: 'A',
    //         prod_pol_code: 'AC01001',
    //         prod_pol_name: '世界逍遙遊 （World)',
    //         pol_dt: '2021-02-20 00:00:00.0',
    //         pol_due_dt: '2021-02-26 00:00:00.0',
    //         is_apc: 'Y',
    //         endrsmnt_count: '0',
    //         insr_info: 'Jerry(Người được bảo hiểm)',
    //       },
    //     ],
    //     CarQueryList: [
    //       {
    //         cntr_no: '210100005161',
    //         pol_no: 'CI20CB0055301',
    //         proc_no: 'CB2102570932',
    //         prod_code: 'C',
    //         prod_pol_code: 'CF02002',
    //         prod_pol_name: '汽車強制險',
    //         rgst_no: '67C-013.59',
    //         engn_no: 'b',
    //         fram_no: 'a',
    //         pol_dt: '2021-01-29 00:00:00.0',
    //         pol_due_dt: '2022-01-29 00:00:00.0',
    //         renew_pol_cnt: '0',
    //         new_pol_cnt: '0',
    //       },
    //     ],
    //     ScooterQueryList: [
    //       {
    //         cntr_no: '210100005156',
    //         pol_no: 'CI20CA0005017',
    //         proc_no: 'CA2108402188',
    //         prod_code: 'C',
    //         prod_pol_code: 'CF02001',
    //         prod_pol_name: '機車強制險',
    //         rgst_no: null,
    //         engn_no: 'AA',
    //         fram_no: '111111',
    //         pol_dt: '2021-01-29 00:00:00.0',
    //         pol_due_dt: '2022-01-29 00:00:00.0',
    //         renew_pol_cnt: '0',
    //         new_pol_cnt: '0',
    //       },
    //     ],
    //   },
    // };
  }

  /**
   * @description 取得汽車險明細
   */
  getCarQueryDetail(payload) {
    return this.api.post('Policy/Compulsory/CarQueryDetail', payload, {
      mask: true,
      alert: true,
    }) as IResponse;
  }
  /**
   * @description 取得摩托車明細
   */
  getMotoQueryDetail(payload) {
    return this.api.post('Policy/Compulsory/ScooterQueryDetail', payload, {
      mask: true,
      alert: true,
    });
  }

  /**
   * @description 取得旅遊險明細
   * @param payload 傳入資料
   */
  getTravelQueryDetail(payload) {
    return this.api.post('Policy/Compulsory/TravelQueryDetail', payload, {
      mask: true,
      alert: true,
    }) as IResponse;
    // return {
    //   status: 200,
    //   message: '成功',
    //   success: true,
    //   total: 1,
    //   data: [
    //     {
    //       policy_data: {
    //         type_pol: 'B',
    //         prod_pol_code: 'AC01001',
    //         prod_pol_name: '世界逍遙遊 （World)',
    //         pol_no: 'CI21T1000236',
    //         pol_dt: '2021-02-19 00:00:00',
    //         pol_due_dt: '2021-02-25 00:00:00',
    //         prem: '113000',
    //       },
    //       insr_data: [
    //         {
    //           insr_name: 'Jerry',
    //           insr_birthday: '2000-01-01',
    //           coverages: [
    //             {
    //               coverageTypeId: 'T03',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '出國旅行不便保險',
    //               prod_id: 'F08',
    //               amt: '6000000',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '46',
    //                   coverageItemId: 'T03004',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '旅行證件丟失',
    //                   getamt_field: 'amt',
    //                 },
    //               ],
    //             },
    //             {
    //               coverageTypeId: 'T03',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '出國旅行不便保險',
    //               prod_id: 'F09',
    //               amt: '6000000',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '45',
    //                   coverageItemId: 'T03003',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '緊縮或取消旅行費用',
    //                   getamt_field: 'amt',
    //                 },
    //               ],
    //             },
    //             {
    //               coverageTypeId: 'T06',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '旅行和住宿的其他費用',
    //               prod_id: 'F10',
    //               amt: '10000000',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '84',
    //                   coverageItemId: 'T06001',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName:
    //                     '被保險人或同伴返回越南的經濟艙額外費用',
    //                   getamt_field: 'amt',
    //                 },
    //                 {
    //                   coverageNo: '85',
    //                   coverageItemId: 'T06002',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName:
    //                     '被保險人或其陪同人員因在該保險範圍內需要治療傷害或疾病而產生的宿伙食費用也包括在本保單内',
    //                   getamt_field: 'amt',
    //                 },
    //               ],
    //             },
    //             {
    //               coverageTypeId: 'T03',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '出國旅行不便保險',
    //               prod_id: 'F03',
    //               amt: '4000000',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '43',
    //                   coverageItemId: 'T03001',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '行李延誤',
    //                   getamt_field: 'amt',
    //                 },
    //               ],
    //             },
    //             {
    //               coverageTypeId: 'T03',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '出國旅行不便保險',
    //               prod_id: 'F12',
    //               amt: '4000000',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '48',
    //                   coverageItemId: 'T03006',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '個人現金損失',
    //                   getamt_field: 'amt',
    //                 },
    //               ],
    //             },
    //             {
    //               coverageTypeId: 'T04',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '國外個人責任',
    //               prod_id: 'F02',
    //               amt: '200000000',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '68',
    //                   coverageItemId: 'T04001',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName:
    //                     '在保險期內，與法律責任（針對第三方的人和/或財產）有關的被保險人的最高支付率（扣除額：200萬/箱）',
    //                   getamt_field: 'amt',
    //                 },
    //               ],
    //             },
    //             {
    //               coverageTypeId: 'T03',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '出國旅行不便保險',
    //               prod_id: 'F11',
    //               amt: '10000000',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '47',
    //                   coverageItemId: 'T03005',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '行李遺失和損壞',
    //                   getamt_field: 'amt',
    //                 },
    //               ],
    //             },
    //             {
    //               coverageTypeId: 'T03',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '出國旅行不便保險',
    //               prod_id: 'F04',
    //               amt: '2500000',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '44',
    //                   coverageItemId: 'T03002',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '飛機延誤',
    //                   getamt_field: 'amt',
    //                 },
    //               ],
    //             },
    //             {
    //               coverageTypeId: 'T05',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '國外緊急援助',
    //               prod_id: 'F01',
    //               amt: '1000000000',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '70',
    //                   coverageItemId: 'T05002',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '埋葬和葬禮',
    //                   getamt_field: 'amt',
    //                 },
    //                 {
    //                   coverageNo: '72',
    //                   coverageItemId: 'T05004',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '醫療緊急運送',
    //                   getamt_field: 'amt',
    //                 },
    //                 {
    //                   coverageNo: '69',
    //                   coverageItemId: 'T05001',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '將未滿18歲的兒童遣返越南或居住國',
    //                   getamt_field: 'amt',
    //                 },
    //                 {
    //                   coverageNo: '71',
    //                   coverageItemId: 'T05003',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '探親',
    //                   getamt_field: 'amt',
    //                 },
    //                 {
    //                   coverageNo: '73',
    //                   coverageItemId: 'T05005',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '遣返',
    //                   getamt_field: 'amt',
    //                 },
    //               ],
    //             },
    //             {
    //               coverageTypeId: 'T02',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '醫療費用',
    //               prod_id: 'F00',
    //               amt: '200000000',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '23',
    //                   coverageItemId: 'T02001',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '門診治療',
    //                   getamt_field: 'amt',
    //                 },
    //                 {
    //                   coverageNo: '24',
    //                   coverageItemId: 'T02002',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName:
    //                     '治療肢折或受傷所必需的醫療輔助工具（例如石膏模，繃帶）和醫師開具的助行器',
    //                   getamt_field: 'amt',
    //                 },
    //                 {
    //                   coverageNo: '25',
    //                   coverageItemId: 'T02005',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName:
    //                     '放射療法，熱療法或光療以及醫師規定的其他此類治療',
    //                   getamt_field: 'amt',
    //                 },
    //                 {
    //                   coverageNo: '26',
    //                   coverageItemId: 'T02006',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '牙科治療，但僅包括用於急性牙痛緩解',
    //                   getamt_field: 'amt',
    //                 },
    //                 {
    //                   coverageNo: '27',
    //                   coverageItemId: 'T02007',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '住院治療',
    //                   getamt_field: 'amt',
    //                 },
    //                 {
    //                   coverageNo: '28',
    //                   coverageItemId: 'T02008',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '有關手術費用',
    //                   getamt_field: 'amt',
    //                 },
    //               ],
    //             },
    //             {
    //               coverageTypeId: 'T01',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '人身意外',
    //               prod_id: 'FAA',
    //               amt: '400000000',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '1',
    //                   coverageItemId: 'T01001',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '意外死亡',
    //                   getamt_field: 'amt',
    //                 },
    //                 {
    //                   coverageNo: '2',
    //                   coverageItemId: 'T01002',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '永久性完全失能',
    //                   getamt_field: 'amt',
    //                 },
    //               ],
    //             },
    //             {
    //               coverageTypeId: 'T99',
    //               coverageTypeZhname: null,
    //               coverageTypeVnname: null,
    //               coverageTypeEnname: null,
    //               coverageTypeName: '協助服務',
    //               prod_id: 'F99',
    //               amt: '',
    //               coverageItems: [
    //                 {
    //                   coverageNo: '992',
    //                   coverageItemId: 'T99003',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '醫療轉診',
    //                   getamt_field: '',
    //                 },
    //                 {
    //                   coverageNo: '990',
    //                   coverageItemId: 'T99001',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '出行前諮詢服務',
    //                   getamt_field: '',
    //                 },
    //                 {
    //                   coverageNo: '991',
    //                   coverageItemId: 'T99002',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '口譯員推薦； 法律推薦',
    //                   getamt_field: '',
    //                 },
    //                 {
    //                   coverageNo: '993',
    //                   coverageItemId: 'T99004',
    //                   coverageItemZhname: null,
    //                   coverageItemVnname: null,
    //                   coverageTypeEnname: null,
    //                   coverageItemName: '24小時緊急求救/國際緊急情況',
    //                   getamt_field: '',
    //                 },
    //               ],
    //             },
    //           ],
    //         },
    //       ],
    //       policyholder_data: {
    //         customer_name: 'Jerry',
    //         phone_no: '0834632146',
    //         email: 'james.chen729@gmail.com',
    //       },
    //     },
    //   ],
    // };
  }

  /**
   * @description 取得保單 PDF
   * @param payload 傳入資料
   */
  getPolicyPdf(payload) {
    return this.api.post('Policy/Compulsory/GetPolicyPdf', payload, {
      mask: true,
      alert: true,
    }) as Promise<IResponse>;
  }

  /** 我要續保詳細資料 */
  GetDueSoonDetail(payload) {
    return this.api.post(
      'Policy/Compulsory/GetDueSoonDetail',
      payload
    ) as IResponse;
  }

  /** 保戶服務中心保單變更牌卡 */
  GetEndRsmntCard(payload) {
    return this.api.post(
      'Policy/Compulsory/GetEndRsmntCard',
      payload
    ) as IResponse;
  }
  /** 保單變更資料檢核  */
  CheckForPolicy(payload) {
    return this.api.post('Policy/Compulsory/CheckForPolicy', payload, {
      mask: true,
      alert: true,
    }) as IResponse;
  }
  /** 保單變更資料儲存  */
  SaveDataForPolicy(payload) {
    return this.api.post(
      'Policy/Compulsory/SaveDataForPolicy',
      payload
    ) as IResponse;
    // return {
    //   // status: 500,
    //   status: 200,
    //   message: '成功',
    //   success: true,
    //   total: 0,
    //   data: null,
    // };
  }
  /** 保戶服務中心我要續保牌卡 */
  GetDueSoonCard(payload) {
    return this.api.post('Policy/Compulsory/GetDueSoonCard', payload, {
      mask: false,
      alert: false,
    });
    // return {
    //   status: 200,
    //   message: '成功',
    //   success: true,
    //   total: 3,
    //   data: [
    //     {
    //       cntr_no: '200201912947',
    //       pol_renew_no: 'CI20CA0048599',
    //       prod_pol_code: 'CF02001',
    //       rgst_no: '14K6-9277',
    //       pol_dt: '2020-02-28 20:00:00.0',
    //       pol_due_dt: '2021-02-28 20:00:00.0',
    //       prod_code: 'C',
    //     },
    //     {
    //       cntr_no: '200301913500',
    //       pol_renew_no: 'CI20CA0067878',
    //       prod_pol_code: 'CF02001',
    //       rgst_no: '92S2-5140',
    //       pol_dt: '2020-03-02 07:00:00.0',
    //       pol_due_dt: '2021-03-02 07:00:00.0',
    //       prod_code: 'C',
    //     },
    //     {
    //       cntr_no: '200301935350',
    //       pol_renew_no: 'CI20CA0082530',
    //       prod_pol_code: 'CF02001',
    //       rgst_no: '43T1-8465',
    //       pol_dt: '2020-03-10 07:00:00.0',
    //       pol_due_dt: '2021-03-10 07:00:00.0',
    //       prod_code: 'C',
    //     },
    //   ],
    // };
  }
}
