import { IResponse } from 'src/app/core/interface';
import { ApiHelperService } from './../../utils/api-helper.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  constructor(private api: ApiHelperService) {}
  any;
  /** @description 取得保單清單  */
  getOrderList(payload): any {
    return this.api.post('Order/Compulsory/OrderList', payload);
  }

  /** @description 取得清單  */
  getOrderDetail(payload, type: 'CAR' | 'TRAVEL' = 'CAR'): any {
    switch (type) {
      case 'CAR':
        return this.api.post(
          'Order/Compulsory/CarScooterOrderPayment',
          payload
        );

      case 'TRAVEL':
        return this.api.post('Order/Compulsory/TravelOrderPayment', payload);
    }
  }

  /** @description 編輯資料  */
  getOrderCalcDetail(payload, type: 'CAR' | 'TRAVEL' | 'MOTOR'): any {
    switch (type) {
      case 'CAR':
        return this.api.post('Order/Compulsory/CarScooterOrderEdit', payload);
      // return {
      //   status: 200,
      //   message: 'success',
      //   success: true,
      //   total: 1,
      //   data: {
      //     order_no: 43,
      //     order_status: '1',
      //     aply_no: 'BCB-0221314499',
      //     type_pol: 'B',
      //     prod_pol_code: 'CF02002',
      //     prod_pol_code_nm: '強制險',
      //     pol_dt: '2021-02-04 00:00:00',
      //     pol_due_dt: '2022-02-04 00:00:00',
      //     certificate_type: '1',
      //     certificate_number: '132432123',
      //     customer_name: 'Jerry',
      //     birthday: '2000-01-01',
      //     province: '001',
      //     county: '001',
      //     addr: '11111, test',
      //     mobile: '0834632146',
      //     email: 'test001@gmail.com',
      //     national_id: 'VIETN',
      //     vehc_ownr: 'Jerry',
      //     rgst_no: '51A-888.88',
      //     engn_no: '51A-888.88',
      //     fram_no: '51A-888.88',
      //     vehc_kd: '22',
      //     car_pwr_kd: '99',
      //     max_capl: '1',
      //     comp_prem: 933000,
      //     comp_premvat: 93300,
      //     driver_amt: 0,
      //     driver_cnt: 0,
      //     driver_prem: 0,
      //     passenger_amt: 0,
      //     passenger_cnt: 0,
      //     passenger_prem: 0,
      //     tot_prem_with_vat: 1026300,
      //     tot_prem: 933000,
      //     print_type: '2',
      //   },
      // };
      case 'MOTOR':
        return this.api.post('Order/Compulsory/CarScooterOrderEdit', payload);
      case 'TRAVEL':
        return this.api.post('Order/Compulsory/TravelOrderEdit', payload);
    }
  }

  /** @description 儲存資料  */
  saveOrder(payload, type: 'CAR' | 'TRAVEL' = 'CAR'): any {
    switch (type) {
      case 'CAR':
        return this.api.post('Order/Compulsory/CarScooterOrderSave', payload);
      case 'TRAVEL':
        return this.api.post('Order/Compulsory/TravelOrderSave', payload);
    }
  }

  /** @description 更改訂單狀態  */
  editOrderState(payload): any {
    return this.api.post(
      'Order/Compulsory/OrderStatusModify',
      payload
    ) as IResponse;
  }
}
