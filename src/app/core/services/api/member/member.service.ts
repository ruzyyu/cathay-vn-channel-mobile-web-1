import { CommonService } from 'src/app/core/services/common.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { IResponse } from './../../../interface/index';
import { StorageKeys } from './../../../enums/storage-key.enum';
import { StorageService } from './../../storage.service';
import { Injectable } from '@angular/core';
import { ApiHelperService } from 'src/app/core/services/utils/api-helper.service';

@Injectable({
  providedIn: 'root',
})
export class MemberService {
  constructor(private api: ApiHelperService, private cds: DatapoolService) {}

  // 註冊/修改帳號 -
  MemberReg(payload) {
    return this.api.post('mobileweb/api/member/reg', payload);
  }
  // 設定密碼 - MemberSetPW
  MemberSetPW(payload) {
    return this.api.post('mobileweb/api/member/setpw', payload, {
      mask: true,
      alert: false,
    });
  }

  // MemberCathayLogin 帳號登入
  async MemberCathayLogin(payload) {
    const mMemberinfo: any = await this.api.post(
      'mobileweb/api/member/cathaylogin',
      payload,
      {
        mask: true,
        alert: false,
      }
    );

    if (mMemberinfo.status === 200 && mMemberinfo.data) {
      this.cds.gMobileToken = mMemberinfo.access_token;
      this.cds.userInfo = mMemberinfo.data;
      StorageService.setItem(StorageKeys.mobileToken, mMemberinfo.access_token);
      const dataEncryption: any = CommonService.ObjEncryption(mMemberinfo.data);
      CommonService.SaveUsertoSession(dataEncryption);
    }

    this.MembergetParam();
    return mMemberinfo;
  }

  // 取得支付金耀
  async MembergetParam() {
    const payload = ['cp1', 'cp2', 'cp3', 'cp4', 'cp5', 'cp6'];
    const res = await this.api.post('mobileweb/api/param/getValue', payload, {
      mask: false,
      alert: false,
    });

    if (res.status === 200) {
      this.cds.pay_account = CommonService.AESDecrypt(res.data.cp1);
      this.cds.websitId = CommonService.AESDecrypt(res.data.cp2);
      this.cds.pay_pwd = CommonService.AESDecrypt(res.data.cp3);
      this.cds.VNPay_Url = CommonService.AESDecrypt(res.data.cp4);
      this.cds.cathaylifebaseurl = res.data.cp5;
      this.cds.cathaylifesso = res.data.cp6;
      StorageService.setItem(StorageKeys.cp5, btoa(this.cds.cathaylifebaseurl));
      StorageService.setItem(StorageKeys.cp6, btoa(this.cds.cathaylifesso));
    }
  }

  // MemberVerifyID 驗證身分證ID
  MemberVerifyID(payload) {
    return this.api.post('mobileweb/api/member/verifyid', payload, {
      mask: true,
      alert: true,
    });
  }

  MemberVerifyID2(payload) {
    return this.api.post('mobileweb/api/member/verifyid', payload, {
      mask: true,
      alert: false,
    });
  }

  // MemberVerifyID 驗證身分證ID 無彈窗版本 會員id驗證頁(login/id-verification)使用
  MemberVerifyIDNoAlert(payload) {
    return this.api.post('mobileweb/api/member/verifyid', payload, {
      mask: true,
      alert: false,
    });
  }
  // MemberVerifyEmail 驗證信箱
  MemberVerifyEmail(payload) {
    return this.api.post('mobileweb/api/member/verifyemail', payload, {
      mask: true,
      alert: false,
    });
  }
  // MemberVerifyOTP OTP 驗證
  MemberVerifyOTP(payload) {
    return this.api.post('mobileweb/api/member/verifyotp', payload, {
      mask: true,
      alert: false,
    });
  }
  // MemberSendOTP 註冊/無法登入發送OTP
  MemberSendOTP(payload) {
    return this.api.post('mobileweb/api/member/sendotp', payload, {
      mask: false,
      alert: false,
    });
  }

  // 無法登入驗證是否為會員
  MemberCantLogin(payload) {
    return this.api.post('mobileweb/api/member/cantlogin', payload, {
      mask: true,
      alert: false,
    });
  }
  // 社群帳號登入 - MemberOAUTH
  async MemberOAUTH(payload) {
    const mMemberinfo: any = await this.api.post(
      'mobileweb/api/member/oauth',
      payload,
      {
        mask: true,
        alert: false,
      }
    );

    console.log(mMemberinfo);

    if (mMemberinfo.status === 200 && mMemberinfo.data) {
      this.cds.gMobileToken = mMemberinfo.access_token;
      this.cds.userInfo = mMemberinfo.data;
      StorageService.setItem(StorageKeys.mobileToken, mMemberinfo.access_token);
      const dataEncryption: any = CommonService.ObjEncryption(mMemberinfo.data);
      CommonService.SaveUsertoSession(dataEncryption);
    }

    return mMemberinfo;
  }
  // 驗證社群帳號綁定 - MemberOAUTH
  MemberIsBind(payload) {
    return this.api.post('mobileweb/api/member/isbind', payload, {
      mask: true,
      alert: false,
    });
  }

  // 綁定社群帳號授權 - MemberBind
  MemberBind(payload) {
    return this.api.post('mobileweb/api/member/bind', payload, {
      mask: true,
      alert: true,
    });
  }

  /** 取得MemberCenter 所需要的info */
  GetPolicyCNT(payload) {
    return this.api.post('Policy/Compulsory/GetPolicyCNT', payload, {
      mask: true,
      alert: false,
    }) as IResponse;
  }

  /** 開通線下保單 */
  OCOpen(payload) {
    return this.api.post('mobileweb/api/member/openofflinepolicy', payload, {
      mask: true,
      alert: false,
    }) as IResponse;
  }

  /** 提供會員資料給人壽 */
  providesso(payload) {
    return this.api.post(
      'mobileweb/api/member/providesso',
      payload
    ) as IResponse;
  }

  /** 判斷人壽會員是否為產險會員 */
  async checksso(payload) {
    const rep = (await this.api.post(
      'mobileweb/api/member/checksso',
      payload
    )) as any;

    if (rep.status === 200 && rep.data) {
      const dataEncryption: any = CommonService.ObjEncryption(rep.data);
      CommonService.SaveUsertoSession(dataEncryption);

      StorageService.setItem(StorageKeys.mobileToken, rep.access_token);
      this.cds.gMobileToken = rep.access_token;
      this.cds.userInfo = rep.data;
      this.cds.gIsSSO = true;
    }
    return rep;
  }

  /** 人壽取得會員資料  */
  getsso(payload) {
    return this.api.post('mobileweb/api/member/getsso', payload) as IResponse;
  }

  /**
   * @description
   * 取得人壽會員資訊，呼叫的位置目前是國泰人壽越南的API 會有換的可能
   *
   */
  changesso(payload) {
    return this.api.getsso('ZSWeb/api/getssomember', payload) as any;
  }

  /** 上傳圖片 */
  upload(payload) {
    const formData = new FormData();
    if (payload.image) {
      formData.append('file', payload.image);
    }
    return this.api.postWithFormData(
      'mobileweb/api/member/uploadfiletos3 ',
      formData
    ) as IResponse;
  }

  /** 新增身分證字號/重新驗證 */
  MemberRegID(payload) {
    return this.api.post('mobileweb/api/member/regid ', payload) as IResponse;
  }

  /** 查詢重要通知資料 */
  NoticeList(param) {
    return this.api.post(`Policy/Compulsory/NoticeList`, param, {
      mask: true,
      alert: false,
    });
  }

  // ** 非API區 ↓ */

  /** 更新會員資訊 */
  // 帳號登入
  reflashUserInfo(payload: any | string) {
    if (typeof payload === 'string' && payload === 'reset') {
      const dataEncryption: any = CommonService.ObjEncryption(
        this.cds.userInfo
      );
      CommonService.SaveUsertoSession(dataEncryption);
    } else {
      const {
        email_bk,
        mobile,
        county,
        province,
        addr,
        google_account,
        google_title,
        fb_account,
        fb_title,
        is_validate_policy,
        is_link_sns,
        is_link_id,
      } = payload;
      // 刷新資訊
      this.cds.userInfo.email_bk = email_bk;
      this.cds.userInfo.mobile = mobile;
      this.cds.userInfo.province = province;
      this.cds.userInfo.county = county;
      this.cds.userInfo.addr = addr;
      this.cds.userInfo.google_account = google_account;
      this.cds.userInfo.google_title = google_title;
      this.cds.userInfo.fb_account = fb_account;
      this.cds.userInfo.fb_title = fb_title;
      this.cds.userInfo.is_validate_policy = is_validate_policy;
      this.cds.userInfo.is_link_sns = is_link_sns;
      this.cds.userInfo.is_link_id = is_link_id;

      // 避免重整沒帶到新資料
      const dataEncryption: any = CommonService.ObjEncryption(
        this.cds.userInfo
      );
      CommonService.SaveUsertoSession(dataEncryption);
    }
  }

  reflashSession() {
    // 避免重整沒帶到新資料
    const dataEncryption: any = CommonService.ObjEncryption(this.cds.userInfo);
    CommonService.SaveUsertoSession(dataEncryption);
  }

  /** 驗證是否有第三方登入 */
  async authThirdLogin(payload) {
    if (payload) {
      const rep: IResponse = await this.MemberIsBind(payload);
      if (rep.status === 200) {
        const { facebook, google } = rep.data;
        let mstr = '';
        if (facebook === 'Y' && google === 'Y') {
          mstr = 'both';
        }
        if (facebook === 'Y' && google === 'N') {
          mstr = 'facebook';
        }
        if (facebook === 'N' && google === 'Y') {
          mstr = 'google';
        }
        return mstr;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  /** 強制險查詢 */
  InsuranceQueryApi(payload) {
    return this.api.post('Policy/Compulsory/PoliceCheckPolicy', payload, {
      mask: true,
      alert: false,
    });
  }
}
