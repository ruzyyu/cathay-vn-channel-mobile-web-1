import { IResponse } from './../../../interface/index';
import { ApiHelperService } from './../../utils/api-helper.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SendEmailService {
  constructor(private api: ApiHelperService) {}
  /** 送信 */
  sendMail(payload) {
    return this.api.post<IResponse>('mobileweb/api/email/send', payload);
  }
}
