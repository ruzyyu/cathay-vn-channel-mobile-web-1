import { IResponse } from './../../../interface/index';
import { ApiHelperService } from 'src/app/core/services/utils/api-helper.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ClaimService {
  constructor(private api: ApiHelperService) {}

  /** 取得會員的理賠進度清單 */
  async getClaimList(payload) {
    return this.api.post('Claim/Compulsory/GetClaimList', payload, {
      mask: true,
      alert: true,
    }) as IResponse;
  }

  /** 取得快速理賠班機資訊 */
  async getFastFlyClaimList(payload) {
    return this.api.post('Claim/Compulsory/GetFastFlyClaimList', payload, {
      mask: true,
      alert: true,
    }) as IResponse;
  }

  postSaveClaim(payload) {
    return this.api.post('Claim/Compulsory/SaveClaim', payload, {
      mask: true,
      alert: true,
    }) as IResponse;
  }

  GetPolicyList(payload) {
    return this.api.post('Claim/Compulsory/GetPolicyList', payload, {
      mask: true,
      alert: false,
    }) as IResponse;
  }

  async saveSupplementDocs(payload) {
    return this.api.post('Claim/Compulsory/SaveSupplementDocs', payload, {
      mask: true,
      alert: false,
    }) as IResponse;
  }
}
