/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ClaimService } from './claim.service';

describe('Service: Claim', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClaimService],
    });
  });

  it('should ...', inject([ClaimService], (service: ClaimService) => {
    expect(service).toBeTruthy();
  }));
});
