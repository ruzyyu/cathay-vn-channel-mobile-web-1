import { ApiHelperService } from 'src/app/core/services/utils/api-helper.service';
import { IResponse } from 'src/app/core/interface';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InsuranceService {
  constructor(private api: ApiHelperService) {}

  /** 機車試算 */
  calcInsurance(payload) {
    return this.api.post<IResponse>(
      'Insurance/Compulsory/CalucateComp',
      payload
    );
  }

  /** 旅遊試算 */
  calcTravelInsurance(payload) {
    return this.api.post('Insurance/Compulsory/CalucateTravelT1T3T4', payload);
  }

  /** @description 核保  */
  checkOrderOK(payload) {
    return this.api.post('Insurance/Compulsory/CheckForComp', payload, {
      mask: true,
      alert: true,
    });
  }

  /** @description 核保  */
  checkTravelOrderOK(payload) {
    return this.api.post('Insurance/Compulsory/CheckForTravelT1T3T4', payload, {
      mask: true,
      alert: true,
    });
  }
}
