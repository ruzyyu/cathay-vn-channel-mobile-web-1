import { DatapoolService } from './../../datapool/datapool.service';
import { ApiHelperService } from 'src/app/core/services/utils/api-helper.service';
import { Injectable } from '@angular/core';
import device from 'current-device';

@Injectable({
  providedIn: 'root',
})
export class LocalService {
  FLangType: any;
  constructor(private api: ApiHelperService, private cds: DatapoolService) {
    this.FLangType = this.cds.gLangType;
  }

  /**取得最新消息列表
   *@typeid 0:全部 2:新聞公告 3:優惠活動 4:首頁通知
  */
  NewsGetList(typeid) {
    return this.api.get(`mobileweb/api/news/getlist?typeid=${typeid}`, {});
  }

  /** 取得最新消息內容 */
  NewsGetContent(payload) {
    return this.api.post('mobileweb/api/news/getcontent', payload);
  }

  JSON(payload) {
    return this.api._get(payload);
  }

  // Banner 輪播 列表 BannerGetList
  BannerGetList() {
    let desc: string;
    const device = this.bowerJudgment();
    console.log(this.cds.gLangType);
    switch (this.cds.gLangType) {
      case 'vi-VN':
        desc = `VN-${device}`;
        break;
      default:
        desc = `EN-${device}`;
    }
    return this.api.get(`mobileweb/api/banner/getlist/${desc}`);
  }

  /** 裝置辦定 */
  bowerJudgment() {
    if (device.desktop()) {
      return 'PC';
    } else if (device.tablet()) {
      return 'PDA';
    } else if (device.mobile()) {
      return 'Mobile';
    }
  }
}
