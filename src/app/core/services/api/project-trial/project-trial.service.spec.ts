import { TestBed } from '@angular/core/testing';

import { ProjectTrialService } from './project-trial.service';

describe('ProjectTrialService', () => {
  let service: ProjectTrialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProjectTrialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
