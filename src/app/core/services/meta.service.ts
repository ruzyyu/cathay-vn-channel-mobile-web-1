import { Injectable } from '@angular/core';
import { Title, Meta, MetaDefinition } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root',
})
export class MetaService {
  constructor(private meta: Meta, private metaTitle: Title) {}

  private getMeta(url): MetaDefinition[] {
    switch (url) {
      case '/':
        return [
          {
            name: 'title',
            content:
              'Phi Nhân Thọ Cathay - Bảo hiểm online',
          },
          {
            name: 'description',
            content:
              'Mua bảo hiểm chưa bao giờ dễ dàng đến thế với Cathay. Sản phẩm đa dạng, chi phí hấp dẫn, dịch vụ tận tình sẵn sàng đáp ứng mọi nhu cầu của khách hàng',
          },
        ];

      case '/about':
        return [
          {
            name: 'title',
            content: 'Phi Nhân Thọ Cathay Việt Nam - Công Ty Bảo Hiểm Hàng Đầu Đài Loan',
          },
          {
            name: 'description',
            content:
              'Công ty bảo hiểm phi nhân thọ Cathay Việt Nam luôn phấn đấu không ngừng với mong muốn cung cấp cho khách hàng những sản phẩm và dịch vụ bảo hiểm uy tín, chất lượng hàng đầu. Quyền lợi khách hàng luôn được bảo vệ tối ưu',
          },
        ];

      case '/about/opening':
        return [
          {
            name: 'title',
            content: 'Gia Nhập Phi Nhân Thọ Cathay - Việc Làm',
          },
          {
            name: 'description',
            content:
              'Nhận ngay tin tức tuyển dụng mới nhất để có cơ hội gia nhập Cathay Insurance và trở thành thành viên của đại gia đình Cathay',
          },
        ];
      case '/help':
        return [
          {
            name: 'title',
            content: 'Tư Vấn Bảo Hiểm Cathay - Hỗ Trợ Giải Đáp Thắc Mắc',
          },
          {
            name: 'description',
            content:
              'Hỗ trợ tư vấn về các loại bảo hiểm TNDS bắt buộc, bảo hiểm tự nguyện xe ô tô, xe máy, bảo hiểm du lịch trong nước, bảo hiểm du lịch nước ngoài. Giải đáp tất cả các câu hỏi liên quan đến sản phẩm, dịch vụ của Phi Nhân Thọ Cathay Việt Nam',
          },
        ];
      case '/help/news-detail':
        return [
          {
            name: 'title',
            content: 'Phi Nhân Thọ Cathay',
          },
        ];

      case '/member':
        return [
          {
            name: 'title',
            content: 'Trung Tâm Khách Hàng Cathay | Hội Viên Cathay',
          },
          {
            name: 'description',
            content:
              'Sử dụng trung tâm khách hàng trực tuyến tra cứu hợp đồng, yêu cầu bồi thường, kiểm tra tiến độ bồi thường, gia hạn hợp đồng... Lập tức nhận được thông tin khi có chương trình và sự kiện đặc biệt do Cathay tổ chức',
          },
        ];
      case '/member/insurance-query':
        return [
          {
            name: 'title',
            content:
              'Tra Cứu Thông Tin Bảo Hiểm TNDS Bắt Buộc | Phi Nhân Thọ Cathay',
          },
          {
            name: 'description',
            content:
              'Kiểm tra thông tin chi tiết của bảo hiểm trách nhiệm dân sự bắt buộc mà bạn đã sở hữu một cách nhanh chóng và dễ dàng tại website trực tuyến của Phi Nhân Thọ Cathay',
          },
        ];
      case '/member/my-order':
        return [
          {
            name: 'title',
            content: 'Tra Cứu Thông Tin Đơn Bảo Hiểm | Phi Nhân Thọ Cathay',
          },
          {
            name: 'description',
            content:
              'Hiển thị tất cả các loại đơn bảo hiểm ô tô, bảo hiểm xe máy, bảo hiểm du lịch mà khách hàng đã mua thông qua trang web và đại lý của Cathay',
          },
        ];
      case '/member/my-order/detail':
        return [
          {
            name: 'title',
            content: 'Kiểm Tra Thông Tin Đơn Bảo Hiểm | Phi Nhân Thọ Cathay',
          },
          {
            name: 'description',
            content:
              'Kiểm tra thông tin chi tiết, tải file giấy chứng nhận bảo hiểm của các loại bảo hiểm ô tô, bảo hiểm xe máy, bao hiểm du lịch mà khách hàng đã mà khách hàng đã mua thông qua trang web và đại lý của Cathay',
          },
        ];
      case '/member/my-order/detail':
        return [
          {
            name: 'title',
            content:
              'Kiểm Tra Giấy Chứng Nhận Bảo Hiểm | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/member/my-policy':
        return [
          {
            name: 'title',
            content: 'Tìm Kiếm Quy Tắc Sản Phẩm Bảo Hiểm | Phi Nhân Thọ Cathay',
          },
          {
            name: 'description',
            content:
              'Tìm kiếm trực tuyến chính sách và quyền lợi bảo hiểm của các sản phẩm mà khách hàng đã tin tưởng và lựa chọn bảo hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/member/my-policy/detail':
        return [
          {
            name: 'title',
            content: 'Tải Quy Tắc Sản Phẩm Bảo Hiểm | Phi Nhân Thọ Cathay',
          },
          {
            name: 'description',
            content:
              'Tải biểu mẫu, và quy tắc quy quy định chính sách và quyền lợi khi tham gia các sản phẩm và dịch vụ do Phi Nhân Thọ Cathay cung cấp',
          },
        ];

      case '/member/claim':
        return [
          {
            name: 'title',
            content: 'Bồi Thường Bảo Hiểm | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/member/introduction':
        return [
          {
            name: 'title',
            content: 'Thông Tin Hội Viên | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/member/fast-claim':
        return [
          {
            name: 'title',
            content:
              'Bồi Thường Nhanh Bảo Hiểm Trễ Chuyến | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/member/claim-apply':
        return [
          {
            name: 'title',
            content: 'Yêu Cầu Bồi thường Nhanh | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/member/claim-result':
        return [
          {
            name: 'title',
            content:
              'Tình Trạng Xử Lý Bồi Thường | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/member/search':
        return [
          {
            name: 'title',
            content: 'Tìm Kiếm | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];

      case '/member/my-policy/insurance-changing':
        return [
          {
            name: 'title',
            content: 'Sửa Đổi Đơn Bảo Hiểm | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/member/maintain':
        return [
          {
            name: 'title',
            content: 'Thông Tin Hội Viên | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/member/important-notice':
        return [
          {
            name: 'title',
            content: 'Thông Báo Quan Trọng | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];

      case '/member/verify':
        return [
          {
            name: 'title',
            content:
              'Xác minh các đơn mua bảo hiểm với hệ thống trực tuyến của bảo hiểm cathay',
          },
          {
            name: 'description',
            content:
              'Truy cập trang web trực tuyến để xác minh đơn mua bảo hiểm mà bạn đã sở hữu khi lựa chọn bảo hiểm cathay. Mua bảo hiểm online ngay tại website của Cathay.',
          },
        ];
      case '/login':
        return [
          {
            name: 'title',
            content: 'Đăng Nhập Hội Viên | Phi Nhân Thọ Cathay',
          },
          {
            name: 'description',
            content:
              'Đăng nhập hội viên Cathay ngay để tra cứu hợp đồng bảo hiểm, yêu cầu bồi thường, kiểm tra tiến độ bồi thường, gia hạn hợp đồng và tham gia các chương trình hấp dẫn đặc biệt chỉ dành riêng cho hội viên',
          },
        ];
      case '/login/register-id':
        return [
          {
            name: 'title',
            content: 'Đăng Ký Hội Viên | Phi Nhân Thọ Cathay',
          },
          {
            name: 'description',
            content:
              'Gia nhập hội viên Cathay để hưởng vô vàn ưu đãi và sử dụng những tính năng co cùng tiện lợi chỉ dành riếng cho hội viên như tra cứu hợp đồng, yêu cầu bồi thường, kiểm tra tiến độ bồi thường, gia hạn hợp đồng',
          },
        ];
      case '/online-insurance':
        return [
          {
            name: 'title',
            content:
              'Bảo Hiểm Trực Tuyến Cathay - Chủ Động Bảo Vệ Trong Tầm Tay',
          },
          {
            name: 'description',
            content:
              'Sản phẩm bảo hiểm trực tuyến đa dạng: bảo hiểm ô tô, bảo hiểm xe máy, bảo hiểm du lịch. Thao tác đơn giản, thanh toán online, thông báo bồi thường tự động dễ dàng thuận tiện, hỗ trợ khách hàng 24/7',
          },
        ];
      case '/online-insurance/travel-insurance':
        return [
          {
            name: 'title',
            content:
              'Bảo Hiểm Du Lịch - Cathay Người Bạn Đồng Hành Trong Mọi Chuyến Đi',
          },
          {
            name: 'description',
            content:
              'Gói Bảo hiểm du lịch linh hoạt phù hợp cho chuyến đi trong và ngoài nước, hỗ trợ khi trễ chuyến, hủy chuyến, mất thiệt hại hành lý. Với vài thao tác đơn giản trước giờ khởi hành 6 tiếng, Cathay sẽ giúp chuyến đi của bạn an toàn và trọn vẹn hơn',
          },
        ];
      case '/online-insurance/car-insurance':
        return [
          {
            name: 'title',
            content:
              'Mua Bảo hiểm Cho Xe Ô Tô Ở Đâu Uy Tín? Chọn Ngay Bảo Hiểm Cathay!',
          },
          {
            name: 'description',
            content:
              'Mua bảo hiểm xe ô tô trực tuyến dễ dàng, an toàn, thuận tiện. Nhận giấy chứng nhận bảo hiểm điện tử sau vài phút thao tác. Hỗ trợ tra cứu thông bảo hiểm trực tuyến chính xác nhanh chóng',
          },
        ];
      case '/online-insurance/motor-insurance':
        return [
          {
            name: 'title',
            content:
              'Bảo Hiểm Xe Máy | Xe Máy Điện | Cathay An Tâm Trên Mọi Nẻo Đường',
          },
          {
            name: 'description',
            content:
              'Mua bảo hiểm xe máy, xe điện tại trang web của Cathay. Thao tác đơn giản, cấp đơn nhanh chóng. Thời hạn bảo hiểm linh hoạt từ 1 đến 3 năm. Nhận giấy chứng nhận điện tử qua email ngay sau khi hoàn tất thanh toán',
          },
        ];
      case '/online-insurance/property-insurance':
        return [
          {
            name: 'title',
            content: 'Bảo Hiểm Tài Sản - Hạn Chế Thiệt Hại với Cathay',
          },
          {
            name: 'description',
            content:
              'Phi Nhân Thọ Cathay cung cấp bảo hiểm cho mọi rủi ro về tài sản, gánh vác nỗi lo cho doanh nghiệp khi xảy ra những sự cố bất ngờ không lường trước được, kể cả những tổn thất khi gián đoạn kinh doanh',
          },
        ];
      case '/online-insurance/cargo-insurance':
        return [
          {
            name: 'title',
            content:
              'Bảo Hiểm Hàng Hóa Vận Chuyển Cathay - Giảm Bớt Nỗi Lo Rủi Ro Hàng Hóa',
          },
          {
            name: 'description',
            content:
              'Lựa chọn bảo hiểm hàng hóa vận chuyển nội địa, đường biển, đường hàng không, đường bộ của Cathay ngay để bảo vệ thiệt hại cho hàng hóa trong quá trình vận chuyển.',
          },
        ];
      case '/online-insurance/injury-insurance':
        return [
          {
            name: 'title',
            content:
              'Bảo Hiểm Tai Nạn Cathay - Lá Chắn Bảo Vệ Cho Bạn Và Gia Đình Thân Yêu',
          },
          {
            name: 'description',
            content:
              'Xóa tan lo sợ khi tai nạn bất ngờ ập đến với bảo hiểm Tại nạn của Cathay. Phí bảo hiểm hợp lý, quyền lợi bồi thường cao. Thủ tục cấp bảo hiểm nhanh chóng. Giao giấy chứng nhận bảo hiểm tại nhà. Tư vấn tận tình, hỗ trợ giải đáp thắc mắc 24/7',
          },
        ];
      case '/online-insurance/house-insurance':
        return [
          {
            name: 'title',
            content: 'Bảo Hiểm Nhà Tư Nhân - Cùng Cathay Bảo Vệ Tổ Ấm',
          },
          {
            name: 'description',
            content:
              'Bảo hiểm toàn diện cho thiệt hại xảy ra đối với ngôi nhà và tài sản bên trong do các nguyên nhân rủi ro không lường trước. Bảo hiểm linh hoạt, chi phí cạnh tranh, có nhân viên tư vấn riêng tại nhà hỗ trợ giải quyết mọi vấn đề thắc mắc',
          },
        ];
      case '/online-insurance/liability-insurance':
        return [
          {
            name: 'title',
            content:
              'Bảo Hiểm Trách Nhiệm - Cathay Đồng Hành Gánh Vác Với Doanh Nghiệp',
          },
          {
            name: 'description',
            content:
              'Thiết kế gói bảo hiểm riêng biệt cho doanh nghiệp của bạn. Cathay bảo vệ doanh nghiệp trước trách nhiệm pháp lý đối với thiệt hại gây ra do sản phẩm, dịch vụ hoặc tổn thất gây ra trong quá trình kinh doanh, thực hiện công việc',
          },
        ];
      case '/online-insurance/engineering-insurance':
        return [
          {
            name: 'title',
            content:
              'Bảo Hiểm Kỹ Thuật - Bảo Hiểm Công Trình - An Tâm Thi Công Với Cathay',
          },
          {
            name: 'description',
            content:
              'Cathay bảo hiểm cho những tổn thất phát sinh trong quá trình xây dựng công trình, những rủi ro khi xây dựng, lắp đặt máy móc thiết bị. Hỗ trợ tư vấn giới thiệu những giải pháp dành riêng cho từng doanh nghiệp',
          },
        ];
      case '/news?type=all':
        return [
          {
            name: 'title',
            content: 'Kiến Thức - Thông Tin Tư Vấn Bảo Hiểm - Cathay',
          },
          {
            name: 'description',
            content:
              'Thông tin sản phẩm, thông báo chính thức của Cathay, tin tức liên quan đến ngành bảo hiểm, kiến thức về lĩnh vực bảo hiểm',
          },
        ];
      case '/news?type=3':
        return [
          {
            name: 'title',
            content: 'Thông Tin Ưu Đãi - Chương Trình CSKH Khách Hàng - Cathay',
          },
          {
            name: 'description',
            content:
              'Chương trình khuyến mãi, chương trình đặc biệt dành cho hội viên, các sự kiện ưu đãi do Cathay tổ chức',
          },
        ];

      case '/login/register-data':
        return [
          {
            name: 'title',
            content: 'Đăng Kí Hội Viên | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/login/otp-verification':
        return [
          {
            name: 'title',
            content: 'Nhận Mã Xác Thực | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/login/change-password':
        return [
          {
            name: 'title',
            content: 'Đổi Mật Khẩu | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/login/id-verification':
        return [
          {
            name: 'title',
            content: 'Xác Thực Danh Tính | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/login/find-account':
        return [
          {
            name: 'title',
            content: 'Tìm Lại Mật Khẩu | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/login/bind-account':
        return [
          {
            name: 'title',
            content: 'Đăng Nhập Hội Viên | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/sitemap':
        return [
          {
            name: 'title',
            content: 'Sơ Đồ Trang Web | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/search':
        return [
          {
            name: 'title',
            content: 'Trang Tìm Kiếm | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/payment':
        return [
          {
            name: 'title',
            content: 'Thanh Toán Phí Bảo Hiểm | Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/online-insurance/project-trial-motor/calcAmt':
        return [
          {
            name: 'title',
            content: 'Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/online-insurance/project-trial-car/calcAmt':
        return [
          {
            name: 'title',
            content: 'Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];
      case '/online-insurance/project-trial-travel/calcAmt':
        return [
          {
            name: 'title',
            content: 'Bảo Hiểm Phi Nhân Thọ Cathay',
          },
        ];

      default:
        break;
    }
  }

  addTags(url) {
    const urlarr = url.split('?');
    let mdata: MetaDefinition[];
    this.getMeta(urlarr[0])
      ? (mdata = this.getMeta(urlarr[0]))
      : (mdata = this.getMeta(url));
    this.meta.addTags(mdata);

    if (mdata) {
      const mtitle = mdata.filter((item) => item.name === 'title');
      this.metaTitle.setTitle(mtitle[0].content);
    }
  }

  // search 搜尋頁面 顯示畫面用
  getMetaData(url: string) {
    if (url && this.getMeta(url)) {
      const inputData = this.getMeta(url);
      const outputData: any = {};
      inputData.forEach((data) => {
        outputData[data.name] = data.content;
      });
      return outputData;
    }
    return null;
  }
}
