import { motorUserInfo, ICarUserInfo, IConfirm } from './../../interface/index';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Validators } from '@angular/forms';
import { ValidatorsService } from './../utils/validators.service';
import { Injectable, Input } from '@angular/core';
import moment from 'moment';
@Injectable({
  providedIn: 'root',
})
export class OnlinecalcService {
  constructor(private vi: ValidatorsService, private cds: DatapoolService) {}

  paylist = [
    {
      id: '1',
      title: this.cds.gLang.C04.step2.C0025,
      defaultIcon: 'icons-ic-pay-creditcard i-icon pay-icon',
      activeIcon: 'icons-ic-pay-creditcard-w i-icon pay-w-icon',
      type: 'InternationalCard',
    },
    {
      id: '2',
      title: this.cds.gLang.C04.step2.C0026,
      defaultIcon: 'icons-ic-pay-debitcard i-icon pay-icon',
      activeIcon: 'icons-ic-pay-debitcard-w i-icon pay-w-icon',
      type: 'DomesticBank',
    },
    {
      id: '3',
      title: this.cds.gLang.C04.step2.C0027,
      defaultIcon: 'icons-ic-pay-wallet i-icon pay-icon',
      activeIcon: 'icons-ic-pay-wallet-w i-icon pay-w-icon',
      type: 'VTCPay',
    },
  ];

  initData_motor(userInfo, motorUserInfo: motorUserInfo, motorValue: string) {
    /** 如果是付款失敗的上一頁..導回資料 */
    console.log('initData_motor');
    if (this.cds.gTCalc_to_save && this.cds.gTCalc_to_save.type === 'motor') {
      userInfo = this.cds.gTCalc_to_save.userinfo;
      const mOrder = this.cds.gTCalc_to_save.saveorder;

      motorUserInfo = {
        prod_Polcode: 'CF02002',
        vehcOwnr: mOrder.vehc_ownr,
        carpwrkd: mOrder.car_pwr_kd,
        procDate: mOrder.pol_dt
          ? moment(mOrder.pol_dt, 'YYYY-MM-DD').format('YYYY-MM-DD')
          : '',
        procDate_end: mOrder.pol_due_dt
          ? moment(mOrder.pol_due_dt, 'YYYY-MM-DD').format('YYYY-MM-DD')
          : '',
        vehcKd: mOrder.vehc_kd,
        maxCapl: mOrder.max_capl,
        rgstNo: mOrder.rgst_no,
        plate: mOrder.fram_no,
        engn_no: mOrder.engn_no,
      };
    }

    const rep = {
      certificate_number: [
        userInfo.certificate_number_12 || userInfo.certificate_number_9,
        [this.vi.fontLengthNumber, Validators.required],
      ],
      name: [
        userInfo.customer_name,
        [
          Validators.required,
          this.vi.languageFormat,
          this.vi.languagelength(160),
        ],
      ],
      birthday: [userInfo.birthday, [Validators.required]],
      sex: [userInfo.sex || '1', [Validators.required]],
      mobile: [userInfo.mobile, [Validators.required, this.vi.vnPhone]],
      email: [userInfo.email, [Validators.required, this.vi.email]],
      email_bk: [userInfo.email_bk, [this.vi.email]],
      province: [userInfo.province || '', [Validators.required]],
      county: [userInfo.county || '', [Validators.required]],
      address: [userInfo.addr, [Validators.maxLength(240)]],

      motor_prodPolcode: [motorUserInfo.prod_Polcode, []],
      motor_vkd: [motorUserInfo.vehcKd, [Validators.required]],
      motor_mkd: [motorUserInfo.carpwrkd, []],
      motor_procDate: [
        motorUserInfo.procDate
          ? moment(motorUserInfo.procDate).format('YYYY-MM-DD')
          : '',
        [Validators.required],
      ],
      motor_procDateEnd: [
        motorUserInfo.procDate_end
          ? moment(motorUserInfo.procDate_end).format('YYYY-MM-DD')
          : '',
        [Validators.required],
      ],
      motor_people: [motorUserInfo.maxCapl, []],
      // 個人資訊
      motor_name: [
        motorUserInfo.vehcOwnr,
        [
          Validators.required,
          this.vi.languageFormat,
          this.vi.languagelength(160),
        ],
      ],
      motor_license_plate: [
        motorUserInfo.rgstNo,
        [Validators.required, this.vi.vnRgstNo(motorValue)],
      ],
      motor_body_number: [motorUserInfo.plate, []],
      motor_engn_no: [motorUserInfo.engn_no, []],
      r_year: [
        motorUserInfo.procDateYear || '1',
        { updateOn: 'change', validators: [] },
      ], // 年份
    };
    return rep;
  }

  initData_car(userInfo, carUserInfo: ICarUserInfo) {
    /** 如果是付款失敗的上一頁..導回資料 */
    if (this.cds.gTCalc_to_save && this.cds.gTCalc_to_save.type === 'car') {
      userInfo = this.cds.gTCalc_to_save.userinfo;
      const mOrder = this.cds.gTCalc_to_save.saveorder;
      carUserInfo = {
        vehcKd: mOrder.vehc_kd,
        carPwrKd: mOrder.car_pwr_kd,
        procDate: moment(mOrder.pol_dt, 'YYYY-MM-DD').format('DD-MM-YYYY'),
        procDateEnd: moment(mOrder.pol_due_dt, 'YYYY-MM-DD').format(
          'DD-MM-YYYY'
        ),
        maxCapl: mOrder.max_capl,
        vehcOwnr: mOrder.vehc_ownr,
        rgstNo: mOrder.rgst_no,
        framNo: mOrder.fram_no,
        engnNo: mOrder.engn_no,
      };
    }

    const rep = {
      certificate_number: [
        userInfo.certificate_number_12 || userInfo.certificate_number_9,
        [this.vi.fontLengthNumber, Validators.required],
      ],
      name: [
        userInfo.customer_name,
        [
          Validators.required,
          this.vi.languageFormat,
          this.vi.languagelength(160),
        ],
      ],
      birthday: [userInfo.birthday, [Validators.required]],
      sex: [userInfo.sex || '1', [Validators.required]],
      mobile: [userInfo.mobile, [Validators.required, this.vi.vnPhone]],
      email: [userInfo.email, [Validators.required, this.vi.email]],
      email_bk: [userInfo.email_bk, [this.vi.email]],
      province: [userInfo.province || '', [Validators.required]],
      county: [userInfo.county || '', [Validators.required]],
      address: [
        userInfo.addr,
        [Validators.required, Validators.maxLength(240)],
      ],
      car_vkd: [carUserInfo.vehcKd, [Validators.required]],
      car_ckd: [carUserInfo.carPwrKd, [Validators.required]],
      car_people: [
        carUserInfo.maxCapl === 0 ? '' : carUserInfo.maxCapl,
        [Validators.required, this.vi.vnCheckQty],
      ],
      car_procDate: [
        carUserInfo.procDate
          ? moment(carUserInfo.procDate).format('DD/MM/YYYY')
          : '',
        [Validators.required],
      ],
      car_procDateEnd: [
        carUserInfo.procDateEnd
          ? moment(carUserInfo.procDateEnd).format('DD/MM/YYYY')
          : '',
        [Validators.required],
      ],
      car_name: [
        carUserInfo.vehcOwnr,
        [
          Validators.required,
          this.vi.languageFormat,
          this.vi.languagelength(160),
        ],
      ],
      car_license_plate: [
        carUserInfo.rgstNo,
        [Validators.required, this.vi.vnRgstCarNo],
      ], // 車牌
      car_body_number: [carUserInfo.framNo, [Validators.required]], // 車身
      car_engine_number: [carUserInfo.engnNo, [Validators.required]], // 引擎
      r_year: [
        carUserInfo.procDateYear || '1',
        { updateOn: 'change', validators: [] },
      ], // 年份
    };
    return rep;
  }

  initData_travel(userInfo) {
    console.log('initData_travel');

    /** 如果是付款失敗的上一頁..導回資料 */
    if (this.cds.gTCalc_to_save && this.cds.gTCalc_to_save.type === 'travel') {
      userInfo = this.cds.gTCalc_to_save.userinfo;
      const mOrder = this.cds.gTCalc_to_save.saveorder;
    }

    const rep = {
      // 試算資料
      Areainput: ['', []],
      rangeDateS: ['', []],
      rangeDateE: ['', []],
      timePick: ['', []],
      userPick: ['', []],
      // 用戶資料
      certificate_number: [
        userInfo.certificate_number_12 || userInfo.certificate_number_9,
        [this.vi.fontLengthNumber, Validators.required],
      ],
      name: [
        userInfo.customer_name,
        [
          Validators.required,
          this.vi.languageFormat,
          this.vi.languagelength(160),
        ],
      ],
      birthday: [userInfo.birthday, [Validators.required]],
      sex: [userInfo.sex || '1', [Validators.required]],
      mobile: [userInfo.mobile, [Validators.required, this.vi.vnPhone]],
      email: [userInfo.email, [Validators.required, this.vi.email]],
      email_bk: [userInfo.email_bk, [this.vi.email]],
      province: [userInfo.province || '', [Validators.required]],
      county: [userInfo.county || '', [Validators.required]],
      address: [
        userInfo.addr,
        [Validators.required, Validators.maxLength(240)],
      ],
    };

    return rep;
  }

  resetData() {
    if (this.cds.gTCalc_to_save) {
      this.cds.gTCalc_to_save.type = 'none';
    }
  }

  initMotor(): motorUserInfo {
    return {
      /** 商品細類別 */
      prod_Polcode: 'CF02002',
      /** 車輛種類代號 */
      vehcKd: '',
      /** 車輛保險類型 */
      carpwrkd: '',
      /** 保單生效日 */
      procDate: '',
      /** 保單結束日 */
      procDate_end: '',
      /** 座位數: 機車固定 2 */
      maxCapl: 2,
      /** 車主姓名 */
      vehcOwnr: '',
      /** 車牌號碼 */
      rgstNo: '',
      /** 車身號碼 */
      plate: '',
    };
  }

  calcCKD(vkd, count): string {
    if (vkd === '21') {
      const a = Number(count) / 6;
      let carPwrKdValue = 'A4';
      if (a > 4) {
        return carPwrKdValue;
      }
      switch (Math.trunc(a)) {
        case 0:
          carPwrKdValue = 'A1';
          break;
        case 1:
          carPwrKdValue = 'A2';
          break;
        case 2:
        case 3:
        case 4:
          carPwrKdValue = 'A3';
          break;
        default:
          break;
      }
      return carPwrKdValue;
    } else if (vkd === '31') {
      const mint = Number(count);
      if (mint <= 3) {
        return '3';
      }
      if (mint >= 56) {
        return '56';
      }
      return String(count);
    }
  }

  /** 轉換tempuser 到 formal */
  converuser(tempuser) {
    const {
      customer_name,
      certificate_number_9,
      certificate_number_12,
      birthday,
      email,
      sex,
      mobile,
      county,
      province,
      addr,
    } = tempuser;

    this.cds.userInfo.customer_name = customer_name.trim();
    this.cds.userInfo.certificate_number_9 = certificate_number_9;
    this.cds.userInfo.certificate_number_12 = certificate_number_12;
    this.cds.userInfo.birthday = birthday;
    this.cds.userInfo.sex = sex;
    this.cds.userInfo.email = email;
    this.cds.userInfo.mobile = mobile;
    this.cds.userInfo.county = county;
    this.cds.userInfo.province = province;
    this.cds.userInfo.addr = addr;
  }

  /** 註冊會員註冊到一半跑掉 要做初始化.... */
  Inituser() {
    if (!this.cds.userInfo.seq_no) {
      this.cds.userInfo.customer_name = '';
      this.cds.userInfo.certificate_number_9 = '';
      this.cds.userInfo.certificate_number_12 = '';
      this.cds.userInfo.birthday = '';
      this.cds.userInfo.sex = '';
      this.cds.userInfo.email = '';
      this.cds.userInfo.mobile = '';
      this.cds.userInfo.county = '';
      this.cds.userInfo.province = '';
      this.cds.userInfo.addr = '';
    }
  }

  /** 付款二次確認窗 */
  openPayConfirm(): IConfirm {
    return {
      title: '',
      content: this.cds.gLang.payment.L0004,
      type: 'succese',
      btnConfirm: this.cds.gLang.payment.L0005,
      btnCancel: this.cds.gLang.payment.L0006,
    };
  }
}
