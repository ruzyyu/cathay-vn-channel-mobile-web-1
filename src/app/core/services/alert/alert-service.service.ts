import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AlertServiceService {
  private alertSubject: Subject<any> = new Subject();

  // 初始化
  regiditsv(): Observable<any> {
    return this.alertSubject.asObservable();
  }
  setAlertStream(payload) {
    this.alertSubject.next(payload);
  }
  constructor() {}

  // 開啟alter
  open(param) {
    const payload = {
      ...param,
      action: 'open',
    };
    this.setAlertStream(payload);
  }

  // 關閉alter
  close() {
    this.setAlertStream({ action: 'close' });
  }
}
