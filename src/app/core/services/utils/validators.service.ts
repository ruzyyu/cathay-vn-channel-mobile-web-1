import { Injectable } from '@angular/core';
import { ValidatorFn, AbstractControl } from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
@Injectable({
  providedIn: 'root',
})
export class ValidatorsService {
  /**
   * 長度必須等於
   * @param length 指定長度
   */
  equalLength(length: number) {
    return (control: AbstractControl): { [key: string]: any } => {
      if (control.value) {
        return control.value.length === length ? null : { equalLength: length };
      }
      return null;
    };
  }
  /**
   * 越南身分證號，未傳入生日或生日格式錯誤只驗證長度是否正確
   * 邏輯為
   * 12碼才進行驗證，驗證
   *    1.第四碼代表世紀
   *      0=1900年男性
   *      1=1900年女性
   *      2=2000年男性
   *      3=2000年女性
   *    2.第五六碼代表年分
   *      21=XX21年
   *      99=XX99年
   * @param date 生日 (dd/mm/yyyy) 或 日期物件
   * sex 男 1  女  0
   */
  vnCertNumber(date?: string, sex?: string) {
    return (control: AbstractControl): { [key: string]: any } => {
      // 僅12碼才檢驗以下
      if (control.value && control.value.length === 12) {
        if (date && (sex === '0' || sex === '1')) {
          const yearLast2Code = `${
            new Date(date).getFullYear() % 100
          }`.padStart(2, '0');
          let yearFirst2Code = `${new Date(date).getFullYear()}`.slice(0, 2);
          if (yearFirst2Code === '19' && sex === '1') {
            yearFirst2Code = '0';
          }
          if (yearFirst2Code === '19' && sex === '0') {
            yearFirst2Code = '1';
          }
          if (yearFirst2Code === '20' && sex === '1') {
            yearFirst2Code = '2';
          }
          if (yearFirst2Code === '20' && sex === '0') {
            yearFirst2Code = '3';
          }
          const regex = new RegExp(`^\\d{4}${yearLast2Code}\\d{6}$`);
          const regexFirst = new RegExp(`^\\d{3}${yearFirst2Code}`);
          return regex.exec(control.value) && regexFirst.exec(control.value)
            ? null
            : { vnCertNumber: 'l0001' };
        }
      }
      return null;
    };
  }

  /**
   * 越南身分證號，未傳入生日或生日格式錯誤只驗證長度是否正確
   * 邏輯為
   * 2020年生，身分證必須12碼
   * @param date 生日 (dd/mm/yyyy) 或 日期物件
   */
  vn2020ID(date?: string) {
    return (control: AbstractControl): { [key: string]: any } => {
      return control.value &&
        control.value.length === 9 &&
        new Date(date).getFullYear() >= 2020
        ? { vn2020ID: 'l0001' }
        : null;
    };
  }

  /** 手機號碼 09 xxx */
  vnPhone(control: AbstractControl) {
    if (control.value) {
      // return /^02[48][\.-]?\d{4}[\.-]?\d{4}$|^0(2\d{2}|3[2-9]|5[689]|7[06-9]|8[1-5])[\.-]?\d{4}[\.-]?\d{3}$/.exec(

      return /^0\d{9}$/.exec(control.value) ? null : { vnPhone: true };
    }
    return null;
  }

  /**
   *
   */

  /**
   * @description1.前面：類型≠電動車：
   *  1 4碼英數字；類型=電動車：5碼英數字；前2碼為數字
   *  2.中間："-" (預帶，拆成前後兩格)
   *  3.後面：4碼數字或5碼數字(若為5碼數字，系統後送時自己補第3碼後要有".")
   *  4.無法輸入特殊符號(ex: "." "-"，但是Đ要可以)
   * 73K1-1234
   * 30CD-1234
   * 73K1-123.45
   * 73MĐ1-123.45  (MĐ: 電動車) 另外做，摩托車顯直接置換檢核規則 根據類別
   * @param control
   * @returns
   */

  /** 參考 vn2020ID */
vnRgstNo(motorValue?: string) {
    return (control: AbstractControl): { [key: string]: any } => {
      if (control.value) {
        /** SAG 用 */
        // return /^[A-Z0-9Đ]{4}-\d{4}$|^[A-Z0-9Đ]{4}-\d{3}\.\d{2}$|^[A-Z0-9Đ]{5}-\d{3}\.\d{2}$/.exec(
        /** Mobile 用 */
        if (motorValue === 'C4') {
          return /(^\d{2}[A-Z]{1}Đ[A-Z0-9]{1}-\d{3}.\d{2}$)|(^\d{2}[A-Z]{1}Đ[A-Z0-9]{1}-\d{4}$)/.exec(
            control.value
          )
            ? null
            : { vnRgstNo: true };
        } else {
          return /(^\d{2}[A-Z0-9Đ]{2}-\d{3}.\d{2}$)|(^\d{2}[A-Z0-9Đ]{2}-\d{4}$)/.exec(
            control.value
          )
            ? null
            : { vnRgstNo: true };
        }
      }
      return null;
    };
  }

  /** 越南車牌號 */
  /**
   * @description
   *  1.前面：3碼英數字或4碼英數字，前兩碼為數字
   *    修正成 (前面2數字後面1-2英文)
   *  2.中間："-" (預帶，拆成前後兩格)
   *  3.後面：4碼數字或5碼數字(若為5碼數字，系統後送時自己補第3碼後要有".")
   *  4.無法輸入特殊符號(ex: "." "-")
   * @param control
   * @returns
   */
  vnRgstCarNo(control: AbstractControl) {
    if (control.value) {
      // return ^\d{2}[a-zA-Z]-\d{3}.\d{2}$)|(^\d{2}[a-zA-Z]-\d{4}$)|(^\d{2}-[a-zA-Z]-\d{4}$)|(^\d{2}-[a-zA-Z]-\d{3}.\d{2}$
      return /(^\d{2}[a-zA-Z]{1,2}-\d{3}.\d{2}$)|(^\d{2}[a-zA-Z]{1,2}-\d{4}$)/.exec(
        control.value
      )
        ? null
        : { vnRgstCarNo: true };
    }
    return null;
  }

  /** 日期 */
  date(control: AbstractControl) {
    const date = new Date(control.value);
    return date && date.toISOString && date.toString() !== 'Invalid Date'
      ? null
      : { date: true };
  }

  /**
   * 比指定日期晚
   * @param date 指定日期或物件
   * @param prop 若 date 是物件，指定日期的屬性名稱
   */
  laterThanDate(date: Date | object, prop?: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (control.value && date) {
        const target = new Date(control.value);
        if (prop) {
          if (date[prop]) {
            return target >= date[prop] ? null : { laterThanDate: date[prop] };
          } else {
            return null;
          }
        } else {
          return target >= date ? null : { laterThanDate: date };
        }
      }
      return null;
    };
  }

  /** 2020年後只能用身分證 */
  certTypeForNewPID(birthday: Date): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (control.value && birthday) {
        const year = birthday.getFullYear();
        if (year >= 2020) {
          return control.value === '1' ? null : { certTypeForNewPID: true };
        }
        return null;
      }
      return null;
    };
  }

  /** email 驗證 */
  email(control: AbstractControl): { [key: string]: any } {
    if (control.value) {
      // console.log(control.value);
      // console.log(
      //   /^[a-zA-Z0-9.]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.exec(control.value)
      // );
      // return /^[a-zA-Z0-9.]+@[a-zA-Z0-9-]+\.([a-zA-Z0-9-]{2,4})*$/.exec(
      return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.exec(
        control.value
      )
        ? null
        : { email: true };
    }
    return null;
  }

  /**  檢核倆倆欄位 */
  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  /**  檢核圖檔大小 */
  limitFileSize(limitsize: any) {
    return (control: FormControl) => {
      const file = control.value;
      if (file) {
        const filesize = file.size;
        if (filesize >= 10485760) {
          control.setErrors({ FileSize: true });
        } else {
          control.setErrors(null);
        }
      }
    };
  }

  /**
   * 驗證長度必須是9或12碼的數字
   */
  fontLengthNumber(control: AbstractControl): { [key: string]: any } {
    if (control.value) {
      const numberLength = control.value.length;
      // const numberIf = /[^0-9]/.test(control.value);
      const numberIf = /^[0-9]*$/.test(control.value);

      return (numberLength === 9 || numberLength === 12) && numberIf
        ? null
        : { fontLengthNumber: true };
    }
    return null;
  }
  /** 驗證必須是英文，越文，',.- 空白 */
  languageFormat(control: AbstractControl): { [key: string]: any } {
    if (control.value) {
      const regFont =
        /[^A-Za-zàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ',.-\s]/;
      const checkFont = regFont.test(control.value);
      return !checkFont ? null : { languageFormat: true };
    }
    return null;
  }

  /**
   * 驗證字元大小 越文2 中文2 全形2
   * @param length 限制字元長度
   */
  languagelength(length: number) {
    return (control: AbstractControl): { [key: string]: any } => {
      if (control.value) {
        const vietnameseFont =
          /[^àáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ]/;
        const userNameSplit = control.value.split('');
        // 判斷越文
        const vietnameseFilter = userNameSplit.filter((item: string) => {
          return !vietnameseFont.test(item);
        });
        // 判斷全形與中文字
        const cnglishFilter = userNameSplit.filter((item: string) => {
          return /[\uFF00-\uFFEF\u4E00-\u9FFF]/.test(item);
        });
        if (cnglishFilter || vietnameseFilter) {
          const languageLength =
            cnglishFilter.length +
            vietnameseFilter.length +
            userNameSplit.length;
          return languageLength <= length ? null : { languagelength: true };
        } else {
          return null;
        }
      }
      return null;
    };
  }
  /** 驗證開頭為0 10碼數字 */
  tenNumber(control: AbstractControl): { [key: string]: any } {
    if (control.value) {
      const checkPhone = /[^0-9]/.test(control.value);
      const firstPhone = Number(control.value[0]);
      return !checkPhone && control.value.length === 10 && firstPhone === 0
        ? null
        : { tenNumber: true };
    }
    return null;
  }
  /**
   * @description 檢核變動長度
   */
  //  lengthRange(start: number, end: number) {
  //   return (control: FormControl) => {
  //     return /^\d{${start}}$|^\d{${end}}$/.exec(control.value)
  //     ? null
  //     : { vnCertNumber: 'l0004' };
  //   };

  /** 越南車牌號 */
  vnCheckQty(control: FormControl) {
    return Number(control.value) > 0 ? null : { vnCheckQty: true };
  }

  /**不能含有任何符號 */
  vnPunctReg(control: FormControl) {
    if (control.value) {
      return /^[^.,:;<>\[\]()"!#$%&'*+=?\\^_`{|}~-]+$/.exec(control.value)
        ? null
        : { punctReg: true };
    }
    return null;
  }

  /** 保單號碼區間 12~20 */
  vnLengthInterval(control: FormControl) {
    if (control.value) {
      return control.value.length >= 12 && control.value.length <= 20
        ? null
        : { LengthInterval: true };
    }
    return null;
  }

  /** 密碼長度檢核 8~12 */
  vnLength8to12(control: FormControl) {
    if (control.value) {
      return control.value.length >= 8 && control.value.length <= 12
        ? null
        : { LengthInterval: true };
    }
    return null;
  }

  /** 密碼規則檢核 */
  vnPasswordRules(control: FormControl) {
    if (control.value) {
      return /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,12}$/.test(control.value)
        ? null
        : { passwordRules: true };
    }
    return null;
  }

  /** 班機格式 */
  flightNo(control: AbstractControl) {
    if (control.value) {
      return /^[A-Za-z0-9]{2}\d{1,4}$/.exec(control.value)
        ? null
        : { flightNo: true };
    }
    return null;
  }

  /**
   * 比指定時間晚
   * @param target 指定被比較日期、時間或物件
   * @param date 指定日期、時間或物件
   * @param mode 指定比較方法，例如 >、<、<=、>=、=、!=
   * @param unit 指定比較單位，例如 years、months、weeks、days、hours、minutes 和 seconds
   * @param value 指定比較數值
   * @param prop 若 date 是物件，指定日期的屬性名稱
   */
  compareSpecificTime(
    target: Date | object,
    date: Date | object,
    mode: string,
    unit: any,
    value: number,
    prop?: string
  ): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if ((control.value || control.value === 0) && date) {
        if (prop) {
          if (date[prop]) {
            switch (mode) {
              case '<':
                return moment(target[prop]).diff(date[prop], unit, true) < value
                  ? null
                  : { compareSpecificTime1: date[prop] };
              case '>':
                return moment(target[prop]).diff(date[prop], unit, true) > value
                  ? null
                  : { compareSpecificTime2: date[prop] };
              case '<=':
                return moment(target[prop]).diff(date[prop], unit, true) <=
                  value
                  ? null
                  : { compareSpecificTime3: date[prop] };
              case '>=':
                return moment(target[prop]).diff(date[prop], unit, true) >=
                  value
                  ? null
                  : { compareSpecificTime4: date[prop] };
              case '=':
                return moment(
                  moment(target[prop]).format('YYYY-MM-DD HH:mm')
                ).isSame(moment(date[prop]).format('YYYY-MM-DD HH:mm'))
                  ? null
                  : { compareSpecificTime5: 'not =' };
              case '!=':
                return !moment(
                  moment(target[prop]).format('YYYY-MM-DD HH:mm')
                ).isSame(moment(date[prop]).format('YYYY-MM-DD HH:mm'))
                  ? null
                  : { compareSpecificTime6: 'not !=' };
            }
          } else {
            return null;
          }
        } else {
          switch (mode) {
            case '<':
              return moment(target).diff(date, unit, true) < value
                ? null
                : { compareSpecificTime1: date };
            case '>':
              return moment(target).diff(date, unit, true) > value
                ? null
                : { compareSpecificTime2: date };
            case '<=':
              return moment(target).diff(date, unit, true) <= value
                ? null
                : { compareSpecificTime3: date };
            case '>=':
              return moment(target).diff(date, unit, true) >= value
                ? null
                : { compareSpecificTime4: date };
            case '=':
              return moment(moment(target).format('YYYY-MM-DD HH:mm')).isSame(
                moment(date).format('YYYY-MM-DD HH:mm')
              )
                ? null
                : { compareSpecificTime5: 'not =' };
            case '!=':
              return !moment(moment(target).format('YYYY-MM-DD HH:mm')).isSame(
                moment(date).format('YYYY-MM-DD HH:mm')
              )
                ? null
                : { compareSpecificTime6: 'not !=' };
          }
          // return moment(target).diff(moment(date).toDate(), unit) >= value
          //   ? null
          //   : { compareSpecificTime: date };
        }
      }
      return null;
    };
  }

  /** 年齡限制
   * @param birthday  被保人生日
   * @param age  被限制年齡
   * @param mode  指定比較方法，例如 >、<、<=、>=、=、!=
   */
  ageLimit(birthday: Date, age: number, mode: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (control.value && birthday && age && mode) {
        const diff_ms = Date.now() - birthday.getTime();
        const age_dt = new Date(diff_ms);
        const predictAge = Math.abs(age_dt.getUTCFullYear() - 1970);
        switch (mode) {
          case '<':
            return predictAge < age ? null : { ageLimit1: 'not <' };
          case '>':
            return predictAge > age ? null : { ageLimit2: 'not >' };
          case '<=':
            return predictAge <= age ? null : { ageLimit3: 'not <=' };
          case '>=':
            return predictAge >= age ? null : { ageLimit4: 'not >=' };
          case '=':
            return predictAge === age ? null : { ageLimit5: 'not =' };
          case '!=':
            return predictAge !== age ? null : { ageLimit6: 'not !=' };
        }
        return null;
      }
      return null;
    };
  }

  /** 檢核圖形驗證碼 */
  captchaCheck(captchaResult: number | string) {
    return (control: AbstractControl): { [key: string]: any } =>
      control.value === captchaResult ? null : { captchaCheck: true };
  }

  /**
   * 生日須滿 18歲 (主要用在旅遊險購買)
   * @param date 生日 (dd/mm/yyyy) 或 日期物件
   */
  birthday18year(date?: string) {
    return (control: AbstractControl): { [key: string]: any } => {
      return moment().diff(control.value, 'years') >= 18
        ? null
        : { birthday18year: true };
    };
  }

  /** 檢核保單號碼 */
  policyNumber(policyNumber: any) {
    return /^[A-Z]{2}\d{2}[C][A|B]\d{7}$/.test(policyNumber.value)
      ? null
      : { policyNumber: true };
  }

  /** 再次輸入密碼確認 */
  reenter(value?: string) {
    return (control: AbstractControl): { [key: string]: any } => {
      return control.value === value ? null : { reenter: true };
    };
  }
}
