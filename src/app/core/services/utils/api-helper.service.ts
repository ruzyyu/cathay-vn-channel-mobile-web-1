import { CommonService } from 'src/app/core/services/common.service';
import { IResponse } from './../../interface/index';
import { StorageKeys } from './../../enums/storage-key.enum';
import { StorageService } from './../storage.service';
import { Router } from '@angular/router';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Injectable({
  providedIn: 'root',
})
export class ApiHelperService {
  FKey = 'cathayinslifesso';
  constructor(
    private http: HttpClient,
    public alert: AlertServiceService,
    private router: Router,
    private cds: DatapoolService
  ) {}

  errorCodes = [
    { statusCode: 401, message: 'login timeout' },
    { statusCode: 503, message: 'cant not connection backend service' },
    { statusCode: 504, message: 'service timeout ' },
  ];

  ATTEMPT_COUNT = 1;
  EXCEPT_COUNT = 0;

  showMask() {
    $('body').append(
      "<div class='preloader' id='preloader'><div class=\"lds-default\"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>"
    );
  }

  hideMask() {
    $('#preloader').remove();
  }

  /**
   * @description API專用呼叫
   * @param url 傳遞網址 不用寫入baseurl
   * @param data payload
   * @param setup.mask 遮罩 預設不啟用
   * @param setup.alert 錯誤提示 預設啟用
   */
  async post<T = IResponse>(
    url,
    data = {},
    setup = { mask: false, alert: true }
  ) {
    if (setup.mask) {
      this.showMask();
    }
    const config = this.getRequestHeader();

    return await new Promise<T>(async (resolve, reject) => {
      await this.http
        .post(window['env']['apiUrl'] + url, data, config)
        .pipe(map((rep) => rep))
        .subscribe({
          next: (rep) => {
            resolve(this.successHandler(rep, data, url));
          },
          error: async (error) => {
            this.errorHandler(error, setup.alert);
            resolve(error);
          },
          complete: () => {
            this.hideMask();
          },
        });
    });
  }

  /**
   * @description 國泰人壽交換專用，位置使用環境變數
   * @param url 傳遞網址 不用寫入baseurl
   * @param data payload
   * @param setup.mask 遮罩 預設不啟用
   * @param setup.alert 錯誤提示 預設啟用
   */
  async getsso(url, data: string, setup = { mask: false, alert: true }) {
    if (setup.mask) {
      this.showMask();
    }
    const param = new HttpParams().set('param', data);
    return await new Promise(async (resolve, reject) => {
      await this.http
        .get(this.cds.cathaylifesso + url, { params: param })
        .pipe(map((rep) => rep))
        .subscribe({
          next: (rep) => {
            resolve(this.successHandler(rep, data, url));
          },
          error: (error) => {
            this.errorHandler(error, setup.alert);
            resolve(error);
          },
          complete: () => {
            this.hideMask();
          },
        });
    });
  }

  async get(url, data = {}, setup = { mask: false, alert: true }) {
    if (setup.mask) {
      this.showMask();
    }
    const config = this.getRequestHeader();
    return await new Promise(async (resolve, reject) => {
      await this.http
        .get(window['env']['apiUrl'] + url, config)
        .pipe(map((rep) => rep))
        .subscribe({
          next: (rep) => {
            resolve(this.successHandler(rep, data, url));
          },
          error: (error) => {
            this.errorHandler(error, setup.alert);
            resolve(error);
          },
          complete: () => {
            this.hideMask();
          },
        });
    });
  }

  /**
   * @description 使用 axios post 方法呼叫 api,且 content type 為 multipart/form-data
   * @param {String} url api 路徑
   * @param {Object} formData request 參數(必填)，預設值為空物件
   * @return {Object} Promise 物件
   */
  async postWithFormData(url, formData = {}) {
    this.showMask();
    const config = this.getRequestHeader();
    config['Content-Type'] = 'multipart/form-data';

    return await new Promise(async (resolve, reject) => {
      await this.http
        .post(window['env']['apiUrl'] + url, formData, config)
        .pipe(map((rep) => rep))
        .subscribe({
          next: (rep) => {
            resolve(this.successHandler(rep, formData, url));
          },
          error: (error) => {
            this.errorHandler(error, true);
            resolve(error);
          },
          complete: () => {
            /** 交換token */
            this.hideMask();
          },
        });
    });
  }

  successHandler(response, reqData, url) {
    // if (!response.body) return response;
    console.group(`${url}\n`);
    if (reqData.entries) {
      const paramsObject = {};
      for (const pair of reqData.entries()) {
        paramsObject[pair[0]] = pair[1];
      }
      console.log('request FormData => ', paramsObject);
    } else {
      console.log('request data => ', reqData);
    }
    console.log('response data => ', response.body.data);

    /** 判斷是否交換token */
    this.changeToken(response);

    console.groupEnd();
    return response.body;
  }

  errorHandler(error, showAlert) {
    this.hideMask();
    console.group(`[url  ${error.url}]\n`);
    // const stateStatus = 500;
    let message = '發生異常狀況，請稍後再試。';
    console.log(error);

    if (error.error) {
      const mapStatusError = this.errorCodes.find(
        (item) => item.statusCode === error.status
      );

      if (mapStatusError) {
        error.message = mapStatusError.message;
      }

      /** 國泰要求 */
      if (error.status === 999) {
        error.error.message = error.error.data;
      }

      // 登出->重新登入
      if (error.status === 401) {
        this.reLogin();
        if (!this.cds.gMobileToken) {
          showAlert = false;
        } else {
          this.router.navigate(['/login']);
        }
      }

      if (showAlert) {
        /** member 的是 error.error.message  */
        /** 其他都是 的是 error.message  */
        this.alert.open({
          type: 'error',
          title: this.cds.gLang.ER.ER039,
          content: mapStatusError ? error.message : error.error.message,
        });
      }

      console.log('error status => ', error.status);
    } else {
      if (error.message) {
        message = error.data;
      }

      if (showAlert) {
        this.alert.open({
          type: 'error',
          title: this.cds.gLang.ER.ER039,
          content: message,
        });
      }

      console.log('error message => ', message);
      if (error.message === 'Network Error') {
        message = '網路發生異常，請稍後在試';
      }
    }
    console.groupEnd();
  }

  getRequestHeader() {
    const token =
      this.cds.gMobileToken ||
      this.cds.gNormalToken ||
      'LCJzdWIiOiJjYXRoYXktY3ktd2ViLWd1ZXN0IiwiZXhwIjoxN';
    return {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${token}`)
        .set('LG_CODE', this.cds.gLangType),
      observe: 'response' as 'response', // typescript型別問題
    };
  }

  async _get(url, data = {}) {
    return await new Promise(async (resolve, reject) => {
      $.getJSON(url, function (result) {
        resolve(result);
      });
    });
  }

  async changeToken(response) {
    if (response && response.headers && response.headers.get('custtoken')) {
      // 交換[會員token]

      let mbol = false;
      if (this.cds.gMobileToken) {
        mbol = true;
      }
      if (mbol) {
        StorageService.setItem(
          StorageKeys.mobileToken,
          response.headers.get('custtoken')
        );

        this.cds.gMobileToken = response.headers.get('custtoken');
        return;
      }

      // 沒登入會員 更新[訪客token]
      if (this.cds.gNormalToken) {
        mbol = true;
      }
      if (mbol) {
        await this.reLogin();
      }
    }
  }

  async reLogin() {
    this.cds.gNormalToken = '';
    StorageService.setItem(StorageKeys.memberinfo, '');

    this.cds.gMobileToken = '';
    CommonService.clearMemberinfo();
    const rep: any = await this.post('token');
    if (rep.status === 200) {
      StorageService.setItem(StorageKeys.normarlToken, rep.access_token);
      this.cds.gNormalToken = rep.access_token;
    }
  }
}
