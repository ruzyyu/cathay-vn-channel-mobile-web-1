import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TrustHtmlPipe } from './pipes/trust-html.pipe';
import { OtpCodeDirective } from './directives/otp-code.directive';
import { TenThousandUnitPipe } from './pipes/ten-thousand-unit.pipe';
import { CardNoDirective } from './directives/card-no.directive';
import { RefreshTokenInterceptor } from './interceptors/refresh-token.interceptor';
import { LimitInputDirective } from './directives/limit-input.directive';
import { CarNoformatDirective } from './directives/car-noformat.directive';
import { MotorNoformatDirective } from './directives/motor-noformat.directive';

@NgModule({
  declarations: [
    TrustHtmlPipe,
    OtpCodeDirective,
    TenThousandUnitPipe,
    CardNoDirective,
    LimitInputDirective,
    CarNoformatDirective,
    MotorNoformatDirective,
  ],
  imports: [CommonModule, HttpClientModule],
  exports: [
    HttpClientModule,
    TrustHtmlPipe,
    OtpCodeDirective,
    TenThousandUnitPipe,
    CardNoDirective,
    LimitInputDirective,
    CarNoformatDirective,
    MotorNoformatDirective,
  ],
  providers: [
    // InterceptorProviders
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenInterceptor,
      multi: true,
    },
  ],
})
export class CoreModule {}
