import { HttpResponse } from '@angular/common/http';

/** 群組  */
export const groupBy = (arr: any[], funcProp: (item: any) => any): { [key: string]: any[] } => {
  return arr.reduce((acc, val) => {
    (acc[funcProp(val)] = acc[funcProp(val)] || []).push(val);
    return acc;
  }, {});
};

/** 取得 Blob header 檔案名稱 */
export const execFilename = (resp: HttpResponse<Blob>) => {
  const rawContent = resp.headers.get('content-disposition');
  try {
    return /filename="(.*)"/.exec(rawContent)[0].replace('filename=', '').replace(/;/g, '').replace(/\"/g, '').trim();
  } catch { return ''; }
};
