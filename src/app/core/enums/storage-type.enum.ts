/** storage type enum */
export enum StorageTypes {
  session = 'session',
  local = 'local',
  cookies = 'cookies'
}
