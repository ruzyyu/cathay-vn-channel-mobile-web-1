import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ResponseInterceptor } from './response.interceptor';
import { RequestInterceptor } from './request.interceptor';
import { TxnseqInterceptor } from './txnseq.interceptor';
import { LoadingInterceptor } from './loading.interceptor';

/** Http interceptor providers in outside-in order */
export const InterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: TxnseqInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true },
];
