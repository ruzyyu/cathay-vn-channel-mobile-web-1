import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
/** httpClient request 中繼器 */
export class TxnseqInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newReq = req.clone();
    const seq = [...Array(10).keys()].map(() => (Math.floor(Math.random() * 10) || 0)).join('');
    if (newReq?.body instanceof FormData) {
      (newReq.body as FormData).set('TXNSEQ', seq);
    } else if (newReq?.body?.MWHEADER) { newReq.body.MWHEADER.TXNSEQ = seq; }

    return next.handle(newReq);
  }

}
