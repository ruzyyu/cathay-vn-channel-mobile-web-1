import { StorageKeys } from 'src/app/core/enums/storage-key.enum';
import { StorageService } from 'src/app/core/services/storage.service';
import { DatapoolService } from './../services/datapool/datapool.service';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpClient,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { concatMap, map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
  constructor(private http: HttpClient, private cds: DatapoolService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      map((rep) => rep),
      catchError((err) => {
        if (err.status === 401) {
          return this.refreshToken(request, next);
        } else {
          return throwError(err);
        }
      })
    );
  }

  refreshToken(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return this.http
      .post(
        window['env']['apiUrl'] + 'token',
        {},
        {
          headers: new HttpHeaders()
            .set(
              'Authorization',
              `Bearer LCJzdWIiOiJjYXRoYXktY3ktd2ViLWd1ZXN0IiwiZXhwIjoxN`
            )
            .set('LG_CODE', this.cds.gLangType),
          observe: 'response' as 'response', // typescript型別問題
        }
      )
      .pipe(
        concatMap((rep: any) => {
          if (rep.status === 200) {
            StorageService.setItem(
              StorageKeys.normarlToken,
              rep.body.access_token
            );
            this.cds.gNormalToken = rep.body.access_token;

            const newReq = request.clone({
              headers: new HttpHeaders()
                .set('Authorization', `Bearer ${rep.body.access_token}`)
                .set('LG_CODE', this.cds.gLangType),
            });
            return next.handle(newReq);
          } else {
            return throwError(rep);
          }
        })
      );
  }
}
