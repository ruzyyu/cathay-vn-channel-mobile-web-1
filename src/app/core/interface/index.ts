export interface UserInfo {
  // 會員流水號
  seq_no?: number;
  // 姓名
  customer_name?: string;
  // 身分證字號9
  certificate_number_9?: string;
  // 身分證字號12
  certificate_number_12?: string;
  // 生日
  birthday?: string;
  // 註冊email
  email?: string;
  // 備用email
  email_bk?: string;
  // 性別
  sex?: string;
  // 手機
  mobile?: string;
  // 意見類型 大類
  optiontype?: number;
  // 意見類型 小類
  optiontypeDetail?: string;
  // 意見詳細內容
  option?: string;
  // 郡縣
  county?: string;
  // 省份
  province?: string;
  // 詳細地址
  addr?: string;
  // 身份證ID連結
  is_link_id?: string;
  // FB帳號
  fb_account?: string;
  // FB名稱
  fb_title?: string;
  // Google帳號
  google_account?: string;
  // Google名稱
  google_title?: string;
  // 社群帳號連結
  is_link_sns?: string;
  // 已設定密碼
  is_set_pw?: string | boolean;
  // 密碼
  kor?: string;
  /** 開通線下保單 */
  is_validate_policy?: string;
  /** 成功後回傳 */
  session?: string;
  /** 身分證驗證未通過前值存放 */
  new_id?: string;
  /** 身分證驗證錯誤訊息 */
  remark?: string;
  /** 圖形驗證碼 */
  captchaCode?: string | number;
}

export interface UserInfoTemp {
  // 會員流水號
  seq_no?: number;
  // 姓名
  customer_name?: string;
  // 身分證字號9
  certificate_number_9?: string;
  // 身分證字號12
  certificate_number_12?: string;
  // 生日
  birthday?: string;
  // 註冊email
  email?: string;
  // 備用email
  email_bk?: string;
  // 性別
  sex?: string;
  // 手機
  mobile?: string;
  // 郡縣
  county?: string;
  // 省份
  province?: string;
  // 詳細地址
  addr?: string;
  // 身份證ID連結
  is_link_id?: string;
  // FB帳號
  fb_account?: string;
  // FB名稱
  fb_title?: string;
  // Google帳號
  google_account?: string;
  // Google名稱
  google_title?: string;
  // 社群帳號連結
  is_link_sns?: string;
  // 已設定密碼
  is_set_pw?: string;
  // 密碼
  kor?: string;
  /** 開通線下保單 */
  is_validate_policy?: string;
  /** 成功後回傳 */
  session?: string;
  /** 用以存放帳號 */
  account?: string;
}

export interface OtpCode {
  otpOne?: number;
  otpTwo?: number;
  otpThr?: number;
  otpFou?: number;
  otpFiv?: number;
  otpSix?: number;
}
export interface motorUserInfo {
  /** 商品細類別 */
  prod_Polcode: string;
  /** 車輛種類代號 */
  vehcKd: string;
  /** 車輛保險類型 */
  carpwrkd: string;
  /** 保單生效起日 */
  procDate: string;
  /** 保單結束起日 */
  procDate_end: string;
  // 保險年限日期
  procDateYear?: string;
  /** 座位數: 機車固定 2 */
  maxCapl: 2;

  // 欄位待確認
  /** 車主姓名 */
  vehcOwnr?: string;
  // 車牌號碼
  rgstNo?: string;
  // 車身號碼
  plate?: string;
  // 引擎號碼
  engn_no?: string;
}

export interface ICarUserInfo {
  // 車種
  vehcKd?: string;
  // 類型
  carPwrKd?: string;
  // 保險生效日期
  procDate?: string;
  // 保險結束日期
  procDateEnd?: string;
  // 保險年限日期
  procDateYear?: string;
  // 保障人數
  maxCapl?: number;
  // 車主姓名
  vehcOwnr?: string;
  // 車牌號碼
  rgstNo?: string;
  // 車身號碼
  framNo?: string;
  // 引擎號碼
  engnNo?: string;
}

// 汽機車試算結果
export interface ICalcOrder {
  compprem?: number;
  comppremvat?: number;
  passengerprem?: number;
  passengeramt?: number;
  driverprem?: number;
  driveramt?: number;
  totprem?: number;
  totpremwithvat?: number;
}

export interface insuranceQueryList {
  // 保單號碼
  pol_no?: string;
  // 種類
  option?: string;
  // 車牌號碼
  rgst_no?: string;
  // 車身號碼
  fram_no?: string;
  // 引擎號碼
  engn_no?: string;
}

export interface regUser {}

export interface IPublicCommon {
  airportList?: Array<IAirportList>;
  countryList?: Array<ICountry>;
  countyList?: Array<ICounty>;
  /** 幾人座 */
  carUseList?: Array<IVehicleUse>;
  /** 汽車類型 */
  carList?: Array<IVechicleKd>;
  /** 摩托車類型 */
  motoList?: Array<IVechiclemotoKd>;
  /** ???這才是旅遊地點下拉 */
  nationList?: Array<INation>;
  /** 省分 */
  provinceList?: Array<IProvince>;
  glang?: any;
  /** OTP頁確認身分用 1. 註冊(register) 2. 找回帳密 (find) 3. 第三方綁定 (bind)*/
  gOtpAuto?: string;
  backList?: Array<IBank>;
}

// 機場下拉選單
export interface IAirport {
  airportName?: string;
  iataCode?: string;
  gpsCode?: string;
  municipality?: string;
}

export interface IAirportList {
  group?: string;
  groupname?: string;
  isoCountry?: string;
  data?: Array<IAirport>;
}

// 國籍下拉選單
export interface ICountry {
  countryValue?: string;
  countryName?: string;
}

// 縣下拉選單
export interface ICounty {
  provinceValue?: string;
  provinceZhname?: string;
  provinceVnname?: string;
  countyValue?: string;
  countyZhname?: string;
  countyVnname?: string;
}

// 省下拉選單
export interface IProvince {
  provinceValue?: string;
  provinceZhname?: string;
  provinceVnname?: string;
  countys: Array<ICounty>;
}

// 車輛種類
export interface IVehicleUse {
  vehcKdValue?: string;
  carPwrKdValue?: string;
  carPwrKdName?: string;
  zhName?: string;
  vnName?: string;
  enName?: string;
}

// 汽車種類用途
export interface IVechicleKd {
  seqNo?: number;
  vechicleKdValue?: string;
  vechicleKdName?: string;
  zhName?: string;
  vnName?: string;
  enName?: string;
}

// 車輛種類
export interface IVehicleUse {
  vehcKdValue?: string;
  carPwrKdValue?: string;
  carPwrKdName?: string;
  zhName?: string;
  vnName?: string;
  enName?: string;
}

// 機扯種類
export interface IVechiclemotoKd {
  id?: number;
  vehcKdValue?: string;
  carPwrKdValue?: string;
  carPwrKdName?: string;
  zhName?: string;
  vnName?: string;
  enName?: string;
}

// 國家多國語系對應檔
export interface INation {
  nationNameEng?: string;
  nationCode?: string;
  nationNameShrt?: string;
  nationNameCn?: string;
  nationNameVn?: string;
  nationType?: string;
  active?: boolean;
}

/** Confirm專用 */
export interface IConfirm {
  title: string;
  content: string;
  type: string;
  btnConfirm?: string;
  btnCancel?: string;
}

// TODO:訂單區
/** 試算結果 */
export interface ICalcInsurance {
  order_no?: number;
  order_status?: string;
  aply_no?: string;
  type_pol?: string;
  prod_pol_code?: string;
  prod_pol_code_nm?: string;
  pol_dt?: string;
  pol_due_dt?: string;
  certificate_type?: string;
  certificate_number?: string;
  customer_name?: string;
  birthday?: string;
  addr?: string;
  mobile?: string;
  email?: string;
  national_id?: string;
  vehc_ownr?: string;
  rgst_no?: string;
  engn_no?: string;
  fram_no?: string;
  vehc_kd?: string;
  car_pwr_kd?: string;
  max_capl?: number;
  comp_prem?: number;
  comp_premvat?: number;
  driver_amt?: number;
  driver_cnt?: number;
  driver_prem?: number;
  passenger_amt?: number;
  passenger_cnt?: number;
  passenger_prem?: number;
  tot_prem_with_vat?: number;
  tot_prem?: number;
  print_type?: string;
  pol_dt_kind?: string;
}

// 被保人清單
export interface IInsr_data {
  seq_no: 1;
  flight_no: 'VJ143';
  flight_date: '2020-11-30';
  departure_airport: 'HAN';
  arrival_airport: 'SGN';
}

// 理賠機場資料
export interface IFlight_data {
  seq_no?: number;
  flight_no?: string;
  flight_date?: string;
  departure_airport?: string;
  arrival_airport?: string;
}

export interface ICalcTravelInsurance {
  order_no?: string;
  order_status?: number;
  aply_no?: string;
  type_pol?: string;
  prod_pol_code?: string;
  prod_pol_code_nm?: string;
  pol_dt?: string;
  pol_due_dt?: string;
  certificate_type?: string;
  certificate_number?: string;
  customer_name?: string;
  birthday?: string;
  addr?: string;
  mobile?: string;
  email?: string;
  sex?: string;
  national_id?: string;
  tot_prem_with_vat?: number;
  tot_prem?: number;
  print_type?: string;
  sde_kd?: string;
  tral_loc?: string;
  trip_to?: string;
  add_prem?: boolean;
  bag_loss?: boolean;
  proj_type?: string;
  insr_data?: Array<any>;
  flight_data?: Array<any>;
}

/** 汽車險清單 */
export interface ICarInsurance {
  /** 契約編號 */
  cntr_no: string;
  /** 保單號碼 */
  pol_no: string;
  /** 受理編號 */
  proc_no: string;
  /** 商品類別代號 */
  prod_code: string;
  /** 商品保單類別代號 */
  prod_pol_code: string;
  /** 車牌號碼 */
  rgst_no: string;
  /** 引擎號碼 */
  engn_no: string;
  /** 車身號碼 */
  fram_no: string;
  /** 保單期間_起日時 */
  pol_dt: string;
  /** 保單期間_迄日時 */
  pol_due_dt: string;
  /** 續保資料狀態 */
  renew_pol_cnt: string;
  /** 續保狀態 */
  new_pol_cnt: string;
}

/** 機車險清單 */
export interface IMotoInsurance {
  /** 契約編號 */
  cntr_no: string;
  /** 保單號碼 */
  pol_no: string;
  /** 受理編號 */
  proc_no: string;
  /** 商品類別代號 */
  prod_code: string;
  /** 商品保單類別代號 */
  prod_pol_code: string;
  /** 車牌號碼 */
  rgst_no: string;
  /** 引擎號碼 */
  engn_no: string;
  /** 車身號碼 */
  fram_no: string;
  /** 保單期間_起日時 */
  pol_dt: string;
  /** 保單期間_迄日時 */
  pol_due_dt: string;
  /** 續保資料狀態 */
  renew_pol_cnt: string;
  /** 續保狀態 */
  new_pol_cnt: string;
}

/** 旅遊險清單 */
export interface ITraveInsurance {
  cntr_no: string;
  pol_no: string;
  proc_no: string;
  prod_code: string;
  prod_pol_code: string;
  pol_dt: string;
  pol_due_dt: string;
  is_apc: string;
  endrsmnt_count: string;
  insr_info: string;
}

/** 保險總清單 */
export interface IPolicyList {
  trave: ITraveInsurance;
  moto: IMotoInsurance;
  car: ICarInsurance;
}

export interface IPolicydata {
  /** 進件管道 */
  type_pol?: string;
  /** 商品保單類別代號 */
  prod_pol_code?: string;
  /** 保單號碼 */
  pol_no?: string;
  /** 保單期間_起日時 */
  pol_dt?: string;
  /** 保單期間_迄日時 */
  pol_due_dt?: string;
  /** 實收保費 */
  prem?: number;
}

/** 保障範圍 */
export interface ICoverages {
  /** 保障項目類別 */
  coverageTypeId?: String;
  /** 類別名稱(中文) */
  coverageTypeZhname?: String;
  /** 類別名稱(越文) */
  coverageTypeVnname?: String;
  /** 商品代號 */
  prod_id?: String;
  /** 	承保保險金額 */
  amt?: number;
  /** 體傷保額 */
  injr_amt?: number;
  /** 財損保額 */
  aset_amt?: number;
  /** 保障項目列表 */
  coverageItems?: ICoverageItems;
  /** 附加條款(無資料時回傳null) */
  additional_terms?: Array<string>;
}

/** 	保障項目列表 */
export interface ICoverageItems {
  /** 保障序號 */
  coverageNo?: number;
  /** 保障項目類別 */
  coverageItemId?: string;
  /** 項目名稱(中文) */
  coverageItemZhname?: string;
  /** 項目名稱(越文) */
  coverageItemVnname?: string;
  /** 取得金額欄位，amt，aset_amt，aset_amt */
  getamt_field?: string;
}

export interface IVehicleReginfo {
  /**   車輛種類代號  */
  vehc_kd?: string;
  /** 機動車使用性質*/
  vehc_purp?: string;
  /** 座位數        */
  max_capl?: number;
  /** 載重          */
  max_load?: string;
  /** 車身編號      */
  fram_no?: string;
  /** 引擎編號      */
  engn_no?: string;
}

export interface IPolicyholderData {
  customer_name?: string;
  phone_no?: string;
}

// 商品保障項目對應檔
// export interface itemCorrespond {}

// 商品投保專案種類 ... 暫定寫死

// 保單條款
// export interface xxxxx {}

// 訂單區

/** 	訂單清單第一階層 */
export interface IOrderlist {
  travel_order_list?: Array<IOrdertravel>;
  car_order_list?: Array<IOrdercar>;
  scooter_order_list?: Array<IOrdercar>;
}

/** 	訂單清單 旅遊險 */
export interface IOrdertravel {
  order_no?: number;
  order_status?: string;
  aply_no?: string;
  tral_loc?: string;
  pol_dt?: string;
  pol_due_dt?: string;
  insr_cnt?: number;
  prod_code?: string;
  prod_pol_code?: string;
  prod_pol_code_nm?: string;
  tot_prem?: number;
  tot_prem_withvat?: number;
  checked?: boolean;
}

/** 	訂單清單 汽車險 */
/** 	訂單清單 機車 */
export interface IOrdercar {
  order_no?: number;
  order_status?: string;
  /** 訂單號碼 */
  aply_no?: string;
  /** 車牌號碼 */
  rgst_no?: string;
  /** 車身編號 */
  fram_no?: string;
  /** 保單期間_起日時 */
  pol_dt?: string;
  pol_due_dt?: string;
  insr_cnt?: number;
  prod_code?: string;
  prod_pol_code?: string;
  prod_pol_code_nm?: string;
  /** tot_prem */
  tot_prem?: number;
  tot_prem_withvat?: number;
  checked?: boolean;
}

export interface IResponse {
  data?: any;
  message?: string;
  status?: number;
  success?: boolean;
  total?: number;
  error?: any;
}

export interface IImageFile {
  imgType?: string;
  imgName?: string;
}

/** 最新消息列表 */
export interface INews {
  id: number;
  type_id: number;
  type: string;
  image_url: string[];
  start_date: string;
  end_date: string;
  title: string;
  default_image: string;
}

/** 最新消息 Tag */
export interface INewsTag {
  tag: string | number;
  label: string;
  queryParams: {};
}

/** 關於我們 地圖 */
export interface IAboutMap {
  type: string;
  title: string;
  p1: string;
  p2: string;
  p3: string;
  p4: string;
  iframeSrc: string;
  position: {
    lat: number;
    lng: number;
  };
}
export interface IPayInfo {
  order_no: string;
  aply_no: string;
  prod_pol_code: string;
  amt: number;
  uamt: number;
}

export interface IBank {
  seqno: string;
  name: string;
}

/** 旅遊險 頁面資料 */
export interface ITravelPageData {
  /**
   * 旅遊險選擇項目
   *
   * 國內：0
   * 國外：1
   */
  status: 0 | 1;
}

/** FAQ 項目 */
export interface IFaq {
  /**
   * 分類
   * 1: 個人商品
   * 2: 企業商品
   */
  tag: number;
  /** 險種 */
  type: number;
  /**
   * 險種分類
   * 1: 投保
   * 2: 常見問題
   * 3: 其他
   */
  subType: number;
  sort: number;
  title: string;
  content: string;
}

export interface IFaqTag {
  title: string;
  type: number | null;
}

/** 幫助中心側邊選單項目 */
export interface IHelpDropdownItem {
  id: number;
  name: string;
  tag: string;
  root?: boolean;
  content: {
    type: number;
    name: string;
    title: string;
  }[];
}

/** 幫助中心文件下載項目 */
export interface IHelpDownloadItem {
  itemId: number;
  itemType: number;
  type: string;
  title: string;
  content: {
    option: string;
    document: string;
  }[];
}

/** 首頁 - 聰明投保，無後顧之憂 項目 */
export interface IIndexDialogue {
  time: string;
  content: string;
}

/** 首頁 - 大家都在保 項目 */
export interface IIntroduction {
  bTitle: string;
  description: string;
  link: string;
  content: { title: string; content: string }[];
  defaultImg: string;
  defaultSrcset: string;
  activeImg: string;
  activeSrcset: string;
}

/** 首頁 - 人數統計 項目 */
export interface INumberCount {
  title: string;
  total: string;
}

/** Route 資料 */
export interface IRouteData {
  /** 麵包屑 - 繁中 */
  breadcrumb: string;
  /** 麵包屑 - 越文 */
  vnbreadcrumb: string;
  /** 麵包屑 - 英文 */
  enbreadcrumb: string;
  /** 是否隱藏 title */
  hideTitle?: boolean;
}

/** 麵包屑 */
export interface IBreadcrumb {
  /** 名稱 */
  name: string;
  /** 路徑 */
  path: string;
}

export interface IJob {
  id: string;
  type: string;
  title: string;
  area: string;
  email?: string;
  explain: { value: string; children?: string[] }[];
  content: { value: string; children?: string[] }[];
}
