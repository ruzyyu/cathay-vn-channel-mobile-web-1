import { Pipe, PipeTransform, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'trustHtml'
})
/** 字串轉為 HTML */
export class TrustHtmlPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }

  transform(value) {
    let str = value + '';

    str = decodeURIComponent(str);
    return this.sanitizer.bypassSecurityTrustHtml(str);
  }

}
