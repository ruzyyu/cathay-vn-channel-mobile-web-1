import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({
  name: 'tenThousand'
})
/** 若金額超過 1 百萬，除 1 萬後顯示單位 '萬' */
export class TenThousandUnitPipe implements PipeTransform {
  constructor(private decimal: DecimalPipe) { }

  transform(value: string | number, ...args: unknown[]) {
    let num = +((value + '').replace(/,/g, ''));
    if (isNaN(num)) { return ''; }

    if (num >= 1000000) {
      num = Math.round(num / 10000);
      return this.decimal.transform(num) + ' 萬';
    } else {
      return this.decimal.transform(num) + ' 元';
    }
  }
}
