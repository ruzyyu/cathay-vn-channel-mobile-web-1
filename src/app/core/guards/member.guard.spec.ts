import { TestBed } from '@angular/core/testing';

import { MemberGuard } from './member.guard';

describe('MemberService', () => {
  let service: MemberGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MemberGuard);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
