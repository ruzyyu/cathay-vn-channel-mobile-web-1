import { RenewInsuranceComponent } from './components/renew-insurance/renew-insurance.component';
import { ChooseInsuranceComponent } from './components/choose-insurance/choose-insurance.component';
import { ChangeInsuranceTipComponent } from './components/change-insurance-tip/change-insurance-tip.component';
import { PaymentFailComponent } from './components/payment-fail/payment-fail.component';
import { CDatePipe } from './pipes/c-date.pipe';
import { CapsuleStylePipe } from './pipes/capsule-style.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { GoogleMapsModule } from '@angular/google-maps';
import { CoreModule } from '../core/core.module';
import { HeaderComponent } from './layout/headers/header/header.component';
import { MainLayoutComponent } from './layout/app-layout/main-layout/main-layout.component';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './layout/footers/footer/footer.component';
import { SlickModule } from 'ngx-slick';
import { GotopComponent } from './layout/footers/gotop/gotop.component';
import { NewsMessageComponent } from './layout/headers/news-message/news-message.component';
import { AlertMessageComponent } from './layout/footers/alert-message/alert-message.component';
import { NumberPipe } from './pipes/css-pipe.pipe';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { ExpandPanelComponent } from './components/expand-panel/expand-panel.component';
import { IdentityComponent } from './model/identity/identity.component';
import { AlterComponent } from './model/alert/alert.component';
import { ConfirmComponent } from './model/confirm/confirm/confirm.component';
import { LoginComponent } from './components/login/login.component';
import { SelectMenuComponent } from './components/select-menu/select-menu.component';
import { BreadcrumbComponent } from './layout/breadcrumb/breadcrumb.component';
import { CountdownTimerComponent } from './components/countdown-timer/countdown-timer.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { LangPipe } from './pipes/lang.pipe';
import { ThirdBindComponent } from './model/third-bind/third-bind.component';
import { DaterangepickerComponent } from './components/daterangepicker/daterangepicker.component';
import { PluidNamePipe } from './pipes/pluid-name.pipe';
import { CapsulePipe } from './pipes/capsule.pipe';
import { UserinfoComponent } from './components/userinfo/userinfo.component';
import { ModalComponent } from './components/modal/modal.component';
import { ThousandsPipe } from './pipes/thousands.pipe';
import { FastpairComponent } from './components/fastpair/fastpair.component';
import { BaseClassComponent } from './components/base-class/base-class.component';
import { PlancardComponent } from './components/plancard/plancard.component';
import { NumberSuffixPipe } from './pipes/number-suffix.pipe';
import { PipePluTypePipe } from './pipes/pipe-plu-type.pipe';
import { CaptchaComponent } from './components/captcha/captcha.component';
import { LeavePageComponent } from './model/leave-page/leave-page.component';
import { V1Component } from './components/travel/v1/v1.component';
import { T44Component } from './components/travel/t44/t44.component';
import { T44v1Component } from './components/travel/t44v1/t44v1.component';
import { W1Component } from './components/travel/w1/w1.component';
import { W2Component } from './components/travel/w2/w2.component';
import { W7Component } from './components/travel/w7/w7.component';
import { ContactServiceComponent } from './model/contact-service/contact-service.component';
import { MoneyUnitPipe } from './pipes/money-unit.pipe';
import { HtmlTransformPipe } from './pipes/html-transform.pipe';

@NgModule({
  declarations: [
    HeaderComponent,
    MainLayoutComponent,
    FooterComponent,
    GotopComponent,
    NewsMessageComponent,
    AlertMessageComponent,
    NumberPipe,
    ThousandsPipe,
    ExpandPanelComponent,
    IdentityComponent,
    AlterComponent,
    DatepickerComponent,
    SelectMenuComponent,
    ConfirmComponent,
    LoginComponent,
    BreadcrumbComponent,
    CountdownTimerComponent,
    DatepickerComponent,
    LangPipe,
    ThirdBindComponent,
    DaterangepickerComponent,
    PluidNamePipe,
    CapsulePipe,
    CapsuleStylePipe,
    CDatePipe,
    UserinfoComponent,
    ModalComponent,
    FastpairComponent,
    BaseClassComponent,
    PaymentFailComponent,
    PlancardComponent,
    ChangeInsuranceTipComponent,
    ChooseInsuranceComponent,
    RenewInsuranceComponent,
    NumberSuffixPipe,
    PipePluTypePipe,
    CaptchaComponent,
    LeavePageComponent,
    V1Component,
    T44Component,
    T44v1Component,
    W1Component,
    W2Component,
    W7Component,
    ContactServiceComponent,
    MoneyUnitPipe,
    HtmlTransformPipe,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    SlickModule.forRoot(),
    HttpClientModule,
    HttpClientJsonpModule,
    GoogleMapsModule,
  ],
  exports: [
    MainLayoutComponent,
    FormsModule,
    ReactiveFormsModule,
    SlickModule,
    NumberPipe,
    ThousandsPipe,
    ExpandPanelComponent,
    IdentityComponent,
    AlterComponent,
    DatepickerComponent,
    SelectMenuComponent,
    ConfirmComponent,
    LoginComponent,
    BreadcrumbComponent,
    CountdownTimerComponent,
    LangPipe,
    PluidNamePipe,
    CapsulePipe,
    CapsuleStylePipe,
    CDatePipe,
    NumberSuffixPipe,
    PipePluTypePipe,
    ThirdBindComponent,
    DaterangepickerComponent,
    UserinfoComponent,
    FastpairComponent,
    BaseClassComponent,
    GoogleMapsModule,
    PaymentFailComponent,
    PlancardComponent,
    ChangeInsuranceTipComponent,
    ChooseInsuranceComponent,
    RenewInsuranceComponent,
    CaptchaComponent,
    CoreModule,
    LeavePageComponent,
    V1Component,
    T44Component,
    T44v1Component,
    W1Component,
    W2Component,
    W7Component,
    ContactServiceComponent,
    MoneyUnitPipe,
    HtmlTransformPipe,
  ],
  providers: [
    NumberPipe,
    ThousandsPipe,
    LangPipe,
    CapsulePipe,
    CapsuleStylePipe,
    CDatePipe,
  ],
  entryComponents: [
    LoginComponent,
    UserinfoComponent,
    ModalComponent,
    IdentityComponent,
    FastpairComponent,
    ChangeInsuranceTipComponent,
    ChooseInsuranceComponent,
    RenewInsuranceComponent,
    V1Component,
    T44Component,
    T44v1Component,
    W1Component,
    W2Component,
    W7Component,
  ],
})
export class SharedModule {}
