import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'moneyUnit'
})
export class MoneyUnitPipe implements PipeTransform {
  transform(value: number | string, lang: string): unknown {
    if (lang === 'vi-VN') { return `${value} đ`; }
    if (lang === 'en-US') { return `${value} đ`; }
    if (lang === 'zh-TW') { return `${value} đ`; }
    return value;
  }
}
