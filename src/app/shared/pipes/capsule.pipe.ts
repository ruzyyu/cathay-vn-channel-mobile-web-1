import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'capsule',
})
export class CapsulePipe implements PipeTransform {
  transform(value: string, ...param: string[]): string {
    const change = {
      'zh-TW': {
        takeEffect: '生效中',
        notYetEffect: '尚未生效',
        invalid: '已失效',
      },
      'en-US': {
        takeEffect: 'Effective',
        notYetEffect: 'Not Yet Effective',
        invalid: 'Expired',
      },
      'vi-VN': {
        takeEffect: 'Còn hạn',
        notYetEffect: 'Hiện chưa có hiệu lực',
        invalid: 'Hết hạn',
      },
    };
    const item: any = change[value || 'zh-TW'];

    const d1 = moment(param[0]); // 起始時間
    const d2 = moment(param[1]);
    const now = moment();

    if (now >= d1 && now <= d2) {
      return item.takeEffect;
    }
    if (now < d1) {
      return item.notYetEffect;
    }
    if (now > d2) {
      return item.invalid;
    }
    return '';
  }
}
