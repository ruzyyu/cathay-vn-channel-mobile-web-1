import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberSuffix',
})
export class NumberSuffixPipe implements PipeTransform {
  transform(number: number, args?: any): any {
    if (isNaN(number)) {
      return null;
    } // will only work value is a number
    if (number === null) {
      return null;
    }
    if (number === 0) {
      return null;
    }
    let abs = Math.abs(number);
    const rounder = Math.pow(10, 1);
    const isNegative = number < 0; // will also work for Negetive numbers
    let key = '';

    const powers = [
      { key: 'Q', value: Math.pow(10, 15) },
      { key: 'T', value: Math.pow(10, 12) },
      { key: 'Billion', value: Math.pow(10, 9) },
      { key: 'Million', value: Math.pow(10, 6) },
      { key: '000', value: 1000 },
    ];
    const powers2 = [
      { key: 'Quadrillion', value: Math.pow(10, 15) },
      { key: 'Trillion', value: Math.pow(10, 12) },
      { key: 'Billion', value: Math.pow(10, 9) },
      { key: 'Million', value: Math.pow(10, 6) },
      { key: '000', value: 1000 },
    ];
    const powers3 = [
      { key: 'triệu tỷ', value: Math.pow(10, 15) },
      { key: 'nghìn tỷ', value: Math.pow(10, 12) },
      { key: 'tỷ', value: Math.pow(10, 9) },
      { key: 'triệu', value: Math.pow(10, 6) },
      { key: 'nghìn', value: 1000 },
    ];
    const powers4 = [
      // { key: '千兆', value: Math.pow(10, 15) },
      { key: '兆', value: Math.pow(10, 12) },
      { key: '億', value: Math.pow(10, 8) },
      // { key: '千萬', value: Math.pow(10, 7) },
      // { key: '百萬', value: Math.pow(10, 6) },
      { key: '萬', value: Math.pow(10, 4) },
      { key: '千', value: 1000 },
    ];

    switch (args) {
      case 'en-US':
        for (let i = 0; i < powers.length; i++) {
          let reduced = abs / powers[i].value;
          reduced = (reduced * rounder) / rounder;
          if (reduced >= 1) {
            abs = reduced;
            key = powers[i].key;
            if (key === '000') {
              return (isNegative ? '-' : '') + abs + ',' + key;
            } else {
              return (isNegative ? '-' : '') + abs + ' ' + key;
            }
          }
        }
        break;
      case 'vi-VN':
        for (let i = 0; i < powers3.length; i++) {
          let reduced = abs / powers3[i].value;
          reduced = (reduced * rounder) / rounder;

          if (reduced >= 1) {
            // 越文 → 要把點改成逗號
            const numberTranslate = reduced.toString().replace('.', ',');
            // abs = reduced;
            key = powers3[i].key;
            return (isNegative ? '-' : '') + numberTranslate + ' ' + key;
          }
        }
        break;
      case 'zh-TW':
        for (let i = 0; i < powers4.length; i++) {
          let reduced = abs / powers4[i].value;
          reduced = (reduced * rounder) / rounder;
          if (reduced >= 1) {
            abs = reduced;
            key = powers4[i].key;
            return (isNegative ? '-' : '') + abs + ' ' + key;
          }
        }
        break;
      default:
        for (let i = 0; i < powers2.length; i++) {
          let reduced = abs / powers2[i].value;
          reduced = (reduced * rounder) / rounder;
          if (reduced >= 1) {
            abs = reduced;
            key = powers2[i].key;
            return (isNegative ? '-' : '') + abs + ' ' + key;
          }
        }
        break;
    }
  }
}
