import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'cDate',
})
export class CDatePipe implements PipeTransform {
  transform(value: any, format?: any, firstFormat?: any): any {
    return firstFormat
      ? moment(value, firstFormat).format(format)
      : moment(value).format(format);
  }
}
