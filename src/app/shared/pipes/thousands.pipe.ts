import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'thousands',
})
export class ThousandsPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    const locale = (args && args.locale) || 'vi-VN';
    return +value ? (+value).toLocaleString(locale) : value;
  }
}
