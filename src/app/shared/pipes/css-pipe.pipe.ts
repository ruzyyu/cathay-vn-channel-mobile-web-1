import { Pipe, PipeTransform } from '@angular/core';
@Pipe({ name: 'numberPipe' })
export class NumberPipe implements PipeTransform {
  cssNumber: string[] = [
    'one',
    'two',
    'three',
    'four',
    'fives',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
    'twelve'
  ];

  transform(value: number) {
    return value > 0 ? this.cssNumber[value - 1] : 0;
  }
}
