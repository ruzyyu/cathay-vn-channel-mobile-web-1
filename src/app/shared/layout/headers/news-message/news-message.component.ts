import { PublicService } from 'src/app/core/services/api/public/public.service';
import { Router } from '@angular/router';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-news-message',
  templateUrl: './news-message.component.html',
  styleUrls: ['./news-message.component.scss'],
})
export class NewsMessageComponent implements OnInit, AfterViewInit {
  @Input() FMessage: string;
  @Output() FMarqueeCloseStatus = new EventEmitter<boolean>();

  FLang: any;
  constructor(private go: Router, private cds: DatapoolService) {
    this.FLang = this.cds.gLang.marqueeMessage;
  }

  FDate: any = {};
  ngOnInit() {}
  async ngAfterViewInit() {
    console.log('message', this.FMessage);
    // 不要阻塞
    // this.FMessage = this.FDate.title;
    // 消息通知
    $('.news-message').toast();
  }

  goNews() {
    this.go.navigate(['/news/news-detail'], {
      queryParams: {
        type: this.FDate.type_id,
        option: this.FDate.id,
      },
    });
  }

  closeMarquee() {
    this.FMarqueeCloseStatus.emit();
  }
}
