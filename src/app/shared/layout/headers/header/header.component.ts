import { PolicyService } from './../../../../core/services/api/policy/policy.service';
import { UserInfo } from './../../../../core/interface/index';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { filter } from 'rxjs/operators';
import { Router, NavigationEnd, NavigationExtras } from '@angular/router';
import { LocalService } from './../../../../core/services/api/local/local.service';
import { DatapoolService } from './../../../../core/services/datapool/datapool.service';
import { StorageKeys } from './../../../../core/enums/storage-key.enum';
import { StorageService } from './../../../../core/services/storage.service';
import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Tags } from 'src/app/bff/models';
// import { SocialService } from 'src/app/core/services/social.service';
import { Lang } from 'src/types';
import { CommonService } from 'src/app/core/services/common.service';
import { fromEvent } from 'rxjs';
import moment from 'moment';
import { isScullyGenerated, isScullyRunning } from '@scullyio/ng-lib';
import { NewsMessageComponent } from '../news-message/news-message.component';
import { PublicService } from 'src/app/core/services/api/public/public.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(
    private router: Router,
    private cds: DatapoolService,
    private localapi: LocalService,
    private alter: AlertServiceService,
    private apiPolicy: PolicyService,
    private api: PublicService
  ) {
    this.isComponentInit = false;
    // 跑馬燈有無抓取

    /** 跑馬燈頁面刷新 router 監聽 */
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.adjustNewMessage(!!this.FMarqueeMsg);
      }
    });

    // 設定網址
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        this.type = event.url;
      });

    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }
  /** Language Instantly */
  languageStatus: string;
  FLang: Lang;
  FLangType: any;
  header: Observable<any>;

  type = '';

  FUserInfo: UserInfo;
  FPLUItem: Array<any>;
  FBussinessPlu: Array<any>;
  FAbout: Array<any>;
  FOrder: Array<any>;
  FClaim: Array<any>;
  FMember: Array<any>;

  tagId = 'all';
  tagList: Tags[];

  /** 用於判定 跑馬燈 是否顯示 資料 */
  FMarqueeShow = true;

  /** ----搜尋功能---- */
  FSearch = '';

  /** Cookies 是否被讀取 */
  isComponentInit: boolean;

  /** 偷懶一下 */
  @ViewChild('lymarquee', { read: NewsMessageComponent })
  lymarquee: NewsMessageComponent;

  @HostListener('window:scroll', ['$event'])
  onScroll($event) {
    this.hideHeaderTop($(window).scrollTop());
  }

  ngOnInit() {
    this.closeChildMenu();

    this.isComponentInit = true;
    this.closeChildMenu();

    this.checklogin();

    this.languageStatus = StorageService.getItem(StorageKeys.lang) || 'zh-TW';

    // 個人商品
    this.FPLUItem = [
      {
        itemName: this.FLang.A04.L0001_.s001,
        url: '/online-insurance/car-insurance',
      },
      {
        itemName: this.FLang.A04.L0001_.s002,
        url: '/online-insurance/motor-insurance',
      },
      {
        itemName: this.FLang.A04.L0001_.s003,
        url: '/online-insurance/travel-insurance',
        queryParams: 'in',
      },
      {
        itemName: this.FLang.A04.L0001_.s004,
        url: '/online-insurance/injury-insurance',
      },
      {
        itemName: this.FLang.A04.L0001_.s005,
        url: '/online-insurance/house-insurance',
      },
    ];

    // 企業商品
    this.FBussinessPlu = [
      {
        itemName: this.FLang.A04.L0002_.s001,
        url: '/online-insurance/engineering-insurance',
      },
      {
        itemName: this.FLang.A04.L0002_.s002,
        url: '/online-insurance/liability-insurance',
      },
      {
        itemName: this.FLang.A04.L0002_.s003,
        url: '/online-insurance/property-insurance',
      },
      {
        itemName: this.FLang.A04.L0002_.s004,
        url: '/online-insurance/cargo-insurance',
      },
    ];

    // 關於國泰
    this.FAbout = [
      {
        itemName: this.FLang.A04.L0004_.s001,
        url: '/about',
      },
      {
        itemName: this.FLang.A04.L0018,
        url: '/news',
        queryParams: 'all',
      },
      {
        itemName: this.FLang.A04.L0019,
        url: '/about/opening',
      },
    ];

    // 保單專區
    this.FOrder = [
      {
        itemName: this.FLang.A04.L0003_.s001,
        url: '/member/my-order',
      },
      {
        itemName: this.FLang.A04.L0003_.s002,
        url: '/member/my-policy',
      },
      {
        itemName: this.FLang.A04.L0003_.s003,
        url: '/member/my-policy/insurance-changing',
      },
      {
        itemName: this.FLang.A04.L0003_.s004,
        url: '/about',
      },
      {
        itemName: this.FLang.A04.L0003_.s005,
        url: '/member/insurance-query',
      },
    ];

    // 理賠區
    this.FClaim = [
      {
        itemName: this.FLang.A04.L0014,
        url: 'member/claim',
      },
      {
        itemName: this.FLang.A04.L0015,
        url: 'member/claim/search',
      },
    ];

    // 會員區
    this.FMember = [
      {
        itemName: this.FLang.A04.L0016_.s001,
        url: 'member/maintain',
      },
      {
        itemName: this.FLang.A04.L0016_.s002,
        url: 'login/change-password',
      },
      {
        itemName: this.FLang.A04.L0016_.s003,
        url: 'member/verify',
      },
    ];

    // 修正手機版 nav 多層下拉 增加 slideDown
    $('.dropdown-submenu > a').on('click', function (e) {
      const submenu = $(this);
      const dropdownMenu = submenu.next('.dropdown-menu');
      const isShow = dropdownMenu.hasClass('show');
      $('.dropdown-submenu .dropdown-menu').removeClass('show');
      if (isShow) {
        dropdownMenu
          .first()
          .stop(true, true)
          .slideUp(function () {
            dropdownMenu.removeClass('show');
            submenu.parent().removeClass('show');
          });
      } else {
        submenu.parent().addClass('show');
        dropdownMenu
          .first()
          .stop(true, true)
          .slideDown(function () {
            dropdownMenu.addClass('show');
          });
      }
      const siblings = submenu.parent().siblings();

      $.each(siblings, function (key, item) {
        if ($(item).hasClass('show')) {
          $(item).removeClass('show');
          $(item).children('.dropdown-menu').slideUp();
        }
      });
      e.stopPropagation();
    });

    $('.dropdown').on('hidden.bs.dropdown', function (e) {
      // hide any open menus when parent closes
      $('.dropdown-menu.show').removeClass('show');
      $('.dropdown-submenu.show').removeClass('show');
    });

    // 手機版 nav item slideDown
    $('.nav-item.dropdown').on('show.bs.dropdown', function () {
      const items = $(this).parent().children();
      $(items).each(function () {
        if ($(this).hasClass('show')) {
          $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
          $(this).removeClass('show');
        }
      });
      $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    $('.nav-item.dropdown').on('hide.bs.dropdown', function (e) {
      $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });

    // 如果要切換中文版本，請按 ctrl + alt + b
    fromEvent(window, 'keydown').subscribe((event: any) => {
      if (event.ctrlKey && event.keyCode === 66 && event.altKey) {
        this.changeLanguage('zh-TW');
      }
    });

    // TODO: 手機長按十秒可切換中文版本，如有需要再開啟
    // let firstDate = 0;
    // let interval;
    // fromEvent(document, 'touchstart').subscribe((event: any) => {
    //   interval = setInterval(() => {
    //     firstDate = firstDate + 1;
    //   }, 1000);
    // });

    // fromEvent(document, 'touchend').subscribe((event: any) => {
    //   if (firstDate > 10){
    //     this.changeLanguage('zh-TW');
    //   }
    //   clearInterval(interval);
    //   firstDate = 0;
    // });
  }

  FMarqueeMsg: string;
  async ngAfterViewInit() {
    /** 跑馬燈資訊，如沒有跑馬燈資訊須關閉跑馬燈條 */
    const mDate = await this.api.getMarquee();
    const mstr = mDate.title;
    const FShowMq = mstr;
    this.FMarqueeMsg = mstr;

    if (isScullyGenerated()) {
      this.FMarqueeShow =
        !FShowMq ||
        StorageService.getItem(StorageKeys.cookiesAlertheader) ===
          'cookiesAlertheader';
    }

    // 調整跑馬燈高度
    this.adjustNewMessage(mstr);
  }

  /** Change Language */
  async changeLanguage(lang) {
    console.log('變更語言');
    this.languageStatus = lang;
    let mlang: any;
    // 越文：vi-VN，英文：en-US，中文：zh-TW
    if (lang === 'VN') {
      mlang = await this.localapi.JSON('assets/json/vn.json');
    }
    if (lang === 'EN') {
      mlang = await this.localapi.JSON('assets/json/en.json');
    }
    if (lang === 'zh-TW') {
      mlang = await this.localapi.JSON('assets/json/zh-tw.json');
    }
    this.cds.gLang = mlang || this.cds.gLang;
    StorageService.setItem(StorageKeys.lang, lang);

    // 重導F5
    window.location.reload();
  }

  goPluUrl(index) {
    $('#navbarSupportedContent').collapse('hide');
    this.router.navigate([this.FPLUItem[index].url], {
      queryParams: { xtype: this.FPLUItem[index]?.queryParams },
    });
    this.doCloseMenu();
  }

  goBPluUrl(index) {
    $('#navbarSupportedContent').collapse('hide');
    this.router.navigate([this.FBussinessPlu[index].url]);
  }

  goAboutUrl(index) {
    $('#navbarSupportedContent').collapse('hide');
    if (this.FAbout[index].queryParams) {
      this.router.navigate([this.FAbout[index].url], {
        queryParams: { type: this.FAbout[index].queryParams },
      });
    } else {
      this.router.navigate([this.FAbout[index].url]);
    }
  }

  goOrderUrl(index) {
    $('#navbarSupportedContent').collapse('hide');
    /** 點擊我要續保時 */
    switch (index) {
      case 2:
        this.onPolicyChange();
        break;
      case 3:
        this.onRenewal();
        break;

      default:
        this.router.navigate([this.FOrder[index].url]);
        break;
    }
  }

  goClaimUrl(index) {
    $('#navbarSupportedContent').collapse('hide');
    this.router.navigate([this.FClaim[index].url]);
  }

  goMemberUrl(index) {
    $('#navbarSupportedContent').collapse('hide');

    if (
      this.cds.userInfo.is_validate_policy === 'Y' &&
      this.FMember[index].url === 'member/verify'
    ) {
      this.alter.open({
        type: 'info',
        title: '',
        content: this.FLang.popup.P0001,
      });
      return;
    }
    this.router.navigate([this.FMember[index].url]);
  }

  routerLink(event, url) {
    if (
      this.cds.userInfo.is_validate_policy === 'Y' &&
      url === '/member/verify'
    ) {
      this.alter.open({
        type: 'info',
        title: '',
        content: this.FLang.popup.P0001,
      });
      return;
    }
    this.hideMenu(event);
    this.router.navigate([url]);
  }

  doSearch(keyWord = null) {
    const options: NavigationExtras = {
      queryParams: {
        search:
          (keyWord && keyWord.trim()) || (this.FSearch && this.FSearch.trim()),
      },
    };

    this.router.navigate(['/search'], options);
    this.doCloseMenu();

    this.FSearch = '';
  }
  /** 鍵盤按下enter時執行查詢 */
  doSearchEnter(e) {
    if (e.keyCode === 13 && e.code === 'Enter') {
      this.doSearch();
    }
  }
  doCloseMenu() {
    /** 關閉手機版menu */
    $('.menu-mob').removeClass('show');
    /** 關閉電腦版menu */
    $('.search-icon').addClass('collapsed');
    $('.search-area').removeClass('show');
  }
  /** ----搜尋功能End---- */
  goHelpUrl() {
    $('#navbarSupportedContent').collapse('hide');
    this.router.navigate(['/help'], { queryParams: { type: 'question' } });
  }

  /** 登入登出按鈕顯示 */
  checklogin(value: boolean = false) {
    if (this.cds.userInfo.seq_no) {
      value = true;
    }
    return value;
  }

  /** 登出 */
  async doLogout() {
    // this.auth.signOut();
    StorageService.removeItem(StorageKeys.memberinfo);
    StorageService.removeItem(StorageKeys.mobileToken);
    StorageService.removeItem(StorageKeys.c_user);
    this.checklogin();
    document.location.href = this.router.url;
  }

  /** OnlineInsurance */
  goOnlineInsurance() {
    this.router.navigate(['/online-insurance']);
  }

  /** 預設展開 */
  defaultExpand() {
    setTimeout(() => {
      $('#navbarDropdownMenuLink1').dropdown('show');
    }, 100);
  }

  /** 滑鼠移入開啟 Menu */
  hoverOpenMenu(id) {
    // $(`#${id}`).css('display', 'inline-block');
    $(`#${id}`).addClass('menu_animation');
  }

  /** 滑鼠移出關閉 Menu */
  hoverHideMenu(id) {
    // $(`#${id}`).css('display', 'none');
    $(`#${id}`).removeClass('menu_animation');
  }

  /** 開啟 Menu */
  openMenu(id) {
    // $(`#${id}`).css('display', 'inline-block');
    $(`#${id}`).addClass('menu_animation');
  }

  /** 關閉 Menu */
  hideMenu(event = undefined) {
    // 個人商品
    $('#hideMenu1').removeClass('menu_animation');
    // 商業商品
    $('#hideMenu2').removeClass('menu_animation');
    // 保戶服務
    $('#hideMenu3').removeClass('menu_animation');
    // 關於國泰
    $('#hideMenu4').removeClass('menu_animation');
    if (!event) {
      return;
    }
    event.stopPropagation();
  }

  goMember() {
    this.router.navigate(['/member']);
  }

  travelUrl() {
    this.router.navigate(['/online-insurance/travel-insurance'], {
      queryParams: { xtype: 'in' },
    });
  }

  /** 選單點擊時觸發關閉子層 */
  closeChildMenu() {
    $('.navbar-toggler').on('click', (e) => {
      $('.dropdown-submenu .dropdown-menu').hide();
    });
  }

  // 國內外旅遊 tag
  tagUrl(index) {
    switch (index) {
      case 1:
        this.router.navigate(['/online-insurance/travel-insurance'], {
          queryParams: { xtype: 'in' },
          fragment: 'bookmark-anchor',
        });
        break;
      case 2:
        this.router.navigate(['/online-insurance/travel-insurance'], {
          queryParams: { xtype: 'out' },
          fragment: 'bookmark-anchor',
        });
        break;
      default:
        this.router.navigate(['/online-insurance/travel-insurance'], {
          queryParams: { xtype: 'in' },
          fragment: 'bookmark-anchor',
        });
        break;
    }
  }

  /** header 問題 URL */
  // goHeaderUrl(index) {
  //   $('#collapseExample').collapse('toggle');
  //   switch (index) {
  //     case 0:
  //       this.router.navigate(['/online-insurance/travel-insurance'], {
  //         queryParams: { xtype: 'in' },
  //       });
  //       break;
  //     case 1:
  //       this.router.navigate(['/online-insurance/car-insurance']);
  //       // window.open('/online-insurance/car-insurance');
  //       // window.open('/online-insurance/motor-insurance');
  //       break;
  //     case 2:
  //       this.router.navigate(['/online-insurance']);
  //       break;
  //     case 3:
  //       this.router.navigate(['/member/my-policy']);
  //       break;
  //     case 4:
  //       this.router.navigate(['/member/claim']);
  //       break;
  //   }
  // }

  async onPolicyChange(event = undefined) {
    this.hideMenu(event);

    if (!this.cds.userInfo.seq_no) {
      this.cds.gPre_url = '/member';
      this.cds.gWhocall = 'policyChange';
      this.router.navigate(['/login']);
      return;
    }

    const payload = {
      language: this.FLangType,
      token: this.cds.gToken,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      certificate_type: '1',
      certificate_number_9: this.cds.userInfo.certificate_number_9,
      certificate_number_12: this.cds.userInfo.certificate_number_12,
      customer_name: this.cds.userInfo.customer_name.trim(),
      birthday: this.cds.userInfo.birthday,
      is_validate_policy: this.cds.userInfo.is_validate_policy,
      member_seq_no: this.cds.userInfo.seq_no,
    };

    const rep = await this.apiPolicy.GetEndRsmntCard(payload);

    if (rep.status === 200) {
      this.cds.gEndRsmntCard = rep.data;
    }
    // 呼叫API
    if (this.cds.gEndRsmntCard && this.cds.gEndRsmntCard.length > 0) {
      // 符合條件
      $('#Modal-18').modal('show');
    } else {
      // 無符合條件
      this.alter.open({
        type: 'file ',
        title: this.FLang.F001.L0018,
        content: this.FLang.F001.L0019,
      });
    }
  }

  onInsuranceChangingTip(e) {
    this.cds.xinsuranceChangingData = this.cds.gEndRsmntCard[e];
    $('#Modal-18').modal('hide');
    setTimeout(() => {
      $('#Modal-21').modal('show');
    }, 200);
  }

  async onRenewal(event = undefined) {
    this.hideMenu(event);

    if (!this.cds.userInfo.seq_no) {
      this.cds.gPre_url = '/member';
      this.cds.gWhocall = 'keep';
      this.router.navigate(['/login']);
      return;
    }

    const payload = {
      language: this.FLangType,
      token: this.cds.gToken,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      certificate_type: '1',
      certificate_number_9: this.cds.userInfo.certificate_number_9,
      certificate_number_12: this.cds.userInfo.certificate_number_12,
      customer_name: this.cds.userInfo.customer_name.trim(),
      birthday: this.cds.userInfo.birthday,
      is_validate_policy: this.cds.userInfo.is_validate_policy,
      member_seq_no: this.cds.userInfo.seq_no,
    };

    const rep = await this.apiPolicy.GetDueSoonCard(payload);

    if (rep.status === 200) {
      this.cds.gDueSoonCard = rep.data;
    }
    // 呼叫API
    if (this.cds.gDueSoonCard && this.cds.gDueSoonCard.length > 0) {
      // 符合條件
      $('#Modal-17').modal('show');
    } else {
      // 無符合條件
      this.alter.open({
        type: 'file',
        title: this.FLang.F001.L0018,
        content: this.FLang.F001.L0025,
      });
    }
  }

  /** 強制險查詢 */
  goSearh(event) {
    this.hideMenu(event);
    this.router.navigate(['member/insurance-query']);
  }

  goInsuranceChanging() {
    $('#Modal-21').modal('hide');
    setTimeout(() => {
      this.router.navigate(['member/my-policy/insurance-changing']);
    }, 200);
  }

  async goInsuranceRenew(optionCntrNo: string) {
    $('#Modal-17').modal('hide');
    const GetDueSoonDetail = await this.apiPolicy.GetDueSoonDetail({
      language: this.FLangType,
      token: this.cds.gToken,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      certificate_type: '1',
      certificate_number_9: this.cds.userInfo.certificate_number_9,
      certificate_number_12: this.cds.userInfo.certificate_number_12,
      customer_name: this.cds.userInfo.customer_name.trim(),
      birthday: this.cds.userInfo.birthday,
      is_validate_policy: this.cds.userInfo.is_validate_policy,
      member_seq_no: this.cds.userInfo.seq_no,
      cntr_no: optionCntrNo,
    });
    if (GetDueSoonDetail.status !== 200) {
    } else {
      GetDueSoonDetail.data.pol_dt = CommonService.addOneYear(
        GetDueSoonDetail.data.pol_dt,
        'yyyy-MM-DD HH:mm:ss'
      );
      GetDueSoonDetail.data.pol_due_dt = CommonService.addOneYear(
        GetDueSoonDetail.data.pol_due_dt,
        'yyyy-MM-DD HH:mm:ss'
      );

      // 僅續保會是true ..
      this.cds.gTrafficType = true;
      this.cds.gTrafficicinsurance = GetDueSoonDetail.data;
      setTimeout(() => {
        this.cds.gMotorPluNoList.includes(GetDueSoonDetail.data.prod_pol_code)
          ? this.router.navigate([
              'online-insurance/project-trial-motor/calcAmt',
            ])
          : this.router.navigate([
              'online-insurance/project-trial-car/calcAmt',
            ]);
      });
    }
  }

  /** 調整跑馬燈高度 */
  adjustNewMessage(show: boolean) {
    if (
      show &&
      StorageService.getItem(StorageKeys.cookiesAlertheader) !==
        'cookiesAlertheader'
    ) {
      $('.menu-mob, .com-content').addClass('alert-show-trans');
    } else {
      $('.menu-mob, .com-content').removeClass('alert-show-trans');
    }
  }

  /** 當跑馬燈關閉之後 */
  whenMarqueeClose() {
    this.FMarqueeShow = false;
    StorageService.setItem(
      StorageKeys.cookiesAlertheader,
      'cookiesAlertheader',
      moment().endOf('days').toDate()
    );
    $('.news-message').hide();

    this.adjustNewMessage(false);
  }

  hideHeaderTop(WindowScrollTop: number) {
    const scrollHeight = 300; // HeaderTop 隱藏高度
    const headerMain = $('.header-main');
    WindowScrollTop > scrollHeight
      ? headerMain.addClass('hideTop')
      : headerMain.removeClass('hideTop');
  }
}
