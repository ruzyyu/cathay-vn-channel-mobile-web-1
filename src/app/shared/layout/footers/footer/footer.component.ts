import { isScullyGenerated } from '@scullyio/ng-lib';
import { distinctUntilChanged } from 'rxjs/operators';
import { StorageKeys } from 'src/app/core/enums/storage-key.enum';
import { StorageService } from 'src/app/core/services/storage.service';
import { Router, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import moment from 'moment';
import { versionInfo } from 'src/version-info';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  currentApplicationVersion = environment.appVersion;

  version = '';

  FLang: any;

  // FAlterShow
  FCookieShow: boolean = true;
  FShow = false;

  constructor(private cds: DatapoolService, private router: Router) {
    this.FLang = this.cds.gLang.A02;

    this.router.events
      .pipe(
        distinctUntilChanged((previous: any, current: any) => {
          if (current instanceof NavigationEnd) {
            const urlarr = [
              'project-trial-car',
              'project-trial-motor',
              'project-trial-travel',
            ];
            const mresult = urlarr.filter((item) => current.url.includes(item));
            this.FShow = mresult.length === 0;
            return mresult.length === 0;
          }
          return true;
        })
      )
      .subscribe((x: any) => {
        /** 移除未關閉的日期窗 */
        // console.log('into');
        this.FShow = false;
      });
  }

  ngOnInit() {
    this.generateVer();
    if (isScullyGenerated()) {
      this.FCookieShow =
        StorageService.getItem(StorageKeys.cookiesAlertfotter) ===
        'cookiesAlertfotter';
    }
  }

  generateVer() {
    const date = moment().format('YYYY-MM-DD');
    this.version = `${environment.appVersion}@${versionInfo.hash}`;
  }

  goUrl(type) {
    switch (type) {
      case 0:
        window.open('https://www.cathay-ins.com.tw/');
        break;
      case 1:
        window.open('https://www.cathaylife.com.vn/cathay/');
        break;
      case 2:
        this.router.navigate(['/sitemap']);
        break;
      case 3:
        window.open('assets/pdf/Chinh sach bao mat thong tin.pdf');
        break;
      case 4:
        window.open('assets/pdf/Chinh sach quyen rieng tu.pdf');
        break;
      default:
        this.router.navigate(['']);
        break;
    }
  }

  /** cookie */
  cookieClick() {
    StorageService.setItem(
      StorageKeys.cookiesAlertfotter,
      'cookiesAlertfotter'
    );
    $('.cookie').hide();
  }

  /** img url */
  imgUrl(item: number) {
    item === 1
      ? window.open(
          'https://profile.globalsign.com/SiteSeal/siteSeal/profile/profile.do?p1=fd24cb96&p2=0506a985c1012ea2bd2c11e08316fd170c2fe0a05642f3d5881977f9ff3443b7da08a70e570bfef3061af3c085446dbbda2a10ea850abf151734&p3=94461a42531bf6c1f771504bdd12df6d1fe0a6d0'
        )
      : window.open('http://www.online.gov.vn/Home/WebDetails/5597');
  }
}
