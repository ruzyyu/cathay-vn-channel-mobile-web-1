import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alert-message',
  templateUrl: './alert-message.component.html',
  styleUrls: ['./alert-message.component.scss'],
})
export class AlertMessageComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    // 隱私政策
    // const mcok = StorageService.getItem(StorageKeys.cookiesAlert);
    $('.cookie').toast();
  }

  // cookieAlert() {
  //   StorageService.setItem(StorageKeys.cookiesAlert, 'cookieAlertClose');
  // }
}
