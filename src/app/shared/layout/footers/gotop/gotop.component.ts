import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gotop',
  templateUrl: './gotop.component.html',
  styleUrls: ['./gotop.component.scss'],
})
export class GotopComponent implements OnInit {
  displayGoTop(WindowScrollTop) {
    /** 判斷物件是否存在 */
    if ($('.brand-link').length === 0) { return; }

    const scorllHeight = 800; // top 出現高度
    const gotop = $('.gotop');
    const footerOffsetTop = $('.brand-link').offset().top - $(window).scrollTop();
    const windowsHeight = $(window).height();

    if (WindowScrollTop > scorllHeight) {
      gotop.addClass('show');
      /** 如果有下方高度，則將top放在方框上方 */
      if (this.checkView()) {
        gotop.css('bottom', '68px');
      } else {
        gotop.css('bottom', '0px');
      }
    } else {
      gotop.removeClass('show');
      gotop.css('bottom', '-48px');
    }

    if (footerOffsetTop + 68 < windowsHeight) {
      gotop.addClass('static');
    } else {
      gotop.removeClass('static');
    }

    // 把過渡效果移除，變換 position 時才不會跳動
    if (footerOffsetTop < windowsHeight + 100) {
      gotop.addClass('noAni');
    } else {
      gotop.removeClass('noAni');
    }
  }

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    // 處理畫面，判斷特定網址處理
    router.events.subscribe((event) => {
      this.footerBottom();
    });
  }

  ngOnInit(): void {
    // 顯示隱藏 gotTop 按鈕
    let st = $(window).scrollTop();
    $(window).on('scroll', () => {
      st = $(window).scrollTop();
      this.displayGoTop(st);
    });
    this.displayGoTop(st);

    // 返回頂部
    $('.gotop').on('click', () => {
      $('html,body').animate({ scrollTop: 0 }, 'slow');
      return false;
    });
  }

  checkView() {
    // 需特殊處理頁面
    const pageList = [
      'project-trial-car',
      'project-trial-motor',
      'project-trial-travel',
    ];
    // 檢查是否需特殊處理
    const checkPage: any = (item) => {
      const page = this.router.url.split('/').indexOf(item);
      if (page !== -1) { return page; }
    };
    let check;
    pageList.forEach((item) => {
      if (checkPage(item)) {
        check = checkPage(item);
      }
    });

    if (document.body.clientWidth < 768) {
      return check;
    } else {
      return false;
    }
  }
  footerBottom() {
    const check = this.checkView();
    /** 針對特定網址增加下方高度 */
    if (check) {
      $('footer .brand-link').css('padding-bottom', '80px');
    } else {
      $('footer .brand-link').css('padding-bottom', '12px');
    }
  }
}
