import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Router, NavigationEnd } from '@angular/router';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  HostListener,
} from '@angular/core';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { IBreadcrumb } from 'src/app/core/interface';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadcrumbComponent implements OnInit {
  /** 網頁標題 */
  @Output() title = new EventEmitter<string>();

  breadcrumbs: Observable<any>;

  breadcrumbList: Array<IBreadcrumb>;

  showBreadcrumbList: Array<IBreadcrumb>;

  constructor(private router: Router, private cds: DatapoolService) {
    const mlang = this.cds.gLangType;

    this.breadcrumbs = this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd)
    ) as Observable<NavigationEnd>;

    this.breadcrumbs.subscribe((evt) => {
      const routerUrl = evt.urlAfterRedirects;

      if (routerUrl && typeof routerUrl === 'string') {
        // 取得目前routing url用/區格, [0]=第一層, [1]=第二層 ...etc
        const routerList = routerUrl.slice(1).split('/');

        let mArr: any;
        // 找到根目錄
        mArr = this.router.config.find(
          (item) => item.children && item.children.length > 0
        );

        this.breadcrumbList = [
          {
            name:
              mlang === 'zh-TW'
                ? '首頁'
                : mlang === 'en-US'
                ? 'Home'
                : 'Trang Chủ',
            path: '/',
          },
        ];

        routerList.forEach((item, index) => {
          if (mArr) {
            mArr = mArr.children.find((route) => {
              if (route.path.split('/:')[0] === item.split('?')[0]) {
                // 理賠頁麵包屑處理
                const nameData: any = route.data.dynamic
                  ? route.data.dynamic(item, route)
                  : route.data;

                this.breadcrumbList.push({
                  name: nameData
                    ? mlang === 'zh-TW'
                      ? nameData.breadcrumb
                      : mlang === 'en-US'
                      ? nameData.enbreadcrumb
                      : nameData.vnbreadcrumb
                    : '',
                  path:
                    index === 0
                      ? `/${route.path}`
                      : `${this.breadcrumbList[index].path}/${route.path}`,
                });
              }
              return route.path === item.split('?')[0];
            });
          }
        });
      }
    });
  }

  ngOnInit() {
    // 複製陣列
    const mlist = this.breadcrumbList.slice(0);
    this.title.emit(mlist.pop().name);
    this.setList(document.body.clientWidth)
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setList(event.target.innerWidth)
  }

  setList(width: number) {
    const result = this.breadcrumbList.reduce(
      (a, b) => {
        const length = a.length + b.name.length;
        const count = a.count + 1;

        let line = a.line;
        let currentWidth =
          a.currentWidth + b.name.length * 4 + (count >= 1 ? count * 130 : 0);

        if (currentWidth > width) {
          line = line + 1;
          currentWidth = b.name.length * 4 + 130;
        }

        return {
          count,
          length,
          line,
          currentWidth,
        };
      },
      {
        count: 0,
        length: 0,
        line: 0,
        currentWidth: 0,
      }
    );

    if (result.line > 2) {
      const length = this.breadcrumbList.length;
      const list = this.breadcrumbList.slice(length - 2, length);

      this.showBreadcrumbList = [{ name: '...', path: '' }, ...list];
    } else {
      this.showBreadcrumbList = this.breadcrumbList;
    }
  }
}
