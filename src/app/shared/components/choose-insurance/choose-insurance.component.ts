import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-choose-insurance',
  templateUrl: './choose-insurance.component.html',
  styleUrls: ['./choose-insurance.component.scss'],
})
export class ChooseInsuranceComponent implements OnInit {
  @Output() Action = new EventEmitter();
  // @Input() FEndRsmntCard;
  FLangType: string;
  FLang: any;
  // FEndRsmntCard:any
  constructor(public cds: DatapoolService) {
    this.FLangType = this.cds.gLangType;
    this.FLang = this.cds.gLang;
  }
  ngOnInit(): void {}

  get FEndRsmntCard() {
    console.log(111111111);
    console.log(this.cds.gEndRsmntCard);
    return this.cds.gEndRsmntCard;
  }

  onSubmit(i) {
    this.Action.emit(i);
  }
}
