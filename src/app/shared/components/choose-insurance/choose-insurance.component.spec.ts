import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseInsuranceComponent } from './choose-insurance.component';

describe('ChooseInsuranceComponent', () => {
  let component: ChooseInsuranceComponent;
  let fixture: ComponentFixture<ChooseInsuranceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChooseInsuranceComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
