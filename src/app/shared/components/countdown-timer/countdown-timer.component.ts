import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-countdown-timer',
  templateUrl: './countdown-timer.component.html',
  styleUrls: ['./countdown-timer.component.scss'],
})
/** 使用方式 */
// <app-countdown-timer
//   [time]="setTimeTwo"
//   timeType="seconds"
//   (timeEnd)="timeTwo($event)"
// ></app-countdown-timer>
// ---------------------------
//   [time] 綁定時間，單位秒
//   timeType 綁定類型，以最高單位為命名，假設使用十分十秒最高單位是分鐘，類型使用minute
//   (timeEnd)  時間歸零後回傳true值
/** 使用方式 end */
export class CountdownTimerComponent implements OnInit {
  @Input() time: number;
  @Input() timeType: string;
  @Output() timeEnd = new EventEmitter<boolean>();

  timeName = undefined;
  constructor() {}

  /**
   * @param  {number=undefined} timeSeconds  代入時間
   * @param  {string='seconds'} timeType   代入類型
   */
  countdownTimer(
    timeSeconds: number = undefined,
    timeType: string = 'seconds'
  ) {
    /**
     * @param  {number=undefined} timeSeconds  代入時間
     * @param  {string='seconds'} timeType  代入類型
     */
    const timeConversion = (
      timeSeconds: number = undefined,
      timeType: string = 'seconds'
    ) => {
      if (timeSeconds === 0) {
        this.timeEnd.emit(true);
      }
      if (timeSeconds > 0 && timeSeconds) {
        timeSeconds = timeSeconds - 1;
        /** 計算時分秒 */
        let seconds: number | string = timeSeconds % 60;
        let minute: number | string = ((timeSeconds - seconds) / 60) % 60;
        let hour: number | string =
          ((timeSeconds - seconds - minute * 60) / (60 * 60)) % 24;
        /** 小於十補0 */
        if (seconds < 10) {
          seconds = '0' + seconds;
        }
        if (minute < 10) {
          minute = '0' + minute;
        }
        if (hour < 10) {
          hour = '0' + hour;
        }
        /** 判斷類型 */
        if (timeType === 'seconds') {
          this.timeName = `${seconds}`;
        } else if (timeType === 'minute') {
          this.timeName = `${minute}:${seconds}`;
        } else if (timeType === 'hour') {
          this.timeName = `${hour}:${minute}:${seconds}`;
        } else {
          this.timeName = '無此類型計時器';
        }
        /** 每秒執行一次 */
        setTimeout(function() {
          timeConversion(timeSeconds, timeType);
        }, 1000);
      }
    };
    /** 第一次執行 */
    timeConversion(timeSeconds, timeType);
  }
  ngOnInit(): void {
    this.countdownTimer(this.time, this.timeType);
  }
}
