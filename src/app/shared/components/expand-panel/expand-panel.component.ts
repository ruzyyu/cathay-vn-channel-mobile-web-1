import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from 'src/app/core/services/common.service';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';

@Component({
  selector: 'app-expand-panel',
  templateUrl: './expand-panel.component.html',
  styleUrls: ['./expand-panel.component.scss'],
})
export class ExpandPanelComponent implements OnInit {
  constructor() {}
  @Input() item: any;

  @Input() i: number;

  @Input() titleContent: any;

  @Input() infoContent: any;

  @Input() titleId: string;

  @Input() titleDataTarget: string;

  @Input() titleAriaControls: string;

  @Input() infoId: string;

  @Input() infoAriaLabelledby: string;

  /** 控制首筆，箭頭方向跟內容展開，如有需要請在class加上first-arrow-open , first-content-open */
  firstArrowOpen() {
    const firstData = document.querySelectorAll('.first-arrow-open');
    firstData[0].classList.remove('collapsed');
  }
  firstContentOpen() {
    const firstData = document.querySelectorAll('.first-content-open');
    firstData[0].classList.add('show');
  }

  ngOnInit(): void {
    this.firstArrowOpen();
    this.firstContentOpen();
  }

  contentClickHandler(event) {
    CommonService.contentClickHandler(event);
  }
}
