import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-change-insurance-tip',
  templateUrl: './change-insurance-tip.component.html',
  styleUrls: ['./change-insurance-tip.component.scss'],
})
export class ChangeInsuranceTipComponent implements OnInit {
  @Output() Action = new EventEmitter();
  FLang: any;
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {}

  doSubmit() {
    this.Action.emit();
  }
}
