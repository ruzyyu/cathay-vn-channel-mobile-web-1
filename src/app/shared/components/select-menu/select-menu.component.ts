import { Router, ActivatedRoute } from '@angular/router';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

interface IImgList {
  title: string;
  imgActive: string;
  srcsetActive: string;
  imgDefault: string;
  srcsetDefault: string;
}

@Component({
  selector: 'app-select-menu',
  templateUrl: './select-menu.component.html',
  styleUrls: ['./select-menu.component.scss'],
})
export class SelectMenuComponent implements OnInit {
  @Input() filterId: number;
  @Output() filterIdChange = new EventEmitter<any>();

  FLang: any;

  selectId: number;

  // 位址抓取
  selectItemNumber: number;

  isInitDefaultSlide: boolean;

  /**  滑動元件的參數 */
  slideConfig = {
    dots: true,
    infinite: false,
    variableWidth: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplaySpeed: 1000,
    responsive: [
      {
        breakpoint: 1440.98,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: false,
          dots: true,
        },
      },
      {
        breakpoint: 991.98,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: false,
          prevArrow: false,
          nextArrow: false,
          dots: true,
          centerMode: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
        },
      },
    ],
  };

  imgList: Array<IImgList>;
  viewList: Array<IImgList>;

  constructor(
    private cds: DatapoolService,
    private activatedRoute: ActivatedRoute
  ) {
    this.FLang = this.cds.gLang.C01;
    this.isInitDefaultSlide = false;
  }

  ngOnInit(): void {
    const mbaseurl = 'assets/img/online/';
    const mallimg = mbaseurl + 'ic-tab-all';
    const mallcar = mbaseurl + 'ic-tab-car';
    const mallmotor = mbaseurl + 'ic-tab-motor';
    const malltravel = mbaseurl + 'ic-tab-travel';

    this.imgList = [
      {
        title: this.FLang.C0002,
        imgActive: `${mallimg}-g.png`,
        srcsetActive: `${mallimg}-g@2x.png 2x`,
        imgDefault: `${mallimg}-w.png`,
        srcsetDefault: `${mallimg}-w@2x.png 2x`,
      },
      {
        title: this.FLang.C0003,
        imgActive: `${mallcar}-g.png`,
        srcsetActive: `${mallcar}-g@2x.png 2x`,
        imgDefault: `${mallcar}-w.png`,
        srcsetDefault: `${mallcar}-w@2x.png 2x`,
      },
      {
        title: this.FLang.C0004,
        imgActive: `${mallmotor}-g.png`,
        srcsetActive: `${mallmotor}-g@2x.png 2x`,
        imgDefault: `${mallmotor}-w.png`,
        srcsetDefault: `${mallmotor}-w@2x.png 2x`,
      },
      {
        title: this.FLang.C0005,
        imgActive: `${malltravel}-g.png`,
        srcsetActive: `${malltravel}-g@2x.png 2x`,
        imgDefault: `${malltravel}-w.png`,
        srcsetDefault: `${malltravel}-w@2x.png 2x`,
      },
    ];

    this.viewList = this.imgList;

    // queryParams type
    this.activatedRoute.queryParams.subscribe((params) => {
      switch (params.type) {
        case 'car':
          this.selectId = 1;
          break;
        case 'motor':
          this.selectId = 2;
          break;
        case 'travel':
          this.selectId = 3;
          break;
        default:
          this.selectId = 0;
      }

      setTimeout(() => {
        this.filterIdChange.emit(this.selectId);
      });
    });
  }

  onFilterChange(index: number) {
    this.selectId = index;
    this.filterIdChange.emit(index);
    $('.tab.filter.onlinecard.slider').slick('slickGoTo', index);
  }

  afterSlickChange(event) {
    const { currentSlide } = event.slick;

    if (!this.isInitDefaultSlide) {
      if (currentSlide !== this.selectId) {
        event.slick.slickGoTo(this.selectId);
        this.filterIdChange.emit(this.selectId);
      } else {
        this.isInitDefaultSlide = true;
      }
    } else if (this.filterId !== currentSlide) {
      this.filterIdChange.emit(currentSlide);
    }
  }

  beforeSlickChange(event) {
    if (!this.isInitDefaultSlide) {
      return;
    }

    const { nextSlide } = event;

    this.selectId = nextSlide;
  }
}
