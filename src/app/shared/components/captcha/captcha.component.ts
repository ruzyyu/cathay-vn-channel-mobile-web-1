import { AfterViewInit, Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-captcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.scss'],
})
export class CaptchaComponent implements AfterViewInit {
  /** 圖形驗證碼產出結果 */
  @Output() captchaResult = new EventEmitter<string>();

  /** 圖形驗證碼元素 ID */
  captchaId = 'captchaCode';

  /** 驗證碼長度 */
  codeLength = 4;

  /** 背景圖片路徑 */
  bgImagePath = './assets/img/captcha/bg.jpg';

  /** 驗證碼字碼列表 */
  alpha = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

  ngAfterViewInit() {
    this.createCaptcha();
  }

  /** 建立圖形驗證碼 */
  createCaptcha() {
    // 建立背景圖片
    const img = new Image();
    img.src = this.bgImagePath;

    // 背景圖片讀取完後才繪製 Canvas
    img.onload = (event) => {
      this.generateCanvas(event);
    };
  }

  /** 產生圖形驗證碼 Canvas */
  generateCanvas(event) {
    /** 圖形驗證 Canvas 元素 */
    const captchaElement: any = document.getElementById(this.captchaId);

    if (captchaElement && 'getContext' in captchaElement) {
      /** Canvas Context */
      const ctx = captchaElement.getContext('2d');
      /** Canvas 高度 */
      const height = captchaElement.height;
      /** Canvas 寬度 */
      const width = captchaElement.width;

      // 清空 Canvas 內容
      ctx.clearRect(0, 0, width, height);

      // 設定 Canvas 背景圖片
      ctx.fillStyle = ctx.createPattern(event.target, 'repeat');
      ctx.fillRect(0, 0, width, height);

      // 設定 Canvas 文字樣式
      ctx.font = 'bold 34px Roboto Slab';
      ctx.fillStyle = '#ccc';
      ctx.textAlign = 'center';

      // 設定 canvas 驗證碼文字
      ctx.fillText(this.getCaptchaCode(), width / 2, height / 1.4);
    } else {
      // 錯誤訊息
      console.error(
        `Can't found #${this.captchaId} element or #${this.captchaId} is not canvas element.`
      );
    }
  }

  /** 取得驗證碼文字 */
  getCaptchaCode() {
    let code = '';

    // 產生驗證碼文字
    for (let i = 0; i < this.codeLength; i++) {
      code += `${this.alpha[Math.floor(Math.random() * this.alpha.length)]} `;
    }

    // 丟出圖形產出結果
    this.captchaResult.emit(code.replace(/\s*/g, ''));
    // 移除文字尾空白
    return code.trim();
  }
}
