import { Router } from '@angular/router';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DatapoolService } from './../../../core/services/datapool/datapool.service';

@Component({
  selector: 'app-payment-fail',
  templateUrl: './payment-fail.component.html',
  styleUrls: ['./payment-fail.component.scss'],
})
export class PaymentFailComponent implements OnInit {
  constructor(private cds: DatapoolService, private go: Router) {
    this.FLang = this.cds.gLang.popError;
  }
  FLang: any;

  @Output() xAction: EventEmitter<any> = new EventEmitter();

  ngOnInit() {}

  onPaymentFail(event) {
    $('#paymentFailModal').modal('hide');
    this.xAction.next(event);
    if (this.go.url.indexOf('payment') !== -1) {
      const mUrl = this.go.url.replace('payment', '');
      this.go.navigate([mUrl, 'personInfo'], {
        replaceUrl: true,
      });
    }
  }
  closePop() {
    $('#paymentFailModal').modal('hide');
  }
}
