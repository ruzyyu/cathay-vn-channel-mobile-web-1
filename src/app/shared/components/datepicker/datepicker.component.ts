import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { DatapoolService } from './../../../core/services/datapool/datapool.service';
import * as moment from 'moment';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
})
export class DatepickerComponent implements OnInit, AfterViewInit, OnChanges {
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }
  @Input() placeholder: string;
  @Input() label: string;
  @Input() controls;
  @Input() value: string;
  @Input() xdisabled: boolean;
  // 若需開啟文字輸入功能則傳入false
  @Input() xReadonly = true;
  @Input() xid: string;
  @Input() xpolDays: any; // 保險天數
  /**
   * @description
   * @param birthday 生日窗 限制最大為今天 (用於新建用)
   *        normal   常態日期窗 不限制最大天數
   */
  @Input() xType: string;
  @Input() config: any;
  @Output() getDate = new EventEmitter<string | number | string[]>();
  datapickerValue = undefined;

  /** 用以判斷日期開關，只要有關閉日期都必須將他調整為false */
  Fcheck = false;

  /** 用以判斷是否存過值 */
  FEvType: string;
  /** 緩存日期 */
  FDate;
  /** 文字輸入狀態碼 */
  inputStatus = false;

  FLang: any;

  byPrevious;

  defaultConfig: any = {
    opens: 'left',
    autoApply: true,
    showDropdowns: true,
    singleDatePicker: true,
    minYear: 1900,
    maxYear: new Date().getFullYear(),
    maxDate: new Date(),
    locale: {
      format: 'DD/MM/YYYY',
      separator: ' - ',
      applyLabel: '送出',
      cancelLabel: '取消',
      daysOfWeek: [
        this.cds.gLang.dateRangePicker.week.W0007,
        this.cds.gLang.dateRangePicker.week.W0001,
        this.cds.gLang.dateRangePicker.week.W0002,
        this.cds.gLang.dateRangePicker.week.W0003,
        this.cds.gLang.dateRangePicker.week.W0004,
        this.cds.gLang.dateRangePicker.week.W0005,
        this.cds.gLang.dateRangePicker.week.W0006,
      ],
      monthNames: [
        this.cds.gLang.dateRangePicker.month.M0001,
        this.cds.gLang.dateRangePicker.month.M0002,
        this.cds.gLang.dateRangePicker.month.M0003,
        this.cds.gLang.dateRangePicker.month.M0004,
        this.cds.gLang.dateRangePicker.month.M0005,
        this.cds.gLang.dateRangePicker.month.M0006,
        this.cds.gLang.dateRangePicker.month.M0007,
        this.cds.gLang.dateRangePicker.month.M0008,
        this.cds.gLang.dateRangePicker.month.M0009,
        this.cds.gLang.dateRangePicker.month.M0010,
        this.cds.gLang.dateRangePicker.month.M0011,
        this.cds.gLang.dateRangePicker.month.M0012,
      ],
    },
  };

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.value) {
      $(() => {
        if (
          changes.value.currentValue !== changes.value.previousValue &&
          changes.value.currentValue
        ) {
          const msplit = changes.value.currentValue.split(/\s?[-~]\s?/);
          const mid = `#${this.xid}`;

          if (Array.isArray(msplit) && msplit.length > 0) {
            $(mid).data('daterangepicker').setStartDate(msplit[0]);
            $(mid).data('daterangepicker').setEndDate(msplit[1]);
            setTimeout(() => {
              msplit[1]
                ? $(mid).val(`${msplit[0]} - ${msplit[1]}`)
                : $(mid).val(`${msplit[0]}`);
            }, 1);
          }
        } else {
          setTimeout(() => {
            // 可使用文字輸入不清除資料
            if (!this.xReadonly) {
              return;
            }
            $(`#${this.xid}`).val('');
          }, 1);
        }
      });
    }
  }

  datepicker(config) {
    const mid = `#${this.xid}`;

    /** 判斷尺寸處理手機版畫面 */
    if ($(window).width() < 564) {
      config.drops = 'down';
    }
    /** 初始化datePicker */
    $(mid).daterangepicker(config);

    /** 開啟日曆時觸發 */
    $(mid).on('showCalendar.daterangepicker', (ev, picker) => {
      // 清除點擊時日期(文字輸入功能開啟時)
      if (this.FEvType !== 'apply' && !this.xReadonly && !this.inputStatus) {
        // 可使用文字輸入不清除資料
        if ($(mid).hasClass('date1')) {
          this.FEvType = 'apply';
          return;
        }
        $(mid).val('');
      }
      setTimeout(() => {
        // 第一行若最後一列為 .weekend.off.ends.available 則隱藏
        $('.weekend.off.ends.available[data-title^="r0c6"]').parent().hide();
        // 最後一行若第一列為 .weekend.off.ends.off.disabled 則隱藏
        $('.weekend.off.ends.off.disabled[data-title^="r5c0"]').parent().hide();
      }, 0);
    });

    /** 直接關閉日曆時觸發 */
    $(mid).on('hide.daterangepicker', (ev, picker) => {
      // 使用文字輸入時(文字輸入功能開啟時)
      if (this.inputStatus && !this.xReadonly) {
        let inputData = $(mid).val();
        // datepicker重啟時資料格式會改變，故此先轉換日月資料位置
        if (inputData && typeof inputData !== 'number') {
          const day = inputData.slice(0, 2);
          const month = inputData.slice(3, 5);
          const year = inputData.slice(6, 10);
          inputData = `${month}/${day}/${year}`;
        }

        // 判斷若為保單變更，則送出不同格式日期
        if (this.xpolDays) {
          this.FDate = picker.startDate;
          this.setDateValue();
          this.datapickerValue = picker.startDate.format('YYYY-MM-DD');
          this.getDate.emit(this.datapickerValue);
        } else {
          this.getDate.emit(inputData);
        }
        this.Fcheck = false;
        this.FEvType = 'apply';
        return;
      }

      // 再第一次選值時將this.FEvType 設置為 'apply' 避免每次都清掉資料
      if (this.FEvType === 'apply') {
        this.FDate = picker.startDate;
        this.setDateValue();
        this.datapickerValue = picker.startDate.format('YYYY-MM-DD');
        this.getDate.emit(this.datapickerValue);
      } else {
        $(mid).val('');
        this.getDate.emit(undefined);
      }
      this.Fcheck = false;
    });
    /** 直接日曆送出時觸發 */
    $(mid).on('apply.daterangepicker', (ev, picker) => {
      this.FDate = picker.startDate;
      this.setDateValue();
      this.datapickerValue = picker.startDate.format('YYYY-MM-DD');
      this.getDate.emit(this.datapickerValue);
      this.Fcheck = false;
      this.FEvType = 'apply';
    });
  }

  /**
   * @description
   * @param icon 判斷點擊icon用
   */
  doShowDate(icon = undefined) {
    this.Fcheck = !this.Fcheck;
    if (this.Fcheck) {
      $('.daterangepicker').remove();
      this.datepicker({ ...this.defaultConfig, ...this.config });

      // '被保人資料' 不是點擊icon需做冒泡處理
      if (icon !== 'icon' && this.xid && this.xid.indexOf('inusred') !== -1) {
        $(`#${this.xid}`).on('click', (event) => {
          event.stopPropagation();
        });
      } else {
        $(`#${this.xid}`).click();
      }

      // 手機版 關閉日期用
      $('.ranges').on('click', () => {
        if (this.FEvType === 'apply' && this.FDate) {
          this.setDateValue();
          this.datapickerValue = this.FDate.format('YYYY-MM-DD');
          this.getDate.emit(this.datapickerValue);
        } else {
          $(`#${this.xid}`).val('');
          this.getDate.emit(undefined);
        }
        $('.daterangepicker').css('display', 'none');
        this.Fcheck = false;
      });
    } else {
      $('.daterangepicker').remove();
    }
  }
  openData() {
    $('.daterangepicker').css('display', 'block');
  }

  /** 保單變更的值須加上第二日期 xpolDays */
  setDateValue() {
    if (this.xpolDays && this.FDate) {
      $(`#${this.xid}`).val(
        this.FDate.format('DD/MM/YYYY') +
          ` – ${moment(this.FDate.format('YYYY-MM-DD'))
            .add(this.xpolDays, 'days')
            .format('DD/MM/yyyy')}`
      );
    } else {
      $(`#${this.xid}`).val(this.FDate.format('DD/MM/YYYY'));
    }
  }

  ngOnInit(): void {}

  ngAfterViewInit() {
    // 首次生成
    this.datepicker({ ...this.defaultConfig, ...this.config });
    $(`#${this.xid}`).val(this.value);
  }

  // 生日專用 轉換Keyup狀態(文字輸入功能開啟時)
  CheckInputStatus(event) {
    if (this.xReadonly) {
      return;
    }
    if (event.type === 'keydown') {
      this.inputStatus = true;
    }
    if (event.type === 'blur') {
      this.inputStatus = false;
    }
    if (event.key === 'Enter') {
      $(`#${this.xid}`).blur();
      this.inputStatus = true;
    }
  }
}
