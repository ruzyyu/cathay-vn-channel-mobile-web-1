import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FastpairComponent } from './fastpair.component';

describe('FastpairComponent', () => {
  let component: FastpairComponent;
  let fixture: ComponentFixture<FastpairComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FastpairComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FastpairComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
