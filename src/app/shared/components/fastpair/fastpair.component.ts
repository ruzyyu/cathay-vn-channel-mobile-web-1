import { IAirportList } from './../../../core/interface/index';
import { PublicService } from 'src/app/core/services/api/public/public.service';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import {
  Component,
  OnInit,
  EventEmitter,
  Input,
  Output,
  OnDestroy,
  AfterViewInit,
  HostListener,
} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-fastpair',
  templateUrl: './fastpair.component.html',
  styleUrls: ['./fastpair.component.scss'],
})
export class FastpairComponent implements OnInit, OnDestroy, AfterViewInit {
  constructor(
    private vi: ValidatorsService,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private cds: DatapoolService,
    private api: PublicService
  ) {
    this.FLang = this.cds.gLang.C02.fastpair;
    this.FErrorLang = this.cds.gLang.ER;
    this.FPHLang = this.cds.gLang.PH;
    this.FLangType = this.cds.gLangType;

    // 無法理賠所有項目 => 只能選擇班機延誤
    this.FNoClaims = this.cds.gNoClaims;
    if (this.FNoClaims) {
      this.FClamKind = this.FClamKindList[0];
    }
  }
  /** 航班資訊清單 */
  @Input() data: any;
  /** 選單標題 */
  @Input() modalName: string;

  /** 航班資訊清單 */
  @Input() id: number;
  /**
   *  表單種類
   *    calc: 試算
   *    manual: 手動理賠
   *    fast: 快速理賠
   */
  @Input() FType: 'calc' | 'manual' | 'fast' = 'calc';

  /** 快速理賠航班資訊 */
  @Input() FFlightData: any[] = [];

  @Output() xAction = new EventEmitter();

  FLang: any;
  FErrorLang: any;
  FPHLang: any;
  FLangType: string;

  // 表單檢核
  group: FormGroup;
  controls: { [key: string]: AbstractControl };
  validators: { [key: string]: any };

  keyword: string;
  FSubmit = false;

  FSelectedFlightData: any;

  FNoClaims = false;
  FClamKindList: any[] = [];
  FClamKind: any = {};

  // 機場
  FAirportList: Array<IAirportList>;
  FilterList: Array<IAirportList>;

  // 代號存檔
  FSAtaCode: string;
  FEAtaCode: string;

  FAirplaneDate: string;
  FMinDate: Date;
  FMaxDate: Date;
  FMinYear: number;
  FMaxYear: number;
  FAbroad: boolean;

  FReadonly: boolean;
  // 小網日期不得開啟填寫功能
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }

  async ngOnInit() {
    this.dateFormatChange();
    if (this.data.FPlan && this.FType === 'calc') {
      this.FAbroad = this.data.FPlan <= 3;
    }

    this.FAirplaneDate = this.data.airplaneDate
      ? moment(this.data.airplaneDate).format('DD/MM/YYYY')
      : '';

    // 手動理賠取代，原先的理賠資訊
    this.FClamKindList =
      this.cds.gClamKindList && !this.cds.gIsFastClaim
        ? this.cds.gClamKindList
        : [
            // 班機延誤
            { text: this.cds.gLang.F05.F0004, value: 'FD' },
            // 改降其他機場
            { text: this.cds.gLang.F05.F0005, value: 'AC' },
            // 班機取消
            { text: this.cds.gLang.F05.F0006, value: 'FC' },
          ];

    if (this.data.clamKind) {
      this.FClamKind =
        this.FClamKindList.find((item) => item.value === this.data.clamKind) ||
        {};
    }

    this.validators = {
      airplaneNo: [
        this.data.airplaneNo,
        [Validators.required, this.vi.flightNo],
      ],
      airPortS: [this.data.airPortS, [Validators.required]],
      airPortE: [this.data.airPortE, [Validators.required]],
      airplaneDate: [this.data.airplaneDate || '', [Validators.required]],
    };
    this.formValidate();

    // await this.getAirplane();
    this.FAirportList = this.cds.gPubCommon.airportList;
    // 國內 僅呈現VN
    if (
      this.FAbroad ||
      this.cds.gClaimData.prod_pol_code === 'AC01005' ||
      this.cds.gClaimData.prod_pol_code === 'AC01013'
    ) {
      this.FAirportList = this.FAirportList.filter(
        (item) => item.isoCountry === 'VN'
      );
    }

    // 小網日期不得開啟填寫功能
    if ($(document).innerWidth() > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }

  ngAfterViewInit() {
    $('.dropdown-menu').scroll(() => {
      const nowH = $('app-fastpair .dropdown-menu.show').scrollTop();
      const maxH = $('app-fastpair .dropdown-menu.show').prop('scrollHeight');

      if ((nowH / maxH) * 100 > 85) {
        this.FilterList = this.FilterList.concat(
          this.FAirportList.slice(
            this.FilterList.length,
            this.FilterList.length + 10
          )
        );
      }
    });

    // 當 dropdown 為 modol 最後一個元素並關閉選單時移除下方高度
    $('.custom-modal-dropdown').on('hide.bs.dropdown', (e) => {
      $('.custom-modal-dropdown').parent().removeClass('custom-dropdown-mb');
    });

    /** 搜尋文字若與資料不符則進行清除 */
    $('.dropdownClear01').on('hide.bs.dropdown', (event) => {
      // console.log('this.FSAtaCode');
      // console.log(this.FSAtaCode);
      if (!this.FSAtaCode) {
        $('.dropdownClear01 input').val('');
        this.controls.airPortS.reset();
        this.FilterList = this.FAirportList.slice(0, 10);
      } else {
        this.FSAtaCode = undefined;
      }
    });
    $('.dropdownClear02').on('hide.bs.dropdown', (event) => {
      if (!this.FEAtaCode) {
        $('.dropdownClear02 input').val('');
        this.controls.airPortE.reset();
        this.FilterList = this.FAirportList.slice(0, 10);
      } else {
        this.FEAtaCode = undefined;
      }
    });
  }

  ngOnDestroy() {
    this.closeModel();
  }

  /** 建立驗證表單 */
  formValidate() {
    // 建立驗證表單
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });

    this.group = this.fb.group(validator);

    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          airplaneNo:
            this.FErrorLang.enter.ER001 + this.FErrorLang.enter.ER001_.ES017,
          airplaneDate:
            this.FErrorLang.select.ER001 +
            ' ' +
            this.FErrorLang.select.ER001_.ES004,
          airPortS:
            this.FErrorLang.enter.ER001 + this.FErrorLang.enter.ER001_.ES018,
          airPortE:
            this.FErrorLang.enter.ER001 + this.FErrorLang.enter.ER001_.ES019,
        }[control_key];
      }
      /** 新檢核end */
      return (
        {
          required: this.FErrorLang.ER017,
          flightNo:
            this.FErrorLang.enter.ER002 + this.FErrorLang.enter.ER002_.ES005,
        }[type] || this.FErrorLang.ER023
      );
    }
  }

  onAction() {
    $(`#dropdownFilter1`).dropdown('hide');
    $(`#dropdownFilter2`).dropdown('hide');

    const mbol = this.checkSubmit();
    if (mbol) {
      return;
    }

    this.FSubmit = true;

    if (this.FSelectedFlightData) {
      this.xAction.emit(this.FSelectedFlightData);

      this.closeModel();
    } else if (this.group.valid) {
      const payload: any = {
        airplaneNo: this.controls.airplaneNo.value.toUpperCase(),
        airPortS: this.controls.airPortS.value,
        airPortE: this.controls.airPortE.value,
        airplaneDate: this.controls.airplaneDate.value,
      };

      // 不是試算額外傳入申請種類
      if (this.FType !== 'calc') {
        payload.clamKind = this.FClamKind.value;
      }

      this.xAction.emit(payload);

      this.closeModel();
    }
  }

  onAirportInputFilter(event) {
    return !!/[a-zA-Z\s]/.exec(event.key);
  }

  onFilterList(event, form, selector) {
    const mstr = this.controls[form].value || '';
    if (mstr && mstr.length > 0) {
      $(`#${selector}`).dropdown('show');
    }

    if (this.FAirportList) {
      this.FilterList = this.FAirportList.reduce((arr, item) => {
        // 檢查此國家清單內有無符合的機場 iataCode
        let hasIataCode = false;

        const data = item.data.filter((airport) => {
          if (airport.iataCode.includes(mstr.toUpperCase())) {
            hasIataCode = true;
          }

          return (
            item.groupname.toUpperCase().includes(mstr.toUpperCase()) ||
            airport.airportName.toUpperCase().includes(mstr.toUpperCase()) ||
            airport.iataCode.includes(mstr.toUpperCase())
          );
        });

        if (data.length > 0) {
          // 如果有 IATACode 拉到最前顯示
          if (hasIataCode) {
            arr = [{ ...item, data }, ...arr];
          } else {
            arr.push({ ...item, data });
          }
        }

        return arr;
      }, []).slice(0, 10);
    }

    if (this.controls[form].value && this.controls[form].value.length > 0) {
      const mdown = $('.custom-modal-dropdown');
      const btn = mdown.find('.dropdown-toggle');
      if ($(btn).attr('aria-expanded') === 'true') {
        mdown.parent().addClass('custom-dropdown-mb');
        mdown.parents('.modal-body').scrollTop((mdown as any).offsetTop - 200);
      }
    }
  }

  /** 資料處理 */
  // async getAirplane() {
  //   if (!this.cds.gPubCommon.airportList) {
  //     await this.api.gAirPortList();
  //   }
  // }

  getDate(event) {
    let date;
    if (event && event.indexOf('/') !== -1) {
      date = this.datePipe.transform(event, 'yyyy-MM-dd');
    } else {
      date = event;
    }
    this.controls.airplaneDate.setValue(date);
    this.FAirplaneDate = event ? moment(event).format('DD/MM/YYYY') : '';
  }

  onSelectFlightData(item) {
    const alreadyExist = this.FSelectedFlightData === item;

    this.FSelectedFlightData = alreadyExist ? null : item;

    if (!alreadyExist) {
      this.group.disable();
    } else {
      this.group.enable();
    }
  }

  onSelectClamKind(item) {
    this.FClamKind = item;
  }

  /** 機場下拉 */
  onAirplaneClick(ataCode, type) {
    type === 1 ? (this.FSAtaCode = ataCode) : (this.FEAtaCode = ataCode);
    type === 1
      ? this.controls.airPortS.setValue(ataCode)
      : this.controls.airPortE.setValue(ataCode);
  }

  /** 機場enter選擇 */
  onAirplaneEnter(event, selector) {
    event.preventDefault();
    if (this.FilterList.length === 1 && this.FilterList[0].data.length === 1) {
      this.onAirplaneClick(
        this.FilterList[0].data[0].iataCode,
        selector === 'dropdownFilter1' ? 1 : 2
      );
      $(`#${selector}`).dropdown('hide');
    }
  }

  onAirplaneDown(event, selector) {
    event.preventDefault();
  }

  onSearchInputClick(event, form, selector) {
    const controlName = event.currentTarget.id;
    $(`#${selector}`).dropdown('show');

    this.onFilterList(event, controlName, selector);
    $(event.currentTarget).focus();

    if (form === 'airPortE') {
      const mdown = $('.custom-modal-dropdown');
      const btn = mdown.find('.dropdown-toggle');
      if ($(btn).attr('aria-expanded') === 'true') {
        mdown.parent().addClass('custom-dropdown-mb');
        mdown.parents('.modal-body').scrollTop((mdown as any).offsetTop - 200);
      }
    }
  }

  /** 畫面處理 */
  closeModel() {
    $('#modelFast').modal('hide');
  }

  checkSubmit() {
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });
    return (
      !this.FSelectedFlightData &&
      (this.group.invalid || this.FSelectedFlightData)
    );
  }

  onkeyenent(sender) {
    let dropdown: any;
    let btn: any;
    sender.currentTarget.id === 'airPortS'
      ? (btn = '#dropdownFilter1')
      : (btn = '#dropdownFilter2');
    switch (sender.keyCode) {
      case 38:
        // up
        $(btn).focus();
        break;

      case 40:
        // down
        $(btn).focus();
        break;
    }
  }

  /** 起始日期 結束日期 賦值 */
  dateFormatChange() {
    const sd = new Date(this.data.sdate);
    const ed = new Date(this.data.edate);
    const today = new Date();
    if (this.IsClaim()) {
      this.FMinDate = this.dateIsBigToday(sd)
        ? moment(today).add(1, 'days').toDate()
        : sd;
      this.FMaxDate = this.dateIsBigToday(ed) ? today : ed;
    } else {
      this.FMinDate = sd;
      this.FMaxDate = ed;
      this.FMinYear = sd.getFullYear();
      this.FMaxYear = ed.getFullYear();
    }
  }

  /** 日期是否大於今日 */
  dateIsBigToday(date) {
    if (date) {
      const today = new Date();
      if (date >= today) {
        return true;
      }
    }
    return false;
  }

  IsClaim() {
    if (this.FType === 'calc') {
      return false;
    }
    if (this.FType === 'manual' || this.FType === 'fast') {
      return true;
    }
  }

    /** 將 input 值 轉換成大寫 */
  changeToUpperCase(event){
    const input = event.target;
    const start = input.selectionStart;
    const end = input.selectionEnd;
    input.value = input.value.toLocaleUpperCase();
    input.setSelectionRange(start, end);
  }
}
