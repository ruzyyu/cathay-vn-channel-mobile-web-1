import { Observable } from 'rxjs';
import { StorageKeys } from './../../../core/enums/storage-key.enum';
import { StorageService } from './../../../core/services/storage.service';
import {
  Component,
  Input,
  OnInit,
  Output,
  ViewContainerRef,
  EventEmitter,
} from '@angular/core';
import { Router } from '@angular/router';
import { SocialService } from '../../../core/services/social.service';
import { UserInfo } from '../../../core/interface/index';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { Location } from '@angular/common';
import { Lang } from 'src/types';
import { CommonService } from 'src/app/core/services/common.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  @Output() xRegister = new EventEmitter();
  @Output() xforgetPassword = new EventEmitter();

  // 登入用
  @Input() xPop: boolean;
  @Input() xtype: string;
  @Input() xCheckid = false;
  @Output() xlogin = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    private apiMember: MemberService,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private router: Router,
    private auth: SocialService,
    public viewContainerRef: ViewContainerRef,
    private location: Location
  ) {
    /** 判斷如果已經登入就直接轉至member頁面 */
    if (this.cds.userInfo.seq_no && this.cds.gMobileToken) {
      this.router.navigateByUrl('member');
    }
    this.FLang = this.cds.gLang;
  }

  /** 宣告區Start */
  userInfo: UserInfo;
  validators: {};
  group: FormGroup;
  controls;
  FLoginError = false;
  FLang: Lang;
  urlHistory: Observable<any>;
  /** login 主頁使用 */
  FLogin: boolean;
  /** 宣告區End */

  /** 建立驗證表單 */
  formValidate() {
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });
    this.group = this.fb.group(validator);
    // 錯誤訊息顯示
    this.controls = this.group.controls;

    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }
  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          user_account:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES011,
          user_password:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES010,
        }[control_key];
      }
      /** 新檢核end */
      return (
        {
          LengthInterval: this.FLang.ER.ER022,
          passwordRules: this.FLang.ER.ER022,
          email: this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES003,
        }[type] || this.FLang.ER.ER017
      );
    }
    return this.FLang.ER.ER017;
  }
  /** FB登入 */
  fblogin() {
    const many: any = this.auth
      .signInWithFB()
      .then((res) => {
        console.log(res);

        this.auth.user = res;
        const payload: any = {
          fb_account: this.auth.user.id,
          google_account: '',
          name: this.auth.user.name,
        };
        this.checkSocialLogin(payload);
        this.auth.signOut();
      })
      .catch((err) => {
        this.auth
          .signOut()
          .then()
          .catch((err) => {
            if (err === 'Not logged in') {
              console.log(err);
            }
          });
      });

    this.auth.signOut();
  }
  /** google 登入 */
  googlelogin() {
    const many: any = this.auth
      .signInWithGoogle()
      .then((res) => {
        this.auth.user = res;
        const payload: any = {
          fb_account: '',
          google_account: this.auth.user.id,
          name: this.auth.user.name,
        };
        this.checkSocialLogin(payload);
      })
      .catch((err) => {
        this.auth
          .signOut()
          .then()
          .catch((err) => {
            if (err === 'Not logged in') {
              console.log(err);
            }
          });
      });
  }
  /** google 登出 */
  googleSignOut() {
    this.auth
      .signOutWithGoogle()
      .then()
      .catch((err) => {
        if (err === 'Not logged in') {
          this.router.navigateByUrl('login');
        }
      });
  }
  /** 社群登入判斷 */
  async checkSocialLogin(payload) {
    const mchannel = payload.fb_account ? 'fb' : 'google';
    const maccount = payload.fb_account
      ? payload.fb_account
      : payload.google_account;
    const mname = payload.name;

    payload = await this.apiMember.MemberOAUTH(payload);
    this.doCloseLogin();
    if (payload.status === 200) {
      this.cds.userInfo = payload.data;
      this.cds.gMobileToken = payload.access_token;

      if (this.xPop) {
        this.xlogin.emit();
      } else {
        this.router.navigate(['/member'], {
          replaceUrl: true,
        });
      }
    } else {
      /** 60002 會員不存在 */
      /** 60004  ?????  */
      if (payload.error.status === 60002 || payload.error.status === 60004) {
        if (this.xPop) {
          /** 第三方註冊流程 */
          if (payload.error.status === 60002) {
            this.xlogin.emit({
              channel: mchannel,
              type: 'social',
              account: maccount,
              name: mname,
            });
          }
        } else {
          this.router.navigate(['login/id-verification']);
        }
      } else {
        /** 例外錯誤處理 */
        this.router.navigateByUrl('login');
      }
    }
  }
  /** 測試用login pop */
  popLogin() {
    $('#popLogin').modal('show');
  }

  /** 關閉login彈窗 */
  doCloseLogin() {
    $('#model-Login').modal('hide');
  }

  goFindAccount() {
    this.xforgetPassword.emit();
    // this.router.navigateByUrl('login/find-account');
  }
  goRegister() {
    this.xRegister.emit('');
    // this.router.navigateByUrl('login/register-id');
  }
  ngOnInit(): void {
    /** 砍掉FB的cookie */

    StorageService.removeItem(StorageKeys.c_user);
    // if (this.cds.userInfo.seq_no && this.cds.gMobileToken) {
    //   this.router.navigateByUrl(localStorage.getItem('prevPage'));
    //   return;
    // }
    /** 表單驗證項目 */
    this.validators = {
      user_account: ['', [Validators.required, this.vi.email]],
      user_password: [
        '',
        [Validators.required, this.vi.vnLength8to12, this.vi.vnPasswordRules],
      ],
      // user_password: ['', [this.vi.vnLength8to12]],
    };
    this.formValidate();
    this.checkView();
    this.clearUserDate();
  }
  async doSubmit() {
    /** 解除封印 允許後檢核 */
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });

    /** 檢查判斷是否驗證成功 */
    if (this.group.status !== 'VALID' && this.router.url === '/login') {
      let resultError;
      for (const key in this.controls) {
        if (!resultError && this.controls[key].invalid) {
          resultError = key;
        }
      }
      if (resultError) {
        CommonService.scrollto(
          `lyc_${resultError}`,
          $('.header-main').height()
        );
      }
      return;
    }
    if (this.group.status !== 'VALID') {
      return;
    }

    let payload: any = {
      email: this.controls.user_account.value,
      kor: this.controls.user_password.value,
    };
    payload = await this.apiMember.MemberCathayLogin(payload);
    if (payload.status === 200) {
      this.doCloseLogin();
      this.cds.userInfo = payload.data;
      StorageService.setItem(StorageKeys.mobileToken, payload.access_token);
      this.cds.gMobileToken = payload.access_token;
      if (this.xPop) {
        this.xlogin.emit();
      } else {
        if (this.cds.gPre_url) {
          // 取得上一頁網址
          const getUrl = new URL(location.href);
          const getPrevUrl = new URL(
            getUrl.href.replace(getUrl.pathname, '') + this.cds.gPre_url
          );
          const urlParamsObj = {};
          if (getPrevUrl && getPrevUrl.searchParams) {
            for (const pair of (getPrevUrl.searchParams as any).entries()) {
              urlParamsObj[pair[0]] = pair[1];
            }
            this.router.navigate([getPrevUrl.pathname], {
              queryParams: urlParamsObj,
            });
          } else {
            this.router.navigate([this.cds.gPre_url]);
          }
        } else {
          this.location.back();
        }
        this.cds.gPre_url = '';
      }
    } else {
      /**
       * 50004 密碼錯誤
       * 50099 帳號已鎖
       * 60002: 找不到資料
       * 60004 尚未建立密碼
       * */
      const errorStatus = [60002, 50004, 50099, 60004];

      // 如果帳號已鎖，則開啟 modal
      if (payload.error.status === 50099) {
        $('#Modal-13').modal('show');
        return;
      }

      const errorCheck = !!errorStatus.find(
        (item) => item === payload.error.status
      );
      if (errorCheck) {
        this.FLoginError = true;
      } else {
        /** 例外錯誤處理 */
        this.FLoginError = true;
        this.router.navigate(['/login'], {
          replaceUrl: true,
        });
      }
    }
  }

  checkView() {
    /** 處理彈窗畫面高度問題 */
    if (document.body.clientHeight < 650) {
      $('.modal-dialog').css({
        'min-height': 'calc(100% - 1rem)',
        margin: '0.5rem auto',
      });
      $('.btn.btn-primary').css({
        padding: '6px 9px 4px',
        'font-size': '1rem',
      });
      $('.btn.btn-white').css({
        padding: '6px 9px 4px',
        'font-size': '1rem',
      });
      $('.explain').css({
        'font-size': '0.875rem',
      });
    }

    /** 判斷如果在 login頁面時，按鈕寬度是250px */
    if (this.router.url === '/login') {
      this.FLogin = true;
      $('.btn').css('width', '250px');
      $('.jobdetails').removeClass('pop');
      if (this.cds.gLangType === 'zh-TW') {
        return;
      }
      if (this.cds.gLangType === 'en-US') {
        return;
      }
      if (this.cds.gLangType === 'vi-VN') {
        $('.login-p .title').css('justify-content', 'space-around');
        $('.login-p .title').removeClass('width-pop');
        $('.login-p .title').addClass('width-change');
      }
    } else {
      this.FLogin = false;
      $('.jobdetails').addClass('pop');
      if (this.cds.gLangType === 'zh-TW') {
        return;
      }
      if (this.cds.gLangType === 'en-US') {
        return;
      }
      if (this.cds.gLangType === 'vi-VN') {
        $('.login-p .title').css('justify-content', 'space-around');
        $('.login-p .title').removeClass('width-change');
        $('.login-p .title').addClass('width-pop');
      }
    }
  }

  /** 註冊資料初始化 */
  clearUserDate() {
    Object.keys(this.cds.userInfoTemp).forEach((key) => {
      this.cds.userInfoTemp[key] = undefined;
    });
  }

  /** 帳號欄位切換至密碼欄位 */
  focusPassword() {
    $('#XPassword').focus();
  }

  /** 清除字串 */
  doClearInput(name: string) {
    this.group.controls[`${name}`].setValue('');
  }
}
