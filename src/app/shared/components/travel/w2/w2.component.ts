import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-w2',
  templateUrl: './w2.component.html',
  styleUrls: ['./w2.component.scss'],
})
export class W2Component implements OnInit {
  FLang: any;
  FLangType: string;
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang.C02.W2;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit(): void {}
}
