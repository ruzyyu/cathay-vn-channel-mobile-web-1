import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-t44',
  templateUrl: './t44.component.html',
  styleUrls: ['./t44.component.scss'],
})
export class T44Component implements OnInit {
  FLang: any;
  FLangType: string;
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang.C02.t44;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit(): void {}
}
