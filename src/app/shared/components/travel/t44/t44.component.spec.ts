import { ComponentFixture, TestBed } from '@angular/core/testing';

import { T44Component } from './t44.component';

describe('T44Component', () => {
  let component: T44Component;
  let fixture: ComponentFixture<T44Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ T44Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(T44Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
