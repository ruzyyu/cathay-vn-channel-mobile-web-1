import { ComponentFixture, TestBed } from '@angular/core/testing';

import { T44v1Component } from './t44v1.component';

describe('T44v1Component', () => {
  let component: T44v1Component;
  let fixture: ComponentFixture<T44v1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ T44v1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(T44v1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
