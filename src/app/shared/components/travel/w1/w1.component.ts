import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-w1',
  templateUrl: './w1.component.html',
  styleUrls: ['./w1.component.scss'],
})
export class W1Component implements OnInit {
  FLang: any;
  FLangType: string;
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang.C02.W1;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit(): void {}
}
