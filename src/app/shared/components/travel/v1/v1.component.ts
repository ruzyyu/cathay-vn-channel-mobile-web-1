import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-v1',
  templateUrl: './v1.component.html',
  styleUrls: ['./v1.component.scss'],
})
export class V1Component implements OnInit {
  FLang: any;
  FLangType: string;
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang.C02.v1;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit(): void {}
}
