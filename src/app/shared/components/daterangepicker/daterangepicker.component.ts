import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  HostListener,
} from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import * as moment from 'moment';

@Component({
  selector: 'app-daterangepicker',
  templateUrl: './daterangepicker.component.html',
  styleUrls: ['./daterangepicker.component.scss'],
})
export class DaterangepickerComponent
  implements OnInit, OnChanges, AfterViewInit {
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }

  @Input() placeholder: string;
  @Input() label: string;
  @Input() SDate: string;
  @Input() EDate: string;
  @Input() xid: string;
  @Input() value: string;

  @Output() getDate = new EventEmitter<any>();
  @Output() cancel = new EventEmitter<any>();

  Fid: string;
  datapickerValue = undefined;
  FLang: any;
  byPrevious;
  FEvType;

  FStartDate: string;
  FEndDate: string;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    setTimeout(() => {
      const tableHeight = $('.calendar-table').height();
      $('.dateRange').css('bottom', `${tableHeight}px`);
    }, 300);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.value && changes.value.currentValue) {
      $(() => {
        const msplit = changes.value.currentValue.split('-');
        const mid = `#${this.xid}`;
        if (Array.isArray(msplit) && msplit.length > 0) {
          this.FEvType = 'apply';
          if ($(mid)) {
            const picker = $(mid).data('daterangepicker');

            if (picker) {
              picker.setStartDate(msplit[0]);
              picker.setEndDate(msplit[1]);
            }
          }
        }
      });
      /** 帽子高度 */
      const tableHeight = $('.calendar-table').height();
      $('.dateRange').css('bottom', `${tableHeight}px`);
    }

    // if (changes.value && changes.value.currentValue) {

    //   const msplit = changes.value.currentValue.split('~');
    //   const mid = `#${this.xid}`;

    //   if (Array.isArray(msplit) && msplit.length > 0) {
    //     $(mid).data('daterangepicker').setStartDate(msplit[0]);
    //     $(mid).data('daterangepicker').setEndDate(msplit[1]);
    //   }
    // }
  }
  Dodaterangepicker(linkedCalendars) {
    const minDate: any = this.SDate ? new Date(this.SDate) : new Date();
    const maxDate: any = this.EDate
      ? new Date(this.EDate)
      : moment(new Date()).add(1, 'years').toDate();

    $(this.Fid).daterangepicker({
      opens: 'left',
      autoApply: true,
      showDropdowns: false,
      singleDatePicker: false,
      linkedCalendars,
      // 可選擇日期為 當前日期+1年
      minYear: minDate.getFullYear(),
      minDate,
      maxYear: maxDate.getFullYear(),
      maxDate,
      // 最大區間
      maxSpan: {
        days: 90,
      },
      locale: {
        format: 'DD/MM/YYYY',
        separator: ' - ',
        applyLabel: '送出',
        cancelLabel: '取消',
        daysOfWeek: [
          this.cds.gLang.dateRangePicker.week.W0007,
          this.cds.gLang.dateRangePicker.week.W0001,
          this.cds.gLang.dateRangePicker.week.W0002,
          this.cds.gLang.dateRangePicker.week.W0003,
          this.cds.gLang.dateRangePicker.week.W0004,
          this.cds.gLang.dateRangePicker.week.W0005,
          this.cds.gLang.dateRangePicker.week.W0006,
        ],
        monthNames: [
          this.cds.gLang.dateRangePicker.month.M0001,
          this.cds.gLang.dateRangePicker.month.M0002,
          this.cds.gLang.dateRangePicker.month.M0003,
          this.cds.gLang.dateRangePicker.month.M0004,
          this.cds.gLang.dateRangePicker.month.M0005,
          this.cds.gLang.dateRangePicker.month.M0006,
          this.cds.gLang.dateRangePicker.month.M0007,
          this.cds.gLang.dateRangePicker.month.M0008,
          this.cds.gLang.dateRangePicker.month.M0009,
          this.cds.gLang.dateRangePicker.month.M0010,
          this.cds.gLang.dateRangePicker.month.M0011,
          this.cds.gLang.dateRangePicker.month.M0012,
        ],
      },
    });
  }
  daterangepicker() {
    const mid = `#${this.xid}`;
    $(window).ready(() => {
      if ($(window).width() <= 610) {
        this.Dodaterangepicker(false);
      } else {
        this.Dodaterangepicker(true);
      }
      this.doDOM();
      $(mid).val(this.placeholder);
      if (this.value) {
        $(this.Fid).val(this.value);
      }

      if (this.FStartDate) {
        $(mid).data('daterangepicker').setStartDate(this.FStartDate);
        $(mid).data('daterangepicker').setEndDate(this.FEndDate);
      }

      /** 直接關閉日曆時觸發 */
      $(mid).on('hide.daterangepicker', (ev) => {
        if (this.FEvType !== 'apply') {
          $(mid).val('');
          this.getDate.emit(undefined);
        }
      });

      /** 直接日曆送出時觸發 */
      $(mid).on('apply.daterangepicker', (ev, picker) => {
        /** 判斷range日期不相等 */
        if (
          picker.startDate.format('DD/MM/YYYY') ===
          picker.endDate.format('DD/MM/YYYY')
        ) {
          $(mid).val('');
          this.getDate.emit(undefined);
          $(mid).removeClass('date1');
          this.FEvType = undefined;
          return;
        }else{
          $(mid).addClass('date1');
        }
        /** 畫面顯示格式 */
        const startDayView = picker.startDate.format('DD/MM/YYYY');
        const endDayView = picker.endDate.format('DD/MM/YYYY');
        this.datapickerValue = `${startDayView}-${endDayView}`;
        $(mid).val(this.datapickerValue);
        /** 日期送出格式 */
        const startDay = picker.startDate.format('YYYY-MM-DD');
        const endDay = picker.endDate.format('YYYY-MM-DD');
        this.FEvType = 'apply';
        this.getDate.emit({ startDay, endDay });
      });
    });
  }
  doShowDate() {
    $(this.Fid).click();
  }

  /** 調整日期DOM位置 */
  doDOM() {
    const getButton = document.querySelector('.drp-buttons');
    const getDrpSelected = document.querySelector('.drp-selected');
    getButton.removeChild(getDrpSelected);

    const dateRangePicker = document.querySelector('.daterangepicker');
    const creatNode = document.createElement('div');
    creatNode.className = 'dateRange';
    /** 日期顯示時觸發，用以控制時間顯示 */
    $(this.Fid).on('showCalendar.daterangepicker', (ev, picker) => {
      const startDate = picker.startDate.format('DD-MM-YYYY');
      const endDay = picker.endDate.format('DD-MM-YYYY');
      creatNode.innerHTML = `
      <div class="showDate">
          <div>${this.cds.gLang.dateRangePicker.start}</div>
          <div>${startDate}</div>
      </div>
      <div class="showDate">
          <div>${this.cds.gLang.dateRangePicker.end}</div>
          <div>${endDay}</div>
      </div>`;
      setTimeout(() => {
        // 除了第一行以外的所有行數 若第一列為 .weekend.off.ends.available 則隱藏
        $(
          '.weekend.off.ends.available[data-title$="c0"]:not([data-title^="r0"]'
        )
          .parent()
          .hide();
        // 第一行若最後一列為 .weekend.off.ends.available 則隱藏
        $('.weekend.off.ends.available[data-title^="r0c6"]').parent().hide();
        // 最後一行若第一列為 .weekend.off.ends.off.disabled 則隱藏
        $('.weekend.off.ends.off.disabled[data-title^="r5c0"]').parent().hide();
        const tableHeight = $('.calendar-table').height();
        $('.dateRange').css('bottom', `${tableHeight}px`);
        // 處理被datepicker 覆蓋的樣式
        if (window.innerWidth < 627) {
          $('.off.ends:not([data-title^="r0"]').css('display', `inline-block`);
        }
      }, 0);

      /** 點擊日期(天數)時觸發 */
      $(this.Fid).on('clickDate.daterangepicker', () => {
        // 除了第一行以外的所有行數 若第一列為 .weekend.off.ends.available 則隱藏
        $(
          '.weekend.off.ends.available[data-title$="c0"]:not([data-title^="r0"]'
        )
          .parent()
          .hide();
        // 第一行若最後一列為 .weekend.off.ends.available 則隱藏
        $('.weekend.off.ends.available[data-title^="r0c6"]').parent().hide();
        // 最後一行若第一列為 .weekend.off.ends.off.disabled 則隱藏
        $('.weekend.off.ends.off.disabled[data-title^="r5c0"]').parent().hide();
        // 處理被datepicker 覆蓋的樣式
        if (window.innerWidth < 627) {
          $('.off.ends:not([data-title^="r0"]').css('display', `inline-block`);
        }
      });
    });
    const getLeft = document.querySelector('.drp-calendar.left');
    dateRangePicker.insertBefore(creatNode, getLeft);
  }
  openData() {
    $('.daterangepicker ').css('display', 'block');
  }
  ngOnInit(): void {
    $('.daterangepicker').remove();
    this.Fid = `#${this.xid}`;
    this.daterangepicker();

    /** 點擊背景時關閉日期 */
    setTimeout(() => {
      $('.ranges').on('click', () => {
        $('.daterangepicker').css('display', 'none');
        //     $(`#${this.xid}`).val('');
        //     this.getDate.emit(undefined);
      });
    }, 0);
  }

  ngAfterViewInit() {
    if (this.value) {
      const msplit = this.value.split('-');
      const mid = `#${this.xid}`;

      console.log(msplit);

      if (Array.isArray(msplit) && msplit.length > 0) {
        this.FStartDate = msplit[0];
        this.FEndDate = msplit[1];
        // $(mid).data('daterangepicker').setStartDate(msplit[0]);
        // $(mid).data('daterangepicker').setEndDate(msplit[1]);
      }
    }
    setTimeout(() => {
      if (!this.value) {
        if (this.SDate) {
          const date = this.SDate;
          const mid = `#${this.xid}`;
          if ($(mid) && moment(date) > moment()) {
            const daterangepicker = $(mid).data('daterangepicker');
            daterangepicker.setStartDate(date);
            daterangepicker.setEndDate(date);
            $(mid).val('');
          }
        }
      }
    }, 200);
  }
}
