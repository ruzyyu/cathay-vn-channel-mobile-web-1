import { MemberService } from './../../../core/services/api/member/member.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AlertServiceService } from './../../../core/services/alert/alert-service.service';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { UserInfo, IImageFile } from './../../../core/interface/index';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-identity',
  templateUrl: './identity.component.html',
  styleUrls: ['./identity.component.scss'],
})
export class IdentityComponent implements OnInit {
  // @Input() data: any;
  @Output() xAction = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    private alter: AlertServiceService,
    private vi: ValidatorsService,
    private router: Router,
    private cds: DatapoolService,
    private api: MemberService,
    private sanitizer: DomSanitizer,
    private apiMember: MemberService
  ) {
    this.FLang = this.cds.gLang;
  }
  /** 宣告區 */

  FFrontimg: any;
  FBackimg: any;
  FFrontimgFile: IImageFile;
  FBackimgFile: IImageFile;
  userInfo: UserInfo;
  validators: {};
  group: FormGroup;
  controls;
  FLang: any;
  /** 檢驗id長度 */
  idLength: number;
  /** 圖片格式錯誤 */
  fronterrorMsg: string;
  backerrorMsg: string;
  /** 建立驗證表單 */
  formValidate() {
    // 建立驗證表單
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });

    this.group = this.fb.group(validator, {
      updateOn: 'blur',
    });

    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol && xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];
      return (
        {
          consistency: this.FLang.F07.content.F0046,
          required: '',
          vnCertNumber: this.FLang.ER.ER036,
          vn2020ID:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
          equalLength:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES001,
        }[type] || this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES001
      );
    }
    return this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES001;
  }

  /** 檢查ID */
  checkIDLength() {
    if (this.cds.userInfo.certificate_number_9) {
      this.idLength = 12;
    }
    if (this.cds.userInfo.certificate_number_12) {
      this.idLength = 9;
    }
  }

  ngOnInit(): void {
    this.userInfo = this.cds.userInfo;
    this.checkIDLength();

    this.validators = {
      idnum: [
        '',
        {
          validators: [Validators.required, this.vi.equalLength(this.idLength)],
          updateOn: 'change',
        },
      ],
      frontfile: ['', [Validators.required]],
      bkfile: ['', [Validators.required]],
    };

    this.formValidate();
  }

  async setfile(event) {
    const imgFile = event.target.files[0];

    /** 判斷圖片類型 */
    const fileTypeArr: any = ['.jpg', '.jpeg', '.heic', '.heif'];
    let filesType = imgFile.name;
    filesType = filesType.substring(filesType.lastIndexOf('.'));

    /** 圖片重載 */
    if (fileTypeArr.indexOf(filesType) < 0) {
      this.fronterrorMsg = this.FLang.F05.F0068;
      return;
    }

    // 直接取代圖片
    const filesize = imgFile.size;
    const reader = new FileReader();
    if (filesize <= 7340032 && !(fileTypeArr.indexOf(filesType) < 0)) {
      // 上傳
      const payload = { image: imgFile };

      const rep = await this.api.upload(payload);
      if (rep.status === 200) {
        this.FFrontimgFile = {
          imgName: rep.data.filename,
          imgType: imgFile.type,
        };
        this.controls.frontfile.setValue(imgFile.name);
        reader.readAsDataURL(imgFile); // base 64
        reader.onload = () => {
          this.FFrontimg = reader.result;
        };
      } else {
        this.FFrontimg = null;
      }
    } else {
      this.FFrontimg = null;
    }
  }

  async setbkfile(event) {
    const imgFile = event.target.files[0];

    /** 判斷圖片類型 */
    const fileTypeArr: any = ['.jpg', '.jpeg', '.heic', '.heif'];
    let filesType = imgFile.name;
    filesType = filesType.substring(filesType.lastIndexOf('.'));

    /** 圖片重載 */
    this.backerrorMsg = '';
    if (fileTypeArr.indexOf(filesType) < 0) {
      this.backerrorMsg = this.FLang.F05.F0068;
      return;
    }

    // 直接取代圖片
    const filesize = imgFile.size;
    const reader = new FileReader();
    if (filesize <= 7340032 && !(fileTypeArr.indexOf(filesType) < 0)) {
      // 上傳
      const payload = { image: imgFile };
      const rep = await this.api.upload(payload);
      if (rep.status === 200) {
        this.controls.bkfile.setValue(imgFile.name);
        this.FBackimgFile = {
          imgName: rep.data.filename,
          imgType: imgFile.type,
        };
        reader.readAsDataURL(imgFile);
        reader.onload = () => {
          this.FBackimg = reader.result;
        };
      } else {
        this.FBackimg = null;
      }
    } else {
      this.FBackimg = null;
    }
  }

  /** 回上一層 */
  doback() {
    this.xAction.emit();
  }

  /** 呼叫API進行存檔 */
  async doSubmit() {
    /** 解除封印 允許後檢核 */
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });

    // 解鎖id驗證
    this.group
      .get('idnum')
      .setValidators([
        this.vi.vnCertNumber(this.cds.userInfo.birthday, this.cds.userInfo.sex),
        this.vi.vn2020ID(this.cds.userInfo.birthday),
        this.vi.equalLength(this.idLength),
      ]);
    this.group.get('idnum').updateValueAndValidity();

    /** 檢查判斷是否驗證成功 */
    if (this.group.status !== 'VALID') {
      let resultError;
      for (const key in this.controls) {
        if (!resultError && this.controls[key].invalid) {
          resultError = key;
        }
      }
      if (resultError) {
        $('.modal-body').animate(
          // { scrollTop: $(`#lyc_idnum`).offset().top - $(`#lyc_idnum`).offset().top },
          { scrollTop: 0 },
          'show'
        );
      }
      return;
    }
    if (this.group.status !== 'VALID') {
      return;
    }

    // stop here if form is invalid
    if (this.group.valid) {
      // 圖片上傳
      const payload = {
        language: this.cds.gLangType,
        seq_no: this.cds.userInfo.seq_no,
        certificate_number_new: this.controls.idnum.value,
        img_kind: `${this.FFrontimgFile.imgType},${this.FBackimgFile.imgType}`,
        img_name: `${this.FFrontimgFile.imgName},${this.FBackimgFile.imgName}`,
        is_link_id: this.cds.userInfo.is_link_id === 'N' ? 'A' : 'R',
      };

      const rep = await this.api.MemberRegID(payload);
      if (rep.status === 200) {
        this.cds.userInfo.new_id = this.controls.idnum.value;
        this.cds.userInfo.is_link_id =
          this.cds.userInfo.is_link_id === 'N' ? 'A' : 'R';

        // 更新 使用者資料
        this.apiMember.reflashUserInfo(payload);
        // 呼叫API 開啟Alter回到主畫面
        this.alter.open({
          type: 'success',
          title: this.FLang.F07.content.F0048,
          content: '',
          callback: this.doback.bind(this),
        });
      }
    }
  }

  /** 清除圖片 */
  clearImg(xtype: number) {
    if (xtype === 1) {
      this.FFrontimg = null;
      this.controls.frontfile.setValue('');
    }
    if (xtype === 2) {
      this.FBackimg = null;
      this.controls.bkfile.setValue('');
    }
  }

  // Call this method in the image source, it will sanitize it.

  sFrontimg() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.FFrontimg);
  }

  sBackimg() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.FBackimg);
  }
}
