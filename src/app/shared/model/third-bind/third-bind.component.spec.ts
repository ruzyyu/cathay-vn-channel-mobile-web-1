import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdBindComponent } from './third-bind.component';

describe('ThirdBindComponent', () => {
  let component: ThirdBindComponent;
  let fixture: ComponentFixture<ThirdBindComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThirdBindComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdBindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
