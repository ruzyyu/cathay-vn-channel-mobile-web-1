import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-alter',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlterComponent implements OnInit {
  title: string;
  content: string;
  type: string;
  callback: any;
  submitTitle: string;
  FLang: any;
  constructor(
    private alert: AlertServiceService,
    private router: Router,
    private cds: DatapoolService
  ) {
    this.FLang = this.cds.gLang;
    this.alert.regiditsv().subscribe((obj) => {
      switch (obj.action) {
        case 'open':
          const {
            type,
            title,
            content,
            callback,
            submitTitle = this.FLang.AlertButton.A0001,
          } = obj;
          this.title = title;
          this.content = content;
          this.type = type;
          this.callback = callback;
          this.submitTitle = submitTitle;

          $('#alter').modal('show');

          /** 修正齊頭齊尾排版問題 */
          setTimeout(() => {
            const judgeHeight = $('.judge_height');
            judgeHeight.height() > 30
              ? judgeHeight.addClass('custom_text_align')
              : judgeHeight.removeClass('custom_text_align');
          }, 200);

          break;

        case 'close':
          $('#alter').modal('hide');
          break;
      }
    });
  }

  doClose() {
    // 實例化
    if (this.callback) {
      this.callback();
    }
  }

  ngOnInit(): void {}
}
