import { IConfirm } from './../../../../core/interface/index';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
})
export class ConfirmComponent implements OnInit {
  @Input() data: IConfirm;
  @Output() ActionEvent = new EventEmitter<boolean>();

  // gPub: IConfirm;

  constructor() {}

  get gPub(): IConfirm {
    return (
      this.data || {
        title: '',
        content: '',
        type: '',
        btnConfirm: '確定',
        btnCancel: '再想想',
      }
    );
  }

  doAction(type: boolean) {
    this.ActionEvent.emit(type);
  }

  ngOnInit(): void {
    // TS無法這樣解構
    //  const { title, content, type } = this.data;
  }
}
