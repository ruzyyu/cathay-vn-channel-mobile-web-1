import { Component, Input, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-contact-service',
  templateUrl: './contact-service.component.html',
  styleUrls: ['./contact-service.component.scss'],
})
export class ContactServiceComponent implements OnInit {
  @Input() title;

  @Input() infoOne;

  @Input() infoTwo;
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }
  FLang: any;
  ngOnInit(): void {}
}
