import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OtpVerificationRoutingModule } from './otp-verification-routing.module';
import { OtpVerificationComponent } from './otp-verification.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [OtpVerificationComponent],
  imports: [
    CommonModule,
    OtpVerificationRoutingModule,
    FormsModule,
    SharedModule,
  ],
})
export class OtpVerificationModule {}
