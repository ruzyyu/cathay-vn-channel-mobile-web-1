import { CommonService } from 'src/app/core/services/common.service';
import { StorageKeys } from './../../../core/enums/storage-key.enum';
import { StorageService } from './../../../core/services/storage.service';
import { SocialService } from '../../../core/services/social.service';
import { UserInfoTemp, IConfirm, OtpCode } from '../../../core/interface/index';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

import {
  Validators,
  FormBuilder,
  FormGroup,
} from '@angular/forms';

import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
@Component({
  selector: 'app-otp-verification',
  templateUrl: './otp-verification.component.html',
  styleUrls: ['./otp-verification.component.scss'],
})
export class OtpVerificationComponent implements OnInit {
  constructor(
    private alert: AlertServiceService,
    private fb: FormBuilder,
    private apiMember: MemberService,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private router: Router,
    private auth: SocialService
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
    /** 需有信箱跟手機才能進此頁面 */
    if (!(this.cds.userInfoTemp.email && this.cds.userInfoTemp.mobile)) {
      this.router.navigate(['login'], { replaceUrl: true });
    }
  }
  /** POP */
  popTitle: string;
  popInfoOne: string;
  popInfoTwo: string;
  /** 宣告區Start */
  FConfirm: IConfirm;
  userInfoTemp: UserInfoTemp;
  otpCode: OtpCode;
  validators: {};
  group: FormGroup;
  controls;
  FLang: any;
  FLangType: any;
  /** 初始顯示 email 與  mobile */
  emailFormat = undefined;
  mobileFormat = undefined;
  otpError = false;
  setTimeOne = 600;
  setTimeTwo = 60;
  TimeOneEnd = false;
  TimeTwoEnd = false;
  loadTime = false;

  /** OTP驗證次數 */
  FCount = 1;
  /** 建立驗證表單 */
  formValidate() {
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });
    this.group = this.fb.group(validator);
    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  /** 點擊backspace執行 */
  keyupTwo() {
    // this.focusUp('otpTwo');
    if (!this.controls.otpTwo.value) { this.focusUp('otpTwo'); }
  }
  keyupThr() {
    // this.focusUp('otpThr');
    if (!this.controls.otpThr.value) { this.focusUp('otpThr'); }
  }
  keyupFou() {
    // this.focusUp('otpFou');
    if (!this.controls.otpFou.value) { this.focusUp('otpFou'); }
  }
  keyupFiv() {
    // this.focusUp('otpFiv');
    if (!this.controls.otpFiv.value) { this.focusUp('otpFiv'); }
  }
  keyupSix() {
    // this.focusUp('otpSix');
    if (!this.controls.otpSix.value) { this.focusUp('otpSix'); }
  }
  inputOne() {
    if (/[0-9]/.test(this.controls.otpOne.value)) {
      this.doFontLengthSet(this.controls.otpOne.value, 'otpOne');
      this.focusDown('otpOne');
    }
  }
  inputTwo() {
    if (/[0-9]/.test(this.controls.otpTwo.value)) {
      this.doFontLengthSet(this.controls.otpTwo.value, 'otpTwo');
      this.focusDown('otpTwo');
    }
  }
  inputThr() {
    if (/[0-9]/.test(this.controls.otpThr.value)) {
      this.doFontLengthSet(this.controls.otpThr.value, 'otpThr');
      this.focusDown('otpThr');
    }
  }
  inputFou() {
    if (/[0-9]/.test(this.controls.otpFou.value)) {
      this.doFontLengthSet(this.controls.otpFou.value, 'otpFou');
      this.focusDown('otpFou');
    }
  }
  inputFiv() {
    if (/[0-9]/.test(this.controls.otpFiv.value)) {
      this.doFontLengthSet(this.controls.otpFiv.value, 'otpFiv');
      this.focusDown('otpFiv');
    }
  }
  inputSix() {
    this.doFontLengthSet(this.controls.otpSix.value, 'otpSix');
    this.submitHandler();
  }

  /** 切割input內字串，使其回傳最新更改的一個值 */
  doFontLengthSet(value, valueName: any) {
    if (value.length > 1 && value) {
      const viewValue = value.slice(1, 2);
      switch (valueName) {
        case 'otpOne':
          this.group.patchValue({
            otpOne: viewValue,
          });
          break;
        case 'otpTwo':
          this.group.patchValue({
            otpTwo: viewValue,
          });
          break;
        case 'otpThr':
          this.group.patchValue({
            otpThr: viewValue,
          });
          break;
        case 'otpFou':
          this.group.patchValue({
            otpFou: viewValue,
          });
          break;
        case 'otpFiv':
          this.group.patchValue({
            otpFiv: viewValue,
          });
          break;
        case 'otpSix':
          this.group.patchValue({
            otpSix: viewValue,
          });
          break;
      }
    }
  }
  /** 回登入頁 */
  goLogin() {
    this.router.navigate(['login'], { replaceUrl: true });
  }
  /** 註冊 */
  doRegister() {
    this.cds.userInfo.is_set_pw = false;
    this.router.navigate(['login/change-password'], { replaceUrl: true });
  }
  /** 第三方資料綁定 */
  async doBind() {
    let payload: UserInfoTemp;
    payload = {
      seq_no: 0,
      customer_name: this.userInfoTemp.customer_name.trim(),
      certificate_number_9: this.userInfoTemp.certificate_number_9 || '',
      certificate_number_12: this.userInfoTemp.certificate_number_12 || '',
      birthday: this.userInfoTemp.birthday,
      email: this.userInfoTemp.email,
      email_bk: '',
      mobile: this.userInfoTemp.mobile,
      sex: this.userInfoTemp.sex,
      county: this.userInfoTemp.county,
      province: this.userInfoTemp.province,
      addr: this.userInfoTemp.addr,
      is_link_id: 'N',
      fb_account: this.userInfoTemp.fb_account,
      fb_title: this.userInfoTemp.fb_title,
      google_account: this.userInfoTemp.google_account,
      google_title: this.userInfoTemp.google_title,
      is_link_sns: 'Y',
      is_set_pw: 'N',
      kor: '',
      is_validate_policy: 'N',
    };
    const register = await this.apiMember.MemberReg(payload);
    if (register.status === 200) {
      /** 線上試算 第三方流程 */
      const mbol = await this.onlineProject_flow(payload);
      if (mbol) { return; }

      this.doUserDataClear();
      this.doBindDataClear();
      this.alert.open({
        type: 'success',
        title: this.FLang.B055.content.B0011,
        content: this.FLang.B055.content.B0012,
        callback: this.goLogin,
      });
    } else {
      /** 例外錯誤處理 */
      this.router.navigate(['login'], { replaceUrl: true });
    }
  }
  doFind() {
    this.cds.userInfo.is_set_pw = false;
    this.router.navigate(['login/change-password'], { replaceUrl: true });
  }
  async submitHandler() {
    if (
      !(
        this.cds.userInfoTemp.email &&
        this.cds.userInfoTemp.mobile &&
        this.controls.otpOne.value &&
        this.controls.otpTwo.value &&
        this.controls.otpThr.value &&
        this.controls.otpFou.value &&
        this.controls.otpFiv.value &&
        this.controls.otpSix.value
      )
    ) {
      return (this.otpError = true);
    }

    const otpCode =
      this.controls.otpOne.value +
      this.controls.otpTwo.value +
      this.controls.otpThr.value +
      this.controls.otpFou.value +
      this.controls.otpFiv.value +
      this.controls.otpSix.value;

    /** 如果時間到，回傳驗證碼失效 */
    if (this.TimeOneEnd) {
      this.otpError = true;
      return;
    }

    /** API 呼叫 */
    const payload = {
      mobile: this.cds.userInfoTemp.mobile,
      otp: otpCode,
    };
    const otpVerification = await this.apiMember.MemberVerifyOTP(payload);
    if (otpVerification.status === 200) {
      // if (otpCode === '123456') {

      switch (this.cds.gPubCommon.gOtpAuto) {
        /** 會員註冊 須設定密碼 */
        case 'register':
          this.doRegister();
          break;
        case 'bind':
          this.doBind();
          break;
        case 'find':
          this.doFind();
          break;
        default:
          this.router.navigate(['login'], { replaceUrl: true });
          break;
      }
    } else {
      // if (otpVerification['error'].status === 60006) {
      // 驗證失敗 60006
      this.otpError = true;
      // } else {
      // 例外錯誤處理
      // this.otpError = true;
      // }
    }
  }

  /** 線上試算註冊流程 */
  async onlineProject_flow(payload): Promise<boolean> {
    /** 試算註冊流程 */

    if (this.cds.gTCalc_to_save) {
      if (this.cds.gTCalc_to_save.saveorder) {
        const payload: any = {};
        this.userInfoTemp.google_account
          ? (payload.google_account = this.userInfoTemp.google_account)
          : (payload.fb_account = this.userInfoTemp.fb_account);

        const rep: any = await this.apiMember.MemberOAUTH(payload);

        if (rep.status === 200) {
          /** 此段流程屬於新增流程 */
          this.cds.gMobileToken = rep.access_token;
          this.cds.userInfo = rep.data;
          StorageService.setItem(StorageKeys.mobileToken, rep.access_token);
          const dataEncryption: any = CommonService.ObjEncryption(rep.data);
          CommonService.SaveUsertoSession(dataEncryption);

          this.cds.gTCalc_to_save.userinfo = rep.data;

          this.router.navigate(
            [
              `/online-insurance/project-trial-${this.cds.gTCalc_to_save.type}/payment`,
            ],
            {
              queryParams: {
                action: 'pay',
              },
              replaceUrl: true,
            }
          );
          this.doUserDataClear();
          return true;
        }
      }
    }
  }

  doUserDataClear() {
    /** 使用者 資料清空 */
    Object.keys(this.cds.userInfoTemp).forEach((key) => {
      this.cds.userInfoTemp[key] = undefined;
    });
    /** 使用者 資料清空 end */
  }
  doBindDataClear() {
    /** 第三方 資料清空 */
    Object.keys(this.auth.user).forEach((key) => {
      this.auth.user[key] = undefined;
    });
    /** 第三方 資料清空 */
  }
  showContactService() {
    $('#popContactService').modal('show');
  }

  focusDown(value: string) {
    switch (value) {
      case 'otpOne':
        document.getElementsByName('otpTwo')[0].focus() as unknown;
        break;
      case 'otpTwo':
        document.getElementsByName('otpThr')[0].focus() as unknown;
        break;
      case 'otpThr':
        document.getElementsByName('otpFou')[0].focus() as unknown;
        break;
      case 'otpFou':
        document.getElementsByName('otpFiv')[0].focus() as unknown;
        break;
      case 'otpFiv':
        document.getElementsByName('otpSix')[0].focus() as unknown;
        break;
    }
  }
  focusUp(value: string) {
    switch (value) {
      case 'otpSix':
        document.getElementsByName('otpFiv')[0].focus() as unknown;
        break;
      case 'otpFiv':
        document.getElementsByName('otpFou')[0].focus() as unknown;
        break;
      case 'otpFou':
        document.getElementsByName('otpThr')[0].focus() as unknown;
        break;
      case 'otpThr':
        document.getElementsByName('otpTwo')[0].focus() as unknown;
        break;
      case 'otpTwo':
        document.getElementsByName('otpOne')[0].focus() as unknown;
        break;
    }
  }
  /**
   * @param  {boolean} value 倒數計數器回傳值，正常為true
   */
  timeOne(value: boolean) {
    this.TimeOneEnd = value;
    if (!value) { return; }
    $('.vcode-input').attr('disabled', 'disabled');
  }
  /**
   * @param  {boolean} value 倒數計數器回傳值，正常為true
   */
  timeTwo(value: boolean) {
    this.TimeTwoEnd = value;
  }
  async timeRestart() {
    /** 第一次點擊時 發送簡訊與重置計時器(計時器切換) */
    if (this.FCount < 3) {
      this.FCount = this.FCount + 1;
      this.TimeOneEnd = false;
      $('.vcode-input').removeAttr('disabled');
      this.TimeTwoEnd = false;
      this.loadTime = !this.loadTime;
      /** 重置From值 */
      this.group.patchValue({
        otpOne: undefined,
        otpTwo: undefined,
        otpThr: undefined,
        otpFou: undefined,
        otpFiv: undefined,
        otpSix: undefined,
      });
      this.otpError = false;
      /** OTP 驗證碼發送 */
      let noticeAction;
      switch (this.cds.gPubCommon.gOtpAuto) {
        case 'register':
          noticeAction = 'reg';
          break;
        case 'find':
          noticeAction = 'cantlogin';
          break;
      }
      const payload = {
        email: this.cds.userInfoTemp.email,
        mobile: this.cds.userInfoTemp.mobile,
        notice_action: noticeAction,
      };
      const MemberSendOTP: any = this.apiMember.MemberSendOTP(payload);
      this.router.navigate(['login/otp-verification'], { replaceUrl: true });
    } else {
      this.showContactService();
    }
  }
  /** 切割初始E-mail與電話顯示格式 */
  textHide() {
    if (!this.cds.userInfoTemp.mobile || !this.cds.userInfoTemp.email) {
      return false;
    }
    /** 處理電話格式為 0912-xxx-789 */
    const mobileHide = this.cds.userInfoTemp.mobile;
    const mobileFirst = mobileHide.split('').splice(0, 4);
    const mobileSecond = '-xxx-';
    const mobileLast = mobileHide.split('').splice(7, 9);
    const mobileTotal = mobileFirst.join('') + mobileSecond + mobileLast.join('');
    this.mobileFormat = mobileTotal;
    /**
     * 處理電子郵件格式為五碼以上 sa**le@mail.com
     * 五碼或五碼以下s****@mail.com
     */
    const emailHide = this.cds.userInfoTemp.email;
    const emailSearch = emailHide.indexOf('@');
    if (emailSearch <= 5) {
      const emailStarLength = emailHide.split('').splice(1, emailSearch).length;
      const emailFirst = emailHide.split('').splice(0, 1);
      const emailSecond = [];
      for (let i = 0; i < emailStarLength - 1; i++) {
        emailSecond.push('*');
      }
      const emailLast = emailHide.split('').splice(emailSearch, emailHide.length);
      const emailTotal =
        emailFirst.join('') + emailSecond.join('') + emailLast.join('');
      this.emailFormat = emailTotal;
    } else {
      const emailStarLength = emailHide.split('').splice(2, emailSearch - 4)
        .length;
      const emailFirst = emailHide.split('').splice(0, 2);
      const emailSecond = [];
      for (let i = 0; i < emailStarLength; i++) {
        emailSecond.push('*');
      }
      const emailLast = emailHide
        .split('')
        .splice(emailSearch - 2, emailHide.length);
      const emailTotal =
        emailFirst.join('') + emailSecond.join('') + emailLast.join('');
      this.emailFormat = emailTotal;
    }
  }

  ngOnInit(): void {
    /** POP 設定 */
    this.popTitle = this.FLang.B014A.content.B0014;
    this.popInfoOne = this.FLang.B014A.content.B0015;
    this.popInfoTwo = this.FLang.B014A.content.B0016;

    this.textHide();

    this.otpCode = this.cds.otpCode;
    this.userInfoTemp = this.cds.userInfoTemp;
    /** 表單驗證項目 */
    this.validators = {
      otpOne: [this.otpCode.otpOne, [Validators.required]],
      otpTwo: [this.otpCode.otpTwo, [Validators.required]],
      otpThr: [this.otpCode.otpThr, [Validators.required]],
      otpFou: [this.otpCode.otpFou, [Validators.required]],
      otpFiv: [this.otpCode.otpFiv, [Validators.required]],
      otpSix: [this.otpCode.otpSix, [Validators.required]],
    };
    this.formValidate();
  }
}
