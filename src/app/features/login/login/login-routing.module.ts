import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MemberLoginComponent } from './login.component';

const routes: Routes = [{ path: '', component: MemberLoginComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberLoginRoutingModule { }
