import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MemberLoginRoutingModule } from './login-routing.module';
import { MemberLoginComponent } from './login.component';

import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [MemberLoginComponent],
  imports: [CommonModule, MemberLoginRoutingModule, FormsModule, SharedModule],
  exports: [MemberLoginComponent],
})
export class MemberLoginModule {}
