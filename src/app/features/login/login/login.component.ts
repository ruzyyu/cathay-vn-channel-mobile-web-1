import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Lang } from 'src/types';

@Component({
  selector: 'app-member-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class MemberLoginComponent implements OnInit {
  constructor(private cds: DatapoolService, private router: Router) {
    this.FLang = this.cds.gLang;
  }
  FLang: Lang;

  goFindAccount() {
    this.router.navigate(['login/find-account'], {
      replaceUrl: true,
    });
  }
  goRegister(value) {
    this.router.navigate(['login/register-id'], {
      replaceUrl: true,
    });
  }
  ngOnInit(): void {}
}
