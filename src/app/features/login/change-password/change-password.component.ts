import { IResponse } from './../../../core/interface/index';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { UserInfo, UserInfoTemp } from '../../../core/interface/index';
import { Component, OnInit } from '@angular/core';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/core/services/common.service';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private apiMember: MemberService,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private alert: AlertServiceService,
    private location: Location,
    private router: Router
  ) {
    this.FLang = this.cds.gLang;
    /** 需有信箱才能進此頁面 */
    if (!(this.cds.userInfoTemp.email || this.cds.userInfo.email)) {
      this.router.navigate(['login'], { replaceUrl: true });
    }
  }
  /** 宣告區Start */
  Fkorerror = false;
  userInfo: UserInfo;
  userInfoTemp: UserInfoTemp;
  validators: {};
  group: FormGroup;
  controls;
  FLang: any;

  get isSetpwd(): boolean {
    return this.userInfo.is_set_pw === 'Y';
  }

  /** 建立驗證表單 */
  formValidate() {
    // 建立驗證表單
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });
    this.group = this.group = this.fb.group(validator, {
      updateOn: 'blur',
    });

    // 再次輸入密碼異動時檢核
    this.group.get('confirmPassword').valueChanges.subscribe((item) => {
      if (item) {
        this.group
          .get('confirmPassword')
          .setValidators([
            Validators.required,
            this.vi.vnLength8to12,
            this.vi.vnPasswordRules,
            this.vi.reenter(
              this.controls.password.value
                ? this.controls.password.value
                : undefined
            ),
          ]);
        this.group.get('confirmPassword').updateValueAndValidity();
      }
    });
    this.group.get('password').valueChanges.subscribe((item) => {
      if (item) {
        this.group
          .get('confirmPassword')
          .setValidators([
            Validators.required,
            this.vi.vnLength8to12,
            this.vi.vnPasswordRules,
            this.vi.reenter(
              this.controls.password.value
                ? this.controls.password.value
                : undefined
            ),
          ]);
        this.group.get('confirmPassword').updateValueAndValidity();
      }
    });

    // 錯誤訊息顯示

    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          password:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES010,
        }[control_key];
      }
      /** 新檢核end */

      return (
        {
          LengthInterval: this.FLang.ER.ER022,
          passwordRules: this.FLang.ER.ER022,
          required: '',
          reenter: this.FLang.B015A.content.B0007,
        }[type] || this.FLang.ER.ER017
      );
    }
    return this.FLang.B015A.content.B0007;
  }

  /** 檢核密碼一致性 */
  // passwordValidators(control: AbstractControl) {
  //   return null;
  // }

  /** 清除字串 */
  doClearInput(name: string) {
    this.group.controls[`${name}`].setValue('');
  }

  /** 回上一層 */
  doback() {
    this.router.navigate(['member'], { replaceUrl: true });
  }
  /** 回登入頁 */
  goLogin() {
    this.router.navigate(['login'], { replaceUrl: true });
  }
  /** 回系統維護頁 */
  goMaintain() {
    this.router.navigate(['/member/maintain'], { replaceUrl: true });
  }
  async doRegister() {
    let certificate_number_9_store: string;
    let certificate_number_12_store: string;
    if (this.userInfoTemp.certificate_number_9) {
      certificate_number_9_store = this.userInfoTemp.certificate_number_9;
      certificate_number_12_store = '';
    }
    if (this.userInfoTemp.certificate_number_12) {
      certificate_number_9_store = '';
      certificate_number_12_store = this.userInfoTemp.certificate_number_12;
    }
    let payload: any;
    payload = {
      seq_no: 0,
      customer_name: this.userInfoTemp.customer_name.trim(),
      certificate_number_9: certificate_number_9_store,
      certificate_number_12: certificate_number_12_store,
      birthday: this.userInfoTemp.birthday,
      email: this.userInfoTemp.email,
      email_bk: '',
      mobile: this.userInfoTemp.mobile,
      sex: this.userInfoTemp.sex,
      county: this.userInfoTemp.county,
      province: this.userInfoTemp.province,
      addr: this.userInfoTemp.addr,
      is_link_id: 'N',
      fb_account: '',
      fb_title: '',
      google_account: '',
      google_title: '',
      is_link_sns: 'N',
      is_set_pw: 'Y',
      kor: this.controls.password.value,
      is_validate_policy: 'N',
    };
    const register = await this.apiMember.MemberReg(payload);
    if (register.status === 200) {
      /** 試算註冊中，回試算付款並登入 */

      if (this.cds.gTCalc_to_save) {
        if (this.cds.gTCalc_to_save.saveorder) {
          // 登入
          const payload = {
            email: this.userInfo.email,
            kor: this.controls.password.value,
          };

          const rep: IResponse = await this.apiMember.MemberCathayLogin(
            payload
          );
          if (rep.status === 200) {
            this.cds.gTCalc_to_save.userinfo = rep.data;
            this.router.navigate(
              [
                `/online-insurance/project-trial-${this.cds.gTCalc_to_save.type}/payment`,
              ],
              {
                queryParams: {
                  action: 'pay',
                },
                replaceUrl: true,
              }
            );
            this.doUserDataClear();
            return;
          }
        }
      }

      this.doUserDataClear();
      this.doRegisterSuccess();
    } else {
      if (register.error.status) {
        /** 例外錯誤處理 */
        this.router.navigate(['login'], { replaceUrl: true });
      }
    }
  }
  async doFind() {
    let payload: any;
    payload = {
      seq_no: this.cds.userInfoTemp.seq_no,
      is_set_pw: 'N',
      kor: this.controls.password.value,
      kor_old: '',
    };
    const findData = await this.apiMember.MemberSetPW(payload);
    if (findData.status === 200) {
      this.doUserDataClear();
      this.doFindPasswordChange();
    } else {
      if (findData.error.status) {
        /** 例外錯誤處理 */
        this.router.navigate(['login'], { replaceUrl: true });
      }
    }
  }
  async setPassword(isSetPw) {
    let payload: any;
    payload = {
      seq_no: this.cds.userInfo.seq_no,
      is_set_pw: isSetPw,
      kor: this.controls.password.value,
      kor_old: this.controls.kor.value,
    };

    const setData = await this.apiMember.MemberSetPW(payload);
    if (setData.status === 200) {
      this.doUserDataClear();
      this.doPasswordChange();
    } else {
      if (setData.error.status === 60008) {
        this.Fkorerror = true;
        if (this.Fkorerror) {
          CommonService.scrollto(`lyc_kor`, $('.header-main').height());
        }
      } else {
        /** 例外錯誤處理 */
        this.router.navigate(['login'], { replaceUrl: true });
      }
    }
  }
  doUserDataClear() {
    /** 使用者 資料清空 */
    Object.keys(this.cds.userInfoTemp).forEach((key) => {
      this.cds.userInfoTemp[key] = undefined;
    });
    /** 使用者 資料清空 end */
  }
  doRegisterSuccess() {
    this.alert.open({
      type: 'success',
      title: this.FLang.B015A.content.B0008, // 註冊完成
      callback: this.goLogin,
    });
  }

  doFindPasswordChange() {
    this.alert.open({
      type: 'success',
      title: this.FLang.F075.content.F0004, // 密碼變更完成
      callback: this.goLogin,
    });
  }

  doPasswordChange() {
    this.alert.open({
      type: 'success',
      title: this.FLang.F075.content.F0004, // 密碼變更完成
      callback: this.goMaintain,
    });
  }

  async doSubmit() {
    // 檢核舊密碼
    this.Fkorerror = false;
    /** 解除封印 允許後檢核 */
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });

    /** 檢查判斷是否驗證成功 */
    if (this.group.status !== 'VALID') {
      let resultError;
      for (const key in this.controls) {
        if (!resultError && this.controls[key].invalid) {
          resultError = key;
        }
      }
      if (resultError) {
        CommonService.scrollto(
          `lyc_${resultError}`,
          $('.header-main').height()
        );
      }
      return;
    }
    if (this.group.status !== 'VALID') {
      return;
    }

    // stop here if form is invalid
    if (this.group.invalid) {
      return;
    }

    /** 新密碼與再次輸入密碼一定要相等才往下執行 */
    if (
      !this.controls.email.value ||
      this.controls.password.value !== this.controls.confirmPassword.value
    ) {
      return;
    }
    switch (this.cds.gPubCommon.gOtpAuto) {
      /** 會員註冊 須設定密碼 */
      case 'register':
        this.doRegister();
        // 狀態碼清除
        this.cds.gPubCommon.gOtpAuto = undefined;
        break;
      case 'find':
        this.doFind();
        // 狀態碼清除
        this.cds.gPubCommon.gOtpAuto = undefined;
        break;
      default:
        if (this.isSetpwd) {
          this.setPassword('Y');
        } else {
          this.setPassword('N');
          this.userInfo.is_set_pw = 'Y';
        }
        // 狀態碼清除
        this.cds.gPubCommon.gOtpAuto = undefined;
        break;
    }
  }

  async ngOnInit() {
    this.userInfo = this.cds.userInfo;
    this.userInfoTemp = this.cds.userInfoTemp;
    if (!this.userInfo.is_set_pw) {
      /** 表單驗證項目 */
      this.validators = {
        password: [
          '',
          [Validators.required, this.vi.vnLength8to12, this.vi.vnPasswordRules],
        ],
        confirmPassword: [
          '',
          [
            Validators.required,
            this.vi.vnLength8to12,
            this.vi.vnPasswordRules,
            this.vi.reenter(
              this.controls ? this.controls.password.value : undefined
            ),
          ],
        ],
        email: [this.userInfoTemp.account, []],
      };
      // 新增刪除kor
      if (!this.isSetpwd) {
      }
    } else {
      /** 表單驗證項目 */
      this.validators = {
        kor: [
          '',
          this.isSetpwd
            ? [
                Validators.required,
                this.vi.vnLength8to12,
                this.vi.vnPasswordRules,
              ]
            : [],
        ],
        password: [
          '',
          [Validators.required, this.vi.vnLength8to12, this.vi.vnPasswordRules],
        ],
        confirmPassword: [
          '',
          [
            Validators.required,
            this.vi.vnLength8to12,
            this.vi.vnPasswordRules,
            this.vi.reenter(
              this.controls ? this.controls.password.value : undefined
            ),
          ],
        ],
        email: [this.userInfo.email, []],
      };
      // 新增刪除kor
      if (!this.isSetpwd) {
      }
    }

    this.formValidate();
  }
}
