import { Component, OnInit } from '@angular/core';
import { UserInfoTemp } from '../../../core/interface/index';
import { Router } from '@angular/router';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { CommonService } from 'src/app/core/services/common.service';
import { isScullyGenerated } from '@scullyio/ng-lib';

@Component({
  selector: 'app-register-id',
  templateUrl: './register-id.component.html',
  styleUrls: ['./register-id.component.scss'],
})
export class MemberRegisteridComponent implements OnInit {
  constructor(
    private alert: AlertServiceService,
    private fb: FormBuilder,
    private apiMember: MemberService,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private router: Router
  ) {
    this.FLang = this.cds.gLang;
  }
  userInfoTemp: UserInfoTemp;
  validators: {};
  group: FormGroup;
  controls;
  FLang: any;
  FGoogleToken: any = undefined;
  FCertificate_number: string = undefined;
  /** 建立驗證表單 */
  formValidate() {
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });
    this.group = this.fb.group(validator);
    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }
  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          certificate_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES004,
          name: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
          birthday:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
          sex:
            this.FLang.ER.select.ER001 + ' ' + this.FLang.ER.enter.ER001_.ES001,
          mobile: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES006,
          email: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES007,
          email_bk:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES008,
          province:
            this.FLang.ER.select.ER001 + ' ' + this.FLang.ER.enter.ER001_.ES002,
          county:
            this.FLang.ER.select.ER001 + ' ' + this.FLang.ER.enter.ER001_.ES003,
          address:
            this.FLang.ER.enter.ER001 + ' ' + this.FLang.ER.enter.ER001_.ES009,
        }[control_key];
      }
      /** 新檢核end */
      return (
        {
          fontLengthNumber:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
        }[type] || this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007
      );
    }
    return this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007;
  }
  // 註冊/修改
  async registerId() {
    /** 解除封印 允許後檢核 */
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });

    /** 檢查判斷是否驗證成功 */
    if (!(this.group.status === 'VALID' && this.FGoogleToken)) {
      let resultError;
      for (const key in this.controls) {
        if (!resultError && this.controls[key].invalid) {
          resultError = key;
        }
      }
      if (resultError) {
        CommonService.scrollto(
          `lyc_${resultError}`,
          $('.header-main').height()
        );
      }
      return;
    }

    this.resetGrecaptcha();
    const payload: any = {
      certificate_number: this.controls.certificate_number.value,
    };
    const registerId = await this.apiMember.MemberVerifyID(payload);
    if (registerId.status === 200) {
      if (this.controls.certificate_number.value.length === 9) {
        this.cds.userInfoTemp.certificate_number_9 = this.controls.certificate_number.value;
        this.cds.userInfoTemp.certificate_number_12 = '';
      }
      if (this.controls.certificate_number.value.length === 12) {
        this.cds.userInfoTemp.certificate_number_9 = '';
        this.cds.userInfoTemp.certificate_number_12 = this.controls.certificate_number.value;
      }
      /** 攜帶OTP狀態碼 */
      this.cds.gPubCommon.gOtpAuto = 'register';
      return this.router.navigateByUrl('login/register-data');
    } else {
      /** 60001 帳號已存在 */
      return this.doWarning();
      // if (registerId.error.status === 60001) {
      //   return this.doWarning();
      // } else {
      //   /// 500以下跳出錯誤
      //   /** 例外錯誤處理 */
      //   return this.doWarning();
      // }
    }
  }
  doWarning() {
    this.alert.open({
      type: 'warning',
      title: this.FLang.B051A.content.B0007,
      content: this.FLang.B051A.content.B0008,
      submitTitle: this.FLang.B051A.content.B0009,
      callback: this.doLogin,
    });
  }
  doLogin() {
    this.router.navigateByUrl('login');
  }

  /** 創造Grecaptcha */
  createGrecaptcha() {
    if (isScullyGenerated()) {
      grecaptcha.render('recaptchaClient', {
        sitekey: '6Lcru8YaAAAAAI3JMOgwk2BR1YtECpzt_jSolLZW',
        callback: (token) => {
          this.FGoogleToken = token;
        },
        'expired-callback': () => {
          this.resetGrecaptcha();
          this.createGrecaptcha();
        },
        hl:
          this.cds.gLangType === 'vi-VN'
            ? 'vi'
            : this.cds.gLangType === 'zh-TW'
            ? 'zh-TW'
            : 'en',
      } as any);
    }
  }
  /** 重置Grecaptcha */
  resetGrecaptcha() {
    grecaptcha.reset();
    this.FGoogleToken = undefined;
  }

  keepId(id) {
    this.FCertificate_number = id;
    Object.keys(this.cds.userInfoTemp).forEach((key) => {
      this.cds.userInfoTemp[key] = undefined;
    });
    if (id.length === 9) {
      this.cds.userInfoTemp.certificate_number_9 = this.FCertificate_number;
    }
    if (id.length === 12) {
      this.cds.userInfoTemp.certificate_number_12 = this.FCertificate_number;
    }
  }

  ngOnInit(): void {
    if (this.cds.userInfoTemp.certificate_number_9) {
      this.keepId(this.cds.userInfoTemp.certificate_number_9);
    }
    if (this.cds.userInfoTemp.certificate_number_12) {
      this.keepId(this.cds.userInfoTemp.certificate_number_12);
    }
    /** 表單驗證項目 */
    this.validators = {
      certificate_number: [
        this.FCertificate_number,
        [this.vi.fontLengthNumber, Validators.required],
      ],
    };
    this.formValidate();
    this.createGrecaptcha();
  }
}
