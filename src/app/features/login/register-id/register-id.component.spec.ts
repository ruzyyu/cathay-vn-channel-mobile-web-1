import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberRegisteridComponent } from './register-id.component';

describe('MemberRegisteridComponent', () => {
  let component: MemberRegisteridComponent;
  let fixture: ComponentFixture<MemberRegisteridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MemberRegisteridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberRegisteridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
