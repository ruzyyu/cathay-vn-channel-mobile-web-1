import { DatePipe } from '@angular/common';
import { PublicService } from 'src/app/core/services/api/public/public.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import {
  UserInfoTemp,
  ICounty,
  IProvince,
} from '../../../core/interface/index';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

import {
  Validators,
  FormBuilder,
  FormGroup,
} from '@angular/forms';

import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { CommonService } from 'src/app/core/services/common.service';

import * as moment from 'moment';

@Component({
  selector: 'app-register-data',
  templateUrl: './register-data.component.html',
  styleUrls: ['./register-data.component.scss'],
})
export class RegisterDataComponent implements OnInit {

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private apiMember: MemberService,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private apiPublic: PublicService,
    private datePipe: DatePipe
  ) {
    this.FLang = this.cds.gLang;

    /** 需有id 才能進此頁面 */
    if (
      !(
        this.cds.userInfoTemp.certificate_number_9 ||
        this.cds.userInfoTemp.certificate_number_12
      )
    ) {
      this.router.navigate(['login'], { replaceUrl: true });
    }

    // 判斷是否來自壽險並給予狀態碼
    if (this.cds.gIsSSO){
      this.cds.gPubCommon.gOtpAuto = 'register';
      this.cds.gIsSSO = false;
    }
  }

  /** 生日規則
   * 1. 最大歲數不得超過 100歲
   * 2. 最小歲數 日期不得小於當日
   * 3. 日期若沒資料時預設起始歲數30歲
   */
  get mixDate() {
    return moment(new Date()).subtract(100, 'years');
  }

  get maxDate() {
    return new Date();
  }

  get startDate() {
    return this.userInfoTemp.birthday
      ? moment(this.userInfoTemp.birthday).toDate()
      : moment(new Date()).subtract(30, 'years');
  }
  /** 宣告區Start */
  userInfoTemp: UserInfoTemp;
  validators: {};
  group: FormGroup;
  controls;
  FCountyList: Array<ICounty>;
  FProvinceList: Array<IProvince>;
  FLangType = this.cds.gLangType;
  FEmailError = false;
  FLang: any;
  FIdCheck = false;
  FMinDate: Date;
  FReadonly: boolean;
  // 小網日期不得開啟填寫功能
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }
  /** 省縣 */
  async getprovinceList() {
    // 縣市初始化
    if (!this.cds.gPubCommon.provinceList) {
      await this.apiPublic.gCountyList();
    }

    if (this.cds.gPubCommon && this.cds.gPubCommon.provinceList) {
      this.FProvinceList = this.cds.gPubCommon.provinceList
        ? this.cds.gPubCommon.provinceList
        : [];
    } else {
      this.FProvinceList = [];
    }

    if (this.FProvinceList.length > 0) {
      let mindex = Number(this.userInfoTemp.province) - 1 || 0;
      if (mindex < 0) {
        mindex = 0;
      }
      this.FCountyList = this.FProvinceList[mindex].countys;
    }

    // 重新賦予資料
  }
  notifyCounty(event) {
    this.controls.county.setValue('');
    this.controls.county.markAsUntouched();
    const index = event.target.selectedIndex - 1;
    this.FCountyList = this.FProvinceList[index].countys;
  }
  /** 宣告區End */
  /** 建立驗證表單 */
  formValidate() {
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });

    this.group = this.fb.group(validator);

    // 日期異動時檢測
    this.group.get('birthday').valueChanges.subscribe((item) => {
      if (item && this.FIdCheck) {
        this.group
          .get('certificate_number')
          .setValidators([
            this.vi.vnCertNumber(
              this.controls.birthday.value,
              this.controls.sex.value === 'F' ? '0' : '1'
            ),
            this.vi.vn2020ID(this.controls.birthday.value),
            this.vi.fontLengthNumber,
          ]);
        this.group.get('certificate_number').updateValueAndValidity();
      }
    });

    // 性別異動時檢測
    this.group.get('sex').valueChanges.subscribe((item) => {
      if (item && this.FIdCheck) {
        this.group
          .get('certificate_number')
          .setValidators([
            this.vi.vnCertNumber(
              this.controls.birthday.value,
              this.controls.sex.value === 'F' ? '0' : '1'
            ),
            this.vi.fontLengthNumber,
          ]);
        this.group.get('certificate_number').updateValueAndValidity();
      }
    });

    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          certificate_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES004,
          customer_name:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
          birthday:
            this.FLang.ER.select.ER001 +
            ' ' +
            this.FLang.ER.select.ER001_.ES005,
          sex:
            this.FLang.ER.select.ER001 +
            ' ' +
            this.FLang.ER.select.ER001_.ES001,
          mobile: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES006,
          email: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES007,
          email_bk:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES008,
          province:
            this.FLang.ER.select.ER001 +
            ' ' +
            this.FLang.ER.select.ER001_.ES002,
          county:
            this.FLang.ER.select.ER001 +
            ' ' +
            this.FLang.ER.select.ER001_.ES003,
          addr: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES009,
        }[control_key];
      }
      /** 新檢核end */

      return (
        {
          vnCertNumber: this.FLang.ER.ER036,
          vn2020ID:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
          addr: this.FLang.B054.content.B0020,
          vnPhone: this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES002,
          email: this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES003,
          languageFormat:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
          languagelength:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES001,
          tenNumber:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES002,
          maxlength: this.FLang.ER.ER043
        }[type] || this.FLang.ER.ER017
      );
    }
    return this.FLang.ER.ER017;
  }
  // 註冊/修改
  async register() {
    /** 解除封印 允許後檢核 */
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });

    /** 身分證額外檢核 */
    this.group
      .get('certificate_number')
      .setValidators([
        this.vi.fontLengthNumber,
        Validators.required,
        this.vi.vnCertNumber(
          this.controls.birthday.value,
          this.controls.sex.value === 'F' ? '0' : '1'
        ),
        this.vi.vn2020ID(this.controls.birthday.value),
      ]);
    this.group.get('certificate_number').updateValueAndValidity();
    /* 開啟日期與性別 身分證檢測 */
    this.FIdCheck = true;

    /** 檢查判斷是否驗證成功 */
    if (this.group.status !== 'VALID') {
      let resultError;
      for (const key in this.controls) {
        if (!resultError && this.controls[key].invalid) {
          resultError = key;
        }
      }
      if (resultError) {
        CommonService.scrollto(
          `lyc_${resultError}`,
          $('.header-main').height()
        );
      }
      return;
    }
    if (this.group.status !== 'VALID') {
      return;
    }

    const payload = {
      email: this.controls.email.value,
    };
    const registerEmail = await this.apiMember.MemberVerifyEmail(payload);
    if (registerEmail.status === 200) {
      if (this.controls.sex.value === 'F') {
        this.controls.sex.value = '0';
      }
      if (this.controls.sex.value === 'M') {
        this.controls.sex.value = '1';
      }
      this.cds.userInfoTemp.customer_name = this.controls.customer_name.value.trim();
      this.cds.userInfoTemp.email = this.controls.email.value;
      this.cds.userInfoTemp.sex = this.controls.sex.value;
      this.cds.userInfoTemp.mobile = this.controls.mobile.value;
      this.cds.userInfoTemp.county = this.controls.county.value;
      this.cds.userInfoTemp.province = this.controls.province.value;
      this.cds.userInfoTemp.addr = this.controls.addr.value;
      this.cds.userInfoTemp.account = this.controls.email.value;
      /** OTP 驗證碼發送 */
      const payload = {
        email: this.controls.email.value,
        mobile: this.controls.mobile.value,
        notice_action: 'reg',
      };
      const MemberSendOTP: any = this.apiMember.MemberSendOTP(payload);
      this.router.navigate(['login/otp-verification'], { replaceUrl: true });
    } else {
      /** 60001 信箱已存在 */
      if (registerEmail.error.status === 60001) {
        this.FEmailError = true;
      } else {
        /** 例外錯誤處理 */
        this.FEmailError = true;
        this.router.navigate(['login'], { replaceUrl: true });
      }
    }
  }

  /** 取得日期 */
  getDate($event) {
    let birthday;
    if ($event && $event.indexOf('/') !== -1) {
      birthday = this.datePipe.transform($event, 'yyyy-MM-dd');
    } else {
      birthday = $event;
    }
    this.cds.userInfoTemp.birthday = birthday;
    // 日期格式轉換並存入
    const dateChange = this.datePipe.transform($event, 'dd/MM/yyyy');
    this.controls.birthday.setValue(dateChange);
    this.controls.birthday.markAsTouched();
  }
  /** daterangepicker 取值範例 */
  // getDate($event) {
  //   console.log($event.startDay);
  //   console.log($event.endDay);
  // }

  /** 人壽會員日期格式處理 */
  getResDate() {
    if (this.cds.userInfoTemp.birthday) {
      if (this.cds.userInfoTemp.birthday.indexOf('-') !== -1) {
        const date = this.cds.userInfoTemp.birthday;
        this.controls.birthday.setValue(date);
        this.controls.birthday.markAsTouched();
        return `${date.slice(8, 10)}/${date.slice(5, 7)}/${date.slice(0, 4)}`;
      }
    }
  }

  /** 只要信箱內容改變就調整FEmailError */
  doEmailError() {
    this.FEmailError = false;
  }
  /** 將 input 值 轉換成大寫 */
  changeToUpperCase(event){
    const input = event.target;
    const start = input.selectionStart;
    const end = input.selectionEnd;
    input.value = input.value.toLocaleUpperCase();
    input.setSelectionRange(start, end);
  }
  ngOnInit(): void {
    /** 表單驗證項目 */
    this.userInfoTemp = this.cds.userInfoTemp;
    this.validators = {
      customer_name: [
        this.userInfoTemp.customer_name,
        [
          this.vi.languageFormat,
          this.vi.languagelength(160),
          Validators.required,
        ],
      ],
      certificate_number: [this.userInfoTemp.certificate_number_9 ||　this.userInfoTemp.certificate_number_12, [Validators.required]],
      birthday: ['', [Validators.required]],
      email: [this.userInfoTemp.email, [this.vi.email, Validators.required]],
      sex: [this.userInfoTemp.sex, [Validators.required]],
      mobile: [
        this.userInfoTemp.mobile,
        [this.vi.tenNumber, Validators.required],
      ],
      province: [this.userInfoTemp.province || '', [Validators.required]],
      county: [this.userInfoTemp.county || '', [Validators.required]],
      addr: [
        this.userInfoTemp.addr,
        [Validators.maxLength(240), Validators.required],
      ],
    };
    this.formValidate();
    // 下拉清單
    this.getprovinceList();

    // 小網日期不得開啟填寫功能
    if ($(document).innerWidth() > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }
}
