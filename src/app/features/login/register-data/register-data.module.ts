import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterDataRoutingModule } from './register-data-routing.module';
import { RegisterDataComponent } from './register-data.component';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [RegisterDataComponent],
  imports: [CommonModule, RegisterDataRoutingModule, SharedModule],
})
export class RegisterDataModule {}
