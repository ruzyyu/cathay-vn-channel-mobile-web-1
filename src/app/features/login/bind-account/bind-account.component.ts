import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialService } from '../../../core/services/social.service';
import { UserInfo } from '../../../core/interface/index';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { CommonService } from 'src/app/core/services/common.service';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
@Component({
  selector: 'app-bind-account',
  templateUrl: './bind-account.component.html',
  styleUrls: ['./bind-account.component.scss'],
})
export class BindAccountComponent implements OnInit {
  constructor(
    private alert: AlertServiceService,
    private router: Router,
    private fb: FormBuilder,
    private apiMember: MemberService,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private auth: SocialService
  ) {
    this.FLang = this.cds.gLang;
    /** 需有第三方綁定才能進此頁面 */
    try {
      const bind = this.auth.user.provider;
    } catch (error) {
      this.router.navigate(['login'], { replaceUrl: true });
    }
    if (
      !(
        this.cds.userInfoTemp.certificate_number_9 ||
        this.cds.userInfoTemp.certificate_number_12
      )
    ) {
      this.router.navigate(['login'], { replaceUrl: true });
    }
  }
  /** 宣告區Start */
  userInfo: UserInfo;
  validators: {};
  group: FormGroup;
  controls;
  FLoginError = false;
  FIDError = false;
  FLang: any;
  /** 宣告區End */
  /** 建立驗證表單 */
  formValidate() {
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });
    this.group = this.fb.group(validator);
    // 錯誤訊息顯示
    this.controls = this.group.controls;

    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }
  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          user_account:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES011,
          user_password:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES010,
        }[control_key];
      }
      /** 新檢核end */

      return (
        {
          email: this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES014,
          LengthInterval: this.FLang.ER.ER022,
          passwordRules: this.FLang.ER.ER022,
        }[type] || this.FLang.ER.ER017
      );
    }
    return this.FLang.ER.ER017;
  }
  showBind() {
    this.alert.open({
      type: 'success',
      title: this.FLang.B055.content.B0011,
      content: this.FLang.B055.content.B0012,
      callback: this.goMember,
    });
  }
  /** 綁定成功才可執行 */
  goMember() {
    this.router.navigate(['member'], { replaceUrl: true });
  }
  goFindAccount() {
    this.router.navigate(['login/find-account'], { replaceUrl: true });
  }
  ngOnInit(): void {
    /** 表單驗證項目 */
    this.validators = {
      user_account: ['', [Validators.required, this.vi.email]],
      user_password: [
        '',
        [Validators.required, this.vi.vnLength8to12, this.vi.vnPasswordRules],
      ],
    };
    this.formValidate();
  }
  async doSubmit() {
    /** 解除封印 允許後檢核 */
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });

    /** 檢查判斷是否驗證成功 */
    if (this.group.status !== 'VALID') {
      let resultError;
      for (const key in this.controls) {
        if (!resultError && this.controls[key].invalid) {
          resultError = key;
        }
      }
      if (resultError) {
        CommonService.scrollto(
          `lyc_${resultError}`,
          $('.header-main').height()
        );
      }
      return;
    }
    if (this.group.status !== 'VALID') {
      return;
    }

    const payload: any = {
      email: this.controls.user_account.value,
      kor: this.controls.user_password.value,
    };
    const login = await this.apiMember.MemberCathayLogin(payload);
    /** 登入成功 */
    if (login.status === 200) {
      this.cds.userInfo = login.data;
      /** 判斷會員ID是否等於前頁檢測ID */
      if (this.cds.userInfoTemp.certificate_number_9) {
        if (
          this.cds.userInfoTemp.certificate_number_9 !==
          login.data.certificate_number_9
        ) {
          this.FLoginError = false;
          return (this.FIDError = true);
        }
      }
      if (this.cds.userInfoTemp.certificate_number_12) {
        if (
          this.cds.userInfoTemp.certificate_number_12 !==
          login.data.certificate_number_12
        ) {
          this.FLoginError = false;
          return (this.FIDError = true);
        }
      }
      const payload: any = {
        seq_no: login.data.seq_no,
        fb_account: this.cds.userInfoTemp.fb_account,
        fb_title: this.cds.userInfoTemp.fb_title,
        google_account: this.cds.userInfoTemp.google_account,
        google_title: this.cds.userInfoTemp.google_title,
      };
      const MemberLogin: any = await this.apiMember.MemberBind(payload);
      /** 綁定成功 */
      if (MemberLogin.status === 200) {
        this.cds.userInfo.is_link_sns = MemberLogin.data.is_link_sns;
        if (MemberLogin.data.fb_account) {
          this.cds.userInfo.fb_account = MemberLogin.data.fb_account;
          this.cds.userInfo.fb_title = MemberLogin.data.fb_title;
        }
        if (MemberLogin.data.google_account) {
          this.cds.userInfo.google_account = MemberLogin.data.google_account;
          this.cds.userInfo.google_title = MemberLogin.data.google_title;
        }
        this.showBind();
      } else {
        /** 例外錯誤處理 */
        this.router.navigate(['login'], { replaceUrl: true });
      }
    } else {
      /** 60002 找不到資料, 50004 密碼錯誤,60004 尚未建立密碼 */
      const errorStatus = [60002, 50004, 60004];
      const errorCheck = !!errorStatus.find(
        (item) => item === login.error.status
      );
      if (errorCheck) {
        this.FLoginError = true;
        this.FIDError = false;
      } else {
        /** 例外錯誤處理 */
        this.FLoginError = true;
        this.FIDError = false;
        this.router.navigate(['login'], { replaceUrl: true });
      }
    }
  }
}
