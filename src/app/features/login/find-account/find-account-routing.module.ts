import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FindAccountComponent } from './find-account.component';

const routes: Routes = [{ path: '', component: FindAccountComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FindAccountRoutingModule { }
