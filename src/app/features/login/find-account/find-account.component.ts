import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { CommonService } from 'src/app/core/services/common.service';

import { UserInfoTemp } from '../../../core/interface/index';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { ValidatorsService } from 'src/app/core/services/utils/validators.service';

import * as moment from 'moment';

@Component({
  selector: 'app-find-account',
  templateUrl: './find-account.component.html',
  styleUrls: ['./find-account.component.scss'],
})
export class FindAccountComponent implements OnInit {
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private apiMember: MemberService,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private datePipe: DatePipe
  ) {
    this.FLang = this.cds.gLang;
  }
  userInfoTemp: UserInfoTemp;
  validators: {};
  group: FormGroup;
  controls;
  FSubmitError = false;
  FLang: any;
  FReadonly: boolean;
  // 小網日期不得開啟填寫功能
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }
  /** 建立驗證表單 */
  formValidate() {
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });

    this.group = this.fb.group(validator);

    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          certificate_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES004,
        }[control_key];
      }
      /** 新檢核end */
      return (
        {
          fontLengthNumber:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
        }[type] || this.FLang.ER.ER017
      );
    }
    return this.FLang.ER.ER017;
  }
  /** 取得日期 */
  getDate($event) {
    let birthday;
    if ($event && $event.indexOf('/') !== -1) {
      birthday = this.datePipe.transform($event, 'yyyy-MM-dd');
    } else {
      birthday = $event;
    }
    this.cds.userInfoTemp.birthday = birthday;
    // 日期格式轉換並存入
    const dateChange = this.datePipe.transform($event, 'dd/MM/yyyy');
    this.controls.birthday.setValue(dateChange);
    this.controls.birthday.markAsTouched();
  }

  async doSubmit() {
    /** 解除封印 允許後檢核 */
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });

    /** 檢查判斷是否驗證成功 */
    if (this.group.status !== 'VALID') {
      let resultError;
      for (const key in this.controls) {
        if (!resultError && this.controls[key].invalid) {
          resultError = key;
        }
      }
      if (resultError) {
        CommonService.scrollto(
          `lyc_${resultError}`,
          $('.header-main').height()
        );
      }
      return;
    }
    if (this.group.status !== 'VALID') {
      return;
    }

    if (this.controls.certificate_number.value.length === 9) {
      this.cds.userInfoTemp.certificate_number_9 = this.controls.certificate_number.value;
    }
    if (this.controls.certificate_number.value.length === 12) {
      this.cds.userInfoTemp.certificate_number_12 = this.controls.certificate_number.value;
    }

    /** api 判斷id and date */
    const payload: any = {
      certificate_number: this.controls.certificate_number.value,
      birthday: this.cds.userInfoTemp.birthday,
    };
    const findAccount = await this.apiMember.MemberCantLogin(payload);
    if (findAccount.status === 200) {
      /** 資料存入 */
      this.cds.userInfoTemp.seq_no = findAccount.data.seq_no;
      this.cds.userInfoTemp.email = findAccount.data.email;
      this.cds.userInfoTemp.mobile = findAccount.data.mobile;
      this.cds.userInfoTemp.account = findAccount.data.account;

      /** OTP 驗證碼發送 */
      const payload = {
        email: findAccount.data.email,
        mobile: findAccount.data.mobile,
        notice_action: 'cantlogin',
      };
      const MemberSendOTP: any = this.apiMember.MemberSendOTP(payload);
      this.cds.gPubCommon.gOtpAuto = 'find';
      this.router.navigateByUrl('login/otp-verification');
    } else {
      this.FSubmitError = true;
    }
  }

  ngOnInit(): void {
    this.userInfoTemp = this.cds.userInfoTemp;
    this.validators = {
      certificate_number: ['', [Validators.required, this.vi.fontLengthNumber]],
      birthday: ['', [Validators.required]],
    };
    this.formValidate();

    // 小網日期不得開啟填寫功能
    if ($(document).innerWidth() > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }

  /** 生日規則
   * 1. 最大歲數不得超過 100歲
   * 2. 最小歲數 日期不得小於當日
   * 3. 日期若沒資料時預設起始歲數30歲
   */
  get mixDate() {
    return moment(new Date()).subtract(100, 'years');
  }

  get maxDate() {
    return new Date();
  }

  get startDate() {
    return this.userInfoTemp.birthday
      ? moment(this.userInfoTemp.birthday).toDate()
      : moment(new Date()).subtract(30, 'years');
  }
}
