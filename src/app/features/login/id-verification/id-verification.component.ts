import { Component, OnInit } from '@angular/core';
import { UserInfoTemp } from '../../../core/interface/index';
import { Router } from '@angular/router';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { SocialService } from '../../../core/services/social.service';
import { CommonService } from 'src/app/core/services/common.service';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { isScullyGenerated } from '@scullyio/ng-lib';
@Component({
  selector: 'app-id-verification',
  templateUrl: './id-verification.component.html',
  styleUrls: ['./id-verification.component.scss'],
})
export class IdVerificationComponent implements OnInit {
  idName: undefined;
  constructor(
    private alert: AlertServiceService,
    private fb: FormBuilder,
    private apiMember: MemberService,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private router: Router,
    private auth: SocialService
  ) {
    this.FLang = this.cds.gLang;
    /** 需有第三方綁定才能進此頁面 */
    try {
      const bind = this.auth.user.provider;
    } catch (error) {
      this.router.navigate(['login'], { replaceUrl: true });
    }
  }
  userInfoTemp: UserInfoTemp;
  validators: {};
  group: FormGroup;
  controls;
  FLang: any;
  googleStatusPOP = false;
  facebookStatusPOP = false;
  FGoogleToken: any = undefined;
  FCertificate_number: string = undefined;
  /** 建立驗證表單 */
  formValidate() {
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });
    this.group = this.fb.group(validator);
    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }
  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          certificate_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES004,
        }[control_key];
      }
      /** 新檢核end */
      return (
        {
          fontLengthNumber:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
        }[type] || this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007
      );
    }
    return this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007;
  }
  /** 判斷綁定哪個社群 */
  checkThird() {
    this.checkNumberLength();
    /** google */
    if (this.auth.user.provider === 'GOOGLE') {
      this.cds.userInfoTemp.google_account = this.auth.user.id;
      this.cds.userInfoTemp.google_title = this.auth.user.name;
      this.cds.userInfoTemp.fb_account = '';
      this.cds.userInfoTemp.fb_title = '';
    }
    /** facebook */
    if (this.auth.user.provider === 'FACEBOOK') {
      this.cds.userInfoTemp.google_account = '';
      this.cds.userInfoTemp.google_title = '';
      this.cds.userInfoTemp.fb_account = this.auth.user.id;
      this.cds.userInfoTemp.fb_title = this.auth.user.name;
    }
  }
  /** 判斷ID是9碼或12碼 */
  checkNumberLength() {
    if (this.controls.certificate_number.value.length === 9) {
      this.cds.userInfoTemp.certificate_number_9 = this.controls.certificate_number.value;
      this.cds.userInfoTemp.certificate_number_12 = '';
    }
    if (this.controls.certificate_number.value.length === 12) {
      this.cds.userInfoTemp.certificate_number_9 = '';
      this.cds.userInfoTemp.certificate_number_12 = this.controls.certificate_number.value;
    }
  }
  // 檢驗ID
  async checkId() {
    /** 解除封印 允許後檢核 */
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });
    /** 檢查判斷是否驗證成功 */
    if (!(this.group.status === 'VALID' && this.FGoogleToken)) {
      let resultError;
      for (const key in this.controls) {
        if (!resultError && this.controls[key].invalid) {
          resultError = key;
        }
      }
      if (resultError) {
        CommonService.scrollto(
          `lyc_${resultError}`,
          $('.header-main').height()
        );
      }
      return;
    }
    if (!(this.group.status === 'VALID' && this.FGoogleToken)) {
      return;
    }
    this.resetGrecaptcha();
    const payload: any = {
      certificate_number: this.controls.certificate_number.value,
    };
    const verificationId: any = await this.apiMember.MemberVerifyIDNoAlert(
      payload
    );
    /** 200 ID 沒重複 */
    if (verificationId.status === 200) {
      this.checkThird();
      /** 攜帶OTP狀態碼 */
      this.cds.gPubCommon.gOtpAuto = 'bind';
      return this.router.navigateByUrl('login/register-data');
    } else {
      /** 60001 帳號已存在 */
      if (verificationId.error.status === 60001) {
        const payload: any = {
          certificate_number: this.controls.certificate_number.value,
        };
        const verBind: any = await this.apiMember.MemberIsBind(payload);
        /** 200 API成功  */
        if (verBind.status === 200) {
          if (verBind.data.google === 'Y') {
            this.googleStatusPOP = true;
          }
          if (verBind.data.google === 'N') {
            this.googleStatusPOP = false;
          }
          if (verBind.data.facebook === 'Y') {
            this.facebookStatusPOP = true;
          }
          if (verBind.data.facebook === 'N') {
            this.facebookStatusPOP = false;
          }

          if (verBind.data.google === 'Y' && verBind.data.facebook === 'Y') {
            this.checkNumberLength();
            this.showThirdBind();
          }

          if (verBind.data.google === 'Y' && verBind.data.facebook === 'N') {
            if (this.auth.user.provider === 'GOOGLE') {
              this.checkNumberLength();
              this.showThirdBind();
            } else {
              this.checkThird();
              return this.router.navigateByUrl('login/bind-account');
            }
          }

          if (verBind.data.google === 'N' && verBind.data.facebook === 'Y') {
            if (this.auth.user.provider === 'FACEBOOK') {
              this.checkNumberLength();
              this.showThirdBind();
            } else {
              this.checkThird();
              return this.router.navigateByUrl('login/bind-account');
            }
          }

          if (verBind.data.google === 'N' && verBind.data.facebook === 'N') {
            this.checkThird();
            return this.router.navigateByUrl('login/bind-account');
          }
        } else {
          /** 例外錯誤處理 */
          return this.goLogin();
        }
      } else {
        /** 例外錯誤處理 */
        return this.goLogin();
      }
    }
  }
  /** 回登入頁 */
  goLogin() {
    this.router.navigateByUrl('login');
  }
  /** 補登驗證 */
  showThirdBind() {
    $('#popThirdBind').modal('show');
  }
  /** 創造Grecaptcha */
  createGrecaptcha() {
    if (isScullyGenerated()) {
      grecaptcha.render('recaptchaClient', {
        sitekey: '6Lcru8YaAAAAAI3JMOgwk2BR1YtECpzt_jSolLZW',
        callback: (token) => {
          this.FGoogleToken = token;
        },
        'expired-callback': () => {
          this.resetGrecaptcha();
          this.createGrecaptcha();
        },
        hl:
          this.cds.gLangType === 'vi-VN'
            ? 'vi'
            : this.cds.gLangType === 'zh-TW'
            ? 'zh-TW'
            : 'en',
      } as any);
    }
  }
  /** 重置Grecaptcha */
  resetGrecaptcha() {
    grecaptcha.reset();
    this.FGoogleToken = undefined;
  }

  keepId(id) {
    this.FCertificate_number = id;
    Object.keys(this.cds.userInfoTemp).forEach((key) => {
      this.cds.userInfoTemp[key] = undefined;
    });
    if (id.length === 9) {
      this.cds.userInfoTemp.certificate_number_9 = this.FCertificate_number;
    }
    if (id.length === 12) {
      this.cds.userInfoTemp.certificate_number_12 = this.FCertificate_number;
    }
  }

  ngOnInit(): void {
    if (this.cds.userInfoTemp.certificate_number_9) {
      this.keepId(this.cds.userInfoTemp.certificate_number_9);
    }
    if (this.cds.userInfoTemp.certificate_number_12) {
      this.keepId(this.cds.userInfoTemp.certificate_number_12);
    }

    /** 表單驗證項目 */
    this.validators = {
      certificate_number: [
        this.FCertificate_number,
        [this.vi.fontLengthNumber, Validators.required],
      ],
    };
    this.formValidate();

    this.createGrecaptcha();
  }
}
