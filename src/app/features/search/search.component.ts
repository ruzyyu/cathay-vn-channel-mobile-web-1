import { MetaService } from './../../core/services/meta.service';
import { PublicService } from 'src/app/core/services/api/public/public.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { LocalService } from './../../core/services/api/local/local.service';
import { jobDetail } from 'src/app/data/job';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cds: DatapoolService,
    private publicService: PublicService,
    private api: LocalService,
    private metaData: MetaService
  ) {
    this.FLang = this.cds.gLang;
  }
  /** 資料處理 */
  FLang: any;
  FLangType = this.cds.gLangType;
  FRouterData: any = [];
  FI18nData: any = {};
  FDataTotal: any = [];
  FTitle: any = [];
  FContent: any = [];
  FUrl: any = [];

  /** ----搜尋功能---- */
  FSearchText: string = undefined;
  FSearchDataAll: any = [];
  FSearchArray: any = [];
  FSearchNumber = 0;
  FTags: any = [];
  FCount = 1;
  FViewItem = 10;
  FMore = true;
  /** 將router 資料重組成物件 */
  getRouter() {
    let routerDataObj: any;
    this.router.config[0].children.forEach((routerDate) => {
      if (routerDate.data && routerDate.path) {
        routerDataObj = {
          title: this.checkRouterI18n(routerDate),
          path: routerDate.path,
        };
        this.FRouterData.push(routerDataObj);
        // 塞除router內通用網域
        this.FRouterData = this.FRouterData.filter((common) => {
          return common.path.indexOf('/:') === -1;
        });
      }
      this.checkChildRouter(
        routerDate,
        routerDataObj,
        this.FRouterData,
        undefined
      );
    });
  }
  /** 將router 子資料取至父層 */
  checkChildRouter(
    routerDate: any,
    routerDataObj: any,
    routerDataArray: any,
    path
  ) {
    if (routerDate.children) {
      path = path ? path + '/' + routerDate.path : routerDate.path;
      routerDate.children.forEach((childrenDate) => {
        if (childrenDate.data && childrenDate.path) {
          routerDataObj = {
            title: this.checkRouterI18n(childrenDate),
            path: `${path}/${childrenDate.path}`,
          };
          routerDataArray.push(routerDataObj);
        }
        this.checkChildRouter(childrenDate, routerDataObj, routerDataArray, path);
      });
    }
  }

  checkRouterI18n(title) {
    if (this.FLangType === 'zh-TW') {
      return title.data.breadcrumb;
    }
    if (this.FLangType === 'vi-VN') {
      return title.data.vnbreadcrumb;
    }
    if (this.FLangType === 'en-US') {
      return title.data.enbreadcrumb;
    }
    return null;
  }

  /** 將資料存至 FI18nData */
  getI18nData() {
    Object.keys(this.FLang).forEach((date) => {
      this.FI18nData[date] = this.checkChildI18n(this.FLang[date]);
    });
  }

  /** 將多國語子層資料取至父層，目前最多四層 */
  checkChildI18n(key) {
    const allContent = [];
    if (typeof key === 'string') {
      allContent.push(key);
    } else {
      for (const i in key) {
        if (typeof key[i] === 'object') {
          const content = Object.values(key[i]);
          content.forEach((data) => {
            if (typeof data === 'object') {
              const contentChild = Object.values(data);
              contentChild.forEach((childData) => {
                allContent.push(childData);
              });
            } else {
              allContent.push(data);
            }
          });
        } else {
          if (key[i]) {
            allContent.push(key[i]);
          }
        }
      }
    }
    return allContent;
  }
  /** 將多國語與router資料整理在一起 */
  doDataTotal() {
    this.FRouterData.forEach((data) => {
      switch (data.path) {
        case 'about':
          if (!this.FI18nData.G01) {
            return;
          }
          data.content = Object.values(this.FI18nData.G01);
          data.img = 'assets/img/img-bn-about.png';
          break;
        case 'about/opening':
          if (!this.FI18nData.G021) {
            return;
          }
          data.content = Object.values(this.FI18nData.G021);
          data.img = 'assets/img/img-bn-job.png';
          break;
        case 'about/opening/opening-detail':
          data.img = 'assets/img/img-latestnews-01.jpg';
          break;
        case 'help':
          if (!this.FI18nData.H01) {
            return;
          }
          data.content = Object.values(this.FI18nData.H01).concat(
            Object.values(this.FI18nData.H02)
          );
          data.img = 'assets/img/img-bn-faq.png';
          break;
        case 'member/claim':
          data.img = 'assets/img/img-latestnews-01.jpg';
          break;
        case 'online-insurance':
          if (!this.FI18nData.C01) {
            return;
          }
          data.content = Object.values(this.FI18nData.C01);
          data.img = 'assets/img/img-bn-insure.png';
          break;
        case 'online-insurance/car-insurance':
          if (!this.FI18nData.D01) {
            return;
          }
          data.content = Object.values(this.FI18nData.D01);
          data.img = 'assets/img/commodity/bn-car-800-x-470.png';
          break;
        case 'online-insurance/motor-insurance':
          if (!this.FI18nData.D02) {
            return;
          }
          data.content = Object.values(this.FI18nData.D02);
          data.img = 'assets/img/commodity/bn-scooter-800-x-470.png';
          break;
        case 'online-insurance/travel-insurance':
          if (!this.FI18nData.D03) {
            return;
          }
          data.content = Object.values(this.FI18nData.D03);
          data.img = 'assets/img/commodity/bn-travel-800-x-470.png';
          break;
        case 'online-insurance/injury-insurance':
          if (!this.FI18nData.D04) {
            return;
          }
          data.content = Object.values(this.FI18nData.D04);
          data.img = 'assets/img/commodity/bn-injury-800-x-470.png';
          break;
        case 'online-insurance/house-insurance':
          if (!this.FI18nData.D05) {
            return;
          }
          data.content = Object.values(this.FI18nData.D05);
          data.img = 'assets/img/commodity/bn-house-800-x-470.png';
          break;
        case 'online-insurance/engineering-insurance':
          if (!this.FI18nData.E01) {
            return;
          }
          data.content = Object.values(this.FI18nData.E01);
          data.img = 'assets/img/commodity/bn-engineering-800-x-470.png';
          break;
        case 'online-insurance/liability-insurance':
          if (!this.FI18nData.E02) {
            return;
          }
          data.content = Object.values(this.FI18nData.E02);
          data.img = 'assets/img/commodity/bn-liability-800-x-470.png';
          break;
        case 'online-insurance/property-insurance':
          if (!this.FI18nData.E03) {
            return;
          }
          data.content = Object.values(this.FI18nData.E03);
          data.img = 'assets/img/commodity/bn-property-800-x-470.png';
          break;
        case 'online-insurance/cargo-insurance':
          if (!this.FI18nData.E04) {
            return;
          }
          data.content = Object.values(this.FI18nData.E04);
          data.img = 'assets/img/commodity/bn-cargo-800-x-470.png';
          break;
        case 'sitemap':
          if (!this.FI18nData.A05) {
            return;
          }
          data.content = Object.values(this.FI18nData.A05);
          data.img = 'assets/img/img-bn-sitemap.png';
          break;
        default:
          break;
      }
    });
  }

  /** 取得最新消息資料 */
  async getNews() {
    if (!this.FSearchText) {
      return false;
    }
    const payload = {
      key: this.FSearchText,
    };
    const res: any = await this.publicService.SearchGetList(payload);

    if (!res.data) {
      return;
    }
    const newData = res.data;
    const newArr: any = [];
    let newObj: any;
    newData.forEach((data) => {
      // 預設圖片資料轉換
      if (data.default_image && data.image_url.length === 0) {
        switch (data.default_image) {
          case '1':
            data.image_url.push('assets/img/new/img-latestnews-01.jpg');
            break;
          case '2':
            data.image_url.push('assets/img/new/img-latestnews-02.jpg');
            break;
          case '3':
            data.image_url.push('assets/img/new/img-latestnews-03.jpg');
            break;
          case '4':
            data.image_url.push('assets/img/new/img-latestnews-04.jpg');
            break;
          case '5':
            data.image_url.push('assets/img/new/img-latestnews-05.jpg');
            break;
          default:
            return;
        }
      }

      // html 轉成純文字
      const content = data.content.replace(/<[^>]*>?/gm, '');
      newObj = {
        title: data.title,
        img: data.image_url[0],
        path: `news/news-detail?type=${data.type_id}&option=${data.id}`,
        content,
      };
      newArr.push(newObj);
    });
    return newArr;
  }

  checkDataIsFromNews(path) {
    return path.indexOf('news/news-detail?') !== -1 ? true : false;
  }

  transformNewData({ title, path, content, img }) {
    let searchResult = false;
    if (this.searchHandler(title) !== -1) {
      title = this.doHighlight(title);
      searchResult = true;
    }
    if (this.searchHandler(content) !== -1) {
      content = this.doHighlight(content);
      searchResult = true;
    }
    if (searchResult) {
      return {
        title,
        path,
        content,
        img,
      };
    }
    return null;
  }

  /** 取得 opening 資料 */
  getOpeningData() {
    const openingDate = jobDetail;
    const OpeningDateArr: any = [];
    let OpeningDateObj: any;
    // let OpeningDetail: any;
    openingDate.forEach((data) => {
      const contentAll: any = [];
      data.content.forEach((dataArr) => {
        contentAll.push(dataArr.value);
      });
      data.explain.forEach((dataArr) => {
        contentAll.push(dataArr.value);
      });
      contentAll.push(data.area);
      OpeningDateObj = {
        title: data.title,
        img: 'assets/img/img-bn-job.png',
        path: 'about/opening',
        content: contentAll,
      };
      // 目前無相對應網址，故先註解
      // OpeningDetail = {
      //   title: data.title,
      //   img: 'assets/img/img-bn-job.png',
      //   path: `about/opening/opening-detail?jobId=${data.id}`,
      //   content: contentAll,
      // };
      // OpeningDateArr.push(OpeningDateObj, OpeningDetail);
      OpeningDateArr.push(OpeningDateObj);
    });

    return OpeningDateArr;
  }

  /** 取得JSON檔資料 */
  async getIntroductionJSON(index) {
    const lang = this.cds.gLangType;
    // JSON 檔 資料處理
    const dataJson: any = await this.api.JSON(
      `assets/json/contract/${lang}/contract0${index}.json`
    );
    const dataArr: any = [];
    dataJson.forEach((item) => {
      dataArr.push(item.title, item.content);
      item.nextLevelContent.forEach((data) => {
        dataArr.push(data.title, data.content);
        data.text.forEach((text) => {
          dataArr.push(text);
        });
      });
    });
    return dataArr;
  }

  /** 取得保險理賠介紹資料 */
  async getIntroduction() {
    const lang = this.cds.gLangType;

    // 車險
    const carData = {
      title: this.FLang.F09.L0001_.s001,
      img: 'assets/img/img-bn-claim.png',
      path: `member/claim/introduction?intro=assets%2Fjson%2Fcontract%2F${lang}%2Fcontract01.json`,
      content: [
        this.FLang.F09.L0001_.s001,
        this.FLang.F09.L0011,
        this.FLang.F09.L0012,
        this.FLang.F09.L0019,
        this.FLang.F09.L0020,
        this.FLang.F09.L0021,
        this.FLang.F09.L0022,
        this.FLang.F09.L0023,
        this.FLang.F09.L0024,
        this.FLang.F09.L0025,
        this.FLang.H02['1_1_1_1'].q,
        this.FLang.H02['1_1_1_1'].a,
        this.FLang.H02['1_1_1_2'].q,
        this.FLang.H02['1_1_1_2'].a,
        this.FLang.H02['1_1_2_1'].q,
        this.FLang.H02['1_1_2_1'].a,
      ].concat(await this.getIntroductionJSON(1)),
    };

    // 旅遊險
    const travelData = {
      title: this.FLang.F09.L0001_.s002,
      img: 'assets/img/img-bn-claim.png',
      path: `member/claim/introduction?intro=assets%2Fjson%2Fcontract%2F${lang}%2Fcontract02.json`,
      content: [
        this.FLang.F09.L0001_.s002,
        this.FLang.F09.L0009,
        this.FLang.F09.L0010,
        this.FLang.F09.L0011,
        this.FLang.F09.L0012,
        this.FLang.F09.L0014,
        this.FLang.F09.L0015,
        this.FLang.F09.L0054,
        this.FLang.F09.L0055,
        this.FLang.F09.L0056,
        this.FLang.F09.L0057,
        this.FLang.F09.L0058,
        this.FLang.F09.L0059,
        this.FLang.F09.L0060,
        this.FLang.F09.L0061,
        this.FLang.F09.L0062,
        this.FLang.F09.L0063,
        this.FLang.F09.L0064,
        this.FLang.F09.L0026,
        this.FLang.F09.L0027,
        this.FLang.F09.L0028,
        this.FLang.H02['1_3_1_1'].q,
        this.FLang.H02['1_3_1_1'].a,
        this.FLang.H02['1_3_1_2'].q,
        this.FLang.H02['1_3_1_2'].a,
        this.FLang.H02['1_3_1_3'].q,
        this.FLang.H02['1_3_1_3'].a,
      ].concat(await this.getIntroductionJSON(2)),
    };

    // 意外險
    const injuryData = {
      title: this.FLang.F09.L0001_.s003,
      img: 'assets/img/img-bn-claim.png',
      path: `member/claim/introduction?intro=assets%2Fjson%2Fcontract%2F${lang}%2Fcontract03.json`,
      content: [
        this.FLang.F09.L0001,
        this.FLang.F09.L0009,
        this.FLang.F09.L0010,
        this.FLang.F09.L0011,
        this.FLang.F09.L0012,
        this.FLang.F09.L0013,
        this.FLang.F09.L0014,
        this.FLang.F09.L0015,
        this.FLang.F09.L0016,
        this.FLang.F09.L0017,
        this.FLang.H02['1_4_1_1'].q,
        this.FLang.H02['1_4_1_1'].a,
        this.FLang.H02['1_4_1_2'].q,
        this.FLang.H02['1_4_1_2'].a,
        this.FLang.H02['1_4_2_1'].q,
        this.FLang.H02['1_4_2_1'].a,
      ].concat(await this.getIntroductionJSON(3)),
    };
    // 房屋險
    const houseData = {
      title: this.FLang.F09.L0001_.s004,
      img: 'assets/img/img-bn-claim.png',
      path: `member/claim/introduction?intro=assets%2Fjson%2Fcontract%2F${lang}%2Fcontract04.json`,
      content: [
        this.FLang.F09.L0001_.s004,
        this.FLang.F09.L0010,
        this.FLang.F09.L0011,
        this.FLang.F09.L0012,
        this.FLang.F09.L0019,
        this.FLang.F09.L0029,
        this.FLang.F09.L0030,
        this.FLang.F09.L0030_,
        this.FLang.F09.L0031,
        this.FLang.F09.L0032,
        this.FLang.F09.L0033,
        this.FLang.H02['1_5_1_1'].q,
        this.FLang.H02['1_5_1_1'].a,
        this.FLang.H02['1_5_1_2'].q,
        this.FLang.H02['1_5_1_2'].a,
        this.FLang.H02['1_5_1_3'].q,
        this.FLang.H02['1_5_1_3'].a,
      ].concat(await this.getIntroductionJSON(4)),
    };
    // 工程險
    const engineeringData = {
      title: this.FLang.F09.L0002_.s001,
      img: 'assets/img/img-bn-claim.png',
      path: `member/claim/introduction?intro=assets%2Fjson%2Fcontract%2F${lang}%2Fcontract05.json`,
      content: [
        this.FLang.F09.L0002_.s001,
        this.FLang.F09.L0011,
        this.FLang.F09.L0012,
        this.FLang.F09.L0019,
        this.FLang.F09.L0033,
        this.FLang.F09.L0034,
        this.FLang.F09.L0035,
        this.FLang.F09.L0036,
        this.FLang.F09.L0036_,
        this.FLang.F09.L0037,
        this.FLang.F09.L0043,
        this.FLang.H02['2_1_1_1'].q,
        this.FLang.H02['2_1_1_1'].a,
        this.FLang.H02['2_1_1_2'].q,
        this.FLang.H02['2_1_1_2'].a,
        this.FLang.H02['2_1_2_1'].q,
        this.FLang.H02['2_1_2_1'].a,
      ].concat(await this.getIntroductionJSON(5)),
    };
    // 責任險
    const liabilityData = {
      title: this.FLang.F09.L0002_.s002,
      img: 'assets/img/img-bn-claim.png',
      path: `member/claim/introduction?intro=assets%2Fjson%2Fcontract%2F${lang}%2Fcontract06.json`,
      content: [
        this.FLang.F09.L0002_.s002,
        this.FLang.F09.L0011,
        this.FLang.F09.L0012,
        this.FLang.F09.L0019,
        this.FLang.F09.L0037,
        this.FLang.F09.L0044,
        this.FLang.F09.L0045,
        this.FLang.F09.L0046,
        this.FLang.F09.L0046_,
        this.FLang.F09.L0048,
        this.FLang.F09.L0048_,
        this.FLang.F09.L0049,
        this.FLang.H02['2_2_1_1'].q,
        this.FLang.H02['2_2_1_1'].a,
        this.FLang.H02['2_2_1_2'].q,
        this.FLang.H02['2_2_1_2'].a,
        this.FLang.H02['2_2_1_3'].q,
        this.FLang.H02['2_2_1_3'].a,
      ].concat(await this.getIntroductionJSON(6)),
    };
    // 財產險
    const propertyData = {
      title: this.FLang.F09.L0002_.s003,
      img: 'assets/img/img-bn-claim.png',
      path: `member/claim/introduction?intro=assets%2Fjson%2Fcontract%2F${lang}%2Fcontract07.json`,
      content: [
        this.FLang.F09.L0002_.s003,
        this.FLang.F09.L0010,
        this.FLang.F09.L0011,
        this.FLang.F09.L0012,
        this.FLang.F09.L0019,
        this.FLang.F09.L0037,
        this.FLang.F09.L0050,
        this.FLang.F09.L0051,
        this.FLang.F09.L0051_,
        this.FLang.F09.L0052,
        this.FLang.F09.L0052_,
        this.FLang.F09.L0053,
        this.FLang.H02['2_3_1_1'].q,
        this.FLang.H02['2_3_1_1'].a,
        this.FLang.H02['2_3_1_2'].q,
        this.FLang.H02['2_3_1_2'].a,
        this.FLang.H02['2_3_2_1'].q,
        this.FLang.H02['2_3_2_1'].a,
      ].concat(await this.getIntroductionJSON(7)),
    };
    // 運輸險
    const cargoData = {
      title: this.FLang.F09.L0002_.s004,
      img: 'assets/img/img-bn-claim.png',
      path: `member/claim/introduction?intro=assets%2Fjson%2Fcontract%2F${lang}%2Fcontract08.json`,
      content: [
        this.FLang.F09.L0002_.s004,
        this.FLang.F09.L0011,
        this.FLang.F09.L0012,
        this.FLang.F09.L0019,
        this.FLang.F09.L0037,
        this.FLang.F09.L0038,
        this.FLang.F09.L0039,
        this.FLang.F09.L0040,
        this.FLang.F09.L0041,
        this.FLang.F09.L0042,
        this.FLang.H02['2_4_1_1'].q,
        this.FLang.H02['2_4_1_1'].a,
        this.FLang.H02['2_4_1_2'].q,
        this.FLang.H02['2_4_1_2'].a,
        this.FLang.H02['2_4_1_3'].q,
        this.FLang.H02['2_4_1_3'].a,
      ].concat(await this.getIntroductionJSON(8)),
    };

    const totalData = [
      carData,
      travelData,
      injuryData,
      houseData,
      engineeringData,
      liabilityData,
      propertyData,
      cargoData,
    ];

    return totalData;
  }

  /**
   * 所有資料彙總
   * 格式
   * {
   *    title : 標題,
   *    img : 圖片連結,
   *    path : 連結,
   *    content : [所有資料],
   * }
   */
  async getAllData() {
    this.FDataTotal = this.FRouterData.concat(await this.getNews())
      .concat(this.getOpeningData())
      .concat(await this.getIntroduction());
  }

  ngOnInit(): void {
    this.getRouter();
    this.getI18nData();
    this.doDataTotal();
    /** 判斷router  queryParam改變即執行查詢 */
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.FSearchText = params.get('search');
      this.doSearch();
    });
    this.FTags = [
      this.FLang.A03.L0001,
      this.FLang.A03.L0002,
      this.FLang.A03.L0003,
      this.FLang.A03.L0004,
      this.FLang.A03.L0005,
    ];
  }
  /**
   * @param value 原資料
   * 判斷是否有搜尋到結果，有則回傳位置
   */
  searchHandler(value: any) {
    if (typeof value === 'undefined') {
      return -1;
    }
    if (typeof value === 'string') {
      const firstText = value.toLowerCase();
      const lastText = this.FSearchText.trim().toLowerCase();
      const result = firstText.indexOf(lastText);
      return result;
    }
    if (typeof value === 'object') {
      const lastArray = [];
      value.forEach((item: string) => {
        const firstText = item.toLowerCase();
        const lastText = this.FSearchText.trim().toLowerCase();
        const result = firstText.indexOf(lastText);
        if (result !== -1) {
          lastArray.push(firstText);
        }
      });
      if (lastArray.length === 0) {
        return -1;
      } else {
        return lastArray;
      }
    }
  }
  /**
   * @param value 原資料
   * @returns any
   * 回傳搜尋到的字串
   */
  doHighlight(value: string) {
    const searchReg = new RegExp(`(${this.FSearchText.trim()})`, 'gi');
    return value.replace(searchReg, `<span class="highlight-red">$1</span>`);
  }
  /**
   * @param value 搜尋文字，若未帶參數預設 FSearchText
   * @returns any
   * 搜尋結果
   */
  async doSearch(value: string = null) {
    if (value) {
      this.FSearchText = value;
    }
    if (this.FSearchText) {
      await this.getAllData();
      this.FSearchDataAll = [];
      this.FSearchArray = [];
      this.FMore = true;
      const newArray = [];

      // console.log(this.FDataTotal);

      this.FDataTotal.forEach(({ title, path, content, img }) => {
        // 資料若來最新消息內頁，則顯示以原title content 為主
        if (this.checkDataIsFromNews(path)) {
          const data = { title, path, content, img };
          newArray.push(this.transformNewData(data));
          return;
        }

        const viewData = this.metaData.getMetaData(`/${path}`);
        if (!viewData) {
          return;
        }
        /** 搜尋結果是否有值 */
        let searchRes = false;
        // 總資料 包含 頁面標題、內容，顯示標題、內容
        const allData = [title, content, viewData.title, viewData.description];
        const checkData: any = allData.map((date) => {
          return (date = this.searchHandler(date) !== -1 ? true : false);
        });

        if (checkData.includes(true)) {
          // viewData.title 被搜尋到則高亮
          title = checkData[2]
            ? this.doHighlight(viewData.title)
            : viewData.title;
          // viewData.description 被搜尋到則高亮
          content = checkData[3]
            ? this.doHighlight(viewData.description)
            : viewData.description;
          searchRes = true;
        }

        if (searchRes) {
          newArray.push({
            title,
            path,
            content,
            img,
          });
        }
      });

      /** 將資料存成原始陣列，不可修改內容 */
      newArray.forEach((data) => {
        this.FSearchDataAll.push(data);
      });

      this.FSearchNumber = newArray.length;
      /** 將資料存成暫時陣列，可調整筆數 */
      newArray.forEach((data) => {
        this.FSearchArray.push(data);
      });
      /** 預設顯示十筆 */
      if (this.FSearchArray.length > this.FViewItem) {
        this.FSearchArray.length = this.FViewItem;
      }
      this.doUrlChange();
    } else {
      this.FSearchArray.length = 0;
      this.FSearchNumber = 0;
    }
  }
  /** 鍵盤按下enter時執行查詢 */
  doSearchEnter() {
    this.doSearch();
  }
  /** 執行查看更多按鈕 */
  doViewTen() {
    /** 紀錄點擊次數 */
    this.FCount = this.FCount + 1;
    if (this.FSearchDataAll.length <= this.FCount * this.FViewItem) {
      this.FSearchArray.length = 0;
      this.FSearchDataAll.forEach((item) => {
        this.FSearchArray.push(item);
      });
      this.FMore = false;
    } else {
      this.FSearchArray.length = 0;
      this.FSearchDataAll.forEach((item) => {
        this.FSearchArray.push(item);
      });
      this.FSearchArray.length = this.FCount * this.FViewItem;
    }
  }
  /** 送出搜尋後改變URL */
  doUrlChange() {
    const options: NavigationExtras = {
      queryParams: {
        search: this.FSearchText,
      },
    };
    this.router.navigate(['/search'], options);
  }
}
/** ----搜尋功能End---- */
