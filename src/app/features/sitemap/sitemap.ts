export interface SiteMap {
  /** 大標tag */
  tag: number;
  /** 大標標題 */
  title?: string;
  /** 小標 */
  content: SiteMapContent[];
}

export interface SiteMapContent {
  /** 小標id */
  id: number;
  /** 小標標題 */
  itemName: string;
  /** 小標連結 */
  url: string;
  /** 小標參數 */
  queryParams?: object;
}
