import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { SiteMap } from 'src/app/core/sitemap/sitemap';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { PolicyService } from './../../core/services/api/policy/policy.service';

@Component({
  selector: 'app-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['./sitemap.component.scss'],
})
export class SitemapComponent implements OnInit {
  FLang: any;
  FLangType: any;

  helpList: SiteMap[];
  personalList: SiteMap[];
  commercialList: SiteMap[];
  serviceCenterList: SiteMap[];
  aboutList: SiteMap[];
  otherList: SiteMap[];

  constructor(
    private cds: DatapoolService,
    private router: Router,
    private apiPolicy: PolicyService,
    private alter: AlertServiceService
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit(): void {
    /** 幫助中心 */
    this.helpList = [
      {
        tag: 0,
        title: this.FLang.A05.A0003,
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0003_.s001,
            url: '/help',
            queryParams: { type: 'question-personal' },
          },
          {
            id: 1,
            itemName: this.FLang.A05.A0003_.s002,
            url: '/help',
            queryParams: { type: 'question-business' },
          },
        ],
      },
      {
        tag: 1,
        title: this.FLang.A05.A0004,
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0004_.s001,
            url: '/help',
            queryParams: { type: 'download-personal' },
          },
          {
            id: 1,
            itemName: this.FLang.A05.A0004_.s002,
            url: '/help',
            queryParams: { type: 'download-business' },
          },
        ],
      },
      {
        tag: 2,
        title: this.FLang.A05.A0005,
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0005_.s001,
            url: '/help',
            queryParams: { type: 'info' },
          },
        ],
      },
    ];

    /** 個人商品 */
    this.personalList = [
      {
        tag: 0,
        title: this.FLang.A05.A0007,
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0007_.s001,
            url: '/online-insurance',
            queryParams: { type: 'car' },
          },
          {
            id: 1,
            itemName: this.FLang.A05.A0007_.s002,
            url: '/online-insurance',
            queryParams: { type: 'motor' },
          },
          {
            id: 2,
            itemName: this.FLang.A05.A0007_.s003,
            url: '/online-insurance',
            queryParams: { type: 'travel' },
          },
        ],
      },
      {
        tag: 1,
        title: this.FLang.A05.A0008,
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0008_.s001,
            url: '/online-insurance/car-insurance',
            queryParams: {},
          },
          {
            id: 1,
            itemName: this.FLang.A05.A0008_.s002,
            url: '/online-insurance/motor-insurance',
            queryParams: {},
          },
          {
            id: 2,
            itemName: this.FLang.A05.A0008_.s003,
            url: '/online-insurance/travel-insurance',
            queryParams: {},
          },
          {
            id: 3,
            itemName: this.FLang.A05.A0008_.s004,
            url: '/online-insurance/house-insurance',
            queryParams: {},
          },
          {
            id: 4,
            itemName: this.FLang.A05.A0008_.s005,
            url: '/online-insurance/injury-insurance',
            queryParams: {},
          },
        ],
      },
    ];

    /** 商業商品 */
    this.commercialList = [
      {
        tag: 0,
        title: this.FLang.A05.A0010,
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0010_.s001,
            url: '/online-insurance/engineering-insurance',
            queryParams: {},
          },
          {
            id: 1,
            itemName: this.FLang.A05.A0010_.s002,
            url: '/online-insurance/liability-insurance',
            queryParams: {},
          },
          {
            id: 2,
            itemName: this.FLang.A05.A0010_.s003,
            url: '/online-insurance/property-insurance',
            queryParams: {},
          },
          {
            id: 3,
            itemName: this.FLang.A05.A0010_.s004,
            url: '/online-insurance/cargo-insurance',
            queryParams: {},
          },
        ],
      },
    ];

    /** 保戶服務 */
    this.serviceCenterList = [
      {
        tag: 0,
        title: this.FLang.A05.A0012,
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0012_.s001,
            url: '/member',
            queryParams: {},
          },
          {
            id: 1,
            itemName: this.FLang.A05.A0012_.s002,
            url: '/member/important-notice',
            queryParams: {},
          },
        ],
      },
      {
        tag: 1,
        title: this.FLang.A05.A0013,
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0013_.s001,
            url: 'member/my-order',
            queryParams: {},
          },
          {
            id: 1,
            itemName: this.FLang.A05.A0013_.s002,
            url: 'member/my-policy',
            queryParams: {},
          },
          {
            id: 2,
            // 不執行此處，執行保單變更api
            itemName: this.FLang.A05.A0013_.s003,
            url: '/insurance-changing',
            queryParams: {},
          },
          {
            id: 3,
            // 不執行此處，執行我要續保api
            itemName: this.FLang.A05.A0013_.s004,
            url: '/member',
            queryParams: {},
          },
          {
            id: 4,
            // 不執行此處，執行我要續保api
            itemName: this.FLang.A05.A0013_.s005,
            url: '/member/insurance-query',
            queryParams: {},
          },
        ],
      },
      {
        tag: 2,
        title: this.FLang.A05.A0014,
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0014_.s001,
            url: '/member/claim',
            queryParams: {},
          },
          {
            id: 1,
            itemName: this.FLang.A05.A0014_.s002,
            url: '/member/claim/search',
            queryParams: {},
          },
        ],
      },
      {
        tag: 3,
        title: this.FLang.A05.A0015,
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0015_.s001,
            url: '/member/maintain',
            queryParams: {},
          },
          {
            id: 1,
            itemName: this.FLang.A05.A0015_.s002,
            url: '/login/change-password',
            queryParams: {},
          },
          {
            id: 2,
            // 不執行此處，執行開通線下保單函式
            itemName: this.FLang.A05.A0015_.s003,
            url: '/member/verify',
            queryParams: {},
          },
        ],
      },
    ];

    /** 關於國泰 */
    this.aboutList = [
      {
        tag: 0,
        title: '',
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0016_.s001,
            url: '/about',
            queryParams: {},
          },
          // {
          //   id: 1,
          //   itemName: this.FLang.A05.A0016_.s002,
          //   url: '/about/opening',
          //   queryParams: {},
          // },
          {
            id: 1,
            itemName: this.FLang.A05.A0016_.s003,
            url: '/news',
            queryParams: { type: 'all' },
          },
          {
            id: 2,
            itemName: this.FLang.A05.A0016_.s004,
            url: '/about/opening',
          },
        ],
      },
    ];

    /** 其他 */
    this.otherList = [
      {
        tag: 0,
        title: this.FLang.A05.A0018,
        content: [
          {
            id: 0,
            itemName: this.FLang.A05.A0018_.s001,
            url: 'https://www.cathay-ins.com.vn/',
            queryParams: {},
          },
          {
            id: 1,
            itemName: this.FLang.A05.A0018_.s002,
            url: 'https://www.cathaylife.com.vn/cathay/',
            queryParams: {},
          },
          {
            id: 2,
            itemName: this.FLang.A05.A0019_.s001,
            url:
              'https://drive.google.com/file/d/1YjhseI7Cn6SukickkGOKf8kg68Mz1EDy/view?usp=sharing',
            queryParams: {},
          },
          {
            id: 3,
            itemName: this.FLang.A05.A0019_.s002,
            url:
              'https://drive.google.com/file/d/1sSjIbkacE7vrz5X6vP7D7uUvA95eUxxq/view?usp=sharing',
            queryParams: {},
          },
        ],
      },
      // {
      //   tag: 1,
      //   title: this.FLang.A05.A0019,
      //   content: [
      //     {
      //       id: 0,
      //       itemName: this.FLang.A05.A0019_.s001,
      //       url:
      //         'https://drive.google.com/file/d/1YjhseI7Cn6SukickkGOKf8kg68Mz1EDy/view?usp=sharing',
      //       queryParams: {},
      //     },
      //     {
      //       id: 1,
      //       itemName: this.FLang.A05.A0019_.s002,
      //       url:
      //         'https://drive.google.com/file/d/1sSjIbkacE7vrz5X6vP7D7uUvA95eUxxq/view?usp=sharing',
      //       queryParams: {},
      //     },
      //   ],
      // },
    ];
  }

  helpUrl(tag, id) {
    const result = this.helpList
      .find((type) => type.tag === tag)
      ?.content.find((option) => option.id === id);

    return this.router.navigate([result.url], {
      queryParams: result.queryParams,
    });
  }

  personalUrl(tag, id) {
    const result = this.personalList
      .find((type) => type.tag === tag)
      ?.content.find((option) => option.id === id);

    return this.router.navigate([result.url], {
      queryParams: result.queryParams,
    });
  }

  commercialUrl(tag, id) {
    const result = this.commercialList
      .find((type) => type.tag === tag)
      ?.content.find((option) => option.id === id);

    return this.router.navigate([result.url], {
      queryParams: result.queryParams,
    });
  }

  serviceCenterUrl(tag, id) {
    // 點擊保單變更時 tag=1 id=2 則執行保單變更api
    if (tag === 1 && id === 2) {
      this.onPolicyChange();
      return;
    }

    // 點擊我要續保時 tag=1 id=3 則執行我要續保api
    if (tag === 1 && id === 3) {
      this.onRenewal();
      return;
    }

    // 點擊開通線下保單時 tag=3 id=2 則執行開通線下保單函式
    if (tag === 3 && id === 2) {
      this.activateOffline();
      return;
    }

    const result = this.serviceCenterList
      .find((type) => type.tag === tag)
      ?.content.find((option) => option.id === id);

    return this.router.navigate([result.url], {
      queryParams: result.queryParams,
    });
  }

  aboutUrl(tag, id) {
    const result = this.aboutList
      .find((type) => type.tag === tag)
      ?.content.find((option) => option.id === id);

    return this.router.navigate([result.url], {
      queryParams: result.queryParams,
    });
  }

  otherUrl(tag, id) {
    return this.otherList
      .find((type) => type.tag === tag)
      ?.content.find((option) => option.id === id).url;
  }

  // 保單變更
  async onPolicyChange(event = undefined) {
    if (!this.cds.userInfo.seq_no) {
      this.cds.gPre_url = '/member';
      this.cds.gWhocall = 'policyChange';
      this.router.navigate(['/login']);
      return;
    }

    const payload = {
      language: this.FLang.Type,
      token: this.cds.gToken,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      certificate_type: '1',
      certificate_number_9: this.cds.userInfo.certificate_number_9,
      certificate_number_12: this.cds.userInfo.certificate_number_12,
      customer_name: this.cds.userInfo.customer_name.trim(),
      birthday: this.cds.userInfo.birthday,
      is_validate_policy: this.cds.userInfo.is_validate_policy,
      member_seq_no: this.cds.userInfo.seq_no,
    };

    const rep = await this.apiPolicy.GetEndRsmntCard(payload);

    if (rep.status === 200) {
      this.cds.gEndRsmntCard = rep.data;
    }
    // 呼叫API
    if (this.cds.gEndRsmntCard && this.cds.gEndRsmntCard.length > 0) {
      // 符合條件
      $('#Modal-18').modal('show');
    } else {
      // 無符合條件
      this.alter.open({
        type: 'file ',
        title: this.FLang.F001.L0018,
        content: this.FLang.F001.L0019,
      });
    }
  }

  // 我要續保
  async onRenewal(event = undefined) {
    if (!this.cds.userInfo.seq_no) {
      this.cds.gPre_url = '/member';
      this.cds.gWhocall = 'keep';
      this.router.navigate(['/login']);
      return;
    }

    const payload = {
      language: this.FLang.A05Type,
      token: this.cds.gToken,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      certificate_type: '1',
      certificate_number_9: this.cds.userInfo.certificate_number_9,
      certificate_number_12: this.cds.userInfo.certificate_number_12,
      customer_name: this.cds.userInfo.customer_name.trim(),
      birthday: this.cds.userInfo.birthday,
      is_validate_policy: this.cds.userInfo.is_validate_policy,
      member_seq_no: this.cds.userInfo.seq_no,
    };

    const rep = await this.apiPolicy.GetDueSoonCard(payload);

    if (rep.status === 200) {
      this.cds.gDueSoonCard = rep.data;
    }
    // 呼叫API
    if (this.cds.gDueSoonCard && this.cds.gDueSoonCard.length > 0) {
      // 符合條件
      $('#Modal-17').modal('show');
    } else {
      // 無符合條件
      this.alter.open({
        type: 'file',
        title: this.FLang.F001.L0018,
        content: this.FLang.F001.L0025,
      });
    }
  }

  // 開通線下保單
  activateOffline() {
    if (this.cds.userInfo.is_validate_policy === 'Y') {
      this.alter.open({
        type: 'info',
        title: '',
        content: this.FLang.popup.P0001,
      });
      return;
    }
    this.router.navigate(['/member/verify']);
  }
}
