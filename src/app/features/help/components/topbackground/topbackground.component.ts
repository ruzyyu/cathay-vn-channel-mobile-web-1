import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-topbackground',
  templateUrl: './topbackground.component.html',
  styleUrls: ['./topbackground.component.scss'],
})
export class TopbackgroundComponent implements OnInit {
  @Input() clickItem!: any;

  constructor() {}

  ngOnInit(): void {}
}
