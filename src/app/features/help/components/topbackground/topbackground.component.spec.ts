import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopbackgroundComponent } from './topbackground.component';

describe('TopbackgroundComponent', () => {
  let component: TopbackgroundComponent;
  let fixture: ComponentFixture<TopbackgroundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopbackgroundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopbackgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
