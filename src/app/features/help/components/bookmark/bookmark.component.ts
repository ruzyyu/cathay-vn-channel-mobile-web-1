import { AfterViewInit } from '@angular/core';
import { EventEmitter, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';
@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.scss'],
})
export class BookmarkComponent implements AfterViewInit, OnInit {
  @Output() selectBookmark = new EventEmitter<number>();
  FLang: any;
  tagId = 'question';
  bookmarkList: any;

  constructor(
    private cds: DatapoolService,
    private numberPipe: NumberPipe,
    private route: ActivatedRoute
  ) {
    this.FLang = this.cds.gLang.H01;

    this.bookmarkList = [
      {
        tag: 'question',
        title: this.FLang.H0002,
        queryParams: { type: 'question' },
      },
      {
        tag: 'download',
        title: this.FLang.H0003,
        queryParams: { type: 'download' },
      },
      {
        tag: 'info',
        title: this.FLang.H0004,
        queryParams: { type: 'info' },
      },
    ];
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.route.queryParams.subscribe((queryParams) => {
      if (queryParams && queryParams.type) {
        switch (queryParams.type) {
          case 'question-personal':
            setTimeout(() => {
              this.selectBookmark.emit(1);
            });
            $('#pills-one-tab').tab('show');
            break;
          case 'question-business':
            setTimeout(() => {
              this.selectBookmark.emit(1);
            });
            $('#pills-one-tab').tab('show');
            break;
          case 'download-personal':
            setTimeout(() => {
              this.selectBookmark.emit(2);
            });
            $('#pills-two-tab').tab('show');
            break;
          case 'download-business':
            setTimeout(() => {
              this.selectBookmark.emit(2);
            });
            $('#pills-two-tab').tab('show');
            break;
          case 'info':
            setTimeout(() => {
              this.selectBookmark.emit(3);
            });
            $('#pills-three-tab').tab('show');
            break;
        }
      }
    });
  }

  setBookmark(item: number) {
    this.selectBookmark.emit(item);
  }

  ariaControls(index) {
    return 'pills-' + this.numberPipe.transform(index + 1);
  }

  idControls(index) {
    return 'pills-' + this.numberPipe.transform(index + 1) + '-tab';
  }
}
