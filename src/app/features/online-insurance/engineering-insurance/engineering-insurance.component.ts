import { AfterViewInit, Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/core/services/common.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';

@Component({
  selector: 'app-engineering-insurance',
  templateUrl: './engineering-insurance.component.html',
  styleUrls: ['./engineering-insurance.component.scss'],
})
export class EngineeringInsuranceComponent implements OnInit, AfterViewInit {
  items: any;

  insuranceRangeList = [];

  faqList = [];

  /** 變數 */
  activeIndex = 0;

  // 變數宣告
  FLang: any;
  FLangType: any;

  constructor(private numberPipe: NumberPipe, private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit(): void {
    this.items = {
      icons: [
        {
          defaultImg: 'assets/img/commodity/ic-claim-engineering-collapse.png',
          defaultSrcset:
            'assets/img/commodity/ic-claim-engineering-collapse@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-engineering-collapse-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-engineering-collapse-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-engineering-error.png',
          defaultSrcset:
            'assets/img/commodity/ic-claim-engineering-error@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-engineering-error-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-engineering-error-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-engineering-explosion.png',
          defaultSrcset:
            'assets/img/commodity/ic-claim-engineering-explosion@2x.png 2x',
          activeImg:
            'assets/img/commodity/ic-claim-engineering-explosion-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-engineering-explosion-w@2x.png 2x',
        },
      ],
      content: [
        {
          title: this.FLang.E01.E0002,
          directions: this.FLang.E01.E0003,
        },
        {
          title: this.FLang.E01.E0004,
          directions: this.FLang.E01.E0005,
        },
        {
          title: this.FLang.E01.E0006,
          directions: this.FLang.E01.E0007,
        },
      ],
    };

    this.insuranceRangeList = [
      {
        title: this.FLang.E01.E0009,
        directions: this.FLang.E01.E0010,
        content: this.FLang.E01.E0011,
        defaultImg: 'assets/img/commodity/engineering-item-01.png',
        defaultSrcset: 'assets/img/commodity/engineering-item-01@2x.png 2x',
        activeImg: 'assets/img/commodity/engineering-item-01-m.png',
        activeSrcset: 'assets/img/commodity/engineering-item-01-m@2x.png 2x',
      },
      {
        title: this.FLang.E01.E0012,
        directions: this.FLang.E01.E0013,
        content: this.FLang.E01.E0014,
        defaultImg: 'assets/img/commodity/engineering-item-02.png',
        defaultSrcset: 'assets/img/commodity/engineering-item-02@2x.png 2x',
        activeImg: 'assets/img/commodity/engineering-item-02-m.png',
        activeSrcset: 'assets/img/commodity/engineering-item-02-m@2x.png 2x',
      },
      {
        title: this.FLang.E01.E0015,
        directions: this.FLang.E01.E0016,
        content: this.FLang.E01.E0017,
        defaultImg: 'assets/img/commodity/engineering-item-03.png',
        defaultSrcset: 'assets/img/commodity/engineering-item-03@2x.png 2x',
        activeImg: 'assets/img/commodity/engineering-item-03-m.png',
        activeSrcset: 'assets/img/commodity/engineering-item-03-m@2x.png 2x',
      },
      {
        title: this.FLang.E01.E0018,
        directions: this.FLang.E01.E0019,
        content: this.FLang.E01.E0020,
        defaultImg: 'assets/img/commodity/engineering-item-04.png',
        defaultSrcset: 'assets/img/commodity/engineering-item-04@2x.png 2x',
        activeImg: 'assets/img/commodity/engineering-item-04-m.png',
        activeSrcset: 'assets/img/commodity/engineering-item-04-m@2x.png 2x',
      },
      {
        title: this.FLang.E01.E0021,
        directions: this.FLang.E01.E0022,
        content: this.FLang.E01.E0023,
        defaultImg: 'assets/img/commodity/engineering-item-05.png',
        defaultSrcset: 'assets/img/commodity/engineering-item-05@2x.png 2x',
        activeImg: 'assets/img/commodity/engineering-item-05-m.png',
        activeSrcset: ' assets/img/commodity/engineering-item-05-m@2x.png 2x',
      },
    ];

    this.faqList = [
      {
        title: this.FLang.E01.E0025,
        content: this.FLang.E01.E0025_.s001,
      },
      {
        title: this.FLang.E01.E0026,
        content: this.FLang.E01.E0026_.s001,
      },
      {
        title: this.FLang.E01.E0027,
        content: this.FLang.E01.E0027_.s001,
      },
    ];
  }

  ngAfterViewInit(): void {
    CommonService.autoScrollToAccordionHeader('#accordionQuestion');
  }

  /** Number 轉換 */
  dataFilterNumber(index) {
    return 'select-' + this.numberPipe.transform(index + 2);
  }

  clickStyle(index) {
    this.activeIndex = index;
  }

  cssClassNumber(index) {
    let classPipe = this.numberPipe.transform(index + 2);
    if (index === this.activeIndex) {
      classPipe += ' active';
    }
    return classPipe;
  }

  /** 圖片位置 */
  switchPosition(index) {
    const style = 'col-sm-12 col-lg-6';
    return index % 2 === 0 ? style : style + ' order-lg-first';
  }

  /** FAQ list */
  titleId(index: number) {
    return 'FaqQuestion' + this.numberPipe.transform(index + 1);
  }
  titleDataTarget(index: number) {
    return '#collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  titleAriaControls(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoId(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoAriaLabelledby(index: number) {
    return 'FaqQuestionQue' + this.numberPipe.transform(index + 1);
  }
  titleContent(item: any) {
    return `
    <div class="tab-title d-flex">
      <h4>${item.title}</h4>
      <i class="icons-ic-faq-arrow-up i-icon i-up"></i>
      <i class="icons-ic-faq-arrow-down i-icon i-down"></i>
    </div>
    `;
  }

  infoContent(item: any) {
    return `
    <div class="que-body d-flex">
      <div class="answer custom_text_align white-space-normal">${item.content}</div>
    </div>
    `;
  }

  /** banner 圖片 語言 switch */
  bannerImg(size: number) {
    const img1920 = 'assets/img/commodity/bn-engineering-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-engineering-1200-x-400';
    const img800 = 'assets/img/commodity/bn-engineering-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '.png';
        case 2:
          return img1200 + '.png';
        case 3:
          return img800 + '.png';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en.png';
        case 2:
          return img1200 + '-en.png';
        case 3:
          return img800 + '-en.png';
      }
    }
  }

  /** banner 圖片 語言 switch */
  bannerImgBig(size: number) {
    const img1920 = 'assets/img/commodity/bn-engineering-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-engineering-1200-x-400';
    const img800 = 'assets/img/commodity/bn-engineering-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '@2x.png 2x';
        case 2:
          return img1200 + '@2x.png 2x';
        case 3:
          return img800 + '@2x.png 2x';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en@2x.png 2x';
        case 2:
          return img1200 + '-en@2x.png 2x';
        case 3:
          return img800 + '-en@2x.png 2x';
      }
    }
  }
}
