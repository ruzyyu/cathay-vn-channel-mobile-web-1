import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineeringInsuranceComponent } from './engineering-insurance.component';

describe('EngineeringInsuranceComponent', () => {
  let component: EngineeringInsuranceComponent;
  let fixture: ComponentFixture<EngineeringInsuranceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EngineeringInsuranceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineeringInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
