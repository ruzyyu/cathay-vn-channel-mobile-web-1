import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EngineeringInsuranceComponent } from './engineering-insurance.component';

const routes: Routes = [{ path: '', component: EngineeringInsuranceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EngineeringInsuranceRoutingModule { }
