import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EngineeringInsuranceRoutingModule } from './engineering-insurance-routing.module';
import { EngineeringInsuranceComponent } from './engineering-insurance.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [EngineeringInsuranceComponent],
  imports: [CommonModule, EngineeringInsuranceRoutingModule, SharedModule],
})
export class EngineeringInsuranceModule {}
