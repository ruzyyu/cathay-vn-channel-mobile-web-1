import { UserinfoComponent } from './../../../shared/components/userinfo/userinfo.component';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { OnlinecalcService } from '../../../core/services/onlinecalc/onlinecalc.service';
import { PaymentService } from '../../../core/services/payment/payment.service';
import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  OnDestroy,
  AfterViewInit,
  HostListener,
} from '@angular/core';
import {
  Validators,
  FormGroup,
  FormBuilder,
  AbstractControl,
} from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import {
  UserInfo,
  IProvince,
  ICounty,
  motorUserInfo,
  IVehicleUse,
  IVechiclemotoKd,
  IResponse,
  ICalcOrder,
  IConfirm,
} from 'src/app/core/interface';

import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { PublicService } from 'src/app/core/services/api/public/public.service';
import { CommonService } from 'src/app/core/services/common.service';
import { InsuranceService } from 'src/app/core/services/api/Insurance/insurance.service';
import { ComponentFactoryResolver } from '@angular/core';
import { ModalService } from 'src/app/core/modal/modal.service';
import { LoginComponent } from 'src/app/shared/components/login/login.component';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import moment from 'moment';
import { filter } from 'rxjs/operators';
import { Lang } from 'src/types';

@Component({
  selector: 'app-project-trial-motor',
  templateUrl: './project-trial-motor.component.html',
  styleUrls: ['./project-trial-motor.component.scss'],
})
export class ProjectTrialMotorComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  constructor(
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private fb: FormBuilder,
    private go: Router,
    private pay: PaymentService,
    private api: PublicService,
    private apiInsurance: InsuranceService,
    private componentFactory: ComponentFactoryResolver,
    private modal: ModalService,
    private apiMember: MemberService,
    private calcsv: OnlinecalcService,
    private route: ActivatedRoute
  ) {
    /** 首次登入不允許再 個資頁 */
    this.ro = this.go.events.pipe(
      filter((event) => event instanceof NavigationEnd)
    ) as Observable<NavigationEnd>;
    this.ro.subscribe((evt) => {
      if (
        evt.id === 1 &&
        evt.url === '/online-insurance/project-trial-motor/personInfo'
      ) {
        this.go.navigate(['/online-insurance/project-trial-motor/', 'calcAmt']);
      }

      // 如果不是在本頁的活動就清除日期
      if (
        !(
          this.FPreviousUrl ===
            '/online-insurance/project-trial-motor/personInfo' ||
          this.FPreviousUrl === '/online-insurance/project-trial-motor/calcAmt'
        )
      ) {
        // this.FMotorDate = undefined;
        this.Fdate1 = undefined;
        delete this.cds.gDatapicker.motor;
      }

      if (evt.url === '/online-insurance/project-trial-motor/personInfo') {
        this.FPayTransaction = false;
      }
      this.FPreviousUrl = evt.url;
    });

    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
    this.FTrafficType = this.cds.gTrafficType;
    this.cds.gTrafficType = false;

    if (typeof this.FLang.C04.step1.C0008_.s005 == 'string') {
      this.FLang.C04.step1.C0008_.s005 = this.FLang.C04.step1.C0008_.s005.split(
        '$'
      ) as any;
    }

    /** 付款方式 list */
    this.payList = this.calcsv.paylist;
    this.FSelectPayType = this.payList[0];
  }
  @ViewChild('content', { read: ViewContainerRef }) content: ViewContainerRef;
  @ViewChild('ly_pluInfo', { read: ViewContainerRef })
  ly_pluInfo: ViewContainerRef;
  @ViewChild('ly_userinfo', { read: UserinfoComponent })
  ly_userinfo: UserinfoComponent;

  /** 變數 */
  submitResult: boolean;
  motorNumber = -1;
  insuranceNumber = -1;
  peopleNumber: number | string = 'all';
  amountNumber: number | string = 'all';
  activeNumberAmount: string | number = 'all';
  provinceNumber: string | number = 'all';
  countryNumber: string | number = 'all';
  paymentNumber = -1;
  // 同投保人
  personSameName: string;
  clickSameName = false;

  motorTypeNumber: string;
  // 試算
  trialCalculation = false;
  // 同意合約
  agreeContract = false;
  // 加購
  plusPurchase = false;
  countInsuranceAmount = '0';
  clickNumber = 0;
  FStep: 'calcAmt' | 'personInfo' | 'payment' = 'calcAmt';
  // 合計保費總金額
  FAmount = 0;
  // 乘客傷害險金額
  passengerAmount = 0;
  // 填寫個人資料按鈕 狀態
  checkFormatStatus = false;

  // 保險期間控制
  FMaxYear: number;
  FMaxDate: Date;
  FMinYear: number;
  FMinDate: Date;

  // 有沒有車牌
  filterLicensePlate = null;

  // 存取值
  repPayload: any;

  /** 宣告 */
  userInfo: UserInfo;
  motorUserInfo: motorUserInfo;
  validators: {};
  newMemberInfo: {};
  group: FormGroup;
  FProvinceList: Array<IProvince>;
  FCountyList: Array<ICounty>;
  FCalcOrder: ICalcOrder;
  controls: { [key: string]: AbstractControl };
  payList: any;
  FSelectPayType: any;
  FSyncUserinfo = true;
  amountItemsList: any;
  /** 訂單狀態 */
  FOrder_no = 0;
  /** 保單編號 */
  FAply_no = '';
  // 用於判斷是否是新增會員
  FType: string;
  // 金流選項
  paymant = '';
  // 手機板驗證高度用的
  initInnerHieght: number;
  /** 資料區 */
  motorItemsList?: IVechiclemotoKd[];
  insuranceItemsList: IVehicleUse[];
  /** 註冊渠道 */
  FChannel: string;
  /** 註冊帳號及姓名 */
  FSocialName: string;
  FSocialAccount: string;

  peopleItemsList = [{ option: 1 }];

  /** 續保 保期平移 .. */
  FDate_HH = '00';
  FDate_YYYYMMDD = '';
  FTrafficType: boolean;

  /** 判斷付款[完成否] */
  FPayResult = false;
  /** 判斷付款-2[付款失敗 狀態On] */
  FPayTransaction = false;

  /** 額外錯誤文字email */
  FErrorMail: string;

  FLangType: string;
  FLang: Lang = this.cds.gLang;

  /** 監測router */
  ro: Observable<any>;
  unload: any = [];
  /** 時間 */
  Fdate1: string;
  Fdate2: string = null;
  /** 預設換頁日期不清除 */
  FMotorDate: string;

  /** 存放上一頁網址 */
  FPreviousUrl;

  /** 選擇付款跟同意的提示start */
  FAgreeError: string;
  FPayNoError: string;
  /** 付款二次確認窗 */
  FConfirm: IConfirm;

  /** confirm 專用 */
  FsubscribePayAction: Subscription;

  FReadonly: boolean;

  /** for 輸入法用 */
  motorValue: string;

  // 小網日期不得開啟填寫功能
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }

    this.alignPaymentText();
  }

  ngOnDestroy(): void {
    this.cds.gTrafficicinsurance = null;
    this.unload.forEach((x) => x.unsubscribe());
    if (this.FsubscribePayAction) {
      this.FsubscribePayAction.unsubscribe();
    }
  }

  async ngOnInit() {
    /** 金額 list */
    this.amountItemsList = [
      {
        id: 0,
        option: this.FLang.C04.step2.C0033,
        number: 5 * 1000000,
        unit: 5,
      },
      {
        id: 1,
        option: this.FLang.C04.step2.C0034,
        number: 10 * 1000000,
        unit: 10,
      },
      {
        id: 2,
        option: this.FLang.C04.step2.C0035,
        number: 20 * 1000000,
        unit: 20,
      },
      {
        id: 3,
        option: this.FLang.C04.step2.C0036,
        number: 30 * 1000000,
        unit: 30,
      },
    ];

    /** 假換頁hash 處理 */
    this.route.params.subscribe((params) => {
      this.FStep = params.step;
      if (this.FStep) {
        $('#paymentFailModal').modal('hide');
      }
    });

    /** userinfo註冊換頁.... */
    this.calcsv.Inituser();

    /** 日期切割 */
    const sd = new Date();
    let ed = new Date();
    sd.setFullYear(sd.getFullYear() + 1);
    ed.setDate(ed.getDate() + 1);
    ed = new Date(ed);
    this.FMaxDate = sd;
    this.FMinDate = ed;

    /** userinfo
     *  編輯頁帶入 > cds.userinfo > cds.gTCalc_to_save.userinfo
     */
    this.motorUserInfo = this.calcsv.initMotor(); // this.cds.motorUserInfo;
    this.userInfo = this.cds.userInfo;

    /** 表單驗證項目 */
    this.validators = this.calcsv.initData_motor(
      this.userInfo,
      this.motorUserInfo,
      this.motorValue
    );
    this.formValidate();

    await this.init();

    /** 網頁迴車鍵 */
    if (
      this.cds.gTCalc_to_save &&
      this.cds.gTCalc_to_save.type === 'motor' &&
      !this.route.snapshot.queryParams.action
    ) {
      this.setValue(this.cds.gTCalc_to_save.saveorder);
      if (!this.cds.userInfo.seq_no) {
        this.calcsv.converuser(this.cds.userInfoTemp);
        this.ly_userinfo.doReflashScreen();
      }
      this.calcsv.resetData();
    }

    /** 首次註冊的特別流程 */
    this.route.queryParams.subscribe(async (queryParams) => {
      // 導頁付款.
      if (queryParams.action === 'pay') {
        await this.setValue(this.cds.gTCalc_to_save.saveorder);
        this.cds.gTCalc_to_save.type = 'motor';
        await this.doPayment(
          this.cds.gTCalc_to_save,
          false,
          queryParams.action
        );
        this.calcsv.resetData();
        this.cds.userInfo.seq_no ? (this.FType = 'edit') : (this.FType = 'add');
      }
    });

    // 子視窗存狀態碼，避免關閉錯誤
    this.route.queryParams.subscribe(async (queryParams) => {
      if (queryParams.amount) {
        this.unload.forEach((subscribe) => {
          subscribe.unsubscribe();
        });
      } else {
        if (this.unload.length === 0) {
          this.unload.push(
            // 註冊全局的事件
            fromEvent(window, 'beforeunload').subscribe((event: any) => {
              (event || window.event).returnValue =
                '★資料尚未存檔，確定是否要離開？★';
              return '★資料尚未存檔，確定是否要離開？★';
            })
          );
        }
      }
    });

    /** 編輯頁帶入userinfo */
    if (this.cds.gTrafficicinsurance && this.cds.gTrafficicinsurance.vehc_kd) {
      this.setValue(this.cds.gTrafficicinsurance);
    }

    // 小網日期不得開啟填寫功能
    if ($(document).innerWidth() > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }

  ngAfterViewInit(): void {
    this.route.params.subscribe((params) => {
      switch (params.step) {
        case 'calcAmt':
          // 避免發生 Error: ExpressionChangedAfterItHasBeenCheckedError
          if (this.controls && this.controls.motor_procDate.value) {
            this.Fdate1 = moment(this.controls.motor_procDate.value).format(
              'DD/MM/YYYY'
            );
          }
          if (this.controls && this.controls.motor_procDateEnd.value) {
            this.Fdate2 = moment(this.controls.motor_procDateEnd.value).format(
              'DD/MM/YYYY'
            );
          }
          break;
        case 'personInfo':
          setTimeout(this.alignPaymentText, 500);
          if (this.filterLicensePlate){
            this.controls.motor_body_number.disable();
            this.controls.motor_engn_no.disable();
          }
          break;
      }
    });

    // 預設 車牌號碼 選取
    this.filterLicensePlate = true;
  }

  async init() {
    // 處理從訂單編輯，或是續保流程
    await this.api.gMotorList();
    if (!this.motorItemsList) {
      this.motorItemsList = this.cds.gPubCommon.motoList;
    }
  }

  /** 從保單取值過來 */
  async setValue(param) {
    const {
      fram_no,
      rgst_no,
      engn_no,
      vehc_ownr,
      vehc_kd,
      car_pwr_kd,
      pol_dt,
      pol_due_dt,
      tot_prem_with_vat,
      passenger_amt,
      order_no,
      aply_no,
      pol_dt_kind,
      customer_name,
      paymentNumber,
    } = param;

    /** 訂單狀態 */
    this.FOrder_no = order_no;
    /** 保單編號 */
    this.FAply_no = aply_no;

    // 從保單過來的值
    this.controls.motor_name.setValue(vehc_ownr);
    this.controls.motor_mkd.setValue(car_pwr_kd);

    this.controls.motor_procDate.setValue(moment(pol_dt).format('YYYY-MM-DD'));
    this.controls.motor_procDateEnd.setValue(
      moment(pol_due_dt).format('YYYY-MM-DD')
    );

    this.controls.r_year.setValue(pol_dt_kind);

    moment(param.pol_dt).format('YYYY-MM-DD');
    this.FDate_HH = CommonService.paddingLeft(
      moment(param.pol_dt).get('hours'),
      2
    );
    this.FDate_YYYYMMDD = moment(param.pol_dt).format('YYYY-MM-DD');

    this.Fdate1 = `${moment(this.controls.motor_procDate.value).format(
      'DD/MM/YYYY'
    )}`;

    this.Fdate2 = `${moment(this.controls.motor_procDateEnd.value).format(
      'DD/MM/YYYY'
    )}`;

    this.controls.motor_vkd.setValue(vehc_kd);
    /** 先執行避免值被覆蓋 */
    this.switchLicense(rgst_no ? true : false);
    this.controls.motor_license_plate.setValue(rgst_no);
    this.controls.motor_body_number.setValue(fram_no);
    this.controls.motor_engn_no.setValue(engn_no);
    this.FAmount = tot_prem_with_vat;
    this.passengerAmount = passenger_amt;

    // -----------------------------------------------------------------------
    /** 動作處理 step1 */
    // 機車種類下拉式選單 順序不可調
    // console.log(vehc_kd, car_pwr_kd);

    this.onMotorFilterNumber(
      await this.api.getMotorName(vehc_kd, car_pwr_kd),
      false
    );

    // 試算頁直接開啟 順序不可調
    this.clickNumber = 1;

    // 乘客強制險下拉式選單
    if (passenger_amt !== 0) {
      this.plusPurchase = true;
      this.amountNumber = this.amountItemsList.filter(
        (money) => money.number === passenger_amt
      )[0].id;
    }

    /** 動作處理 step2 */
    // 姓名開關
    if (vehc_ownr === customer_name) {
      this.clickSameName = true;
      this.controls.motor_name.disable();
    }

    // 車牌號碼或車身號碼開關
    if (rgst_no !== '' || fram_no !== '') {
      if (rgst_no) {
        this.filterLicensePlate = true;
        this.controls.motor_body_number.disable();
      } else {
        this.filterLicensePlate = false;
        this.controls.motor_license_plate.disable();
      }
    }

    this.paymentNumber = paymentNumber;

    this.calcTest(1, false);

    /** 清除狀態 */
    if (this.cds.gTCalc_to_save) {
      this.cds.gTCalc_to_save.type = 'none';
    }
    this.cds.gTrafficicinsurance = null;
  }

  // -------------------------------------------------------------------------
  /** 試算 */
  async calcTest(index, isCalc = true) {
    this.controls.motor_vkd.markAsTouched();
    this.controls.motor_procDate.markAsTouched();
    this.controls.motor_procDateEnd.markAsTouched();
    if (this.checkCalc()) {
      return;
    }

    // 送出之前把資料寫入宣告物件內 使用宣告物件來計算
    this.motorUserInfo.maxCapl = this.controls.motor_people.value;
    this.motorUserInfo.procDate = this.controls.motor_procDate.value;
    this.motorUserInfo.procDate_end = this.controls.motor_procDateEnd.value;
    this.motorUserInfo.procDateYear = this.controls.r_year.value;
    this.motorUserInfo.vehcKd =
      this.motorItemsList[this.motorNumber].vehcKdValue;
    this.motorUserInfo.carpwrkd = this.insuranceItemsList[0].carPwrKdValue;

    let payload = {
      language: this.cds.gLangType,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      token: this.cds.gToken,
      typepol: 'B',
      prodpolcode: 'CF02001',
      vehckd: this.motorUserInfo.vehcKd,
      carpwrkd: this.motorUserInfo.carpwrkd,
      procdate: this.motorUserInfo.procDate,
      pol_dt: `${this.motorUserInfo.procDate} ${this.FDate_HH}:00:00`,
      pol_due_dt: `${this.motorUserInfo.procDate_end} ${this.FDate_HH}:00:00`,
      maxcapl: Number(this.motorUserInfo.maxCapl),
      pol_dt_kind: this.motorUserInfo.procDateYear,
    };

    if (this.plusPurchase && this.amountNumber >= 0) {
      const mAttach = {
        driveramt: 0,
        drivercnt: 0,
        passengercnt: 2,
        passengeramt: this.amountItemsList[this.amountNumber].number,
      };

      payload = {
        ...payload,
        ...mAttach,
      };
    }
    const rep: IResponse = await this.apiInsurance.calcInsurance(payload);

    if (rep.status === 200) {
      this.FCalcOrder = rep.data;
      this.FAmount = rep.data.totpremwithvat;

      // 檢核過 開啟下方
      this.clickNumber = 1;
    }

    if (index === 2) {
      // 為了滾動
      if (isCalc) {
        CommonService.scroll(this.ly_pluInfo.element.nativeElement);
      }
    }

    this.checkFormat();
  }

  checkCalc() {
    const mbol = !(
      this.motorNumber >= 0 &&
      this.controls.motor_vkd.valid &&
      this.controls.motor_procDate.valid &&
      this.controls.motor_procDateEnd.valid
    );

    return mbol;
  }

  // -------------------------------------------------------------------------

  /** 共用 */
  styleCss(index) {
    const style = 'text-gray-gr';
    return index === true ? '' : style;
  }

  /** 清除字串 */
  doClearInput(item: string) {
    this.group.controls[`${item}`].setValue('');
  }

  /** alert 窗 */
  compulsoryInsuranceAlertOne() {
    $('#intro_policy1').modal('show');
  }

  compulsoryInsuranceAlertTwo() {
    $('#intro_policy2').modal('show');
  }

  // ------------------------------------------------------------------------
  /** step1 填寫個人資料鍵 檢核 */
  checkFormat(motorType?: number) {
    if (motorType != null) {
      return this.motorNumber === motorType && this.motorUserInfo.procDate
        ? this.checkFormatFun()
        : (this.checkFormatStatus = false);
    } else {
      return this.motorNumber !== -1 && this.motorUserInfo.procDate
        ? this.checkFormatFun()
        : (this.checkFormatStatus = false);
    }
  }

  /** step1 填寫個人資料鍵 檢核 function */
  checkFormatFun() {
    return this.plusPurchase === true
      ? this.amountNumber !== 'all'
        ? (this.checkFormatStatus = true)
        : (this.checkFormatStatus = false)
      : (this.checkFormatStatus = true);
  }

  /** step2 立即付款鍵 檢核 */
  checkInfo(): boolean {
    const mbol =
      this.paymentNumber >= 0 && this.agreeContract && this.group.valid;

    if (!mbol && this.group.invalid) {
      CommonService.scrollto('lyc_motorinfo');
    } else if (!mbol && this.paymentNumber < 0) {
      CommonService.scrollto(`ly_paymentbox`, 50);
    } else if (!mbol && !this.agreeContract) {
      CommonService.scrollto(`ly_agreeContract`);
    }
    this.doTipError();
    return mbol;
  }
  doTipError() {
    this.FPayNoError = '';
    if (this.paymentNumber < 0) {
      this.FPayNoError = this.FLang.PH.PH044;
    }
    this.FAgreeError = '';
    if (!this.agreeContract) {
      this.FAgreeError = this.FLang.PH.PH045;
    }
  }

  /** 選擇付款跟同意的提示end */

  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];
      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          motor_name:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
          motor_license_plate:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES014,
          motor_body_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES015,
          motor_engn_no:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES016,
        }[control_key];
      }
      /** 新檢核end */

      return {
        fontLengthNumber:
          this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
        vnCertNumber: this.FLang.ER.ER036,
        vn2020ID: this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
        required: this.FLang.ER.ER017,
        vnRgstNo: this.FLang.ER.ER023,
        languageFormat:
          this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
        languagelength:
          this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES001,
      }[type];
    }
    return this.FLang.ER.ER023;
  }

  /** 建立驗證表單 */
  formValidate() {
    // 建立驗證表單
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });

    this.group = this.fb.group(validator, {
      updateOn: 'blur',
      validator: this.vi.MustMatch('password', 'confirmPassword'),
    });

    // 日期異動修改第二個日期
    this.group.get('motor_procDate').valueChanges.subscribe((item) => {
      setTimeout(() => {
        this.Fdate1 = moment(this.controls.motor_procDate.value).format(
          'DD/MM/YYYY'
        );
        this.Fdate2 = moment(this.controls.motor_procDateEnd.value).format(
          'DD/MM/YYYY'
        );
      }, 0);
    });

    //  年份異動時修改第二個日期
    this.group.get('r_year').valueChanges.subscribe((item) => {
      // this.Fdate2 = moment(this.controls.motor_procDateEnd.value).format(
      //   'DD/MM/YYYY'
      // );
      if (this.controls.motor_procDate.value) {
        setTimeout(() => {
          this.controls.motor_procDateEnd.setValue(
            moment(this.controls.motor_procDate.value)
              .add(this.controls.r_year.value, 'years')
              .format('YYYY-MM-DD')
          );
          this.Fdate2 = moment(this.controls.motor_procDateEnd.value).format(
            'DD/MM/YYYY'
          );
        }, 0);
      }
      this.initCalc();
    });

    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  // -----------------------------------------------------------------

  /** 車種 */
  onMotorFilterNumber(id: number, reset = true) {
    if (id !== this.motorNumber) {
      if (reset) {
        // 重新選擇需清空資料
        this.plusPurchase = false;
        this.amountNumber = 'all';
        this.clickNumber = 0;
        this.FAmount = 0;
        this.checkFormat(id);
      }
      this.motorNumber = id;
      this.controls.motor_vkd.setValue(
        this.motorItemsList[this.motorNumber].vehcKdValue
      );
    }

    this.insuranceItemsList = this.cds.gPubCommon.motoList.filter((type) => {
      return type.id === this.motorNumber;
    });
  }

  motorFilterItems() {
    if (this.motorNumber < 0) {
      return this.FLang.PH.enter.F0002 + this.FLang.PH.enter.F0002_.FS004;
    }

    // for 輸入法用
    this.motorValue = this.motorItemsList[this.motorNumber].carPwrKdValue;
    // this.calcsv.initData_motor(
    //   this.userInfo,
    //   this.motorUserInfo,
    //   this.motorValue
    // );
    return this.motorItemsList[this.motorNumber].zhName;
  }

  /** 附加保單開關 */
  attrPolicy(xbol) {
    if (xbol) {
      if (!this.amountNumber) {
        this.amountNumber = 0;
      }
      if (!this.peopleNumber) {
        this.peopleNumber = 0;
      }

      // 總金額更新
      this.FAmount = this.FCalcOrder.totpremwithvat;

      this.calcTest(1);
    } else {
      this.calcTest(1);
      this.FAmount = this.FCalcOrder.totpremwithvat;
    }
  }

  /** 背景 disabled */
  styleBackground(type) {
    if (type) {
      return '';
    } else {
      return 'background-color:#e1e4e9';
    }
  }

  amountFilterNumber(index) {
    this.amountNumber = index;

    this.calcTest(1);
  }

  amountFilterOption(index, type) {
    if (!type) {
      return this.FLang.PH.enter.F0002 + this.FLang.PH.enter.F0002_.FS009;
    } else {
      return (
        this.amountItemsList[index]?.option ||
        this.FLang.PH.enter.F0002 + this.FLang.PH.enter.F0002_.FS009
      );
    }
  }

  /** 試算顯示 */
  displayStyle() {
    const style = 'form-proposal';
    return this.clickNumber === 1 ? style : style + ' d-none';
  }

  switchNumber(type) {
    this.FSelectPayType = this.payList[type];
    this.clickNumber = type;
  }

  // -------------------------------------------------------------------
  /** 省份 */
  getProvinceList() {
    if (this.cds.gPubCommon && this.cds.gPubCommon.provinceList) {
      this.FProvinceList = this.cds.gPubCommon.provinceList
        ? this.cds.gPubCommon.provinceList
        : [];
    } else {
      this.FProvinceList = [];
    }
  }

  /** 郡縣 */
  notifyCounty(event) {
    const index = event.t.selectedIndex - 1;
    this.FCountyList = this.FProvinceList[index].countys;
  }

  /** 同投保人 */
  sameName(status: boolean, value: string) {
    if (status === true) {
      this.group.controls[`${value}`].setValue(this.controls.name.value);
      this.controls[value].setValue(this.controls.name.value);
      this.controls[value].disable();
    } else {
      this.group.controls[`${value}`].setValue('');
      this.controls[value].setValue('');
      this.controls[value].enable();
    }
  }

  /** 車牌有無 */
  filterPlate(status) {
    if (status === 1) {
      return true;
    } else {
      return false;
    }
  }

  /** 支付方式 */
  dataTarget(index) {
    return '#Modal-0' + (index + 4);
  }

  /** 金流狀態 */
  paymentSwitch(index) {
    this.FSelectPayType = this.payList[index];
    this.paymentNumber = index;
  }

  // -----------------------------------------------------------------------

  /** 動態元件區 */
  showLogin(xtype: string = 'both', checkID: boolean = false) {
    const component =
      this.componentFactory.resolveComponentFactory(LoginComponent);

    this.content.clear();
    const com = this.content.createComponent(component);
    $('#model-Login').modal('show');
    com.instance.xPop = true;
    com.instance.xtype = xtype;
    /** 是否是因為檢核ID而發起 */
    com.instance.xCheckid = checkID;

    // 立即註冊
    com.instance.xRegister.subscribe((item) => {
      this.cds.gPubCommon.gOtpAuto = 'register';
      $('#model-Login').modal('hide');
      this.go.navigate([
        '/online-insurance/project-trial-motor/',
        'personInfo',
      ]);
      const marr = [
        'certificate_number',
        'name',
        'email',
        'email_bk',
        'mobile',
        'birthday',
        'address',
        'province',
        'county',
      ];
      marr.forEach((item) => {
        this.controls[item].setValue('');
      });
      this.controls.sex.setValue('-1');
    });

    // 忘記密碼
    com.instance.xforgetPassword.subscribe((item) => {
      $('#model-Login').modal('hide');
      this.go.navigateByUrl('login/find-account');
    });

    // 登入
    com.instance.xlogin.subscribe((xitem) => {
      $('#model-Login').modal('hide');
      this.go.navigate([
        '/online-insurance/project-trial-motor/',
        'personInfo',
      ]);
      this.cds.userInfo.seq_no ? (this.FType = 'edit') : (this.FType = 'add');
      this.userInfo = this.cds.userInfo;
      /** 第三方登入 */
      if (xitem && xitem.type === 'social') {
        this.cds.gPubCommon.gOtpAuto = 'bind';
        this.FSocialName = xitem.name;
        this.FSocialAccount = xitem.account;
        this.FChannel = xitem.channel;
      }
      this.ly_userinfo.onUpdateCDStoControl(this.cds.userInfo);
      this.FType = this.ly_userinfo.data.xType;
    });
  }

  /** 切換頁面 */
  async goStep(position) {
    switch (position) {
      case 'center':
        this.go.navigate(['/online-insurance/project-trial-motor&step=1']);
        break;
      case '2':
        // 填寫個人資料
        if (this.cds.userInfo.seq_no) {
          this.FType = 'edit';
          this.go.navigate([
            '/online-insurance/project-trial-motor/',
            'personInfo',
          ]);
        } else {
          // 註冊會員
          // console.log('註冊會員');
          this.FType = 'add';
          this.cds.userInfo.seq_no = 0;
          this.showLogin();
        }

        // 只有在網投購買電動車時觸發
        if (!this.FAply_no && this.motorValue === 'C4') {
          /** 表單驗證項目刷新 for 電動車 */
          this.validators = this.calcsv.initData_motor(
            this.userInfo,
            this.motorUserInfo,
            this.motorValue
          );

          this.formValidate();
        }
        break;
      case '3':
        // this.cds.gWindows = this.pay.openEmptyWin();
        /** 解除封印 允許後檢核 */
        Object.keys(this.controls).forEach((item) => {
          this.controls[item].markAsDirty();
          this.controls[item].markAsTouched();
        });

        /** 身分證額外檢核 */
        this.group
          .get('certificate_number')
          .setValidators([
            this.vi.fontLengthNumber,
            Validators.required,
            this.vi.vnCertNumber(
              this.controls.birthday.value,
              this.controls.sex.value
            ),
            this.vi.vn2020ID(this.controls.birthday.value),
          ]);
        this.group.get('certificate_number').updateValueAndValidity();

        /** 檢查userinfo是否完成 */
        const mbol = this.ly_userinfo.checkPayment();
        if (!mbol) {
          let resultError;
          for (const key in this.controls) {
            if (!resultError && this.controls[key].invalid) {
              resultError = key;
            }
          }

          if (resultError) {
            CommonService.scrollto(`lyc_${resultError}`, 30);
          }
          return;
        }

        if (!this.checkInfo()) {
          // console.log('error2');
          return;
        }

        /**
         * @description 修改會員資料
         * 規則 新增時email = email_bk
         *      修改  僅更改email_bk
         */
        if (this.FSyncUserinfo) {
          this.repPayload = {
            ...this.userInfo,
            email: this.controls.email.value,
            email_bk: this.controls.email_bk.value,
            mobile: this.controls.mobile.value,
            county: this.controls.county.value,
            province: this.controls.province.value,
            addr: this.controls.address.value,
            account: this.controls.email.value, // 忘記密碼帳號
          };

          // 強制將mail交換
          if (this.FType === 'edit') {
            // this.repPayload.email_bk = this.repPayload.email_bk;
            // this.repPayload.email = this.cds.userInfo.email;
          } else {
            // 新增流程須去註冊頁，緩存預計存檔資訊
            this.repPayload.customer_name = this.controls.name.value.trim();
            this.repPayload.birthday = this.controls.birthday.value;
            this.repPayload.is_validate_policy = 'N';
            this.repPayload.seq_no = this.cds.userInfo.seq_no || 0;
            this.repPayload.sex = this.controls.sex.value;

            if (this.FChannel === 'fb') {
              this.repPayload.fb_account = this.FSocialAccount;
              this.repPayload.fb_title = this.FSocialName;
            } else {
              this.repPayload.google_account = this.FSocialAccount;
              this.repPayload.google_title = this.FSocialName;
            }

            // 新增流程驗證Email
            if (this.repPayload.seq_no === 0) {
              const payload = { email: this.repPayload.email };
              const rep: IResponse = await this.apiMember.MemberVerifyEmail(
                payload
              );
              if (rep.status !== 200) {
                CommonService.scrollto('lyc_email');
                this.FErrorMail = this.FLang.ER.ER033;
                return;
              }
            }
          }
        }

        /**
         * @description 訂單存檔 [特別規則]
         * order_status 暫時都是1 因為前端只拿 1、11，但11的單據是無法操作的
         * order_no、aply_no 續保、編輯、訂單付款，都不會是0 因此預設是0，如透過setValue後 應該都會有值
         * FDate_HH 續保單帶來的應該會有不同的值，優先已帶來的值為主
         */
        let payload: any = {
          order_no: this.FOrder_no,
          aply_no: this.FAply_no,
          order_status: '1',
          member_seq_no: this.cds.userInfo.seq_no,
          type_pol: 'B',
          prod_pol_code: 'CF02001',
          prod_pol_code_nm: '強制險',
          pol_dt: this.motorUserInfo.procDate + ` ${this.FDate_HH}:00:00`,
          pol_due_dt:
            this.motorUserInfo.procDate_end + ` ${this.FDate_HH}:00:00`,
          certificate_type: '1',
          certificate_number: this.controls.certificate_number.value,
          customer_name: this.controls.name.value.trim(),
          birthday: this.controls.birthday.value,
          addr: this.controls.address.value,
          province: this.controls.province.value,
          county: this.controls.county.value,
          mobile: this.controls.mobile.value,
          email: this.controls.email_bk.value, // SA要求的
          sex: this.controls.sex.value,
          national_id: 'VIETN',
          vehc_ownr: this.controls.motor_name.value,
          rgst_no: this.filterLicensePlate
            ? this.controls.motor_license_plate.value
            : '',
          engn_no: this.filterLicensePlate
            ? ''
            : this.controls.motor_engn_no.value,
          fram_no: this.filterLicensePlate
            ? ''
            : this.controls.motor_body_number.value,
          vehc_kd: this.motorUserInfo.vehcKd,
          car_pwr_kd: this.motorUserInfo.carpwrkd,
          max_capl: this.controls.motor_people.value,
          comp_prem: this.FCalcOrder.compprem,
          driver_prem: this.FCalcOrder.driverprem,
          tot_prem: this.FCalcOrder.totprem,
          tot_prem_with_vat: this.FCalcOrder.totpremwithvat,
          print_type: '2',
          driver_amt: 0,
          driver_cnt: 0,
          passenger_amt: 0,
          passenger_cnt: 0,
          passenger_prem: this.FCalcOrder.passengerprem,
          comp_premvat: this.FCalcOrder.comppremvat,
          language: this.cds.gLangType,
          token: this.cds.gToken,
          client_target: this.controls.certificate_number.value
            ? this.controls.certificate_number.value
            : this.cds.userInfo.certificate_number_12
            ? this.cds.userInfo.certificate_number_12
            : this.cds.userInfo.certificate_number_9,
          pol_dt_kind: this.motorUserInfo.procDateYear,
          paymentNumber: this.paymentNumber,
        };

        // Attach policy
        if (this.plusPurchase) {
          const mAttach = {
            driver_amt: 0,
            driver_cnt: 0,
            passenger_cnt: 2,
            passenger_amt: Number(
              this.amountItemsList[this.amountNumber].number
            ),
          };

          payload = {
            ...payload,
            ...mAttach,
          };
        }

        this.cds.gTCalc_to_save = {
          type: 'motor',
          saveorder: payload,
          userinfo: this.repPayload,
          paytype: this.FSelectPayType.type,
        };
        this.doPayment(this.cds.gTCalc_to_save, this.FSyncUserinfo);

        /** 資料送出後將緩存日期清空 */
        if (this.cds.gDatapicker.motor) {
          this.Fdate1 = undefined;
          delete this.cds.gDatapicker.motor;
        }
        break;
    }
  }

  testmodel() {
    this.modal.open(LoginComponent);
  }

  /** form - UserInfo */
  setUsertoGroup(event) {
    // province county
    Object.keys(event).forEach((item) => {
      if (this.controls[item]) {
        this.controls[item].setValue(event[item].value);
      }
    });
  }

  // 試算按鈕復原
  initCalc() {
    this.clickNumber = 0;
    this.FAmount = 0;
  }

  getDate(event) {
    if (event) {
      const sdate = moment(event).format('YYYY-MM-DD');
      this.controls.motor_procDate.setValue(sdate);

      // 增加1年
      // const mde = moment(event).add(1, 'years').format('YYYY-MM-DD');
      // const mde = CommonService.addOneYear(sdate, 'YYYY-MM-DD');
      // this.group.controls.motor_procDateEnd.setValue(mde);

      this.controls.motor_procDateEnd.setValue(
        moment(this.controls.motor_procDate.value)
          .add(this.controls.r_year.value, 'years')
          .format('YYYY-MM-DD')
      );

      this.initCalc();
      // this.addOneYear();
    }
  }

  addOneYear() {
    // 起始日期
    const date = moment(
      this.group.controls.motor_procDate.value,
      'yyyy-MM-DD'
    ).format('DD/MM/yyyy');

    // 結束日期
    const dateEnd = moment(
      this.group.controls.motor_procDateEnd.value,
      'yyyy-MM-DD'
    ).format('DD/MM/yyyy');

    // 印上畫面
    // this.cds.gDatapicker.motor = `${date}-${dateEnd}`;
    // this.Fdate1 = `${date}~${dateEnd}`;
    this.cds.gDatapicker.motor = `${date}`;
    this.Fdate1 = `${date}`;
  }

  /** userInfo */
  async userinfo_action(payload, checkID: boolean = false) {
    if (payload) {
      const mstr = await this.apiMember.authThirdLogin(payload);
      this.showLogin(mstr, true);
    }
  }

  /**
   * @description 付款流程
   * @param {object} payload 物件
   * @property {string} type  [ car | motor | travel ],
   * @property {any} saveorder  [訂單存檔資料]
   * @property {any} userinfo  [當前用戶資訊]
   * @param {boolean} updateUser  [是否要更新會員]
   */
  async doPayment(payload, updateUser, action = null) {
    /** 首次註冊流程 透過seq_no === 0 判定是否執行 */
    if (updateUser && payload.userinfo.seq_no === 0) {
      this.pay.regeditFlow(
        payload.userinfo,
        payload.saveorder.certificate_number
      );
      this.go.navigate(['/login/otp-verification']);
      return;
    }

    /** 1 開始存檔 */
    const rep = await this.pay.savetxend(payload, updateUser);
    /** 1.1 無論有無核保成功都需取得 order_no aply_no */
    const { order_no, aply_no } = rep.data;
    this.FOrder_no = order_no || 0;
    this.FAply_no = aply_no;

    if (rep.status === 200) {
      if (action === 'pay') {
        this.go.navigate(
          ['/online-insurance/project-trial-motor/', 'payment'],
          { replaceUrl: true }
        );
      } else {
        this.go.navigate(['/online-insurance/project-trial-motor/', 'payment']);
      }
      /** 2 付款資訊緩存 */
      this.cds.gBeforPayinfo = {
        order_no,
        aply_no,
        prod_pol_code: 'CF02001',
        amt: payload.saveorder.tot_prem_with_vat,
        uamt: payload.saveorder.tot_prem,
      };

      /** 2.1 V1 改版成後啟動 */
      if (CommonService.isIOS()) {
        this.FConfirm = this.calcsv.openPayConfirm();
        setTimeout(() => {
          $('#lyConfirm').modal('show');
        }, 300);
      } else {
        /** 2.2 開始付款 */
        this.ActionYesNo(true);
      }
    } else {
      this.FPayTransaction = true;
      // $('#paymentFailModal').modal('show');
    }
  }
  ActionYesNo(xresult) {
    if (xresult) {
      if (!this.FsubscribePayAction || this.FsubscribePayAction.closed) {
        this.FsubscribePayAction = this.pay.getPayObserve().subscribe(
          (params) => {
            if (params.status === '1') {
              this.FPayTransaction = false;
              this.FPayResult = true;
            }
            this.pay.editOrderState(
              params,
              params.status === '1' ? '4' : '5',
              this.paymentNumber as number
            );
            this.pay.showResult(params.status);
            // 付款成功後清除付款方式狀態
            this.cds.gTCalc_to_save.paytype = undefined;
          },
          (error) => {
            switch (error.status) {
              case -9:
                this.FPayTransaction = true;
                $('#paymentFailModal').modal('show');
                break;
            }
            // console.log(error);
          }
        );
      }
      this.pay.doPayment(
        this.cds.gTCalc_to_save.saveorder.tot_prem_with_vat,
        this.FAply_no + moment().format('yyMMDDHHmmss'),
        this.cds.gTCalc_to_save.paytype
      );
    } else {
      this.go.navigate([
        '/online-insurance/project-trial-motor/',
        'personInfo',
      ]);
    }
    $('#lyConfirm').modal('hide');
  }

  /** 車牌按鈕選擇 */
  switchLicense(status: boolean) {
    const bodyNumber = this.controls.motor_body_number;
    const licensePlate = this.controls.motor_license_plate;
    const engnno = this.controls.motor_engn_no;
    const mKeepPlate = licensePlate.value;
    const mcheckValue = licensePlate.invalid;
    const ArrControl = [licensePlate, bodyNumber, engnno];
    ArrControl.forEach((item) => {
      // item.reset('');
      item.disable();
      item.setValidators([]);
      item.updateValueAndValidity();
    });

    if (status) {
      licensePlate.enable();
      licensePlate.setValidators([
        Validators.required,
        this.vi.vnRgstNo(this.motorValue),
      ]);
      licensePlate.updateValueAndValidity();
    } else {
      bodyNumber.enable();
      bodyNumber.setValidators([Validators.required]);
      bodyNumber.updateValueAndValidity();
      engnno.enable();
      engnno.setValidators([Validators.required]);
      engnno.updateValueAndValidity();
    }
  }

  showtest() {
    $('#paymentFailModal').modal('show');
  }

  alignPaymentText() {
    const paymentData = $('.payment-data');
    if (window.innerWidth <= 768) {
      if (paymentData.length > 0) {
        const maxWidth = Math.max(
          ...paymentData.map((key) => paymentData[key].offsetWidth)
        );
        paymentData.width(maxWidth);
      }
    } else {
      paymentData.css('width', 'auto');
    }
  }
  /** 將 input 值 轉換成大寫 */
  changeToUpperCase(event) {
    const input = event.target;
    const start = input.selectionStart;
    const end = input.selectionEnd;
    input.value = input.value.toLocaleUpperCase();
    input.setSelectionRange(start, end);
  }
}
