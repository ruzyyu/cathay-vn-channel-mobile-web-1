import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PropertyInsuranceRoutingModule } from './property-insurance-routing.module';
import { PropertyInsuranceComponent } from './property-insurance.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [PropertyInsuranceComponent],
  imports: [CommonModule, PropertyInsuranceRoutingModule, SharedModule],
})
export class PropertyInsuranceModule {}
