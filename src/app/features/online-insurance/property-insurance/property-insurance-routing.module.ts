import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PropertyInsuranceComponent } from './property-insurance.component';

const routes: Routes = [{ path: '', component: PropertyInsuranceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertyInsuranceRoutingModule { }
