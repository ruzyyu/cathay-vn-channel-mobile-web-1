import { Component, OnInit, HostListener } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Router } from '@angular/router';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';
import { StyleHelperService } from 'src/app/core/services/style-helper.service';
import { CommonService } from 'src/app/core/services/common.service';

@Component({
  selector: 'app-motor-insurance',
  templateUrl: './motor-insurance.component.html',
  styleUrls: ['./motor-insurance.component.scss'],
})
export class MotorInsuranceComponent implements OnInit {
  FLang: any;

  FLangType: string;

  FNormalList: any;

  // FAQ
  faqData: any;

  constructor(
    private cds: DatapoolService,
    private router: Router,
    private numberPipe: NumberPipe,
    private styleHelper: StyleHelperService
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;

    this.FNormalList = [
      {
        name: '車險',
        img: 'car',
        file: `assets/json/contract/${this.cds.gLangType}/contract01.json`,
      },
      {
        name: '旅遊險',
        img: 'travel',
        file: `assets/json/contract/${this.cds.gLangType}/contract02.json`,
      },
      {
        name: '傷害險',
        img: 'accident',
        file: `assets/json/contract/${this.cds.gLangType}/contract03.json`,
      },
      {
        name: '住火險',
        img: 'fire',
        file: `assets/json/contract/${this.cds.gLangType}/contract04.json`,
      },
    ];

    this.faqData = [
      {
        title: this.FLang.D02.content.D0073,
        content: this.FLang.D02.content.D0073_.s001,
      },
      {
        title: this.FLang.D02.content.D0074,
        content: this.FLang.D02.content.D0074_.s001,
      },
      {
        title: this.FLang.D02.content.D0075,
        content: this.FLang.D02.content.D0075_.s001,
      },
    ];
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.amendButtonPosition();
  }

  ngOnInit(): void {
    this.doSlick();
    this.initCarousel();
  }

  ngAfterViewInit() {
    this.styleHelper.initAdjustCardHeight('#motorTableCompare');
    CommonService.autoScrollToAccordionHeader('#accordionQuestion');
    this.amendButtonPosition();
  }

  /* 判斷Banner尺寸按鈕位置，設置四個尺寸
   * 1. 1200~2300
   * 2. 1199~768
   * 3. 768~375
   * 4. 375~320 */
  amendButtonPosition() {
    if (window.innerWidth <= 2300 && window.innerWidth >= 1200) {
      const left = 602 - (2300 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
    if (window.innerWidth <= 1199 && window.innerWidth >= 768) {
      const left = 257 - (1199 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
    if (window.innerWidth < 768 && window.innerWidth > 375) {
      const left = 284 - (768 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
    if (window.innerWidth <= 375) {
      const left = 107 - (375 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
  }

  goClaimInsurance(index: number) {
    this.router.navigate(['member/claim/introduction'], {
      queryParams: {
        intro: this.FNormalList[index].file,
      },
      fragment: 'introductionDescription',
    });
  }

  doSlick() {
    $('.teaching-phone .slickcard')
      .not('.slick-initialized')
      .slick({
        dots: true,
        infinite: false,
        variableWidth: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplaySpeed: 1000,
        responsive: [
          {
            breakpoint: 1440.98,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
              focusOnSelect: true,
            },
          },
          {
            breakpoint: 991.98,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
              focusOnSelect: true,
            },
          },
          {
            breakpoint: 767.98,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              centerMode: true,
              focusOnSelect: true,
            },
          },
        ],
      });

    $('.teaching-phone .slickcard').on(
      'beforeChange',
      (event, slick, currentSlide, nextSlide) => {
        $('#cardSlide').carousel(nextSlide);
      }
    );
  }

  // 初始化輪播套件 (手機圖片輪播)
  initCarousel() {
    // 初始化輪播套件
    $('#cardSlide').carousel();

    // 小網 tag跟輪播同時動作
    $('#cardSlide').on('slide.bs.carousel', (e: any) => {
      $('.teaching-phone .slickcard').slick('slickGoTo', e.to);
    });
  }

  // banner 跳轉
  bannerUrl() {
    this.router.navigate(['/online-insurance/project-trial-motor/calcAmt']);
  }

  /** FAQ */
  titleContent(item: any) {
    return `
      <div class="tab-title d-flex">
        <h4>${item.title}</h4>
        <i class="icons-ic-faq-arrow-up i-icon i-up"></i>
        <i class="icons-ic-faq-arrow-down i-icon i-down"></i>
      </div>
      `;
  }

  infoContent(item: any) {
    return `
      <div class="que-body d-flex">
        <div class="answer custom_text_align white-space-normal">${item.content}</div>
      </div>
      `;
  }

  titleId(index: number) {
    return 'FaqQuestion' + this.numberPipe.transform(index + 1);
  }
  titleDataTarget(index: number) {
    return '#collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  titleAriaControls(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoId(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoAriaLabelledby(index: number) {
    return 'FaqQuestionQue' + this.numberPipe.transform(index + 1);
  }

  /** 調整 投保3步驟 mobile 高度問題 */
  adjustHeight() {
    const style = 'card-height t-step-row';
    switch (this.FLangType) {
      case 'en-US':
        return 't-step-row-en ' + style;
      case 'vi-VN':
        return 't-step-row-vn ' + style;
      default:
        return 't-step-row-tw ' + style;
    }
  }

  // 調整 投保3步驟 web 第一張
  adjustHeightWeb() {
    const style = 'card-height t-step-row active';
    switch (this.FLangType) {
      case 'en-US':
        return 't-step-row-en ' + style;
      case 'vi-VN':
        return 't-step-row-vn ' + style;
      default:
        return 't-step-row-tw ' + style;
    }
  }

  /** banner 圖片 語言 switch */
  bannerImg(size: number) {
    const img1920 = 'assets/img/commodity/bn-scooter-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-scooter-1200-x-400';
    const img800 = 'assets/img/commodity/bn-scooter-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '.png';
        case 2:
          return img1200 + '.png';
        case 3:
          return img800 + '.png';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en.png';
        case 2:
          return img1200 + '-en.png';
        case 3:
          return img800 + '-en.png';
      }
    }
  }

  /** banner 圖片 語言 switch */
  bannerImgBig(size: number) {
    const img1920 = 'assets/img/commodity/bn-scooter-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-scooter-1200-x-400';
    const img800 = 'assets/img/commodity/bn-scooter-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '@2x.png 2x';
        case 2:
          return img1200 + '@2x.png 2x';
        case 3:
          return img800 + '@2x.png 2x';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en@2x.png 2x';
        case 2:
          return img1200 + '-en@2x.png 2x';
        case 3:
          return img800 + '-en@2x.png 2x';
      }
    }
  }

  checkI18n() {
    if (this.FLangType === 'vi-VN') {
      return 'banner_vn';
    }
    if (this.FLangType === 'zh-TW') {
      return 'banner_tw';
    }
    if (this.FLangType === 'en-US') {
      return 'banner_en';
    }
  }

  stepImg(size: number) {
    const img = 'assets/img/commodity/img-screen-scooter';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img + '-step-1.png';
        case 2:
          return img + '-step-2.png';
        case 3:
          return img + '-step-3.png';
      }
    } else {
      switch (size) {
        case 1:
          return img + '-en-step-1.png';
        case 2:
          return img + '-en-step-2.png';
        case 3:
          return img + '-en-step-3.png';
      }
    }
  }

  stepImgBig(size: number) {
    const img = 'assets/img/commodity/img-screen-scooter';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img + '-step-1@2x.png 2x';
        case 2:
          return img + '-step-2@2x.png 2x';
        case 3:
          return img + '-step-3@2x.png 2x';
      }
    } else {
      switch (size) {
        case 1:
          return img + '-en-step-1@2x.png 2x';
        case 2:
          return img + '-en-step-2@2x.png 2x';
        case 3:
          return img + '-en-step-3@2x.png 2x';
      }
    }
  }
}
