import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MotorInsuranceRoutingModule } from './motor-insurance-routing.module';
import { MotorInsuranceComponent } from './motor-insurance.component';
import { MotorContentComponent } from './components/motor-content/motor-content.component';
import { ProjectComponent } from './components/motor-content/project/project.component';
import { FormatComponent } from './components/motor-content/format/format.component';
import { CommodityComponent } from './components/motor-content/commodity/commodity.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    MotorInsuranceComponent,
    MotorContentComponent,
    ProjectComponent,
    FormatComponent,
    CommodityComponent,
  ],
  imports: [CommonModule, MotorInsuranceRoutingModule, SharedModule],
})
export class MotorInsuranceModule {}
