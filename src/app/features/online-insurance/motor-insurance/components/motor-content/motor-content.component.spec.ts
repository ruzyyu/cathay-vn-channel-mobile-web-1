import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MotorContentComponent } from './motor-content.component';

describe('MotorContentComponent', () => {
  let component: MotorContentComponent;
  let fixture: ComponentFixture<MotorContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MotorContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MotorContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
