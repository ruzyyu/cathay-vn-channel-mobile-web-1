import { Component, Input, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
})
export class ProjectComponent implements OnInit {
  @Input() childItemsOnline: any;
  @Input() childItemsOffline: any;
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }
  FLang: any;

  ngOnInit(): void {}

  styleCss(index) {
    const style = 'col-sm-12 col-lg-6';
    return index % 2 === 0 ? style + ' order-lg-first' : style;
  }
}
