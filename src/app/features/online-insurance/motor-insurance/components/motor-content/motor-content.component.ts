import { Component, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-motor-content',
  templateUrl: './motor-content.component.html',
  styleUrls: ['./motor-content.component.scss'],
})
export class MotorContentComponent implements OnInit {
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }
  FLang: any;
  XcommodityItems: {};
  XprojectItemsOnline: {};
  XprojectItemsOffline: Array<any>;

  ngOnInit(): void {
    this.XcommodityItems = {
      id: 1,
      title: this.FLang.D02.title.D0001,
      img: 'assets/img/commodity/img-bx-scooter.png',
      srcset: 'assets/img/commodity/img-bx-scooter@2x.png 2x',
      icons: [
        {
          defaultImg: 'assets/img/commodity/ic-claim-scooter.png',
          defaultSrcset: 'assets/img/commodity/ic-claim-scooter@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-scooter-w.png',
          activeSrcset: 'assets/img/commodity/ic-claim-scooter-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-scooter-passenger.png',
          defaultSrcset:
            'assets/img/commodity/ic-claim-scooter-passenger@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-scooter-passenger-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-scooter-passenger-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-scooter-body.png',
          defaultSrcset: 'assets/img/commodity/ic-claim-scooter-body@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-scooter-body-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-scooter-body-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-scooter-third.png',
          defaultSrcset:
            'assets/img/commodity/ic-claim-scooter-third@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-scooter-third-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-scooter-third-w@2x.png 2x',
        },
      ],
      content: [
        {
          title: this.FLang.D02.content.D0001,
          directions: this.FLang.D02.content.D0002,
          tag: this.FLang.D02.content.D0003,
          details: [
            {
              option: this.FLang.D02.content.D0003_.S0001,
              content1: this.FLang.D02.content.D0003_.S0010,
              money: this.FLang.D02.content.D0003_.S0002,
              content2: this.FLang.D02.content.D0003_.S0004,
            },
            // {
            //   option: this.FLang.D02.content.D0003_.S0005,
            //   content1: this.FLang.D02.content.D0003_.S0006,
            //   money: this.FLang.D02.content.D0003_.S0007,
            //   content2: '', // this.FLang.D02.content.D0003_.S0008,文案有更改暫時拿掉
            // },
            {
              option: this.FLang.D02.content.D0003_.S0009,
              content1: this.FLang.D02.content.D0003_.S0010,
              money: this.FLang.D02.content.D0003_.S0011,
              content2: this.FLang.D02.content.D0003_.S0012,
            },
          ],
        },
        {
          title: this.FLang.D02.content.D0004,
          directions: this.FLang.D02.content.D0005,
          tag: this.FLang.D02.content.D0006,
          details: [
            {
              option: this.FLang.D02.content.D0006_.S0001,
              content1: this.FLang.D02.content.D0006_.S0006,
              money: this.FLang.D02.content.D0006_.S0003,
              content2: this.FLang.D02.content.D0006_.S0004,
            },
            // {
            //   option: this.FLang.D02.content.D0006_.S0005,
            //   content1: this.FLang.D02.content.D0006_.S0006,
            //   money: this.FLang.D02.content.D0006_.S0007,
            //   content2: this.FLang.D02.content.D0006_.S0008,
            // },
          ],
        },
        {
          title: this.FLang.D02.content.D0007,
          directions: this.FLang.D02.content.D0008,
          tag: this.FLang.D02.content.D0009,
          details: [
            {
              option: this.FLang.D02.content.D0009_.S0001,
              content1: '', // this.FLang.D02.content.D0009_.S0002, 文案有更改暫時拿掉
              money: this.FLang.D02.content.D0009_.S0003,
              content2: this.FLang.D02.content.D0009_.S0004,
            },
          ],
        },
        {
          title: this.FLang.D02.content.D0010,
          directions: this.FLang.D02.content.D0011,
          tag: this.FLang.D02.content.D0012,
          details: [
            {
              option: this.FLang.D02.content.D0012_.S0001,
              content1: this.FLang.D02.content.D0012_.S0010,
              money: this.FLang.D02.content.D0012_.S0003,
              content2: this.FLang.D02.content.D0012_.S0004,
            },
            // {
            //   option: this.FLang.D02.content.D0012_.S0005,
            //   content1: this.FLang.D02.content.D0012_.S0006,
            //   money: this.FLang.D02.content.D0012_.S0007,
            //   content2: '', // this.FLang.D02.content.D0012_.S0008,文案有更改暫時拿掉
            // },
            {
              option: this.FLang.D02.content.D0012_.S0009,
              content1: this.FLang.D02.content.D0012_.S0010,
              money: this.FLang.D02.content.D0012_.S0011,
              content2: this.FLang.D02.content.D0012_.S0012,
            },
          ],
        },
      ],
    };
    this.XprojectItemsOnline = {
      title: this.FLang.D02.content.D0035,
      directions: this.FLang.D02.content.D0036,
      defaultImg: 'assets/img/commodity/img-product-scooter-01.png',
      srcsetImg: 'assets/img/commodity/img-product-scooter-01@2x.png 2x',
      mobileDefaultImg: 'assets/img/commodity/img-product-scooter-01-m.png',
      mobileSrcsetImg:
        'assets/img/commodity/img-product-scooter-01-m@2x.png 2x',
      totalPrice: '122.000',
      content: [
        {
          title: this.FLang.D02.content.D0037,
          directions: this.FLang.D02.content.D0038,
          money: '66.000',
          defaultImg: 'assets/img/commodity/ic-claim-scooter-o.png',
          srcsetImg: 'assets/img/commodity/ic-claim-scooter-o@2x.png 2x',
        },
        {
          title: this.FLang.D02.content.D0039,
          directions: this.FLang.D02.content.D0040,
          money: '56.000',
          defaultImg: 'assets/img/commodity/ic-claim-scooter-passenger-o.png',
          srcsetImg:
            'assets/img/commodity/ic-claim-scooter-passenger-o@2x.png 2x',
        },
      ],
    };

    this.XprojectItemsOffline = [
      {
        title: this.FLang.D02.content.D0041,
        directions: this.FLang.D02.content.D0042,
        defaultImg: 'assets/img/commodity/img-product-scooter-02.png',
        srcsetImg: 'assets/img/commodity/img-product-scooter-02@2x.png 2x',
        mobileDefaultImg: 'assets/img/commodity/img-product-scooter-02-m.png',
        mobileSrcsetImg:
          'assets/img/commodity/img-product-scooter-02-m@2x.png 2x',
        totalPrice: '221.000',
        content: [
          {
            title: this.FLang.D02.content.D0043,
            directions: this.FLang.D02.content.D0044,
            money: '66.000',
            defaultImg: 'assets/img/commodity/ic-claim-scooter-o.png',
            srcsetImg: 'assets/img/commodity/ic-claim-scooter-o@2x.png 2x',
          },
          {
            title: this.FLang.D02.content.D0045,
            directions: this.FLang.D02.content.D0046,
            money: '56.000',
            defaultImg: 'assets/img/commodity/ic-claim-scooter-passenger-o.png',
            srcsetImg:
              'assets/img/commodity/ic-claim-scooter-passenger-o@2x.png 2x',
          },
          {
            title: this.FLang.D02.content.D0047,
            directions: this.FLang.D02.content.D0048,
            money: '99.000',
            defaultImg: 'assets/img/commodity/ic-claim-scooter-third-o.png',
            srcsetImg:
              'assets/img/commodity/ic-claim-scooter-third-o@2x.png 2x',
          },
        ],
      },
      {
        title: this.FLang.D02.content.D0049,
        directions: this.FLang.D02.content.D0050,
        defaultImg: 'assets/img/commodity/img-product-scooter-03.png',
        srcsetImg: 'assets/img/commodity/img-product-scooter-03@2x.png 2x',
        mobileDefaultImg: 'assets/img/commodity/img-product-scooter-03-m.png',
        mobileSrcsetImg:
          'assets/img/commodity/img-product-scooter-03-m@2x.png 2x',
        totalPrice: '1.761.000',
        content: [
          {
            title: this.FLang.D02.content.D0051,
            directions: this.FLang.D02.content.D0052,
            money: '66.000',
            dollar: this.FLang.D02.content.D0030,
            defaultImg: 'assets/img/commodity/ic-claim-scooter-o.png',
            srcsetImg: 'assets/img/commodity/ic-claim-scooter-o@2x.png 2x',
          },
          {
            title: this.FLang.D02.content.D0053,
            directions: this.FLang.D02.content.D0054,
            money: '56.000',
            dollar: this.FLang.D02.content.D0030,
            defaultImg: 'assets/img/commodity/ic-claim-scooter-passenger-o.png',
            srcsetImg:
              'assets/img/commodity/ic-claim-scooter-passenger-o@2x.png 2x',
          },
          {
            title: this.FLang.D02.content.D0055,
            directions: this.FLang.D02.content.D0056,
            money: '99.000',
            defaultImg: 'assets/img/commodity/ic-claim-scooter-third-o.png',
            srcsetImg:
              'assets/img/commodity/ic-claim-scooter-third-o@2x.png 2x',
          },
          {
            title: this.FLang.D02.content.D0057,
            directions: this.FLang.D02.content.D0058,
            money: '1.540.000',
            defaultImg: 'assets/img/commodity/ic-claim-scooter-body-o.png',
            srcsetImg: 'assets/img/commodity/ic-claim-scooter-body-o@2x.png 2x',
          },
        ],
      },
    ];
  }
}
