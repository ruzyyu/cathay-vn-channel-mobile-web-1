import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiabilityInsuranceComponent } from './liability-insurance.component';

describe('LiabilityInsuranceComponent', () => {
  let component: LiabilityInsuranceComponent;
  let fixture: ComponentFixture<LiabilityInsuranceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiabilityInsuranceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiabilityInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
