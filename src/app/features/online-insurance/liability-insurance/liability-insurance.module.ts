import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LiabilityInsuranceRoutingModule } from './liability-insurance-routing.module';
import { LiabilityInsuranceComponent } from './liability-insurance.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [LiabilityInsuranceComponent],
  imports: [CommonModule, LiabilityInsuranceRoutingModule, SharedModule],
})
export class LiabilityInsuranceModule {}
