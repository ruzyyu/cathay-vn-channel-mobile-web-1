import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LiabilityInsuranceComponent } from './liability-insurance.component';

const routes: Routes = [{ path: '', component: LiabilityInsuranceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LiabilityInsuranceRoutingModule { }
