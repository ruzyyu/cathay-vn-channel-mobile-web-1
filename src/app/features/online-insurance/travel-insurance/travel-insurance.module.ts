import { CommodityComponent } from './components/travel-content/commodity/commodity.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TravelInsuranceRoutingModule } from './travel-insurance-routing.module';
import { TravelInsuranceComponent } from './travel-insurance.component';
import { CardSlideComponent } from './components/card-slide/card-slide.component';
import { FlowChartComponent } from './components/flow-chart/flow-chart.component';
import { FaqComponent } from './components/faq/faq.component';
import { BannerComponent } from './components/banner/banner.component';
import { BookmarkComponent } from './components/bookmark/bookmark.component';
import { BgBubbleComponent } from './components/bg-bubble/bg-bubble.component';
import { TravelContentComponent } from './components/travel-content/travel-content.component';
import { GuaranteeComponent } from './components/travel-content/guarantee/guarantee.component';

@NgModule({
  declarations: [
    TravelInsuranceComponent,
    CardSlideComponent,
    FlowChartComponent,
    FaqComponent,
    BannerComponent,
    BookmarkComponent,
    BgBubbleComponent,
    TravelContentComponent,
    GuaranteeComponent,
    CommodityComponent,
  ],
  imports: [CommonModule, TravelInsuranceRoutingModule, SharedModule],
})
export class TravelInsuranceModule {}
