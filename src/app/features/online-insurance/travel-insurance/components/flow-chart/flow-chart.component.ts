import { Component, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Lang } from 'src/types';

@Component({
  selector: 'app-flow-chart',
  templateUrl: './flow-chart.component.html',
  styleUrls: ['./flow-chart.component.scss'],
})
export class FlowChartComponent implements OnInit {
  // 變數宣告
  FLang: Lang;

  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {}
}
