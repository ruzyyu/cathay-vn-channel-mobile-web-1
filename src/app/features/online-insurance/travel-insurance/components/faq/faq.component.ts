import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/core/services/common.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
})
export class FaqComponent implements OnInit {
  // 變數宣告
  FLang: any;
  faqList = [];

  constructor(private numberPipe: NumberPipe, private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {
    this.faqList = [
      {
        title: this.FLang.D03.N0082,
        content: this.FLang.D03.N0082_.s001,
      },
      {
        title: this.FLang.D03.N0083,
        content: this.FLang.D03.N0083_.s001,
      },
      {
        title: this.FLang.D03.N0084,
        content: this.FLang.D03.N0084_.s001,
      },
    ];
  }

  classStyle(index) {
    const tabLink = 'tab-link';
    return index === 0 ? tabLink : tabLink + ' collapsed';
  }

  cardTitleId(index) {
    return 'Question' + this.numberPipe.transform(index + 1);
  }

  ariaControl(index) {
    return 'collapseQue' + this.numberPipe.transform(index + 1);
  }

  dataTarget(index) {
    return '#collapseQue' + this.numberPipe.transform(index + 1);
  }

  cardContentId(index) {
    return 'collapseQue' + this.numberPipe.transform(index + 1);
  }

  classActive(index) {
    return index === 0 ? 'collapse show' : 'collapse';
  }

  ariaLabelledby(index) {
    return 'Question' + this.numberPipe.transform(index + 1);
  }

  contentClickHandler(event) {
    CommonService.contentClickHandler(event);
  }
}
