import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BgBubbleComponent } from './bg-bubble.component';

describe('BgBubbleComponent', () => {
  let component: BgBubbleComponent;
  let fixture: ComponentFixture<BgBubbleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BgBubbleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BgBubbleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
