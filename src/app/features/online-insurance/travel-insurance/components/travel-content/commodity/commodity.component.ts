import { Component, Input, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';

@Component({
  selector: 'app-commodity',
  templateUrl: './commodity.component.html',
  styleUrls: ['./commodity.component.scss'],
})
export class CommodityComponent implements OnInit {
  @Input() childItems!: any;

  activeIndex = 0;
  FLang: any;

  constructor(private numberPipe: NumberPipe, private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {}

  /** Number 轉換 */
  dataFilterNumber(index) {
    return 'select-' + this.numberPipe.transform(index + 1);
  }

  clickStyle(index) {
    this.activeIndex = index;
  }

  /** Number 轉換 */
  cssClassNumber(index) {
    let classPipe = this.numberPipe.transform(index + 1);
    if (index === this.activeIndex) {
      classPipe += ' active';
    }
    return classPipe;
  }
}
