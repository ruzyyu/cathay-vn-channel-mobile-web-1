import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelContentComponent } from './travel-content.component';

describe('TravelContentComponent', () => {
  let component: TravelContentComponent;
  let fixture: ComponentFixture<TravelContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TravelContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
