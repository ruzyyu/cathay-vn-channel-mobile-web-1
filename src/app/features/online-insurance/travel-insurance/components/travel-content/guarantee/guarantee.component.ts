import {
  Component,
  Input,
  EventEmitter,
  Output,
} from '@angular/core';
import { ITravelPageData } from 'src/app/core/interface';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { V1Component } from 'src/app/shared/components/travel/v1/v1.component';
import { Lang } from 'src/types';

@Component({
  selector: 'app-guarantee',
  templateUrl: './guarantee.component.html',
  styleUrls: ['./guarantee.component.scss'],
})
export class GuaranteeComponent {
  // 變數宣告
  FLangIn: Lang['C02']['plancard'];
  FLangOut: Lang['C02']['plancard_out'];
  FLangD03: Lang['D03'];
  FLangType: string;

  /** 國內、國外旅遊顯示 */
  @Input() status;
  @Input() xTravelCalcList: Array<any>;
  @Input() pageData: ITravelPageData;

  @Input() xchooseComponent: string;
  @Output() xchooseComponentChange = new EventEmitter();

  mapping = new Map<string, any>([
    ['v1', V1Component],
    // ['t44', T44Component],
    // ['t44v1', T44v1Component],
    // ['W1', W1Component],
    // ['W2', W2Component],
    // ['W7', W7Component],
  ]);

  constructor(private cds: DatapoolService) {
    this.FLangD03 = this.cds.gLang.D03;
    this.FLangOut = this.cds.gLang.C02.plancard_out;
    this.FLangIn = this.cds.gLang.C02.plancard;
    this.FLangType = this.cds.gLangType;
  }

  isTouch = false;

  ngAfterViewInit() {
    this.initAdjustHeight('#travelIn');
    this.initAdjustHeight('#travelOut');

    // 監聽項目改變
    $('a[data-toggle="pill"]').on('shown.bs.tab', () => {
      this.adjustHeight('#travelIn');
      this.adjustHeight('#travelOut');
    });
  }

  download(type, status) {
    if (type === 0) {
      switch (status) {
        case 1:
          window.open('assets/pdf/T1-T3.pdf', 'T1-T3');
          break;
        case 2:
          window.open('assets/pdf/T4.pdf', 'T4');
          break;
      }
    } else {
      window.open('assets/pdf/T1-T3.pdf', 'T1-T3');
    }
  }

  setPageStatus(status: 0 | 1) {
    this.pageData.status = status;
  }

  changeComponent(value: string) {
    this.xchooseComponent = value;
    this.xchooseComponentChange.emit(this.xchooseComponent);
  }

  initAdjustHeight(selector: string) {
    this.adjustHeight(selector);

    $(window).resize(() => {
      this.adjustHeight(selector);
    });
  }

  adjustHeight(selector: string) {
    const titles = $(selector).find('.compare-item .compare-title');
    $(selector)
      .find('.table-compare .compare-row')
      .each(function () {
        $(this)
          .find('.compare-row-line')
          .each(function (index: number) {
            const height = titles[index].clientHeight + 1;
            $(this).css('height', height);
          });
      });
  }
}
