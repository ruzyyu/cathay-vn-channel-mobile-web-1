import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InjuryInsuranceComponent } from './injury-insurance.component';

describe('InjuryInsuranceComponent', () => {
  let component: InjuryInsuranceComponent;
  let fixture: ComponentFixture<InjuryInsuranceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InjuryInsuranceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InjuryInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
