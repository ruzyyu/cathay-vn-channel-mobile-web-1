import { NumberPipe } from './../../../shared/pipes/css-pipe.pipe';
import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { StyleHelperService } from 'src/app/core/services/style-helper.service';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/core/services/common.service';

@Component({
  selector: 'app-car-insurance',
  templateUrl: './car-insurance.component.html',
  styleUrls: ['./car-insurance.component.scss'],
})
export class CarInsuranceComponent implements OnInit, AfterViewInit {
  constructor(
    private cds: DatapoolService,
    private numberPipe: NumberPipe,
    private router: Router,
    private styleHelper: StyleHelperService
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;

    this.faqData = [
      {
        title: this.FLang.D01.D0047,
        content: this.FLang.D01.D0047_.s001,
      },
      {
        title: this.FLang.D01.D0048,
        content: this.FLang.D01.D0048_.s001,
      },
      {
        title: this.FLang.D01.D0049,
        content: this.FLang.D01.D0049_.s001,
      },
    ];

    this.FNormalList = [
      {
        name: '車險',
        img: 'car',
        file: `assets/json/contract/${this.cds.gLangType}/contract01.json`,
      },
      {
        name: '旅遊險',
        img: 'travel',
        file: `assets/json/contract/${this.cds.gLangType}/contract02.json`,
      },
      {
        name: '傷害險',
        img: 'accident',
        file: `assets/json/contract/${this.cds.gLangType}/contract03.json`,
      },
      {
        name: '住火險',
        img: 'fire',
        file: `assets/json/contract/${this.cds.gLangType}/contract04.json`,
      },
    ];
  }

  FLang: any;
  FLangType: string;
  faqData: any;
  FNormalList: any;

  cardStatus = false;
  cardIndex: number;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.amendButtonPosition();
  }

  ngOnInit(): void {
    this.doSlick();
    this.initCarousel();
  }

  ngAfterViewInit() {
    this.styleHelper.initAdjustCardHeight('#carTableCompare');
    CommonService.autoScrollToAccordionHeader('#accordionQuestion');
    this.amendButtonPosition();
  }

  /* 判斷Banner尺寸按鈕位置，設置四個尺寸
   * 1. 1200~2300
   * 2. 1199~768
   * 3. 768~375
   * 4. 375~320 */
  amendButtonPosition() {
    if (window.innerWidth <= 2300 && window.innerWidth >= 1200) {
      const left = 602 - (2300 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
    if (window.innerWidth <= 1199 && window.innerWidth >= 768) {
      const left = 257 - (1199 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
    if (window.innerWidth < 768 && window.innerWidth > 375) {
      const left = 284 - (768 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
    if (window.innerWidth <= 375) {
      const left = 107 - (375 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
  }

  doSlick() {
    $('.teaching-phone .slickcard')
      .not('.slick-initialized')
      .slick({
        dots: true,
        infinite: false,
        variableWidth: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplaySpeed: 1000,
        responsive: [
          {
            breakpoint: 1440.98,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
              focusOnSelect: true,
            },
          },
          {
            breakpoint: 991.98,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
              focusOnSelect: true,
            },
          },
          {
            breakpoint: 767.98,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              centerMode: true,
              focusOnSelect: true,
            },
          },
        ],
      });

    $('.teaching-phone .slickcard').on(
      'beforeChange',
      (event, slick, currentSlide, nextSlide) => {
        $('#cardSlide').carousel(nextSlide);
      }
    );
  }

  // 初始化輪播套件 (手機圖片輪播)
  initCarousel() {
    // 初始化輪播套件
    $('#cardSlide').carousel();

    // 小網 tag跟輪播同時動作
    $('#cardSlide').on('slide.bs.carousel', function (e: any) {
      $('.teaching-phone .slickcard').slick('slickGoTo', e.to);
    });
  }

  /** FAQ */
  titleContent(item: any) {
    return `
    <div class="tab-title d-flex">
      <h4>${item.title}</h4>
      <i class="icons-ic-faq-arrow-up i-icon i-up"></i>
      <i class="icons-ic-faq-arrow-down i-icon i-down"></i>
    </div>
    `;
  }

  infoContent(item: any) {
    return `
    <div class="que-body d-flex">
      <div class="answer custom_text_align white-space-normal">${item.content}</div>
    </div>
    `;
  }

  titleId(index: number) {
    return 'FaqQuestion' + this.numberPipe.transform(index + 1);
  }
  titleDataTarget(index: number) {
    return '#collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  titleAriaControls(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoId(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoAriaLabelledby(index: number) {
    return 'FaqQuestionQue' + this.numberPipe.transform(index + 1);
  }

  // 理賠所需文件跳頁
  goClaimInsurance(index: number) {
    this.router.navigate(['member/claim/introduction'], {
      queryParams: {
        intro: this.FNormalList[index].file,
      },
      fragment: 'introductionDescription',
    });
  }

  // banner 跳轉
  bannerUrl() {
    this.router.navigate(['/online-insurance/project-trial-car/calcAmt']);
  }

  /** banner 圖片 語言 switch */
  bannerImg(size: number) {
    const img1920 = 'assets/img/commodity/bn-car-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-car-1200-x-400';
    const img800 = 'assets/img/commodity/bn-car-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '.png';
        case 2:
          return img1200 + '.png';
        case 3:
          return img800 + '.png';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en.png';
        case 2:
          return img1200 + '-en.png';
        case 3:
          return img800 + '-en.png';
      }
    }
  }

  /** banner 圖片 語言 switch */
  bannerImgBig(size: number) {
    const img1920 = 'assets/img/commodity/bn-car-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-car-1200-x-400';
    const img800 = 'assets/img/commodity/bn-car-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '@2x.png 2x';
        case 2:
          return img1200 + '@2x.png 2x';
        case 3:
          return img800 + '@2x.png 2x';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en@2x.png 2x';
        case 2:
          return img1200 + '-en@2x.png 2x';
        case 3:
          return img800 + '-en@2x.png 2x';
      }
    }
  }

  checkI18n() {
    if (this.FLangType === 'vi-VN') {
      return 'banner_vn';
    }
    if (this.FLangType === 'zh-TW') {
      return 'banner_tw';
    }
    if (this.FLangType === 'en-US') {
      return 'banner_en';
    }
  }

  stepImg(size: number) {
    const img = 'assets/img/commodity/img-screen-car';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img + '-step-1.png';
        case 2:
          return img + '-step-2.png';
        case 3:
          return img + '-step-3.png';
      }
    } else {
      switch (size) {
        case 1:
          return img + '-en-step-1.png';
        case 2:
          return img + '-en-step-2.png';
        case 3:
          return img + '-en-step-3.png';
      }
    }
  }

  stepImgBig(size: number) {
    const img = 'assets/img/commodity/img-screen-car';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img + '-step-1@2x.png 2x';
        case 2:
          return img + '-step-2@2x.png 2x';
        case 3:
          return img + '-step-3@2x.png 2x';
      }
    } else {
      switch (size) {
        case 1:
          return img + '-en-step-1@2x.png 2x';
        case 2:
          return img + '-en-step-2@2x.png 2x';
        case 3:
          return img + '-en-step-3@2x.png 2x';
      }
    }
  }
}
