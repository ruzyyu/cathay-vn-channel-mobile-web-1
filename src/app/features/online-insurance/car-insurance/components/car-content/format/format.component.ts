import { Component, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-format',
  templateUrl: './format.component.html',
  styleUrls: ['./format.component.scss'],
})
export class FormatComponent implements OnInit {
  FLang: any;
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang.D01;
  }

  ngOnInit(): void {}
}
