import { Component, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-car-content',
  templateUrl: './car-content.component.html',
  styleUrls: ['./car-content.component.scss'],
})
export class CarContentComponent implements OnInit {
  FLang: any;
  commodityItems: any;
  projectItemsOnline: any;
  projectItemsOffline: any;

  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang.D01;

    this.commodityItems = {
      id: 1,
      title: this.FLang.D0001,
      img: 'assets/img/commodity/img-bx-car.png',
      srcset: 'assets/img/commodity/img-bx-car@2x.png 2x',
      icons: [
        {
          defaultImg: 'assets/img/commodity/ic-claim-car.png',
          defaultSrcset: 'assets/img/commodity/ic-claim-car@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-car-w.png',
          activeSrcset: 'assets/img/commodity/ic-claim-car-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-car-passenger.png',
          defaultSrcset:
            'assets/img/commodity/ic-claim-car-passenger@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-car-passenger-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-car-passenger-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-car-damage.png',
          defaultSrcset: 'assets/img/commodity/ic-claim-car-damage@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-car-damage-w.png',
          activeSrcset: 'assets/img/commodity/ic-claim-car-damage-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-car-third.png',
          defaultSrcset: 'assets/img/commodity/ic-claim-car-third@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-car-third-w.png',
          activeSrcset: 'assets/img/commodity/ic-claim-car-third-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-car-goods.png',
          defaultSrcset: 'assets/img/commodity/ic-claim-car-goods@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-car-goods-w.png',
          activeSrcset: 'assets/img/commodity/ic-claim-car-goods-w@2x.png 2x',
        },
      ],
      content: [
        {
          title: this.FLang.D0002,
          directions: this.FLang.D0002_.s001,
          tag: this.FLang.D0003,
          details: [
            {
              option: this.FLang.D0003_.s001,
              content1: this.FLang.D0003_.s010,
              money: this.FLang.D0003_.s003,
              content2: this.FLang.D0003_.s004,
            },
            // {
            //   option: this.FLang.D0003_.s005,
            //   content1: this.FLang.D0003_.s006,
            //   money: this.FLang.D0003_.s007,
            //   content2: this.FLang.D0003_.s008,
            // },
            {
              option: this.FLang.D0003_.s009,
              content1: this.FLang.D0003_.s010,
              money: this.FLang.D0003_.s011,
              content2: this.FLang.D0003_.s012,
            },
          ],
        },
        {
          title: this.FLang.D0004,
          directions: this.FLang.D0004_.s001,
          tag: this.FLang.D0005,
          details: [
            {
              option: this.FLang.D0005_.s001,
              content1: this.FLang.D0005_.s005,
              money: this.FLang.D0005_.s002,
              content2: this.FLang.D0005_.s003,
            },
            // {
            //   option: this.FLang.D0005_.s004,
            //   content1: this.FLang.D0005_.s005,
            //   money: this.FLang.D0005_.s006,
            //   content2: this.FLang.D0005_.s007,
            // },
          ],
        },
        {
          title: this.FLang.D0006,
          directions: this.FLang.D0006_.s001,
          tag: this.FLang.D0007,
          details: [
            {
              option: this.FLang.D0007_.s001,
              content1: '',
              money: this.FLang.D0007_.s002,
              content2: this.FLang.D0007_.s004,
            },
          ],
        },
        {
          title: this.FLang.D0008,
          directions: this.FLang.D0008_.s001,
          tag: this.FLang.D0009,
          details: [
            {
              option: this.FLang.D0009_.s001,
              content1: this.FLang.D0003_.s010,
              money: this.FLang.D0009_.s002,
              content2: this.FLang.D0009_.s011,
            },
            // {
            //   option: this.FLang.D0009_.s003,
            //   content1: this.FLang.D0009_.s004,
            //   money: this.FLang.D0009_.s005,
            //   content2: this.FLang.D0009_.s011,
            // },
            {
              option: this.FLang.D0009_.s006,
              content1: this.FLang.D0009_.s007,
              money: this.FLang.D0009_.s008,
              content2: this.FLang.D0009_.s012,
            },
            // {
            //   option: this.FLang.D0009_.s009,
            //   content1: this.FLang.D0009_.s010,
            //   money: this.FLang.D0009_.s013,
            //   content2: this.FLang.D0009_.s014,
            // },
          ],
        },
        {
          title: this.FLang.D0010,
          directions: this.FLang.D0010_.s001,
          tag: this.FLang.D0011,
          details: [
            {
              option: this.FLang.D0011_.s001,
              content1: '',
              money: this.FLang.D0011_.s002,
              content2: this.FLang.D0011_.s003,
            },
          ],
        },
      ],
    };

    this.projectItemsOnline = {
      title: this.FLang.D0060,
      directions: this.FLang.D0018,
      defaultImg: 'assets/img/commodity/img-product-car-01.png',
      srcsetImg: 'assets/img/commodity/img-product-car-01@2x.png 2x',
      mobileDefaultImg: 'assets/img/commodity/img-product-car-01-m.png',
      mobileSrcsetImg: 'assets/img/commodity/img-product-car-01-m@2x.png 2x',
      totalPrice: '1.480.700',
      content: [
        {
          title: this.FLang.D0019,
          directions: this.FLang.D0019_.s001,
          money: '480.700',
          defaultImg: 'assets/img/commodity/ic-claim-car-o.png',
          srcsetImg: 'assets/img/commodity/ic-claim-car-o@2x.png 2x',
        },
        {
          title: this.FLang.D0020,
          directions: this.FLang.D0020_.s001,
          money: '1.000.000',
          defaultImg: 'assets/img/commodity/ic-claim-car-passenger-o.png',
          srcsetImg: 'assets/img/commodity/ic-claim-car-passenger-o@2x.png 2x',
        },
      ],
    };

    this.projectItemsOffline = [
      {
        title: this.FLang.D0021,
        directions: this.FLang.D0022,
        defaultImg: 'assets/img/commodity/img-product-car-02.png',
        srcsetImg: 'assets/img/commodity/img-product-car-02@2x.png 2x',
        mobileDefaultImg: 'assets/img/commodity/img-product-car-02-m.png',
        mobileSrcsetImg: 'assets/img/commodity/img-product-car-02-m@2x.png 2x',
        totalPrice: '1.964.700',
        content: [
          {
            title: this.FLang.D0023,
            directions: this.FLang.D0023_.s001,
            money: '480.700',
            defaultImg: 'assets/img/commodity/ic-claim-car-o.png',
            srcsetImg: 'assets/img/commodity/ic-claim-car-o@2x.png 2x',
          },
          {
            title: this.FLang.D0024,
            directions: this.FLang.D0024_.s001,
            money: '1.000.000',
            defaultImg: 'assets/img/commodity/ic-claim-car-passenger-o.png',
            srcsetImg:
              'assets/img/commodity/ic-claim-car-passenger-o@2x.png 2x',
          },
          {
            title: this.FLang.D0025,
            directions: this.FLang.D0025_.s001,
            money: '484.000',
            defaultImg: 'assets/img/commodity/ic-claim-scooter-third-o.png',
            srcsetImg:
              'assets/img/commodity/ic-claim-scooter-third-o@2x.png 2x',
          },
        ],
      },
      {
        title: this.FLang.D0026,
        directions: this.FLang.D0027,
        defaultImg: 'assets/img/commodity/img-product-car-03.png',
        srcsetImg: 'assets/img/commodity/img-product-car-03@2x.png 2x',
        mobileDefaultImg: 'assets/img/commodity/img-product-car-03-m.png',
        mobileSrcsetImg: 'assets/img/commodity/img-product-car-03-m@2x.png 2x',
        totalPrice: '10.064.700',
        content: [
          {
            title: this.FLang.D0028,
            directions: this.FLang.D0028_.s001,
            money: '480.700',
            dollar: 'đ',
            defaultImg: 'assets/img/commodity/ic-claim-car-o.png',
            srcsetImg: 'assets/img/commodity/ic-claim-car-o@2x.png 2x',
          },
          {
            title: this.FLang.D0029,
            directions: this.FLang.D0029_.s001,
            money: '1.000.000',

            dollar: 'đ',
            defaultImg: 'assets/img/commodity/ic-claim-car-passenger-o.png',
            srcsetImg:
              'assets/img/commodity/ic-claim-car-passenger-o@2x.png 2x',
          },
          {
            title: this.FLang.D0030,
            directions: this.FLang.D0030_.s001,
            money: '484.000',
            defaultImg: 'assets/img/commodity/ic-claim-car-third-o.png',
            srcsetImg: 'assets/img/commodity/ic-claim-car-third-o@2x.png 2x',
          },
          {
            title: this.FLang.D0031,
            directions: this.FLang.D0031_.s001,
            money: '8.100.000',
            defaultImg: 'assets/img/commodity/ic-claim-car-damage-o.png',
            srcsetImg: 'assets/img/commodity/ic-claim-car-damage-o@2x.png 2x',
          },
        ],
      },
    ];
  }

  ngOnInit(): void {}
}
