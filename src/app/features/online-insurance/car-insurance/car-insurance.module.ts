import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarInsuranceRoutingModule } from './car-insurance-routing.module';
import { CarInsuranceComponent } from './car-insurance.component';
import { CarContentComponent } from './components/car-content/car-content.component';
import { CommodityComponent } from './components/car-content/commodity/commodity.component';
import { FormatComponent } from './components/car-content/format/format.component';
import { ProjectComponent } from './components/car-content/project/project.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    CarInsuranceComponent,
    CarContentComponent,
    CommodityComponent,
    FormatComponent,
    ProjectComponent,
  ],
  imports: [CommonModule, CarInsuranceRoutingModule, SharedModule],
})
export class CarInsuranceModule {}
