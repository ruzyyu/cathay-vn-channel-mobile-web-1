import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineInsuranceIndexComponent } from './online-insurance-index.component';

describe('OnlineInsuranceIndexComponent', () => {
  let component: OnlineInsuranceIndexComponent;
  let fixture: ComponentFixture<OnlineInsuranceIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnlineInsuranceIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineInsuranceIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
