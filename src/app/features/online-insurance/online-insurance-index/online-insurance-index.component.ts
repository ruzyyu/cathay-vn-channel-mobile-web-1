import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-online-insurance-index',
  templateUrl: './online-insurance-index.component.html',
  styleUrls: ['./online-insurance-index.component.scss'],
})
export class OnlineInsuranceIndexComponent {
  FLang: any = this.cds.gLang.C01;
  mapList: any;
  filterId: number;

  constructor(private cds: DatapoolService, private route: ActivatedRoute) {
    this.filterId = 0;

    this.mapList = [
      {
        id: 1,
        type: 'car',
        status: '',
        name: this.FLang.C0003_.card1.s003,
        title: this.FLang.C0003_.card1.s001,
        content: this.FLang.C0003_.card1.s002,
        dataFilter: 'car',
        defaultImg: 'assets/img/online/img-product-car.jpg',
        defaultSrcset: 'assets/img/online/img-product-car@2x.jpg 2x',
        mobileImg: 'assets/img/online/img-product-car-m.png',
        mobileSrcset: 'assets/img/online/img-product-car-m@2x.png 2x',
      },
      {
        id: 2,
        type: 'motor',
        status: '',
        name: this.FLang.C0004,
        title: this.FLang.C0004_.card1.s001,
        content: this.FLang.C0004_.card1.s002,
        dataFilter: 'motorcycle',
        defaultImg: 'assets/img/online/img-product-motor.jpg',
        defaultSrcset: 'assets/img/online/img-product-motor@2x.jpg 2x',
        mobileImg: 'assets/img/online/img-product-motor-m.png',
        mobileSrcset: 'assets/img/online/img-product-motor-m@2x.png 2x',
      },
      {
        id: 3,
        type: 'travel',
        kind: 'internal',
        status: this.FLang.C0005_.card1.s005,
        name: this.FLang.C0005,
        title: this.FLang.C0005_.card1.s002,
        content: this.FLang.C0005_.card1.s003,
        dataFilter: 'trip',
        defaultImg: 'assets/img/online/img-product-travel-01.jpg',
        defaultSrcset: 'assets/img/online/img-product-travel-01@2x.jpg 2x',
        mobileImg: 'assets/img/online/img-product-travel-01-m.png',
        mobileSrcset: 'assets/img/online/img-product-travel-01-m@2x.png 2x',
      },
      {
        id: 3,
        type: 'travel',
        kind: 'oversea',
        status: this.FLang.C0005_.card2.s005,
        name: this.FLang.C0005,
        title: this.FLang.C0005_.card2.s002,
        content: this.FLang.C0005_.card2.s003,
        dataFilter: 'trip',
        defaultImg: 'assets/img/online/img-product-travel-02.jpg',
        defaultSrcset: 'assets/img/online/img-product-travel-02@2x.jpg 2x',
        mobileImg: 'assets/img/online/img-product-travel-02-m.png',
        mobileSrcset: 'assets/img/online/img-product-travel-02-m@2x.png 2x',
      },
    ];
  }

  get filterCardList() {
    return this.filterId === 0
      ? this.mapList
      : this.mapList.filter((item) => item.id === this.filterId);
  }
}
