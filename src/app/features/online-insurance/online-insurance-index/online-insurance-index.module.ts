import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { OnlineInsuranceIndexRoutingModule } from './online-insurance-index-routing.module';
import { OnlineInsuranceIndexComponent } from './online-insurance-index.component';
import { BackgroundComponent } from './components/background/background.component';
import { CardsComponent } from './components/cards/cards.component';

@NgModule({
  declarations: [
    OnlineInsuranceIndexComponent,
    BackgroundComponent,
    CardsComponent,
  ],
  imports: [CommonModule, OnlineInsuranceIndexRoutingModule, SharedModule],
})
export class OnlineInsuranceIndexModule {}
