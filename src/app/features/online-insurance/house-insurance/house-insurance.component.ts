import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';
import { CommonService } from 'src/app/core/services/common.service';

@Component({
  selector: 'app-house-insurance',
  templateUrl: './house-insurance.component.html',
  styleUrls: ['./house-insurance.component.scss'],
})
export class HouseInsuranceComponent implements OnInit, AfterViewInit {
  activeIndex = 0;

  FLang: any;
  FLangType: string;
  faqList: any;
  insuranceRangeList: any;
  items: any;

  constructor(private numberPipe: NumberPipe, private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit(): void {
    this.items = {
      icons: [
        {
          defaultImg: 'assets/img/commodity/ic-claim-house-fire.png',
          defaultSrcset: 'assets/img/commodity/ic-claim-house-fire@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-house-fire-w.png',
          activeSrcset: 'assets/img/commodity/ic-claim-house-fire-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-house-disaster.png',
          defaultSrcset:
            'assets/img/commodity/ic-claim-house-disaster@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-house-disaster-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-house-disaster-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-house-theft.png',
          defaultSrcset: 'assets/img/commodity/ic-claim-house-theft@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-house-theft-w.png',
          activeSrcset: 'assets/img/commodity/ic-claim-house-theft-w@2x.png 2x',
        },
      ],
      content: [
        {
          title: this.FLang.D05.L0002,
          directions: this.FLang.D05.L0002_.s001,
        },
        {
          title: this.FLang.D05.L0003,
          directions: this.FLang.D05.L0003_.s001,
        },
        {
          title: this.FLang.D05.L0004,
          directions: this.FLang.D05.L0004_.s001,
        },
      ],
    };

    this.insuranceRangeList = [
      {
        title: this.FLang.D05.L0006,
        directions: this.FLang.D05.L0007,
        content: this.FLang.D05.L0008,
        defaultImg: 'assets/img/commodity/house-item-01.png',
        defaultSrcset: 'assets/img/commodity/house-item-01@2x.png 2x',
        activeImg: 'assets/img/commodity/house-item-01-m.png',
        activeSrcset: 'assets/img/commodity/house-item-01-m@2x.png 2x',
      },
    ];

    this.faqList = [
      {
        title: this.FLang.D05.L0010,
        content: this.FLang.D05.L0010_.s001,
      },
      {
        title: this.FLang.D05.L0011,
        content: this.FLang.D05.L0011_.s001,
      },
      {
        title: this.FLang.D05.L0012,
        content: this.FLang.D05.L0012_.s001,
      },
    ];
  }

  ngAfterViewInit(): void {
    CommonService.autoScrollToAccordionHeader('#accordionQuestion');
  }

  /** Number 轉換 */
  dataFilterNumber(index) {
    return 'select-' + this.numberPipe.transform(index + 2);
  }

  clickStyle(index) {
    this.activeIndex = index;
  }

  cssClassNumber(index) {
    let classPipe = this.numberPipe.transform(index + 2);
    if (index === this.activeIndex) {
      classPipe += ' active';
    }
    return classPipe;
  }

  /** 圖片位置 */
  switchPosition(index) {
    const style = 'col-sm-12 col-lg-6';
    return index % 2 === 0 ? style : style + ' order-lg-first';
  }

  /** FAQ list */
  titleId(index: number) {
    return 'FaqQuestion' + this.numberPipe.transform(index + 1);
  }
  titleDataTarget(index: number) {
    return '#collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  titleAriaControls(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoId(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoAriaLabelledby(index: number) {
    return 'FaqQuestionQue' + this.numberPipe.transform(index + 1);
  }
  titleContent(item: any) {
    return `
      <div class="tab-title d-flex">
        <h4>${item.title}</h4>
        <i class="icons-ic-faq-arrow-up i-icon i-up"></i>
        <i class="icons-ic-faq-arrow-down i-icon i-down"></i>
      </div>
      `;
  }

  infoContent(item: any) {
    return `
      <div class="que-body d-flex">
        <div class="answer custom_text_align white-space-normal">${item.content}</div>
      </div>
      `;
  }

  /** banner 圖片 語言 switch */
  bannerImg(size: number) {
    const img1920 = 'assets/img/commodity/bn-house-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-house-1200-x-400';
    const img800 = 'assets/img/commodity/bn-house-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '.png';
        case 2:
          return img1200 + '.png';
        case 3:
          return img800 + '.png';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en.png';
        case 2:
          return img1200 + '-en.png';
        case 3:
          return img800 + '-en.png';
      }
    }
  }

  /** banner 圖片 語言 switch */
  bannerImgBig(size: number) {
    const img1920 = 'assets/img/commodity/bn-house-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-house-1200-x-400';
    const img800 = 'assets/img/commodity/bn-house-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '@2x.png 2x';
        case 2:
          return img1200 + '@2x.png 2x';
        case 3:
          return img800 + '@2x.png 2x';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en@2x.png 2x';
        case 2:
          return img1200 + '-en@2x.png 2x';
        case 3:
          return img800 + '-en@2x.png 2x';
      }
    }
  }
}
