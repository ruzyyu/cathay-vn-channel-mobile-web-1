import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HouseInsuranceRoutingModule } from './house-insurance-routing.module';
import { HouseInsuranceComponent } from './house-insurance.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [HouseInsuranceComponent],
  imports: [CommonModule, HouseInsuranceRoutingModule, SharedModule],
})
export class HouseInsuranceModule {}
