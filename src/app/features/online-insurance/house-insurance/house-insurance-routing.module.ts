import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HouseInsuranceComponent } from './house-insurance.component';

const routes: Routes = [{ path: '', component: HouseInsuranceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseInsuranceRoutingModule { }
