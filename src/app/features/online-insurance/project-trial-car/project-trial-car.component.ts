import { UserinfoComponent } from './../../../shared/components/userinfo/userinfo.component';
import { CommonService } from 'src/app/core/services/common.service';
import { OnlinecalcService } from './../../../core/services/onlinecalc/onlinecalc.service';
import { PaymentService } from './../../../core/services/payment/payment.service';
import { LoginComponent } from './../../../shared/components/login/login.component';
import { MemberService } from './../../../core/services/api/member/member.service';
import { PublicService } from 'src/app/core/services/api/public/public.service';
import {
  ICalcOrder,
  IConfirm,
  ICounty,
  IProvince,
  IResponse,
  UserInfo,
} from 'src/app/core/interface/index';
import {
  IVehicleUse,
  IVechicleKd,
  ICarUserInfo,
} from './../../../core/interface/index';
import {
  Component,
  OnInit,
  ViewContainerRef,
  ViewChild,
  ComponentFactoryResolver,
  AfterViewInit,
  OnDestroy,
  HostListener,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  AbstractControl,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { InsuranceService } from 'src/app/core/services/api/Insurance/insurance.service';
import { filter } from 'rxjs/operators';
import { fromEvent, Observable, Subscription } from 'rxjs';
import * as moment from 'moment';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { DatePipe } from '@angular/common';
import { Lang } from 'src/types';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-project-trial-car',
  templateUrl: './project-trial-car.component.html',
  styleUrls: ['./project-trial-car.component.scss'],
})
export class ProjectTrialCarComponent
  implements OnInit, AfterViewInit, OnDestroy {
  constructor(
    private cds: DatapoolService,
    private fb: FormBuilder,
    private go: Router,
    private api: PublicService,
    private apiInsurance: InsuranceService,
    private route: ActivatedRoute,
    private apiMember: MemberService,
    private componentFactory: ComponentFactoryResolver,
    private calcsv: OnlinecalcService,
    private pay: PaymentService,
    private vi: ValidatorsService,
    private datePipe: DatePipe
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
    this.FTrafficType = this.cds.gTrafficType;
    this.cds.gTrafficType = false;

    if (typeof this.FLang.C03.step1.L0023 == 'string') {
      this.FLang.C03.step1.L0023 = this.FLang.C03.step1.L0023.split('$') as any;
    }

    /** 首次登入不允許再 個資頁 */
    this.ro = this.go.events.pipe(
      filter((event) => event instanceof NavigationEnd)
    ) as Observable<NavigationEnd>;
    this.ro.subscribe((evt) => {
      if (
        evt.id === 1 &&
        evt.url === '/online-insurance/project-trial-car/personInfo'
      ) {
        this.go.navigate(['/online-insurance/project-trial-car/', 'calcAmt']);
      }

      // 如果不是在本頁的活動就清除日期
      if (
        !(
          this.FPreviousUrl ===
            '/online-insurance/project-trial-car/personInfo' ||
          this.FPreviousUrl === '/online-insurance/project-trial-car/calcAmt'
        )
      ) {
        this.FCarDate = undefined;
        delete this.cds.gDatapicker.car;
      }

      if (evt.url === '/online-insurance/project-trial-car/personInfo') {
        this.FPayTransaction = false;
      }
      this.FPreviousUrl = evt.url;
    });

    /** 呼叫API */
    /** 付款方式 list */
    this.payList = this.calcsv.paylist;

    /** 加購金額選單 */
    this.amountItemsList = [
      { option: this.FLang.C03.step2.L0037, number: 10 * 1000000, unit: 10 },
      { option: this.FLang.C03.step2.L0038, number: 30 * 1000000, unit: 30 },
      { option: this.FLang.C03.step2.L0039, number: 50 * 1000000, unit: 50 },
      { option: this.FLang.C03.step2.L0040, number: 100 * 1000000, unit: 100 },
      { option: this.FLang.C03.step2.L0041, number: 200 * 1000000, unit: 200 },
    ];
    this.FSelectPayType = this.payList[0];
  }
  @ViewChild('ly_userinfo', { read: UserinfoComponent })
  ly_userinfo: UserinfoComponent;
  @ViewChild('content', { read: ViewContainerRef }) content: ViewContainerRef;
  @ViewChild('ly_pluInfo', { read: ViewContainerRef })
  ly_pluInfo: ViewContainerRef;

  /** 變數 */
  carNumber = -1;
  insuranceNumber = -1;
  amountNumber: number | string = 'all';
  peopleNumber: number | string = 'all';
  paymentNumber = -1;
  clickNumber = 0;
  // 合計保費總金額
  FAmount = 0;
  FStep: 'calcAmt' | 'personInfo' | 'payment' = 'calcAmt';
  // 加購
  plusPurchase = false;
  // 同意合約
  agreeContract = false;
  //  同投保人
  FClickSameName = false;

  //  日期
  Fdate1: string;
  startDate: string;
  endDate: string;

  Fdate2: string;
  FDate2Dis = true;
  FMax2Date: Date;
  FMin2Date: Date;

  /** 額外錯誤文字email */
  FErrorMail: string;

  // 表單檢核
  controls: { [key: string]: AbstractControl };
  validators: {};
  group: FormGroup;

  // 用戶資訊
  userInfo: UserInfo;
  FUserInfo: string;
  carUserInfo: ICarUserInfo;
  FProvinceList: Array<IProvince>;
  FCountyList: Array<ICounty>;
  FCalcOrder: ICalcOrder;

  // 保險期間控制
  FMaxYear: number;
  FMaxDate: Date;
  FMinYear: number;
  FMinDate: Date;

  FCarname: string;
  FSyncUserinfo = true;
  payList: any;
  FSelectPayType: any;

  // 判斷是否[新增會員]
  FType: string;
  // 判斷付款[完成否]
  FPayResult = false;
  /** 判斷付款-2[付款失敗 狀態On] */
  FPayTransaction = false;

  /** 資料區 */
  carItemsList: Array<IVechicleKd>;
  insuranceItemsList: Array<IVehicleUse>;

  /** 訂單狀態 */
  FOrder_no = 0;
  /** 保單編號 */
  FAply_no = '';

  peopleItemsList = [{ option: 1 }];
  FLang: Lang;
  FLangType: string;

  // 手機板驗證高度用的
  initInnerHieght: number;

  /** 監測router */
  ro: Observable<any>;
  unload: any = [];

  amountItemsList: any;
  /** 註冊渠道 */
  FChannel: string;
  /** 註冊帳號及姓名 */
  FSocialName: string;
  FSocialAccount: string;

  /** 續保 保期平移 .. */
  FDate_HH = '00';
  FDate_YYYYMMDD = '';
  FTrafficType: boolean;

  /** 預設換頁日期不清除 */
  FCarDate: string;

  /** 存放上一頁網址 */
  FPreviousUrl;

  /** vkd 這兩類ckd是自動計算*/
  FVkd = ['21', '31'];

  /** 付款二次確認窗 */
  FConfirm: IConfirm;

  /** 選擇付款跟同意的提示start */
  FAgreeError: string;
  FPayNoError: string;

  /** confirm 專用 */
  FsubscribePayAction: Subscription;

  FReadonly: boolean;

  // 小網日期不得開啟填寫功能
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }

    this.alignPaymentText();
  }

  ngAfterViewInit() {
    this.route.params.subscribe((params) => {
      switch (params.step) {
        case 'calcAmt':
          if (this.controls) {
            this.Fdate1 = `${moment(this.controls.car_procDate.value).format(
              'DD/MM/YYYY'
            )}`;
            this.Fdate2 = `${moment(this.controls.car_procDateEnd.value).format(
              'DD/MM/YYYY'
            )}`;
          }
          break;
        case 'personInfo':
          setTimeout(this.alignPaymentText, 500);
          break;
      }
    });
    /** 閏年處理 */
    // this.leapYear();

    /** 我的訂單(修改) 初始畫面加上class */
    let polDt;
    if (this.cds.gTrafficicinsurance && this.cds.gTrafficicinsurance.pol_dt) {
      polDt = this.cds.gTrafficicinsurance.pol_dt;
    }
    if (this.cds.gDatapicker.car || polDt) {
      setTimeout(() => {
        this.FCarDate = this.cds.gDatapicker.car;
      }, 0);
    }
  }

  async ngOnInit() {
    // 換頁處理
    this.route.params.subscribe((params) => {
      this.FStep = params.step;
      if (this.FStep) {
        $('#paymentFailModal').modal('hide');
      }
    });

    this.route.queryParams.subscribe(async (queryParams) => {
      // 導入參數
      if (queryParams && queryParams.xStep) {
        this.FStep = queryParams.xStep;
      }
    });

    /** userinfo註冊換頁.... */
    this.calcsv.Inituser();

    this.FUserInfo = '';

    /** 設定日期元件日期限制 */
    const sd = new Date();
    let ed = new Date();
    sd.setFullYear(sd.getFullYear() + 1);
    ed.setDate(ed.getDate() + 1);
    ed = new Date(ed);
    this.FMaxDate = sd;
    this.FMinDate = ed;

    /** 車籍資料初始化 */
    this.initCarInfo();
    this.userInfo = this.cds.userInfo;

    /** 順序不可調換 ，必須在 validators 建立之前 */
    await this.init();

    /** 表單驗證項目 */

    this.validators = this.calcsv.initData_car(this.userInfo, this.carUserInfo);

    this.formValidate();

    /** 網頁迴車鍵 */
    if (
      this.cds.gTCalc_to_save &&
      this.cds.gTCalc_to_save.type === 'car' &&
      !this.route.snapshot.queryParams.action
    ) {
      this.setValue(this.cds.gTCalc_to_save.saveorder);
      if (!this.cds.userInfo.seq_no) {
        this.calcsv.converuser(this.cds.userInfoTemp);
        this.ly_userinfo.doReflashScreen();
      }
      this.calcsv.resetData();
    }

    this.route.queryParams.subscribe(async (queryParams) => {
      // 導頁付款.
      if (queryParams.action === 'pay') {
        await this.setValue(this.cds.gTCalc_to_save.saveorder);
        this.cds.gTCalc_to_save.type = 'car';
        await this.doPayment(
          this.cds.gTCalc_to_save,
          false,
          queryParams.action
        );
        this.calcsv.resetData();
        this.cds.userInfo.seq_no ? (this.FType = 'edit') : (this.FType = 'add');
      }
    });

    // 子視窗存狀態碼，避免關閉錯誤
    this.route.queryParams.subscribe(async (queryParams) => {
      if (queryParams.amount) {
        this.unload.forEach((subscribe) => {
          subscribe.unsubscribe();
        });
      } else {
        if (this.unload.length === 0) {
          this.unload.push(
            // 註冊全局的事件
            fromEvent(window, 'beforeunload').subscribe((event: any) => {
              (event || window.event).returnValue =
                '★資料尚未存檔，確定是否要離開？★';
              return '★資料尚未存檔，確定是否要離開？★';
            })
          );
        }
      }
    });

    /** 編輯頁帶入userinfo */
    if (this.cds.gTrafficicinsurance && this.cds.gTrafficicinsurance.vehc_kd) {
      this.setValue(this.cds.gTrafficicinsurance);
    }

    // 小網日期不得開啟填寫功能
    if ($(document).innerWidth() > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }

  /**
   * @description 處理從訂單編輯，或是續保流程
   */
  async init() {
    // 汽車車種
    await this.api.gCarList();
    // 汽車use類
    await this.api.gCarTypeList();

    if (!this.carItemsList) {
      this.carItemsList = this.cds.gPubCommon.carList;
    }
  }

  async setValue(param) {
    // 處理從訂單編輯，或是續保流程
    /** 訂單狀態 */
    this.FOrder_no = param.order_no;
    /** 保單編號 */
    this.FAply_no = param.aply_no;

    // 被取代區
    this.controls.car_people.setValue(Number(param.max_capl));
    // this.controls.car_procDate.setValue(
    //   this.datePipe.transform(param.pol_dt, 'yyyy-MM-dd')
    // );
    this.controls.car_procDate.setValue(
      moment(param.pol_dt).format('YYYY-MM-DD')
    );

    this.controls.car_procDateEnd.setValue(
      moment(param.pol_due_dt).format('YYYY-MM-DD')
    );

    this.controls.r_year.setValue(param.pol_dt_kind);

    /** 處理保期平移 */

    // moment(param.pol_dt).format('YYYY-MM-DD');

    this.FDate_HH = CommonService.paddingLeft(
      moment(param.pol_dt).get('hours'),
      2
    );
    this.FDate_YYYYMMDD = moment(param.pol_dt).format('YYYY-MM-DD');

    /** 移動到ngAfterViewInit 處理  */
    // this.Fdate1 = `${moment(param.pol_dt).format('DD/MM/YYYY')}~${moment(
    //   param.pol_due_dt
    // ).format('DD/MM/YYYY')}`;
    this.Fdate1 = `${moment(param.pol_dt).format('DD/MM/YYYY')}`;
    this.Fdate2 = `${moment(param.pol_due_dt).format('DD/MM/YYYY')}`;

    this.controls.car_license_plate.setValue(param.rgst_no);
    this.controls.car_engine_number.setValue(param.engn_no);
    this.controls.car_body_number.setValue(param.fram_no);
    this.controls.car_vkd.setValue(param.vehc_kd);
    this.controls.car_ckd.setValue(param.car_pwr_kd);
    this.controls.car_name.setValue(param.vehc_ownr);
    this.FAmount = param.tot_prem_with_vat;

    // 不給錢的一律重新試算
    if (param.compprem !== 0) {
      this.FCalcOrder = {
        compprem: param.comp_prem,
        comppremvat: param.comp_premvat,
        passengerprem: param.passenger_prem,
        driverprem: param.driver_prem,
        totprem: param.tot_prem,
        totpremwithvat: param.tot_prem_with_vat,
      };
    }
    // ----------------------------------------------------------------------
    /** 動作處理 step1 */
    // vkd 下拉
    this.onCarFilterClick(
      await this.api.getvkd_id(param.vehc_kd, param.car_pwr_kd)
    );
    // ckd 下拉處理 start
    this.insuranceNumber = await this.api.getckd_id(
      param.vehc_kd,
      param.car_pwr_kd
    );

    //  試算頁開啟
    this.clickNumber = 1;
    // 任意險調整 ..
    this.peopleItemsList = [];
    for (
      let index = 0;
      index < Number(this.controls.car_people.value);
      index++
    ) {
      this.peopleItemsList.push({ option: index + 1 });
    }
    this.peopleNumber =
      param.passenger_cnt + param.driver_cnt > 0
        ? param.passenger_cnt + param.driver_cnt - 1
        : -1;
    this.amountNumber = this.amountItemsList.findIndex((item) => {
      return item.number === param.driver_amt;
    });

    if (this.amountNumber >= 0) {
      this.plusPurchase = true;
    }

    this.peopleFilterItems(this.peopleNumber);
    this.amountFilterItems(this.amountNumber);


    this.paymentNumber = param.paymentNumber;

    /** 動作處理 step2 */
    if (param.compprem !== 0) {
      this.calcTest(1, false);
    }

    if (param.vehc_ownr === param.customer_name) {
      this.FClickSameName = true;
      this.controls.car_name.disable();
    }

    /** 清除狀態 */
    if (this.cds.gTCalc_to_save) {
      this.cds.gTCalc_to_save.type = 'none';
    }
    this.cds.gTrafficicinsurance = null;
  }

  ngOnDestroy(): void {
    this.cds.gTrafficicinsurance = null;
    this.unload.forEach((x) => x.unsubscribe());
    if (this.FsubscribePayAction) {
      this.FsubscribePayAction.unsubscribe();
    }
  }

  checkCalc() {
    const mbol = !(
      this.carNumber >= 0 &&
      this.insuranceNumber >= 0 &&
      this.controls.car_people.valid &&
      this.controls.car_procDate.valid &&
      this.controls.car_procDateEnd.valid
    );
    return mbol;
  }
  /** 試算 */
  async calcTest(index, isCalc = true) {
    // 先將加保人歸零
    if (
      !this.plusPurchase &&
      this.peopleItemsList[0] &&
      this.peopleItemsList[0].option
    ) {
      this.peopleItemsList[0].option = null;
    }

    this.controls.car_vkd.markAsTouched();
    this.controls.car_ckd.markAsTouched();
    this.controls.car_procDate.markAsTouched();
    this.controls.car_procDate.markAsDirty();
    this.controls.car_procDateEnd.markAsTouched();
    this.controls.car_procDateEnd.markAsDirty();
    this.controls.car_people.markAsDirty();
    this.controls.car_people.markAsTouched();

    if (this.checkCalc()) {
      return;
    }

    // 送出之前把資料寫入宣告物件內 使用宣告物件來計算
    this.carUserInfo.maxCapl = this.controls.car_people.value;
    this.carUserInfo.procDate =
      CommonService.formatDate(this.controls.car_procDate.value) || '';
    this.carUserInfo.procDateEnd =
      CommonService.formatDate(this.controls.car_procDateEnd.value) || '';
    this.carUserInfo.procDateYear = this.controls.r_year.value;
    this.carUserInfo.vehcKd = this.carItemsList[this.carNumber].vechicleKdValue;

    /** vkd = 21.31  ckd 動態計算 */
    if (this.FVkd.includes(this.carUserInfo.vehcKd)) {
      this.carUserInfo.carPwrKd = CommonService.paddingLeft(
        this.calcsv.calcCKD(
          this.controls.car_vkd.value,
          this.carUserInfo.maxCapl
        ),
        2
      );
    } else {
      this.carUserInfo.carPwrKd = this.insuranceItemsList[
        this.insuranceNumber
      ].carPwrKdValue;
    }

    let payload = {
      language: this.cds.gLangType,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      token: this.cds.gToken,
      typepol: 'B',
      prodpolcode: 'CF02002',
      vehckd: this.carUserInfo.vehcKd,
      carpwrkd: this.carUserInfo.carPwrKd,
      procdate: this.carUserInfo.procDate,
      pol_dt: `${this.carUserInfo.procDate} ${this.FDate_HH}:00:00`,
      pol_due_dt: `${this.carUserInfo.procDateEnd} ${this.FDate_HH}:00:00`,
      maxcapl: Number(this.carUserInfo.maxCapl),
      pol_dt_kind: this.carUserInfo.procDateYear,
    };

    // Attach policy
    if (this.plusPurchase && this.peopleNumber >= 0 && this.amountNumber >= 0) {
      const mAttach = {
        driveramt: this.amountItemsList[this.amountNumber].number,
        drivercnt: 1,
        passengercnt:
          this.peopleNumber >= 0
            ? Number(this.peopleItemsList[this.peopleNumber].option) - 1
            : 0,

        passengeramt:
          this.peopleNumber > 0 // 等於0的時候 人數是1 沒乘客，勿改...核心會錯
            ? this.amountItemsList[this.amountNumber].number
            : 0,
      };

      payload = {
        ...payload,
        ...mAttach,
      };
    }

    const rep: IResponse = await this.apiInsurance.calcInsurance(payload);
    this.FCalcOrder = rep.data;
    if (this.plusPurchase && this.peopleNumber >= 0 && this.amountNumber >= 0) {
      this.FCalcOrder.driveramt = this.amountItemsList[
        this.amountNumber
      ].number;
      this.FCalcOrder.passengeramt = this.amountItemsList[
        this.amountNumber
      ].number;
    }

    // 總金額
    if (rep.status === 200) {
      this.FAmount = rep.data.totpremwithvat;
      if (this.clickNumber === 0) {
        if (isCalc) {
          setTimeout(() => {
            CommonService.scroll(this.ly_pluInfo.element.nativeElement);
          }, 100);
        }
      }
      this.clickNumber = index;
    }
  }

  /** 共用樣式 */
  styleClass(index) {
    const style = 'text-gray-gr';
    return index >= 0 ? '' : style;
  }

  /** 背景 disabled */
  styleBackground(type) {
    if (type) {
      return '';
    } else {
      return 'background-color:#e1e4e9';
    }
  }

  /** alert 窗 */
  compulsoryInsuranceAlert() {
    $('#Modal-48').modal('show');
  }

  modalShowPass() {
    $('#modal_pass').modal('show');
  }

  /** 同投保人 */
  sameName(status: boolean, value: string) {
    if (status) {
      this.controls[value].setValue(this.controls.name.value);
      this.controls[value].disable();
    } else {
      this.controls[value].setValue('');
      this.controls[value].enable();
    }
  }

  // ----------------------------------------------------------------------
  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          car_people:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES013,
          car_name:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
          car_license_plate:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES014,
          car_body_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES015,
          car_engine_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES016,
        }[control_key];
      }
      /** 新檢核end */

      return {
        fontLengthNumber:
          this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
        vnCertNumber: this.FLang.ER.ER036,
        vn2020ID: this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
        vnRgstNo: this.FLang.ER.ER023,
        vnRgstCarNo: this.FLang.ER.ER044,
        vnCheckQty: this.FLang.ER.ER024,
        required: this.FLang.ER.ER017,
        languageFormat:
          this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
        languagelength:
          this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES001,
      }[type];
    }
    return this.FLang.ER.ER023;
  }

  /** 建立驗證表單 */
  formValidate() {
    // 建立驗證表單
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });

    this.group = this.fb.group(validator, { updateOn: 'blur' });

    // 日期異動修改第二個日期
    this.group.get('car_procDate').valueChanges.subscribe((item) => {
      setTimeout(() => {
        this.Fdate1 = moment(this.controls.car_procDate.value).format(
          'DD/MM/YYYY'
        );
        this.Fdate2 = moment(this.controls.car_procDateEnd.value).format(
          'DD/MM/YYYY'
        );
      }, 0);

      if (
        this.controls.r_year.value === '99' &&
        this.controls.car_procDate.value
      ) {
        this.FMax2Date = moment(this.controls.car_procDate.value)
          .add(2.5, 'years')
          .toDate();
        this.FMin2Date = moment(this.controls.car_procDate.value)
          .add(1, 'years')
          .toDate();
        this.FDate2Dis = false;
        setTimeout(() => {
          this.Fdate2 = undefined;
          this.controls.car_procDateEnd.setValue(undefined);
        }, 1);
      } else {
        this.FDate2Dis = true;
      }
    });

    //  年份異動時修改第二個日期
    this.group.get('r_year').valueChanges.subscribe((item) => {
      if (
        this.controls.r_year.value === '99' &&
        this.controls.car_procDate.value
      ) {
        this.FMax2Date = moment(this.controls.car_procDate.value)
          .add(2.5, 'years')
          .toDate();
        this.FMin2Date = moment(this.controls.car_procDate.value)
          .add(1, 'years')
          .toDate();
        this.FDate2Dis = false;
      } else {
        this.FDate2Dis = true;
        // this.Fdate2 = this.controls.car_procDateEnd.value;
        if (this.controls.car_procDate.value) {
          setTimeout(() => {
            this.controls.car_procDateEnd.setValue(
              moment(this.controls.car_procDate.value)
                .add(this.getYearValue(this.controls.r_year.value), 'years')
                .format('YYYY-MM-DD')
            );
            this.Fdate2 = moment(this.controls.car_procDateEnd.value).format(
              'DD/MM/YYYY'
            );
          }, 0);
        }
      }
      this.initCalc();
    });

    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  // 畫面處理區 ↓ ↓ ↓

  /** 車種 */
  onCarFilterClick(index: number) {
    if (index !== this.carNumber) {
      this.carNumber = index;
      this.FVkd.includes(this.carItemsList[this.carNumber].vechicleKdValue)
        ? (this.insuranceNumber = 0)
        : (this.insuranceNumber = -1);
      this.controls.car_vkd.setValue(
        this.carItemsList[this.carNumber].vechicleKdValue
      );
    }

    this.insuranceItemsList = this.cds.gPubCommon.carUseList.filter(
      (item) =>
        item.vehcKdValue === this.carItemsList[this.carNumber].vechicleKdValue
    );

    if (this.insuranceNumber >= 0) {
      this.controls.car_ckd.setValue(
        this.insuranceItemsList[this.insuranceNumber].carPwrKdValue
      );
    }
    this.initCalc();
  }

  carFilterItems() {
    if (this.carNumber < 0) {
      return this.FLang.PH.enter.F0002 + this.FLang.PH.enter.F0002_.FS004;
    }

    // console.log(this.carItemsList);
    return this.carItemsList[this.carNumber].zhName;
  }

  /** onUseClick */
  onUseClick(index) {
    if (index !== this.insuranceNumber) {
      this.insuranceNumber = index;
      this.controls.car_ckd.setValue(
        this.insuranceItemsList[this.insuranceNumber].carPwrKdValue
      );
      this.initCalc();
    }
  }

  onUseFilterNumber() {
    if (this.insuranceNumber < 0) {
      return this.FLang.PH.enter.F0002 + this.FLang.PH.enter.F0002_.FS005;
    }
    return this.insuranceItemsList[this.insuranceNumber].zhName;
  }

  /** 保額 */
  amountFilterNumber(index) {
    this.amountNumber = index;
    this.calcTest(1);
  }

  amountFilterItems(index) {
    return (
      this.amountItemsList[index]?.option ||
      this.FLang.PH.enter.F0002 + this.FLang.PH.enter.F0002_.FS014
    );
  }

  /** 保障人數 */
  peopleFilterNumber(index) {
    this.peopleNumber = index;
    this.calcTest(1);
  }

  peopleFilterItems(index) {
    return (
      this.peopleItemsList[index]?.option ||
      this.FLang.PH.enter.F0002 + this.FLang.PH.enter.F0002_.FS015
    );
  }

  /** 付款切換 */
  paymentSwitch(index) {
    this.FSelectPayType = this.payList[index];
    this.paymentNumber = index;
  }

  /** 支付方式 */
  dataTarget(index) {
    return '#Modal-0' + (index + 4);
  }

  // -----------------------------------------------------------------
  /** 切換頁面 */
  async goStep(position) {
    switch (position) {
      case 'center':
        this.go.navigate(['/online-insurance/project-trial-car']);
        break;
      case '2':
        // 填寫個人資料
        if (this.cds.userInfo.seq_no) {
          this.FType = 'edit';
          // this.FStep = 'personInfo';
          this.go.navigate([
            '/online-insurance/project-trial-car/',
            'personInfo',
          ]);

          CommonService.topFunction();
        } else {
          // 註冊會員
          this.FType = 'add';
          this.showLogin();
          CommonService.topFunction();
        }

        break;
      // 付款
      case '3':
        /** 解除封印 允許後檢核 */
        Object.keys(this.controls).forEach((item) => {
          this.controls[item].markAsDirty();
          this.controls[item].markAsTouched();
        });
        /** 身分證額外檢核 */
        this.group
          .get('certificate_number')
          .setValidators([
            this.vi.fontLengthNumber,
            Validators.required,
            this.vi.vnCertNumber(
              this.controls.birthday.value,
              this.controls.sex.value
            ),
            this.vi.vn2020ID(this.controls.birthday.value),
          ]);
        this.group.get('certificate_number').updateValueAndValidity();

        /** 檢查userinfo是否完成 */
        const mbol = this.ly_userinfo.checkPayment();
        if (!mbol) {
          let resultError;
          for (const key in this.controls) {
            if (!resultError && this.controls[key].invalid) {
              resultError = key;
            }
          }
          if (resultError) {
            CommonService.scrollto(`lyc_${resultError}`, 30);
          }
          return;
        }
        if (!this.checkPayment()) {
          return;
        }

        // 如不是會員，則...
        if (this.userInfo.seq_no === 0) {
          // 線下保單 = N
          this.userInfo.is_validate_policy = 'N';
        }
        let reppayload;
        // 更新會員
        if (this.FSyncUserinfo) {
          reppayload = {
            ...this.cds.userInfo,
            email: this.controls.email.value,
            email_bk: this.controls.email_bk.value,
            mobile: this.controls.mobile.value,
            county: this.controls.county.value,
            province: this.controls.province.value,
            addr: this.controls.address.value,
            account: this.controls.email.value, // 忘記密碼帳號
          };

          // 強制將mail交換
          if (this.FType === 'edit') {
            // reppayload.email_bk = reppayload.email;
            // reppayload.email = this.cds.userInfo.email;
          } else {
            // 新增流程須去註冊頁，緩存預計存檔資訊
            reppayload.customer_name = this.controls.name.value.trim();
            reppayload.birthday = this.controls.birthday.value;
            reppayload.seq_no = this.cds.userInfo.seq_no || 0;
            reppayload.is_validate_policy = 'N';
            reppayload.sex = this.controls.sex.value;

            /** 第三方登入 */
            if (this.FChannel === 'fb') {
              reppayload.fb_account = this.FSocialAccount;
              reppayload.fb_title = this.FSocialName;
            } else {
              reppayload.google_account = this.FSocialAccount;
              reppayload.google_title = this.FSocialName;
            }

            // 新增流程驗證Email
            if (reppayload.seq_no === 0) {
              const payload = { email: reppayload.email };
              const rep: IResponse = await this.apiMember.MemberVerifyEmail(
                payload
              );
              if (rep.status !== 200) {
                CommonService.scrollto('lyc_email');
                this.FErrorMail = this.FLang.ER.ER033;
                return;
              }
            }
          }
        }

        /**
         * @description 訂單存檔 [特別規則]
         * FDate_HH 續保單帶來的應該會有不同的值，優先已帶來的值為主
         * pol_due_dt 必須是一年
         */
        let payload: any = {
          order_no: this.FOrder_no,
          aply_no: this.FAply_no,
          order_status: '1',
          member_seq_no: this.cds.userInfo.seq_no || 0,
          type_pol: 'B',
          prod_pol_code: 'CF02002',
          prod_pol_code_nm: '強制險',
          pol_dt: this.carUserInfo.procDate + ` ${this.FDate_HH}:00:00`,
          pol_due_dt: this.carUserInfo.procDateEnd + ` ${this.FDate_HH}:00:00`,
          certificate_type: '1',
          certificate_number: this.controls.certificate_number.value,
          customer_name: this.controls.name.value.trim(),
          birthday: this.controls.birthday.value,

          addr: this.controls.address.value,
          province: this.controls.province.value,
          county: this.controls.county.value,
          mobile: this.controls.mobile.value,
          email: this.controls.email_bk.value, // SA要求的
          sex: this.controls.sex.value,
          national_id: 'VIETN',
          vehc_ownr: this.controls.car_name.value,
          rgst_no: this.controls.car_license_plate.value,
          engn_no: this.controls.car_engine_number.value,
          fram_no: this.controls.car_body_number.value,
          vehc_kd: this.carUserInfo.vehcKd,
          car_pwr_kd: this.carUserInfo.carPwrKd,
          max_capl: this.controls.car_people.value,
          comp_prem: this.FCalcOrder.compprem,
          comp_premvat: this.FCalcOrder.comppremvat,
          passenger_prem: this.FCalcOrder.passengerprem,
          driver_prem: this.FCalcOrder.driverprem,
          tot_prem: this.FCalcOrder.totprem,
          tot_prem_with_vat: this.FCalcOrder.totpremwithvat,
          print_type: '2',
          driver_amt: 0,
          driver_cnt: 0,
          passenger_cnt: 0,
          passenger_amt: 0,
          // pol_dt_kind: '',
          language: this.cds.gLangType,
          token: this.cds.gToken,
          client_target: this.controls.certificate_number.value
            ? this.controls.certificate_number.value
            : this.cds.userInfo.certificate_number_12
            ? this.cds.userInfo.certificate_number_12
            : this.cds.userInfo.certificate_number_9,
          pol_dt_kind: this.carUserInfo.procDateYear,
          paymentNumber: this.paymentNumber,
        };

        // Attach policy
        if (this.plusPurchase) {
          const mAttach = {
            driver_amt: this.FCalcOrder.driveramt,
            driver_cnt: 1,
            passenger_cnt:
              this.peopleNumber >= 0
                ? Number(this.peopleItemsList[this.peopleNumber].option) - 1
                : 0,

            passenger_amt:
              this.peopleNumber > 0 // 等於0的時候 人數是1 沒乘客，勿改...核心會錯
                ? this.amountItemsList[this.amountNumber].number
                : 0,
          };

          payload = {
            ...payload,
            ...mAttach,
          };
        }

        this.cds.gTCalc_to_save = {
          type: 'car',
          saveorder: payload,
          userinfo: reppayload,
          paytype: this.FSelectPayType.type,
        };

        this.doPayment(this.cds.gTCalc_to_save, this.FSyncUserinfo);

        /** 資料送出後將緩存日期清空 */
        if (this.cds.gDatapicker.car) {
          this.FCarDate = undefined;
          delete this.cds.gDatapicker.car;
        }
    }
  }

  getDate(event) {
    let date;
    if (event && event.indexOf('/') !== -1) {
      date = this.datePipe.transform(event, 'yyyy-MM-dd');
    } else {
      date = event;
    }

    if (event) {
      this.controls.car_procDate.setValue(date);
      if (this.controls.r_year.value !== '99') {
        this.controls.car_procDateEnd.setValue(
          moment(date)
            .add(this.getYearValue(this.controls.r_year.value), 'years')
            .format('YYYY-MM-DD')
        );
      } else {
        this.controls.car_procDateEnd.setValue(
          moment(date).add(1, 'years').format('YYYY-MM-DD')
        );
      }
      this.initCalc();
      // this.addOneYear();
    }
  }

  getDateEnd(event) {
    let date;
    if (event && event.indexOf('/') !== -1) {
      date = this.datePipe.transform(event, 'yyyy-MM-dd');
    } else {
      date = event;
    }

    if (event) {
      if (this.controls.r_year.value === '99') {
        this.controls.car_procDateEnd.setValue(date);
      }
      this.initCalc();
    }
  }
  // 試算按鈕復原
  initCalc() {
    this.clickNumber = 0;
    this.plusPurchase = false;
    this.amountNumber = -1;
    this.peopleNumber = -1;
    this.FAmount = 0;
  }

  /** 異動人數下拉清單 */
  changePeopleItemsList(event) {
    /** max 56 */
    const value = event.target.value;
    const min = 1;
    const max = 56;

    if (parseInt(value) < min || parseInt(value) > max) {
      event.target.value = event.target.value.slice(0, -1);
    }

    this.initCalc();
  }

  /** 附加保單開關 */
  attrPolicy() {
    // 人數下拉
    setTimeout(() => {
      this.peopleItemsList = [];

      if (this.plusPurchase) {
        if (!this.amountNumber) {
          this.amountNumber = 0;
        }
        if (!this.peopleNumber) {
          this.peopleNumber = 0;
        }
        /** 注意修改
         * 當乘客只有1人的時候保持數量0
         * 因後端欄位駕駛與乘客分開...
         */
        if (this.controls.car_people.value) {
          const mCount =
            this.controls.car_people.value === 1
              ? 0
              : Number(this.controls.car_people.value) - 1;

          // this.peopleItemsList = [{ option: this.controls.car_people.value }];
          for (
            let index = 0;
            index < Number(this.controls.car_people.value);
            index++
          ) {
            this.peopleItemsList.push({ option: index + 1 });
          }
          this.peopleFilterNumber(mCount);
        }
      } else {
        if (this.peopleItemsList[0] && this.peopleItemsList[0].option) {
          this.peopleItemsList[0].option = null;
        }
      }
      this.calcTest(1);
    }, 1);
  }

  /** 動態元件區 */
  showLogin(xtype: string = 'both', checkID: boolean = false) {
    const component = this.componentFactory.resolveComponentFactory(
      LoginComponent
    );

    this.content.clear();
    const com = this.content.createComponent(component);
    $('#model-Login').modal('show');
    com.instance.xPop = true;
    com.instance.xtype = xtype;
    /** 是否是因為檢核ID而發起 */
    com.instance.xCheckid = checkID;

    // 立即註冊
    com.instance.xRegister.subscribe((item) => {
      this.cds.gPubCommon.gOtpAuto = 'register';
      $('#model-Login').modal('hide');
      this.go.navigate(['/online-insurance/project-trial-car/', 'personInfo']);
      const marr = [
        'certificate_number',
        'name',
        'email',
        'email_bk',
        'mobile',
        'birthday',
        'address',
        'province',
        'county',
      ];
      marr.forEach((item) => {
        this.controls[item].setValue('');
      });
      this.controls.sex.setValue('-1');
    });

    // 忘記密碼
    com.instance.xforgetPassword.subscribe((item) => {
      $('#model-Login').modal('hide');
      this.go.navigateByUrl('login/find-account');
    });

    // 登入
    com.instance.xlogin.subscribe((xitem) => {
      $('#model-Login').modal('hide');
      this.go.navigate(['/online-insurance/project-trial-car/', 'personInfo']);
      this.cds.userInfo.seq_no ? (this.FType = 'edit') : (this.FType = 'add');
      this.userInfo = this.cds.userInfo;

      /** 第三方登入 */
      if (xitem && xitem.type === 'social') {
        this.cds.gPubCommon.gOtpAuto = 'bind';
        this.FSocialName = xitem.name;
        this.FSocialAccount = xitem.account;
        this.FChannel = xitem.channel;
      }
      this.ly_userinfo.onUpdateCDStoControl(this.cds.userInfo);
      this.FType = this.ly_userinfo.data.xType;
    });
  }

  /**
   * @description 子母傳值
   * @param 會員建檔資料
   *
   * */
  setUsertoGroup(event) {
    Object.keys(event).forEach((item) => {
      if (this.controls[item]) {
        this.controls[item].setValue(event[item].value);
      }
    });
  }

  initCarInfo() {
    this.carUserInfo = {};
    this.carUserInfo.maxCapl = 0;
    this.carUserInfo.vehcKd = '';
    this.carUserInfo.carPwrKd = '';
    this.carUserInfo.procDate = '';
    this.carUserInfo.procDateEnd = '';
    this.carUserInfo.procDateYear = '';
    this.carUserInfo.vehcOwnr = '';
    this.carUserInfo.rgstNo = '';
    this.carUserInfo.framNo = '';
    this.carUserInfo.engnNo = '';
  }

  checkPayment() {
    const mbol =
      this.paymentNumber >= 0 && this.agreeContract && this.group.valid;

    if (!mbol && this.group.invalid) {
      CommonService.scrollto('lyc_carinfo');
    } else if (!mbol && this.paymentNumber < 0) {
      CommonService.scrollto(`ly_paymentbox`, 50);
    } else if (!mbol && !this.agreeContract) {
      CommonService.scrollto(`ly_agreeContract`);
    }
    this.doTipError();

    return mbol;
  }
  doTipError() {
    this.FPayNoError = '';
    if (this.paymentNumber < 0) {
      this.FPayNoError = this.FLang.PH.PH044;
    }
    this.FAgreeError = '';
    if (!this.agreeContract) {
      this.FAgreeError = this.FLang.PH.PH045;
    }
  }

  checkpaysomething() {
    return (
      (this.plusPurchase && this.amountNumber >= 0 && this.peopleNumber >= 0) ||
      (this.clickNumber === 1 && !this.plusPurchase)
    );
  }
  /** userInfo */
  async userinfo_action(payload) {
    if (payload) {
      const mstr = await this.apiMember.authThirdLogin(payload);
      this.showLogin(mstr, true);
    }
  }

  /**
   * @description 付款流程
   * @param {object} payload 物件
   * @property {string} type  [ car | motor | travel ],
   * @property {any} saveorder  [訂單存檔資料]
   * @property {any} userinfo  [當前用戶資訊]
   * @param {boolean} updateUser  [是否要更新會員]
   * */
  async doPayment(payload, updateUser, action = null) {
    /** 首次註冊流程 透過seq_no === 0 判定是否執行 */
    if (updateUser && payload.userinfo.seq_no === 0) {
      this.pay.regeditFlow(
        payload.userinfo,
        payload.saveorder.certificate_number
      );
      this.go.navigate(['/login/otp-verification']);
      return;
    }

    /** 1 開始存檔 */
    const rep = await this.pay.savetxend(payload, updateUser);
    /** 1.1 無論有無核保成功都需取得 order_no aply_no */
    const { order_no, aply_no } = rep.data;
    this.FOrder_no = order_no;
    this.FAply_no = aply_no;

    if (rep.status === 200) {
      if (action === 'pay') {
        this.go.navigate(['/online-insurance/project-trial-car/', 'payment'], {
          replaceUrl: true,
        });
      } else {
        this.go.navigate(['/online-insurance/project-trial-car/', 'payment']);
      }
      /** 2 付款資訊緩存 */
      this.cds.gBeforPayinfo = {
        order_no,
        aply_no,
        prod_pol_code: 'CF02002',
        amt: payload.saveorder.tot_prem_with_vat,
        uamt: payload.saveorder.tot_prem,
      };

      /** 2.1 V1 改版成後啟動 */
      if (CommonService.isIOS()) {
        this.FConfirm = this.calcsv.openPayConfirm();
        setTimeout(() => {
          $('#lyConfirm').modal('show');
        }, 300);
      } else {
        /** 2.2 開始付款 */
        this.ActionYesNo(true);
      }
    } else {
      this.FPayTransaction = true;
    }
  }
  ActionYesNo(xresult) {
    if (xresult) {
      if (!this.FsubscribePayAction || this.FsubscribePayAction.closed) {
        // 觀察者初始化...
        this.FsubscribePayAction = this.pay.getPayObserve().subscribe(
          (params) => {
            if (params.status === '1') {
              this.FPayTransaction = false;
              this.FPayResult = true;
            }
            this.pay.editOrderState(
              params,
              params.status === '1' ? '4' : '5',
              this.paymentNumber as number
            );
            this.pay.showResult(params.status);
            // 付款成功後清除付款方式狀態
            this.cds.gTCalc_to_save.paytype = undefined;
          },
          (error) => {
            console.log(error);
            switch (error.status) {
              case -9:
                this.FPayTransaction = true;
                $('#paymentFailModal').modal('show');
                break;
            }
          }
        );
      }

      // 支付
      this.pay.doPayment(
        this.cds.gTCalc_to_save.saveorder.tot_prem_with_vat,
        this.FAply_no + moment().format('yyMMDDHHmmss'),
        this.cds.gTCalc_to_save.paytype
      );
    } else {
      this.go.navigate(['/online-insurance/project-trial-car/', 'personInfo']);
    }
    $('#lyConfirm').modal('hide');
  }

  // 閏年處理
  leapYear() {
    this.route.params.subscribe((params) => {
      if (params.step === 'calcAmt') {
        if (this.controls) {
          this.Fdate1 = `${moment(this.controls.car_procDate.value).format(
            'DD/MM/YYYY'
          )}-${CommonService.addOneYear(
            this.controls.car_procDate.value,
            'DD/MM/YYYY'
          )}`;
        }
      }
    });
  }

  /** 年份 radio vaule 轉換成年份
   * 1:1年，2:1.5年，3:2年，4:2.5年，99:自訂
   */
  getYearValue(value) {
    switch (value) {
      case '1':
        return '1';
      case '2':
        return '1.5';
      case '3':
        return '2';
      case '4':
        return '2.5';
      case '99':
        return 'custom';
      default:
    }
  }

  alignPaymentText() {
    const paymentData = $('.payment-data');
    if (window.innerWidth <= 768) {
      if (paymentData.length > 0) {
        const maxWidth = Math.max(
          ...paymentData.map((key) => paymentData[key].offsetWidth)
        );
        paymentData.width(maxWidth);
      }
    } else {
      paymentData.css('width', 'auto');
    }
  }

  /** 將 input 值 轉換成大寫 */
  changeToUpperCase(event) {
    const input = event.target;
    const start = input.selectionStart;
    const end = input.selectionEnd;
    input.value = input.value.toLocaleUpperCase();
    input.setSelectionRange(start, end);
  }
}
