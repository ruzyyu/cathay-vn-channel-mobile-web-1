import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectTrialCarComponent } from './project-trial-car.component';

describe('ProjectTrialCarComponent', () => {
  let component: ProjectTrialCarComponent;
  let fixture: ComponentFixture<ProjectTrialCarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectTrialCarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectTrialCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
