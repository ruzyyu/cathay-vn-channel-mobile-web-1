import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectTrialCarComponent } from './project-trial-car.component';

const routes: Routes = [{ path: '', component: ProjectTrialCarComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectTrialCarRoutingModule { }
