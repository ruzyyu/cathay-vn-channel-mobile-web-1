import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CargoInsuranceComponent } from './cargo-insurance.component';

describe('CargoInsuranceComponent', () => {
  let component: CargoInsuranceComponent;
  let fixture: ComponentFixture<CargoInsuranceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CargoInsuranceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CargoInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
