import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CargoInsuranceRoutingModule } from './cargo-insurance-routing.module';
import { CargoInsuranceComponent } from './cargo-insurance.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [CargoInsuranceComponent],
  imports: [CommonModule, CargoInsuranceRoutingModule, SharedModule],
})
export class CargoInsuranceModule {}
