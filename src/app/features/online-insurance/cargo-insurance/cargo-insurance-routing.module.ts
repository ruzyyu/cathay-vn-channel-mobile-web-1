import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CargoInsuranceComponent } from './cargo-insurance.component';

const routes: Routes = [{ path: '', component: CargoInsuranceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CargoInsuranceRoutingModule { }
