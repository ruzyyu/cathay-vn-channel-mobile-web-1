import { AfterViewInit, Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/core/services/common.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';

@Component({
  selector: 'app-cargo-insurance',
  templateUrl: './cargo-insurance.component.html',
  styleUrls: ['./cargo-insurance.component.scss'],
})
export class CargoInsuranceComponent implements OnInit, AfterViewInit {
  activeIndex = 0;

  FLang: any;
  FLangType: string;
  faqList: any;
  insuranceRangeList: any;
  items: any;

  constructor(private numberPipe: NumberPipe, private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit(): void {
    this.items = {
      icons: [
        {
          defaultImg: 'assets/img/commodity/ic-claim-cargo-ship.png',
          defaultSrcset: 'assets/img/commodity/ic-claim-cargo-ship@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-cargo-ship-w.png',
          activeSrcset: 'assets/img/commodity/ic-claim-cargo-ship-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-cargo-truck.png',
          defaultSrcset: 'assets/img/commodity/ic-claim-cargo-truck@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-cargo-truck-w.png',
          activeSrcset: 'assets/img/commodity/ic-claim-cargo-truck-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-cargo-damage.png',
          defaultSrcset: 'assets/img/commodity/ic-claim-cargo-damage@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-cargo-damage-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-cargo-damage-w@2x.png 2x',
        },
      ],
      content: [
        {
          title: this.FLang.E04.E0002,
          directions: this.FLang.E04.E0003,
        },
        {
          title: this.FLang.E04.E0004,
          directions: this.FLang.E04.E0005,
        },
        {
          title: this.FLang.E04.E0006,
          directions: this.FLang.E04.E0007,
        },
      ],
    };

    this.insuranceRangeList = [
      {
        title: this.FLang.E04.E0008,
        directions: this.FLang.E04.E0009,
        content: this.FLang.E04.E0010,
        defaultImg: 'assets/img/commodity/cargo-item-01.png',
        defaultSrcset: 'assets/img/commodity/cargo-item-01@2x.png 2x',
        activeImg: 'assets/img/commodity/cargo-item-01-m.png',
        activeSrcset: 'assets/img/commodity/cargo-item-01-m@2x.png 2x',
      },
      {
        title: this.FLang.E04.E0011,
        directions: this.FLang.E04.E0012,
        content: this.FLang.E04.E0013,
        defaultImg: 'assets/img/commodity/cargo-item-02.png',
        defaultSrcset: 'assets/img/commodity/cargo-item-02@2x.png 2x',
        activeImg: 'assets/img/commodity/cargo-item-02-m.png',
        activeSrcset: 'assets/img/commodity/cargo-item-02-m@2x.png 2x',
      },
      {
        title: this.FLang.E04.E0014,
        directions: this.FLang.E04.E0015,
        content: this.FLang.E04.E0016,
        defaultImg: 'assets/img/commodity/cargo-item-03.png',
        defaultSrcset: 'assets/img/commodity/cargo-item-03@2x.png 2x',
        activeImg: 'assets/img/commodity/cargo-item-03-m.png',
        activeSrcset: 'assets/img/commodity/cargo-item-03-m@2x.png 2x',
      },
      {
        title: this.FLang.E04.E0017,
        directions: this.FLang.E04.E0018,
        content: this.FLang.E04.E0019,
        defaultImg: 'assets/img/commodity/cargo-item-04.png',
        defaultSrcset: 'assets/img/commodity/cargo-item-04@2x.png 2x',
        activeImg: 'assets/img/commodity/cargo-item-04-m.png',
        activeSrcset: 'assets/img/commodity/cargo-item-04-m@2x.png 2x',
      },
    ];

    this.faqList = [
      {
        title: this.FLang.E04.E0022,
        content: this.FLang.E04.E0022_.s001,
      },
      {
        title: this.FLang.E04.E0023,
        content: this.FLang.E04.E0023_.s001,
      },
      {
        title: this.FLang.E04.E0024,
        content: this.FLang.E04.E0024_.s001,
      },
    ];
  }

  ngAfterViewInit(): void {
    CommonService.autoScrollToAccordionHeader('#accordionQuestion');
  }

  cssClassNumber(index) {
    let classPipe = this.numberPipe.transform(index + 2);
    if (index === this.activeIndex) {
      classPipe += ' active';
    }
    return classPipe;
  }

  /** Number 轉換 */
  dataFilterNumber(index) {
    return 'select-' + this.numberPipe.transform(index + 2);
  }

  clickStyle(index) {
    this.activeIndex = index;
  }

  /** 圖片位置 */
  switchPosition(index) {
    const style = 'col-sm-12 col-lg-6';
    return index % 2 === 0 ? style : style + ' order-lg-first';
  }

  /** FAQ list */
  titleId(index: number) {
    return 'FaqQuestion' + this.numberPipe.transform(index + 1);
  }
  titleDataTarget(index: number) {
    return '#collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  titleAriaControls(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoId(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoAriaLabelledby(index: number) {
    return 'FaqQuestionQue' + this.numberPipe.transform(index + 1);
  }
  titleContent(item: any) {
    return `
      <div class="tab-title d-flex">
        <h4>${item.title}</h4>
        <i class="icons-ic-faq-arrow-up i-icon i-up"></i>
        <i class="icons-ic-faq-arrow-down i-icon i-down"></i>
      </div>
      `;
  }

  infoContent(item: any) {
    return `
      <div class="que-body d-flex">
        <div class="answer custom_text_align white-space-normal">${item.content}</div>
      </div>
      `;
  }

  /** banner 圖片 語言 switch */
  bannerImg(size: number) {
    const img1920 = 'assets/img/commodity/bn-cargo-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-cargo-1200-x-400';
    const img800 = 'assets/img/commodity/bn-cargo-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '.png';
        case 2:
          return img1200 + '.png';
        case 3:
          return img800 + '.png';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en.png';
        case 2:
          return img1200 + '-en.png';
        case 3:
          return img800 + '-en.png';
      }
    }
  }

  /** banner 圖片 語言 switch */
  bannerImgBig(size: number) {
    const img1920 = 'assets/img/commodity/bn-cargo-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-cargo-1200-x-400';
    const img800 = 'assets/img/commodity/bn-cargo-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '@2x.png 2x';
        case 2:
          return img1200 + '@2x.png 2x';
        case 3:
          return img800 + '@2x.png 2x';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en@2x.png 2x';
        case 2:
          return img1200 + '-en@2x.png 2x';
        case 3:
          return img800 + '-en@2x.png 2x';
      }
    }
  }
}
