import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectTrialTravelRoutingModule } from './project-trial-travel-routing.module';
import { ProjectTrialTravelComponent } from './project-trial-travel.component';
import { InsuredCardComponent } from './components/insured-card/insured-card.component';

@NgModule({
  declarations: [ProjectTrialTravelComponent, InsuredCardComponent],
  imports: [CommonModule, ProjectTrialTravelRoutingModule, SharedModule],
  entryComponents: [InsuredCardComponent],
  exports: [InsuredCardComponent],
})
export class ProjectTrialTravelModule {}
