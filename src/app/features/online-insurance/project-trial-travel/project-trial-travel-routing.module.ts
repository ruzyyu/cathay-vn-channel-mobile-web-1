import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectTrialTravelComponent } from './project-trial-travel.component';

const routes: Routes = [{ path: '', component: ProjectTrialTravelComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectTrialTravelRoutingModule { }
