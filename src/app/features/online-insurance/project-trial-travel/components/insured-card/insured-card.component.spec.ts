import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuredCardComponent } from './insured-card.component';

describe('InsuredCardComponent', () => {
  let component: InsuredCardComponent;
  let fixture: ComponentFixture<InsuredCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsuredCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuredCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
