import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectTrialTravelComponent } from './project-trial-travel.component';

describe('ProjectTrialTravelComponent', () => {
  let component: ProjectTrialTravelComponent;
  let fixture: ComponentFixture<ProjectTrialTravelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectTrialTravelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectTrialTravelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
