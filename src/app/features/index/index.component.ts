import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';
import { CountUp } from 'countup.js';
import { LocalService } from 'src/app/core/services/api/local/local.service';
import { StorageService } from 'src/app/core/services/storage.service';
import { StorageKeys } from 'src/app/core/enums/storage-key.enum';
import {
  IIndexDialogue,
  IIntroduction,
  INumberCount,
} from 'src/app/core/interface';
import { Lang } from 'src/types';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit, AfterContentInit {
  /** 聰明投保，無後顧之憂 項目列表 */
  dialogueList: IIndexDialogue[];

  /** 大家都在保 項目列表 */
  introductionList: IIntroduction[];

  /** 人數統計 項目列表 */
  numberCountList: INumberCount[];

  constructor(
    private router: Router,
    private cds: DatapoolService,
    private numberPipe: NumberPipe,
    private apiLocal: LocalService
  ) {
    /** 語系 */
    this.FLang = this.cds.gLang;

    this.slickItems = [
      {
        img: 'assets/img/ic-index-accident@2x.png',
        url: '/online-insurance/injury-insurance',
        title: this.FLang.A04.L0026_.card1.s001,
        content: this.FLang.A04.L0026_.card1.s002,
      },
      {
        img: 'assets/img/ic-index-house@2x.png',
        url: '/online-insurance/house-insurance',
        title: this.FLang.A04.L0026_.card2.s001,
        content: this.FLang.A04.L0026_.card2.s002,
      },
    ];

    this.dialogueList = [
      {
        time: '.05s',
        content: this.cds.gLang.A04.L0035_.s001,
      },
      { time: '.3s', content: this.FLang.A04.L0035_.s002 },
      { time: '.1s', content: this.FLang.A04.L0035_.s003 },
      { time: '.4s', content: this.FLang.A04.L0035_.s004 },
      { time: '.02s', content: this.FLang.A04.L0035_.s005 },
    ];

    this.introductionList = [
      {
        bTitle: this.FLang.A04.L0024_.travel.s001,
        description: this.FLang.A04.L0024_.travel.s002,
        link: '/online-insurance/travel-insurance',
        content: [
          {
            title: this.FLang.A04.L0024_.travel.s003,
            content: this.FLang.A04.L0024_.travel.s004,
          },
          {
            title: this.FLang.A04.L0024_.travel.s005,
            content: this.FLang.A04.L0024_.travel.s006,
          },
          {
            title: this.FLang.A04.L0024_.travel.s007,
            content: this.FLang.A04.L0024_.travel.s008,
          },
        ],
        defaultImg: 'assets/img/img-index-travel.png',
        defaultSrcset: 'assets/img/img-index-travel@2x.png 2x',
        activeImg: 'assets/img/img-index-travel-m.png',
        activeSrcset: 'assets/img/img-index-travel-m@2x.png 2x',
      },
      {
        bTitle: this.FLang.A04.L0024_.car.s001,
        description: this.FLang.A04.L0024_.car.s002,
        link: '/online-insurance/car-insurance',
        content: [
          {
            title: this.FLang.A04.L0024_.car.s003,
            content: this.FLang.A04.L0024_.car.s004,
          },
          {
            title: this.FLang.A04.L0024_.car.s005,
            content: this.FLang.A04.L0024_.car.s006,
          },
          {
            title: this.FLang.A04.L0024_.car.s007,
            content: this.FLang.A04.L0024_.car.s008,
          },
        ],
        defaultImg: 'assets/img/img-index-car.png',
        defaultSrcset: 'assets/img/img-index-car@2x.png 2x',
        activeImg: 'assets/img/img-index-car-m.png',
        activeSrcset: 'assets/img/img-index-car-m@2x.png 2x',
      },
      {
        bTitle: this.FLang.A04.L0024_.motor.s001,
        description: this.FLang.A04.L0024_.motor.s002,
        link: '/online-insurance/motor-insurance',
        content: [
          {
            title: this.FLang.A04.L0024_.motor.s003,
            content: this.FLang.A04.L0024_.motor.s004,
          },
          {
            title: this.FLang.A04.L0024_.motor.s005,
            content: this.FLang.A04.L0024_.motor.s006,
          },
          {
            title: this.FLang.A04.L0024_.motor.s007,
            content: this.FLang.A04.L0024_.motor.s008,
          },
        ],
        defaultImg: 'assets/img/img-index-motor.png',
        defaultSrcset: 'assets/img/img-index-motor@2x.png 2x',
        activeImg: 'assets/img/img-index-motor-m.png',
        activeSrcset: 'assets/img/img-index-motor-m@2x.png 2x',
      },
    ];

    this.numberCountList = [
      { title: this.FLang.A04.L0039, total: '10' },
      { title: this.FLang.A04.L0040, total: '11000' },
      { title: this.FLang.A04.L0041, total: '860000' },
    ];
  }

  FLang: Lang;

  /** 是否顯示 Header 訊息 */
  hideAlertHeader = false;

  isExecuted = false;

  /** banner圖片 */
  carouselImg: any[] = [];
  /** banner 數量 */
  carouselAmount: number;

  options = {
    startVal: 1,
    decimalPlaces: 0,
    duration: 2.5,
    useEasing: true,
    useGrouping: true,
    smartEasingAmount: 0.5,
    separator: '.', // 由 , 調整成 .
    decimal: '.',
    prefix: '',
    suffix: '',
  };

  slickItems: Array<any>;

  /** 數字計數 動作 */
  countAnimation() {
    const countList = document.querySelectorAll('.count-item');
    countList.forEach((value) => {
      const endVal = parseInt($(value).attr('data-endVal'), 10);
      const countUp = new CountUp(value.id, endVal, this.options);
      countUp.start();
    });
  }

  /** 滾輪滾至定點 呼叫 countAnimation 計算數字 */
  Waypoint() {
    const waypoint = new Waypoint({
      element: document.getElementById('pick-three'),
      handler: (direction) => {
        if (direction === 'down') {
          if (!this.isExecuted) {
            this.countAnimation();
            this.isExecuted = true;
          }
        }
      },
      offset: '50%',
    });
  }

  ngOnInit(): void {
    this.Waypoint();
    new WOW({
      mobile: true,
    }).init();

    this.getBannerList();

    this.hideAlertHeader =
      StorageService.getItem(StorageKeys.cookiesAlertheader) ===
      'cookiesAlertheader';
  }

  /** banner 輪播 API */
  async getBannerList() {
    const bannerList: any = await this.apiLocal.BannerGetList();
    this.carouselImg = bannerList.data;
    this.carouselAmount = bannerList.data.length;
  }

  ngAfterContentInit() {
    // 傳說中失效的slick ....
    $(function () {
      if (!document.querySelector('.variable')) {
        return;
      }
      $('.variable').slick({
        dots: true,
        infinite: false,
        variableWidth: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplaySpeed: 1000,
        responsive: [
          {
            breakpoint: 1950,
            settings: {
              slidesToShow: 2.8,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 1850,
            settings: {
              slidesToShow: 2.7,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 1750,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 2.4,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 1600,
            settings: {
              slidesToShow: 2.2,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 1450,
            settings: {
              slidesToShow: 2.2,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 1400,
            settings: {
              slidesToShow: 2.2,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 1300,
            settings: {
              slidesToShow: 2.2,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 1280,
            settings: {
              slidesToShow: 2.2,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 1130,
            settings: {
              slidesToShow: 2.2,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 1025,
            settings: {
              slidesToShow: 2.2,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 991.98,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
            },
          },
          {
            breakpoint: 767.98,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              centerMode: true,
            },
          },
        ],
      });
    });
  }

  dataSlide(index) {
    return index;
  }

  classActive(index) {
    const style = 'carousel-item';
    return index === 0 ? style + ' active' : style;
  }

  /** dialogue */
  dialogueClassStyle(index) {
    const style = 'ins-item-pos wow fadeIn text-';
    return index === 4
      ? style + 'five'
      : style + this.numberPipe.transform(index + 1);
  }

  classBg(index) {
    const background = 'ins-box bg-';
    switch (index) {
      case 0:
        return background + 'orange';
      case 1:
        return background + 'green-light';
      case 2:
        return background + 'deep-green';
      case 3:
        return background + 'orange';
      case 4:
        return background + 'green-light';
    }
  }

  /** introduction */
  introductionClassStyle(index) {
    const style = 'col-sm-12 col-lg-6 text-center wow';
    return index === 1 ? style + ' bounceInRight' : style + ' bounceInLeft';
  }

  classCss(index) {
    const style = 'col-sm-12 col-lg-6 wow fadeIn';
    return index === 1 ? style + ' order-lg-first' : style;
  }

  idTag(index) {
    return 'target' + this.numberPipe.transform(index + 1);
  }

  goSlickUrl(title: string) {
    switch (title) {
      case this.FLang.A04.L0026_.card1.s001:
        return this.router.navigate(['/online-insurance/house-insurance']);
      case this.FLang.A04.L0026_.card2.s001:
        return this.router.navigate(['/online-insurance/accident-insurance']);
      default:
        return this.router.navigate(['']);
    }
  }
}
