import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
@Component({
  selector: 'app-pagenotfound',
  templateUrl: './pagenotfound.component.html',
  styleUrls: ['./pagenotfound.component.scss'],
})
export class PagenotfoundComponent implements OnInit {
  FLang: any;

  constructor(private go: Router, private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {
    console.log(404);
  }

  gohome() {
    this.go.navigate(['']);
  }
}
