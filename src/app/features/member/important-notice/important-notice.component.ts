import { Component, OnInit } from '@angular/core';
import moment from 'moment';
import { IResponse } from 'src/app/core/interface';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-important-notice',
  templateUrl: './important-notice.component.html',
  styleUrls: ['./important-notice.component.scss'],
})
export class ImportantNoticeComponent implements OnInit {
  // 重要通知資料
  noticeList = [];

  // 重要通知詳細資料
  noticeDetail = {
    content: null,
    create_date: null,
    tag: null,
  };

  // 所有重要通知資料
  totalNotice = [];

  // 剩餘重要通知資料長度
  leftTotalNotice: number;

  FLang: any;

  constructor(
    private cds: DatapoolService,
    private memberService: MemberService
  ) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {}

  async ngAfterViewInit() {
    await this.getNoticeList();
    this.getMore();
  }

  /** 看更多 */
  getMore() {
    const amount = this.totalNotice.length;
    this.noticeList = [
      ...this.noticeList,
      ...this.totalNotice.slice(
        this.noticeList.length,
        this.noticeList.length + 10
      ),
    ];
    this.leftTotalNotice = amount - this.noticeList.length;
  }

  /** 取得重要通知詳細內容
   * id: 重要通知 Id
   */
  getNoticeDetail(id) {
    this.noticeDetail = this.noticeList[id];
  }

  /** 取得重要通知資料 */
  async getNoticeList() {
    if (this.cds.userInfo.seq_no) {
      const param = {
        member_seq_no: this.cds.userInfo.seq_no,
      };
      const rep: IResponse = await this.memberService.NoticeList(param);
      if (rep.status === 200) {
        this.totalNotice =
          rep.data?.map((item) => ({
            ...item,
            create_date: moment(item.create_date).format('HH:mm YYYY/MM/DD'),
          })) || [];
      }
    }
  }
}
