import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPolicyDownloadComponent } from './my-policy-download.component';

describe('MyPolicyDownloadComponent', () => {
  let component: MyPolicyDownloadComponent;
  let fixture: ComponentFixture<MyPolicyDownloadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyPolicyDownloadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPolicyDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
