import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyPolicyDownloadComponent } from './my-policy-download.component';

const routes: Routes = [{ path: '', component: MyPolicyDownloadComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyPolicyDownloadRoutingModule { }
