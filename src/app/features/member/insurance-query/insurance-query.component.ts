import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { IResponse } from 'src/app/core/interface';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { PublicService } from 'src/app/core/services/api/public/public.service';
import moment from 'moment';
import { CommonService } from 'src/app/core/services/common.service';
import { Lang } from 'src/types';

@Component({
  selector: 'app-insurance-query',
  templateUrl: './insurance-query.component.html',
  styleUrls: ['./insurance-query.component.scss'],
})
export class InsuranceQueryComponent implements OnInit {
  group: FormGroup;
  validators: {};
  controls;
  FLang: Lang;
  FLangType: any;

  /** 汽車車種 */
  carItemsList: any;
  /** 機車車種 */
  motorItemsList: any;
  /** checkbox */
  checkboxList: {
    id: string;
    title: string;
  }[];

  /** 查詢結果 */
  result: {
    policy_number: string;
    carType: string;
    license_plate_number: string;
    body_number: string;
    engine_number: string;
    date_start: string;
    date_end: string;
  };

  /** 查詢結果狀態 */
  checkFormatStatus = false;

  /** 查詢結果狀態 */
  checkPolicyNumber = false;

  date: string;
  searchResultShow = 0;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private vi: ValidatorsService,
    private cds: DatapoolService,
    private apiMember: MemberService,
    private api: PublicService
  ) {
    this.FLang = this.cds.gLang;
    this.cds.insuranceQueryList.pol_no = undefined;
  }

  ngOnInit(): void {
    this.FLangType = this.cds.gLangType;
    this.FLang = this.cds.gLang;

    /** checkboxList */
    this.checkboxList = [
      {
        id: '1',
        title: '車牌號碼',
      },
      {
        id: '2',
        title: '車身號碼',
      },
      {
        id: '3',
        title: '引擎號碼',
      },
    ];

    /** 表單驗證項目 */
    this.validators = {
      // 保單號碼
      policy_number: [
        this.cds.insuranceQueryList.pol_no,
        [Validators.required, this.vi.policyNumber],
      ],
      // 種類選擇
      option: [this.cds.insuranceQueryList.option, [Validators.required]],
      // 車牌號碼
      license_plate_number: [
        this.cds.insuranceQueryList.rgst_no,
        [Validators.required, this.vi.vnRgstCarNo],
      ],
      // 車身號碼
      body_number: [this.cds.insuranceQueryList.fram_no, []],
      // 引擎號碼
      engine_number: [this.cds.insuranceQueryList.engn_no, []],
    };

    this.getCarList();
    this.formValidate();
  }

  /** 連結跳轉區 */
  // 登入頁
  loginUrl() {
    this.router.navigate(['member/my-policy']);
  }

  /** checkbox */
  checkboxId(index) {
    return 'option' + (index + 1);
  }

  checkboxValue(index) {
    return index + 1;
  }

  /** 車種資料取得 */
  async getCarList() {
    // 汽車車種
    this.carItemsList = await this.api.gCarList();
    // 機車車種
    this.motorItemsList = await this.api.gMotorList();

    if (!this.carItemsList || !this.motorItemsList) {
      this.carItemsList = this.cds.gPubCommon.carList;
      this.motorItemsList = this.cds.gPubCommon.motoList;
    }
  }

  /** 驗證表單 */
  formValidate() {
    // 表單建立
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });

    // 驗證規則
    this.group = this.fb.group(validator, {
      updateOn: 'blur',
    });

    // 錯誤訊息驗證規則
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });

    this.controls.policy_number.valueChanges.subscribe((value) => {
      this.cds.insuranceQueryList = {
        ...this.cds.insuranceQueryList,
        pol_no: this.controls.policy_number.value,
      };
      this.policyNumberJudge();
    });

    // 表單驗證初始值
    this.controls.option.disable();
    this.controls.license_plate_number.disable();
    this.controls.body_number.disable();
    this.controls.engine_number.disable();
  }

  /** 選擇輸入種類 */
  switchOptionNumber(optionNumber: string) {
    this.controls.option.value = optionNumber;

    // 清空欄位
    this.controls.license_plate_number.reset();
    this.controls.body_number.reset();
    this.controls.engine_number.reset();
  }

  /** 車牌驗證規則轉換 預設：汽車 */
  changeValidators(policyNumber: string) {
    if (policyNumber.slice(4, 6) === 'CA') {
      // 機車
      this.controls.license_plate_number.setValidators([
        Validators.required,
        this.vi.vnRgstNo,
      ]);
    } else {
      // 汽車
      this.controls.license_plate_number.setValidators([
        Validators.required,
        this.vi.vnRgstCarNo,
      ]);
    }

    // 更新驗證規則
    this.controls.license_plate_number.updateValueAndValidity();
  }

  /** 保單號碼驗證 車牌、車身、引擎 要不要 disabled */
  policyNumberJudge() {
    if (this.controls.policy_number.valid) {
      this.controls.option.enable();
      this.controls.license_plate_number.enable();
      this.controls.body_number.enable();
      this.controls.engine_number.enable();
    } else {
      this.controls.option.disable();
      this.controls.license_plate_number.disable();
      this.controls.body_number.disable();
      this.controls.engine_number.disable();

      // 清空
      this.controls.option.value = '1';
      this.controls.license_plate_number.reset();
      this.controls.body_number.reset();
      this.controls.engine_number.reset();
    }
  }

  /** 查詢驗證 有需要在加 */
  searchVerification() {
    return this.group.controls
      ? (this.checkFormatStatus = false)
      : (this.checkFormatStatus = true);
  }

  /** 查詢 button */
  async searchResult() {
    this.searchResultShow = 0;

    // 使用者輸入資料
    const payload = {
      language: this.cds.gLangType,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      token: this.cds.gToken,
      // 保單號碼
      pol_no: this.controls.policy_number.value,
      // 車牌號碼
      rgst_no: this.controls.license_plate_number.value
        ? this.controls.license_plate_number.value
        : '',
      // 車身號碼
      fram_no: this.controls.body_number.value
        ? this.controls.body_number.value
        : '',
      // 引擎號碼
      engn_no: this.controls.engine_number.value
        ? this.controls.engine_number.value
        : '',
    };

    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });

    // 如果沒有驗證過 就停下來
    // if (!this.group.valid) {
    //   return;
    // }
    /** 先+-用 等小魚回來再改成她想要的形狀 */
    if (
      !(
        this.controls.license_plate_number.valid ||
        this.controls.engine_number.valid ||
        this.controls.body_number.valid
      )
    ) {
      return;
    }
    // 呼叫 查詢API
    const rep: IResponse = await this.apiMember.InsuranceQueryApi(payload);
    if (rep.status === 200) {
      if (rep && rep.data && Array.isArray(rep.data) && rep.data.length === 0) {
        $('#Modal-22').modal('show');
        return;
      }
      const data = rep.data[0];
      this.result = {
        policy_number: data.pol_no,
        carType: data.vehc_kd,
        license_plate_number: data.rgst_no || data.fram_no || data.engn_no,
        body_number: data.fram_no,
        engine_number: data.engn_no,
        date_start: data.pol_dt,
        date_end: data.pol_due_dt,
      };

      // 日期組合
      const dateStart = moment(this.result.date_start).format(
        'HH:mm DD/MM/YYYY'
      );
      const dateEnd = moment(this.result.date_end).format('HH:mm DD/MM/YYYY');
      this.date = dateStart + ' - ' + dateEnd;

      // 車種代號名稱轉換 CA → 機車、CB → 汽車
      if (this.result.policy_number.slice(4, 6) === 'CA') {
        // 機車
        const result = this.motorItemsList.filter(
          (item) => item.vehcKdValue === this.result.carType
        );

        // 找不到名稱
        if (result.length === 0) {
          this.result.carType = '';
        } else {
          // 語系名稱
          switch (this.FLangType) {
            case 'zh-TW':
              this.result.carType = result[0].zhName;
              break;
            case 'en-US':
              this.result.carType = result[0].enName;
              break;
            default:
              this.result.carType = result[0].vnName;
          }
        }
      } else {
        // 汽車
        const result = this.carItemsList.filter(
          (item) => item.vechicleKdValue === this.result.carType
        );

        // 找不到名稱
        if (result.length === 0) {
          this.result.carType = '';
        } else {
          // 語系名稱
          switch (this.FLangType) {
            case 'zh-TW':
              this.result.carType = result[0].zhName;
              break;
            case 'en-US':
              this.result.carType = result[0].enName;
              break;
            default:
              this.result.carType = result[0].vnName;
          }
        }
      }
      // 顯示查詢結果
      this.searchResultShow = 1;
      // 滑動到指定位置
      setTimeout(() => {
        this.scrollTo();
      }, 20);
    } else {
      $('#Modal-22-error').modal('show');
      return;
    }
  }

  /** 滑動到指定位置 */
  scrollTo() {
    CommonService.scrollto('result-hr', 50);
  }

  /** 清除字串 & 狀態 */
  doClearInput(item: string) {
    this.group.controls[`${item}`].reset();
  }

  /** 錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          policy_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES020,
          option:
            this.FLang.ER.select.ER001 + this.FLang.ER.select.ER001_.ES006,
          license_plate_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES014,
          body_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES015,
          engine_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES016,
        }[control_key];
      }
      /** 新檢核end */

      return (
        {
          policyNumber:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES008,
          vnRgstNo: this.FLang.ER.ER023,
          vnRgstCarNo: this.FLang.ER.ER023,
        }[type] || this.FLang.ER.ER017
      );
    }
    return this.FLang.ER.ER017;
  }

  /** 檢核內容 */
  statusCheck() {
    const value = this.controls.option.value;
    switch (value) {
      case '1':
        return (
          this.controls.license_plate_number.dirty &&
          this.controls.license_plate_number.touched &&
          this.controls.license_plate_number.invalid
        );

      case '2':
        // 車身號碼
        this.controls.body_number.setValidators([Validators.required]);

        // 更新驗證規則
        this.controls.body_number.updateValueAndValidity();
        return (
          this.controls.body_number.dirty &&
          this.controls.body_number.touched &&
          this.controls.body_number.invalid
        );

      case '3':
        // 車身號碼
        this.controls.engine_number.setValidators([Validators.required]);

        // 更新驗證規則
        this.controls.engine_number.updateValueAndValidity();
        return (
          this.controls.engine_number.dirty &&
          this.controls.engine_number.touched &&
          this.controls.engine_number.invalid
        );
    }
  }

  /** 將 input 值 轉換成大寫 */
  changeToUpperCase(event){
    const input = event.target;
    const start = input.selectionStart;
    const end = input.selectionEnd;
    input.value = input.value.toLocaleUpperCase();
    input.setSelectionRange(start, end);
  }
}
