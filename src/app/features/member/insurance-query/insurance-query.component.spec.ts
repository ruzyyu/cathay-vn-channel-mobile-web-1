import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceQueryComponent } from './insurance-query.component';

describe('InsuranceQueryComponent', () => {
  let component: InsuranceQueryComponent;
  let fixture: ComponentFixture<InsuranceQueryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsuranceQueryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
