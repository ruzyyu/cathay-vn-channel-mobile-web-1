import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InsuranceQueryComponent } from './insurance-query.component';

const routes: Routes = [{ path: '', component: InsuranceQueryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InsuranceQueryRoutingModule { }
