import { IResponse } from './../../../core/interface/index';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { ValidatorsService } from './../../../core/services/utils/validators.service';
import { AlertServiceService } from './../../../core/services/alert/alert-service.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss'],
})
export class VerifyComponent implements OnInit {
  FLang: any;
  FLangType: string;
  constructor(
    private fb: FormBuilder,
    private vi: ValidatorsService,
    private alter: AlertServiceService,
    private router: Router,
    private api: MemberService,
    private cds: DatapoolService
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }
  /** 宣告區Start */
  validators: {};
  group: FormGroup;
  controls;

  /** 建立驗證表單 */
  formValidate() {
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });

    this.group = this.fb.group(validator, {
      updateOn: 'blur',
    });

    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          orderNum:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES012,
        }[control_key];
      }
      /** 新檢核end */

      return (
        {
          LengthInterval:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES006,
          punctReg: this.FLang.ER.ER040,
          required:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES006,
        }[type] || this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES006
      );
    }
    return this.FLang.F08.L0013;
  }

  ngOnInit() {
    const userInfo = this.cds.userInfo;
    if (userInfo.is_validate_policy === 'Y') {
      this.router.navigate(['/member'], { replaceUrl: true });
    }

    /** 表單驗證項目 */
    this.validators = {
      orderNum: [
        '',
        {
          validators: [
            Validators.required,
            this.vi.vnLengthInterval,
            this.vi.vnPunctReg,
          ],
          updateOn: 'change',
        },
      ],
    };

    this.formValidate();
  }

  doNext() {
    // 我的保單
    this.router.navigate(['member/my-policy']);
  }

  doMaintain() {
    $('#Modal-47').modal('hide');
    this.router.navigate(['member/maintain']);
  }

  /** 清除字串 */
  doClearInput(name: string) {
    this.group.controls[`${name}`].setValue('');
  }

  async doSubmit() {
    this.controls.orderNum.markAsDirty();
    this.controls.orderNum.markAsTouched();
    // stop here if form is invalid
    if (this.checkCalc()) {
      return;
    }

    // 開通線下保單
    const payload = {
      language: this.FLangType,
      token: this.cds.gToken,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      pol_no: this.controls.orderNum.value,
      seq_no: this.cds.userInfo.seq_no,
    };
    const rep: IResponse = await this.api.OCOpen(payload);

    // 呼叫API
    if (rep.status === 200 && rep.data.is_validate_policy === 'Y') {
      this.cds.userInfo.is_validate_policy = 'Y';
      this.api.reflashUserInfo('reset');
      this.alter.open({
        type: 'success',
        title: this.FLang.F08.L0014,
        content: this.FLang.F08.L0015,
        callback: this.doNext,
      });
    } else {
      $('#Modal-47').modal('show');
      this.controls.orderNum.setValue('');
      this.controls.orderNum.markAsUntouched();
    }
  }

  checkCalc() {
    const mbol = !this.controls.orderNum.valid;
    return mbol;
  }

  toUppercase(event) {
    const result = event.target.value.toUpperCase();
    this.group.controls.orderNum.setValue(result);
    console.log(this.group.controls.orderNum);
  }
}
