import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Lang } from 'src/types';
import { BaseClassComponent } from 'src/app/shared/components/base-class/base-class.component';
import { Title } from '@angular/platform-browser';
import { IRouteData } from 'src/app/core/interface';
import { CommonService } from 'src/app/core/services/common.service';

@Component({
  selector: 'app-introduction',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.scss'],
})
export class IntroductionComponent
  extends BaseClassComponent
  implements OnInit, AfterViewInit {
  FLang: Lang;
  FFile: string;
  FTitle: string;
  mlang: 'vi-VN' | 'zh-TW' | 'en-US';
  faqList: any[];
  queryParams: {};

  constructor(
    private route: ActivatedRoute,
    private cds: DatapoolService,
    private router: Router,
    metaTitle: Title
  ) {
    super(metaTitle);
    this.FLang = this.cds.gLang;
    this.mlang = this.cds.gLangType;
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((queryParams) => {
      const { data } = this.route.snapshot;
      const title = [
        this.getTitleFromData(data as IRouteData),
        this.getTitleFromData(data.dynamic(this.router.url)),
      ]
        .filter((val) => !!val)
        .join(' - ');

      this.setTitle(title);
      this.FFile = queryParams.intro;
      if (!this.FFile.includes(this.cds.gLangType)) {
        if (this.FFile.indexOf('.json') !== -1) {
          const contract = this.FFile.slice(
            this.FFile.indexOf('.json') - 10,
            this.FFile.indexOf('.json')
          );
          this.FFile = `assets/json/contract/${this.cds.gLangType}/${contract}.json`;
        }
      }
    });
    this.checkRouter();
  }

  ngAfterViewInit(): void {
    // 如果 fragment 為 introductionDescription，滾動至 說明及理賠應備文件
    this.route.fragment.subscribe((fragment) => {
      if (fragment === 'introductionDescription') {
        setTimeout(() => {
          CommonService.scrollto('description', 60);
        }, 300);
      }
    });
  }

  /** 判斷標題 */
  checkRouter() {
    if (!this.FFile) {
      this.router.navigate(['/']);
    }

    // 切割網址
    const splitResult = this.FFile.split('/').pop().split('.')[0];
    switch (splitResult) {
      // 車險
      case 'contract01':
        this.FTitle = this.FLang.F09.L0001_.s005;
        this.faqList = this.getFaqList(1, 1);
        this.queryParams = {
          type: 'question',
          tag: 'personal',
          option: 'car',
          optionDetail: 2,
        };
        break;
      // 旅遊險
      case 'contract02':
        this.FTitle = this.FLang.F09.L0001_.s002;
        this.faqList = this.getFaqList(1, 3);
        this.queryParams = {
          type: 'question',
          tag: 'personal',
          option: 'travel',
          optionDetail: 2,
        };
        break;
      // 傷害險
      case 'contract03':
        this.FTitle = this.FLang.F09.L0001_.s003;
        this.faqList = this.getFaqList(1, 4);
        this.queryParams = {
          type: 'question',
          tag: 'personal',
          option: 'injury',
          optionDetail: 2,
        };
        break;
      // 住火險
      case 'contract04':
        this.FTitle = this.FLang.F09.L0001_.s006;
        this.faqList = this.getFaqList(1, 5);
        this.queryParams = {
          type: 'question',
          tag: 'personal',
          option: 'house',
          optionDetail: 2,
        };
        break;
      // 工程險
      case 'contract05':
        this.FTitle = this.FLang.F09.L0002_.s005;
        this.faqList = this.getFaqList(2, 1);
        this.queryParams = {
          type: 'question',
          tag: 'business',
          option: 'engineering',
          optionDetail: 2,
        };
        break;
      // 責任險
      case 'contract06':
        this.FTitle = this.FLang.F09.L0002_.s006;
        this.faqList = this.getFaqList(2, 2);
        this.queryParams = {
          type: 'question',
          tag: 'business',
          option: 'liability',
          optionDetail: 2,
        };
        break;
      // 財產險
      case 'contract07':
        this.FTitle = this.FLang.F09.L0002_.s003;
        this.faqList = this.getFaqList(2, 3);
        this.queryParams = {
          type: 'question',
          tag: 'business',
          option: 'property',
          optionDetail: 2,
        };
        break;
      // 貨運險
      case 'contract08':
        this.FTitle = this.FLang.F09.L0002_.s007;
        this.faqList = this.getFaqList(2, 4);
        this.queryParams = {
          type: 'question',
          tag: 'business',
          option: 'cargo',
          optionDetail: 2,
        };
        break;
    }
  }

  getFaqList(type, tag) {
    const source = this.FLang.H02;
    const list: any[] = [];

    Object.keys(source).forEach((key) => {
      const [keyTitle, keyTag] = key.split('_');
      if (keyTitle === type.toString() && keyTag === tag.toString()) {
        const data = source[key];

        list.push({
          title: data.q,
          content: data.a,
        });
      }
    });

    return list.slice(0, 3);
  }

  getTitleFromData(item: IRouteData) {
    if (item.hideTitle) {
      return '';
    }

    switch (this.mlang) {
      case 'zh-TW':
        return item.breadcrumb;
      case 'en-US':
        return item.enbreadcrumb;
      default:
        return item.vnbreadcrumb;
    }
  }
}
