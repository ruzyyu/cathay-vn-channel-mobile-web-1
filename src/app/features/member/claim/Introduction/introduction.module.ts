import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardComponent } from 'src/app/features/member/claim/Introduction/components/card/card.component';
import { ServiceCardComponent } from 'src/app/features/member/claim/Introduction/components/service-card/service-card.component';
import { FaqCardComponent } from 'src/app/features/member/claim/Introduction/components/faq-card/faq-card.component';
import { BackgroundComponent } from 'src/app/features/member/claim/Introduction/components/background/background.component';
import { SharedModule } from 'src/app/shared/shared.module';

import { IntroductionRoutingModule } from './introduction-routing.module';
import { IntroductionComponent } from './introduction.component';

@NgModule({
  declarations: [
    IntroductionComponent,
    CardComponent,
    ServiceCardComponent,
    FaqCardComponent,
    BackgroundComponent,
  ],
  imports: [CommonModule, IntroductionRoutingModule, SharedModule],
})
export class IntroductionModule {}
