import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Lang } from 'src/types';
import { CommonService } from 'src/app/core/services/common.service';

@Component({
  selector: 'app-faq-card',
  templateUrl: './faq-card.component.html',
  styleUrls: ['./faq-card.component.scss'],
})
export class FaqCardComponent implements OnInit, AfterViewInit {
  @Input() faqData: any[];
  @Input() queryParams: {};

  FLang: Lang;

  constructor(private numberPipe: NumberPipe, private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }
  ngAfterViewInit(): void {
    CommonService.autoScrollToAccordionHeader('#accordionQuestion');
  }

  titleContent(item: any) {
    return `
    <div class="tab-title d-flex">
      <h4>${item.title}</h4>
      <i class="icons-ic-faq-arrow-up i-icon i-up"></i>
      <i class="icons-ic-faq-arrow-down i-icon i-down"></i>
    </div>
    `;
  }

  infoContent(item: any) {
    return `
    <div class="que-body d-flex">
      <div class="answer custom_text_align white-space-normal">${item.content}</div>
    </div>
    `;
  }

  titleId(index: number) {
    return 'FaqQuestion' + this.numberPipe.transform(index + 1);
  }
  titleDataTarget(index: number) {
    return '#collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  titleAriaControls(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoId(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoAriaLabelledby(index: number) {
    return 'FaqQuestionQue' + this.numberPipe.transform(index + 1);
  }
  ngOnInit(): void {}
}
