import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-background',
  templateUrl: './background.component.html',
  styleUrls: ['./background.component.scss'],
})
export class BackgroundComponent implements OnInit {
  constructor() {}
  backgroundData = {
    story: [
      {
        url: 'assets/img/img-bn-claim.png',
        srcset: 'assets/img/img-bn-claim@2x.png 2x',
      },
      {
        url: 'assets/img/img-bn-claim-m.png',
        srcset: 'assets/img/img-bn-claim-m@2x.png 2x',
      },
    ],
    scrap: [
      {
        url: 'assets/img/img-banner-shape.png',
        srcset: 'assets/img/img-banner-shape@2x.png 2x',
      },
    ],
  };
  rwdHandler(index: number) {
    /** 判斷第一筆為PC，第二筆為mobile */
    if (index === 0) {
      return 'img-fluid d-none d-md-block';
    } else if (index === 1) {
      return 'img-fluid d-md-none';
    } else {
      return 'img-fluid';
    }
  }
  ngOnInit(): void {}
}
