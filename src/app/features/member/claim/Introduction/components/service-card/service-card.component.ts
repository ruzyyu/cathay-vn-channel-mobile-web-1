import { LocalService } from './../../../../../../core/services/api/local/local.service';
import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { CommonService } from 'src/app/core/services/common.service';

@Component({
  selector: 'app-service-card',
  templateUrl: './service-card.component.html',
  styleUrls: ['./service-card.component.scss'],
})
export class ServiceCardComponent implements OnInit, AfterViewInit {
  @Input() xfile: string;

  FFile: String;
  serviceCardData: any;

  upSrc = 'assets/img/element/ic-collapse-up.png';
  upSrcset = 'assets/img/element/ic-collapse-up@2x.png 2x';
  downSrc = 'assets/img/element/ic-collapse-down.png';
  downSrcset = 'assets/img/element/ic-collapse-down@2x.png 2x';
  // 下層
  openSrc = 'assets/img/element/ic-collapse-minus-b.png';
  openSrcset = 'assets/img/element/ic-collapse-minus-b@2x.png 2x';
  closeSrc = 'assets/img/element/ic-collapse-plus-b.png';
  closeSrcset = 'assets/img/element/ic-collapse-plus-b@2x.png 2x';

  constructor(private api: LocalService) {}

  async ngOnInit() {
    this.serviceCardData = await this.api.JSON(this.xfile);
  }

  ngAfterViewInit(): void {
    CommonService.autoScrollToAccordionHeader('#accordionService');
  }

  getCollapseId(id: number, addHash: boolean) {
    return `${addHash ? '#' : ''}detailsPay${id}`;
  }
}
