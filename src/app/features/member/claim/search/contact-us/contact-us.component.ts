import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
})
export class ContactUsComponent implements OnInit, OnChanges {
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }
  FLang: any;
  /** 理賠資訊 */
  @Input() data: any = {};

  FTel: string;

  ngOnInit() {}

  ngOnChanges() {
    // 防止資料清除
    this.data = this.data || { popup_data: {} };

    this.getTel();
  }

  /** 取得電話號碼 */
  getTel() {
    if (this.data) {
      return (this.FTel = `tel:${this.data.popup_data.mobile}`);
    }
  }
}
