import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndemnityInfoComponent } from './indemnity-info.component';

describe('IndemnityInfoComponent', () => {
  let component: IndemnityInfoComponent;
  let fixture: ComponentFixture<IndemnityInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndemnityInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndemnityInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
