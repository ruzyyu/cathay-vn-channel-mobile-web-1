import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-indemnity-info',
  templateUrl: './indemnity-info.component.html',
  styleUrls: ['./indemnity-info.component.scss'],
})
export class IndemnityInfoComponent implements OnInit, OnChanges {
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }
  FLang: any;
  /** 理賠資訊 */
  @Input() data: any = {};

  FTel: string;

  FPayType: string;

  ngOnInit() {}

  ngOnChanges() {
    // 防止資料清除
    this.data = this.data || { popup_data: {} };

    this.getTel();
    this.getFPayType();
  }

  /** 取得電話號碼 */
  getTel() {
    if (this.data) {
      this.FTel = `tel:${this.data.mobile}`;
    }
  }

  /** 下款方式 */
  getFPayType() {
    if (this.data) {
      switch (this.data.popup_data.pay_type) {
        case '2':
          this.FPayType = this.FLang.F05.F0037;
          break;
        case '3':
          this.FPayType = this.FLang.F05.F0036;
          break;
      }
    }
  }
}
