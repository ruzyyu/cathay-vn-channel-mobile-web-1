import { Component, OnInit, HostListener } from '@angular/core';

import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { ClaimService } from 'src/app/core/services/api/claim/claim.service';
import * as moment from 'moment';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  constructor(
    private alert: AlertServiceService,
    private api: ClaimService,
    private cds: DatapoolService
  ) {
    this.FLang = this.cds.gLang;
  }

  FLang: any;

  FTypeList: any[] = [];
  /** 篩選日期範圍 */
  FDateRange: any;

  /** 理賠進度列表 */
  FClaimOriginList: any = [];

  /** 理賠進度日期篩選 */
  FClaimList: any = [];

  /** 查看賠付資訊 */
  FClaimDetail: any;

  // 日期選擇器
  dateList: { id: number; option: string }[];
  // 日期選擇器 選項
  dateId = 0;
  // 日期選擇器 中文
  selectedDateLabel: string;
  // 日期選擇器 天數
  dateNumber: number;
  // 日期選擇器 年月日
  dateString: moment.unitOfTime.DurationConstructor;
  // 類型選擇器 選項
  typeId = '0';
  // 類型選擇器 名稱
  selectedTypeLabel: string;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth < 768) {
      this.addDropdownHeight();
    } else {
      this.removeDropdownHeightEvent();
    }
  }

  async ngOnInit() {
    this.FTypeList = [
      {
        value: '0',
        text: this.FLang.F06.F0002,
        type: 'all',
        icon: 'icons-ic-status-start',
      },
      {
        value: '1',
        text: this.FLang.F06.F0003,
        type: 'send',
        icon: 'icons-ic-status-start',
      },
      {
        value: '2',
        text: this.FLang.F06.F0004,
        type: 'accept',
        icon: 'icons-ic-status-start',
      },
      {
        value: '3',
        text: this.FLang.F06.F0005,
        type: 'return',
        icon: 'icons-ic-status-return',
      },
      {
        value: '4',
        text: this.FLang.F06.F0006,
        type: 'file',
        icon: 'icons-ic-status-notice-o',
      },
      {
        value: '5',
        text: this.FLang.F06.F0007,
        type: 'complete',
        icon: 'icons-ic-status-processing',
      },
      {
        value: '6',
        text: this.FLang.F06.F0008,
        type: 'deductible',
        icon: 'icons-ic-status-deductible',
      },
    ];

    this.dateList = [
      { id: 0, option: this.FLang.F021.L0035 },
      { id: 1, option: this.FLang.F021.L0010 },
      { id: 2, option: this.FLang.F021.L0011 },
      { id: 3, option: this.FLang.F021.L0012 },
      { id: 4, option: this.FLang.F021.L0013 },
    ];

    await this.getClaimList();

    /** jquery 綁定事件 */
    // const mdown = $('.custom-modal-dropdown');
    // const btn = mdown.find('.dropdown-toggle');
    // if ($(btn).attr('aria-expanded') === 'true') {
    //   mdown.parent().addClass('custom-dropdown-mb');
    //   mdown.parents('.modal-body').scrollTop(mdown.offsetTop - 200);
    // }
  }

  ngAfterViewInit(): void {
    if (window.innerWidth < 768) {
      this.addDropdownHeight();
    } else {
      this.removeDropdownHeightEvent();
    }
  }

  addDropdownHeight() {
    if (this.isFClaimListValue()) {
      return;
    }

    $('.custom-modal-dropdown').on('show.bs.dropdown', () => {
      if (!this.isFClaimListValue()) {
        $('.custom-modal-dropdown').parent().addClass('custom-dropdown-mb');
      }
    });

    // 當 dropdown 為 modol 最後一個元素並關閉選單時移除下方高度
    $('.custom-modal-dropdown').on('hide.bs.dropdown', () => {
      $('.custom-modal-dropdown').parent().removeClass('custom-dropdown-mb');
    });
  }

  removeDropdownHeightEvent() {
    $('.custom-modal-dropdown').off('show.bs.dropdown');
    $('.custom-modal-dropdown').off('hide.bs.dropdown');
  }

  isFClaimListValue() {
    return this.FClaimList.length !== 0 ? true : false;
  }

  async getClaimList() {
    const payload = {
      language: this.cds.gLangType,
      client_target:
        this.cds.userInfo.certificate_number_12 ||
        this.cds.userInfo.certificate_number_9,
      token: this.cds.gToken,
      insured_id9: this.cds.userInfo.certificate_number_9,
      insured_id12: this.cds.userInfo.certificate_number_12,
      insured_name: this.cds.userInfo.customer_name.trim(),
    };

    const res = await this.api.getClaimList(payload);

    if (res.data) {
      this.FClaimOriginList = res.data
        .map((item) => {
          const typeItem = this.FTypeList.find(
            (tItem) => item.op_status === tItem.value
          );

          return {
            ...item,
            typeValue: (typeItem && typeItem.type) || '',
            typeText: (typeItem && typeItem.text) || '',
            typeIcon: (typeItem && typeItem.icon) || 'icons-ic-status-start',
          };
        })
        .sort((a, b) => {
          return (
            moment(b.issue_date).valueOf() - moment(a.issue_date).valueOf()
          );
        });
    }

    this.filterList();
  }

  /** 日期表單選擇 */
  chooseDate(dateId: number) {
    this.selectedDateLabel = this.dateList.find(
      (item) => item.id === dateId
    )?.option;

    switch (dateId) {
      case 0:
        this.dateNumber = 0;
        break;
      case 1:
        this.dateNumber = 3;
        this.dateString = 'months';
        break;
      case 2:
        this.dateNumber = 6;
        this.dateString = 'months';
        break;
      case 3:
        this.dateNumber = 1;
        this.dateString = 'years';
        break;
      case 4:
        this.dateNumber = 2;
        this.dateString = 'years';
        break;
    }

    this.filterList();
  }

  /** 類型表單選擇 */
  chooseType(typeId: string) {
    this.typeId = typeId;
    this.selectedTypeLabel = this.FTypeList.find(
      (item) => item.value === typeId
    )?.text;

    this.filterList();
  }

  /** filter Data */
  filterList() {
    const { dateId, typeId } = this;

    // 資料顯示
    this.FClaimList =
      dateId === 0 && typeId === '0'
        ? // 所有類型
          this.FClaimOriginList
        : // 塞選過後
          this.FClaimOriginList.filter(
            (item) =>
              (typeId === '0' || item.op_status === this.typeId) &&
              (dateId === 0 || this.filterDate(item))
          );
  }

  filterDate(item: any) {
    return moment(item.issue_date, 'YYYY-MM-DD').isBetween(
      moment().subtract(this.dateNumber, this.dateString),
      moment()
    );
  }

  /** 顯示理賠明細 */
  showIndemnityInfo(item?) {
    this.FClaimDetail = item;
    $('#indemnityInfo').modal('show');
  }

  /** 顯示理賠專員 */
  showContactUs(item?) {
    this.FClaimDetail = item;
    $('#contactUs').modal('show');
  }

  /** 顯示附件上傳 */
  showUploadFile(item) {
    this.FClaimDetail = item;
    $('#uploadFile').modal('show');
  }

  async uploadFile(files: any[]) {
    const userInfo = this.cds.userInfo;
    const supplementNo = files[0].supplement_no;
    const fileList = files.map((item) => {
      return {
        doc_kind: item.doc_key,
        doc_name: item.file.imgName,
      };
    });

    // API補件 ...
    const res = await this.api.saveSupplementDocs({
      member_seq_no: this.cds.userInfo.seq_no,
      prod_pol_code: this.FClaimDetail.prod_pol_code,
      insured_id9: userInfo.certificate_number_9,
      insured_id12: userInfo.certificate_number_12,
      supplement_no: supplementNo,
      source: 'B2C',
      file_list: fileList,
    });

    if (res.status === 200) {
      $('#uploadFile').modal('hide');

      await this.getClaimList();
    } else {
      setTimeout(() => {
        this.alert.open({
          type: 'warning',
          title: this.FLang.F06.F0043,
          content: this.FLang.F06.F0044,
        });
      });
    }
  }
}
