import { IResponse } from 'src/app/core/interface';
import { ClaimService } from 'src/app/core/services/api/claim/claim.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Router } from '@angular/router';
import { FastpairComponent } from 'src/app/shared/components/fastpair/fastpair.component';
import {
  Component,
  OnInit,
  ViewContainerRef,
  ViewChild,
  ComponentFactoryResolver,
  HostListener
} from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-insurance',
  templateUrl: './insurance.component.html',
  styleUrls: ['./insurance.component.scss'],
})
export class InsuranceComponent implements OnInit {
  @ViewChild('cfastpair', { read: ViewContainerRef })
  cfastpair: ViewContainerRef;
  FLang: any = {};
  FLangType: string;
  FPolicyQty = 0;
  FDate: string;
  FError: string;
  // 時間緩存器
  FTimeIndex: any = -1;
  FTime: string;

  FNormalList: any;
  FBusinessList: any;
  FTimeList: Array<any> = [];
  FReadonly: boolean;
  FDateValue: string;
  // 小網日期不得開啟填寫功能
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }

  constructor(
    private go: Router,
    private cds: DatapoolService,
    private api: ClaimService,
    private componentFactory: ComponentFactoryResolver,
    private datePipe: DatePipe
  ) {
    this.FLangType = this.cds.gLangType;
    // if (this.FLangType === 'zh-TW') {
    //   this.FLangType = 'en-US';
    // }
    this.FLang = this.cds.gLang;
    // "意外保險",1 contract03
    //     "車險",2 contract01
    // "旅遊險",3 contract02
    // "房屋保險",4 contract04
    // "工程保險",5 contract05
    // "運輸保險",6 contract08
    // "責任保險",7 contract06
    // "財產保險",8 contract07

    this.FNormalList = [
      {
        name: this.FLang.F09.L0001_.s001,
        img: 'car',
        file: `assets/json/contract/${this.cds.gLangType}/contract01.json`,
      },
      {
        name: this.FLang.F09.L0001_.s002,
        img: 'travel',
        file: `assets/json/contract/${this.cds.gLangType}/contract02.json`,
      },
      {
        name: this.FLang.F09.L0001_.s003,
        img: 'accident',
        file: `assets/json/contract/${this.cds.gLangType}/contract03.json`,
      },
      {
        name: this.FLang.F09.L0001_.s004,
        img: 'fire',
        file: `assets/json/contract/${this.cds.gLangType}/contract04.json`,
      },
    ];

    this.FBusinessList = [
      {
        name: this.FLang.F09.L0002_.s001,
        img: 'engineering',
        file: `assets/json/contract/${this.cds.gLangType}/contract05.json`,
      },
      {
        name: this.FLang.F09.L0002_.s002,
        img: 'liability',
        file: `assets/json/contract/${this.cds.gLangType}/contract06.json`,
      },
      {
        name: this.FLang.F09.L0002_.s003,
        img: 'property',
        file: `assets/json/contract/${this.cds.gLangType}/contract07.json`,
      },
      {
        name: this.FLang.F09.L0002_.s004,
        img: 'cargo',
        file: `assets/json/contract/${this.cds.gLangType}/contract08.json`,
      },
    ];
  }

  // 動態元件
  comfastpair: any;
  // 是否登入
  FIsLogin: boolean;
  // 日期元件用
  FMaxDate: Date;
  FMinDate: Date;

  // 理賠種類代號對應檔
  FClamKindName: any = [];
  async ngOnInit() {
    this.FIsLogin = !!this.cds.userInfo.seq_no;
    /** 設定日期元件日期限制 */
    const sd = new Date();
    let ed = new Date();
    sd.setFullYear(sd.getFullYear() - 1);
    // ed.setDate(ed.getDate() + 1);
    ed = new Date(ed);
    this.FMaxDate = ed;
    this.FMinDate = sd;
    // const payload = {
    //   language: this.cds.gLangType,
    //   email: this.cds.userInfo.email,
    //   token: this.cds.gToken,
    //   insured_id9: this.cds.userInfo.certificate_number_9,
    //   insured_id12: this.cds.userInfo.certificate_number_12,
    //   insured_date_time: '2020-01-10 15:00:00',
    //   insured_name: this.cds.userInfo.customer_name,
    //   prod_code: 'A',
    // };

    this.FClamKindName = {
      // 班機延誤
      FD: this.cds.gLang.F05.F0004,
      // 改降其他機場
      AC: this.cds.gLang.F05.F0005,
      // 班機取消
      FC: this.cds.gLang.F05.F0006,
    };

    if (this.cds.userInfo.seq_no) {
      const payload = {
        language: this.cds.gLangType,
        client_target:
          this.cds.userInfo.certificate_number_12 ||
          this.cds.userInfo.certificate_number_9,
        token: this.cds.gToken,
        insured_id9: this.cds.userInfo.certificate_number_9,
        insured_id12: this.cds.userInfo.certificate_number_12,
        birth_day: this.cds.userInfo.birthday,
        insured_name: this.cds.userInfo.customer_name.trim(),
        is_link: this.cds.userInfo.is_validate_policy,
      };
      const rep = await this.api.getFastFlyClaimList(payload);
      if (rep.status === 200 && rep.data) {
        this.FPolicyQty = rep.data.length;
      }
    }

    this.FTimeList = [];
    for (let i = 0; i < 24; i++) {
      this.FTimeList.push({
        option: `${i.toString().padStart(2, '0')}:00`,
      });
    }

    // 小網日期不得開啟填寫功能
    if ($(document).innerWidth() > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }

  doNormalInfo(index: number) {
    this.go.navigate(['member/claim/introduction'], {
      queryParams: {
        intro: this.FNormalList[index].file,
      },
    });
  }

  doBusinessInfo(index: number) {
    this.go.navigate(['member/claim/introduction'], {
      queryParams: {
        intro: this.FBusinessList[index].file,
      },
    });
  }

  getDate($event) {
    let date;
    if ($event && $event.indexOf('/') !== -1) {
      date = this.datePipe.transform($event, 'yyyy-MM-dd');
    } else {
      date = $event;
    }
    this.FDateValue = this.datePipe.transform($event, 'dd/MM/yyyy');
    if ($event) {
      this.FDate = date;
      this.FError = '';
    } else {
      this.FDate = '';
    }
  }

  goLogin() {
    this.cds.gPre_url = '/member/claim';
    this.go.navigate(['/login']);
  }

  gofastClaim() {
    this.cds.gClaimDate = `${this.FDate} ${this.FTime || '00:00:00'}`;
    this.go.navigate(['member/claim/fast-claim']);
  }

  async gonormalClaim() {
    if (this.FDate) {
      const payload = {
        language: this.FLangType,
        token: this.cds.gToken,
        client_target:
          this.cds.userInfo.certificate_number_12 ||
          this.cds.userInfo.certificate_number_9,
        insured_id9: this.cds.userInfo.certificate_number_9,
        insured_id12: this.cds.userInfo.certificate_number_12,
        insured_date_time: `${this.FDate} ${this.FTime || '00:00:00'}`,
        insured_name: this.cds.userInfo.customer_name.trim(),
        prod_code: 'A',
      };
      const rep: IResponse = await this.api.GetPolicyList(payload);

      if (
        rep.status === 200 &&
        rep.data &&
        rep.data.policy_list &&
        rep.data.policy_list.claim_item_list.length > 0
      ) {
        this.cds.gClamKindList = rep.data.policy_list.claim_item_list.map(
          (item) => {
            return {
              text:
                this.FLangType === 'vi-VN'
                  ? item.coverage_detail
                  : this.FLangType === 'zh-TW'
                  ? item.coverage_detail_zh
                  : item.coverage_detail_en,
              value: item.clam_kind,
            };
          }
        );
        this.cds.gClaimData = rep.data.policy_list;
        this.cds.gClaimDate = `${this.FDate} ${this.FTime || '00:00:00'}`;
        this.showfastpair();
      } else {
        $('#noPolicy').modal('show');
      }
    } else {
      this.FError = this.cds.gLang.F05.F0058;
    }
    // claim-apply
    // this.go.navigate(['member/claim/fast-claim']);
  }

  showfastpair() {
    const component = this.componentFactory.resolveComponentFactory(
      FastpairComponent
    );
    this.cfastpair.clear();
    this.comfastpair = this.cfastpair.createComponent(component);
    this.cds.gIsFastClaim = false;

    this.comfastpair.instance.modalName = this.FLang.F05.F0040;
    this.comfastpair.instance.FType = 'manual';
    this.comfastpair.instance.data = {
      type: 'edit',
      sdate: this.cds.gClaimData.pol_date,
      edate: this.cds.gClaimData.pol_due_date,
    };
    $('#modelFast').modal('show');

    // 被保人資料填寫中
    this.comfastpair.instance.xAction.subscribe((item) => {
      this.cds.gSelectedFlightDataList = [
        {
          flight_date: item.airplaneDate,
          clam_kind: item.clamKind,
          flight_no: item.airplaneNo,
          clam_kind_name: this.FClamKindName[item.clamKind],
          scheduled_departure_airport: item.airPortS,
          scheduled_arrival_airport: item.airPortE,
        },
      ];
      this.go.navigate(['member/claim/claim-apply']);
      $('#modelFast').modal('hide');
    });
  }

  onTimeClick(index) {
    this.FTimeIndex = index;
    this.FTime = `${index.toString().padStart(2, '0')}:00:00`;
  }
}
