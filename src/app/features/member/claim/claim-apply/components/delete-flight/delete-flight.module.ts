import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteFlightComponent } from './delete-flight.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DeleteFlightComponent]
})
export class DeleteFlightModule { }
