import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClaimApplyComponent } from './claim-apply.component';

const routes: Routes = [{ path: '', component: ClaimApplyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClaimApplyRoutingModule {}
