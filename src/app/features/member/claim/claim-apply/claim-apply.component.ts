import { ValidatorsService } from './../../../../core/services/utils/validators.service';
import { Router } from '@angular/router';
import { AlertServiceService } from '../../../../core/services/alert/alert-service.service';
import {
  FormGroup,
  FormControl,
  AbstractControl,
  Validators,
} from '@angular/forms';
import { FastpairComponent } from '../../../../shared/components/fastpair/fastpair.component';
import { PublicService } from 'src/app/core/services/api/public/public.service';
import { UserInfo } from '../../../../core/interface/index';
import {
  Component,
  OnInit,
  ComponentFactoryResolver,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { ClaimService } from 'src/app/core/services/api/claim/claim.service';
import { CommonService } from 'src/app/core/services/common.service';

@Component({
  selector: 'app-claim-apply',
  templateUrl: './claim-apply.component.html',
  styleUrls: ['./claim-apply.component.scss'],
})
export class ClaimApplyComponent implements OnInit {
  constructor(
    // API Services
    private api: PublicService,
    private claimApi: ClaimService,
    private memberApi: MemberService,
    // Services
    private cds: DatapoolService,
    private alert: AlertServiceService,

    private cpFactory: ComponentFactoryResolver,
    private router: Router,
    private validators: ValidatorsService
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;

    this.FPayTypeList = [
      // 銀行帳戶匯款
      { text: this.FLang.F05.F0036, value: '1' },
      // 銀行臨櫃提款
      { text: this.FLang.F05.F0037, value: '2' },
    ];

    this.FClamKindName = {
      // 班機延誤
      FD: this.cds.gLang.F05.F0004,
      // 改降其他機場
      AC: this.cds.gLang.F05.F0005,
      // 班機取消
      FC: this.cds.gLang.F05.F0006,
    };
  }
  @ViewChild('cFastPairRef', { read: ViewContainerRef })
  cFastPairRef: ViewContainerRef;

  group: FormGroup;

  controls: { [key: string]: AbstractControl };

  errors: any = {};

  FIsFastClaim: boolean;

  FSyncUserinfo = true;

  FLang: any = {};

  FLangType: string;

  FFlightList: any[] = [];

  FFlightData: any;

  FUserInfo: UserInfo = {};

  FClamKindName: any = {};

  FProvinceList: any[] = [];

  FCountyList: any[] = [];

  FBankList: any[] = [];

  FPayTypeList: any[] = [];

  FPayType: any = {};

  FProvince: any = {};

  FCounty: any = {};

  FBank: any = {};

  FRemovedIndex = -1;

  FData: any = {
    file_id_positive: {},
    file_id_negative: {},
    file_bankbook: {},
  };

  cFastPair: any;

  get FGroupValid() {
    return this.fileValidate && this.group && this.group.valid;
  }

  /** 確認已輸入所有檔案 */
  get fileValidate() {
    this.FFlightList.forEach((item) => {
      if (item.clam_kind !== 'FC' && !item.boarding.file) {
        item.boarding.error = true;
        item.boarding.errorMsg = this.FLang.ER.ER017;
      }
      if (!item['e-ticket'].file) {
        item['e-ticket'].error = true;
        item['e-ticket'].errorMsg = this.FLang.ER.ER017;
      }
    });

    if (!this.FData.file_id_positive.file) {
      this.FData.file_id_positive.error = true;
      this.FData.file_id_positive.errorMsg = this.FLang.ER.ER017;
    }

    if (!this.FData.file_id_negative.file) {
      this.FData.file_id_negative.error = true;
      this.FData.file_id_negative.errorMsg = this.FLang.ER.ER017;
    }

    // if (this.FPayType.value === '1') {
    //   if (!this.FData.file_bankbook.file) {
    //     this.FData.file_bankbook.error = true;
    //     this.FData.file_bankbook.errorMsg = this.FLang.ER.ER017;
    //   }
    // } else {
    //   this.FData.file_bankbook = {};
    // }

    return (
      !this.FFlightList.some(
        (item) => item.boarding.error || item['e-ticket'].error
      ) &&
      !this.FData.file_id_positive.error &&
      !this.FData.file_id_negative.error
      // && !this.FData.file_bankbook.error
    );
  }

  async ngOnInit() {
    const isHaveClaim =
      this.cds.gClaimData && Object.keys(this.cds.gClaimData).length > 0;

    if (!this.cds.userInfo || !isHaveClaim) {
      this.router.navigate(['/member/claim'], { replaceUrl: true });
    }

    this.FUserInfo = this.cds.userInfo;

    this.FIsFastClaim = this.cds.gIsFastClaim;

    this.formValidate();

    this.FFlightList = this.cds.gSelectedFlightDataList.map((item) => ({
      ...item,
      boarding: {},
      'e-ticket': {},
    }));

    await this.api.gCountyList();
    await this.api.gBankList();

    this.FProvinceList = this.cds.gPubCommon.provinceList;
    this.FBankList = this.cds.gPubCommon.backList;

    this.FProvince =
      this.FProvinceList.find(
        (item) => item.provinceValue === this.FUserInfo.province
      ) || {};

    this.FCounty =
      (this.FProvince &&
        this.FProvince.countys &&
        this.FProvince.countys.find((item) => item.countyValue)) ||
      {};
  }

  formValidate() {
    this.group = new FormGroup(
      {
        insured_desp: new FormControl('', [
          Validators.required,
          Validators.maxLength(1000),
        ]),
        pay_type: new FormControl('', [Validators.required]),
        bank_no: new FormControl('', [Validators.required]),
        bank_province: new FormControl('', [Validators.required]),
        bank_city: new FormControl('', [Validators.required]),
        bank_branch_name: new FormControl('', [Validators.required]),
        bank_branch_address: new FormControl('', [Validators.required]),
        bank_account_no: new FormControl('', [Validators.required]),
        certificate_number: new FormControl(
          this.FUserInfo.certificate_number_12 ||
            this.FUserInfo.certificate_number_9,
          [this.validators.fontLengthNumber, Validators.required]
        ),
        name: new FormControl(this.FUserInfo.customer_name, [
          Validators.required,
        ]),
        email: new FormControl(this.FUserInfo.email, [
          Validators.required,
          this.validators.email,
        ]),
        email_bk: new FormControl(this.FUserInfo.email_bk, [
          this.validators.email,
        ]),
        sex: new FormControl(this.FUserInfo.sex || '1', [Validators.required]),
        mobile: new FormControl(this.FUserInfo.mobile, [
          Validators.required,
          this.validators.vnPhone,
        ]),
        birthday: new FormControl(this.FUserInfo.birthday, [
          Validators.required,
        ]),
        address: new FormControl(this.FUserInfo.addr, [
          Validators.required,
          Validators.maxLength(240),
        ]),
        province: new FormControl(this.FUserInfo.province || '', [
          Validators.required,
        ]),
        county: new FormControl(this.FUserInfo.county || '', [
          Validators.required,
        ]),
      },
      { updateOn: 'blur' }
    );

    this.controls = this.group.controls;
  }

  doSaveFile(imgFile, accept?: string) {
    return new Promise(async (resolve) => {
      // 直接取代圖片
      const fileSize = imgFile.size;
      const res: any = {};

      const reader = new FileReader();
      if (!accept.includes(imgFile.name.replace(/(.*)(\.\S+)$/, '$2'))) {
        res.image = null;
        res.error = true;
        res.errorMsg = this.FLang.F05.F0038;
        resolve(res);
      } else if (fileSize <= 7340032) {
        const payload = { image: imgFile };
        const rep = await this.memberApi.upload(payload);
        if (rep.status === 200) {
          res.file = {
            imgName: rep.data.filename,
            imgType: imgFile.type,
          };

          reader.readAsDataURL(imgFile);
          reader.onload = () => {
            res.image = reader.result;
            res.imageName = imgFile.name.replace(/(.*)(\.\S+)$/, '$1$2');
            res.error = false;

            resolve(res);
          };
        } else {
          res.image = null;
          res.error = true;
          res.errorMsg = this.FLang.F05.F0038;
          resolve(res);
        }
      } else {
        res.image = null;
        res.error = true;
        res.errorMsg = this.FLang.F05.F0039;
        resolve(res);
      }
    });
  }

  doShowFastPair(options: { data?: any; mode?: 'fast' | 'manual' } = {}) {
    const component = this.cpFactory.resolveComponentFactory(FastpairComponent);
    this.cFastPairRef.clear();

    this.cFastPair = this.cFastPairRef.createComponent(component);
    const instance: FastpairComponent = this.cFastPair.instance;
    instance.modalName = this.FLang.F05.F0040;

    // 手動理賠
    if (options.mode === 'manual' || !this.FIsFastClaim) {
      instance.FType = 'manual';
      instance.data = {
        type: 'edit',
        sdate: this.cds.gClaimData.pol_date,
        edate: this.cds.gClaimData.pol_due_date,
      };

      if (options.data) {
        this.FFlightData = options.data;

        instance.modalName = this.FLang.F05.F0073;

        instance.data = {
          ...instance.data,
          clamKind: options.data.clam_kind,
          airplaneNo: options.data.flight_no,
          airPortS: options.data.scheduled_departure_airport,
          airPortE: options.data.scheduled_arrival_airport,
          airplaneDate: options.data.flight_date,
        };
      }
    } else {
      instance.FType = 'fast';

      instance.FFlightData = this.cds.gFlightDataList.filter(
        (item) => !this.FFlightList.find((fItem) => fItem.id === item.id)
      );

      instance.data = {
        type: 'edit',
        sdate: this.cds.gClaimData.pol_date,
        edate: this.cds.gClaimData.pol_due_date,
      };
    }

    instance.xAction.subscribe((item) => {
      item = item.clamKind
        ? {
            flight_date: item.airplaneDate,
            clam_kind: item.clamKind,
            flight_no: item.airplaneNo,
            clam_kind_name: this.FClamKindName[item.clamKind],
            scheduled_departure_airport: item.airPortS,
            scheduled_arrival_airport: item.airPortE,
          }
        : item;

      if (this.FFlightData) {
        Object.keys(item).forEach((key) => {
          this.FFlightData[key] = item[key];
        });
        this.FFlightData.boarding = {};
        this.FFlightData['e-ticket'] = {};

        this.FFlightData = null;
      } else {
        this.FFlightList.push({ ...item, boarding: {}, 'e-ticket': {} });
      }
    });

    $('#modelFast').modal('show');
  }

  async doSaveClaim() {
    console.log(this.group);
    const certificateNumber =
      this.cds.userInfo.certificate_number_12 ||
      this.cds.userInfo.certificate_number_9;

    const claimKind = this.FFlightList.map((item) => item.clam_kind).join(',');
    const insuredDateTime = this.FIsFastClaim
      ? `${this.FFlightList[0].flight_date} 00:00:00`
      : this.cds.gClaimDate;

    this.FData = { ...this.FData, ...this.group.value };

    const payload: any = {
      language: this.cds.gLangType,
      client_target: certificateNumber,
      token: this.cds.gToken,
      member_seq_no: this.cds.userInfo.seq_no,

      prod_pol_code: this.cds.gClaimData.prod_pol_code,
      pol_no: this.cds.gClaimData.pol_no,

      insured_id: certificateNumber,
      insured_name: this.cds.userInfo.customer_name.trim(),
      insured_date_time: insuredDateTime,
      insured_phone: this.cds.userInfo.mobile,
      insured_email: this.cds.userInfo.email_bk,
      insured_addr: this.cds.userInfo.addr,
      insured_desp: this.FData.insured_desp,

      inform_id: certificateNumber,
      inform_name: this.cds.userInfo.customer_name.trim(),
      inform_rltp: '1', // 固定帶 1，本人
      inform_phone: this.cds.userInfo.mobile,
      inform_addr: this.cds.userInfo.addr,
      inform_email: this.cds.userInfo.email_bk,
      birthday: this.cds.userInfo.birthday,

      pay_type: this.FPayType.value,

      bank_province: this.FData.bank_province,
      bank_city: this.FData.bank_city,
      bank_name: this.FBank.name,
      bank_no: this.FBank.seqno,
      bank_branch_name: this.FData.bank_branch_name,
      bank_branch_address: this.FData.bank_branch_address,
      bank_account_no: this.FData.bank_account_no,
      bank_account_id: certificateNumber,
      bank_account_name: this.cds.userInfo.customer_name.trim(),

      claim_kind: claimKind,
      source: '1', // 固定帶 1，B2C

      file_id_positive: this.FData.file_id_positive.file.imgName,
      file_id_negative: this.FData.file_id_negative.file.imgName,
    };

    // if (this.FPayType.value === '1') {
    //   payload.bank_account_no = this.FData.bank_account_no;
    //   payload.file_bankbook = this.FData.file_bankbook.file.imgName;
    // }

    this.FFlightList.forEach((item, index) => {
      const idx = index + 1;
      payload[`fly${idx}_aply_kind`] = item.clam_kind;
      payload[`fly${idx}_no`] = item.flight_no;
      payload[`fly${idx}_date`] = item.flight_date;
      payload[`fly${idx}_departure_airport`] = item.scheduled_departure_airport;
      payload[`fly${idx}_arrival_airport`] = item.scheduled_arrival_airport;
      payload[`file${idx}_air_tickets`] = item['e-ticket'].file.imgName;

      if (item.boarding.file) {
        payload[`file${idx}_boarding_pass`] = item.boarding.file.imgName;
      }
    });

    const res = await this.claimApi.postSaveClaim(payload);

    if (res.status === 200) {
      this.router.navigate(['/member/claim/claim-result']);
    } else {
      this.alert.open({
        type: 'warning',
        title: this.FLang.F05.F0041,
        content: this.FLang.F05.F0042.replace(/\n/g, '<br />'),
      });
    }
  }

  async onSetFile(event, item) {
    const imgFile = event.target.files[0];

    Object.assign(item, await this.doSaveFile(imgFile, event.target.accept));
  }

  onClearFile(fileType, index = -1) {
    const data =
      index !== -1 ? this.FFlightList[index][fileType] : this.FData[fileType];

    data.file = null;
    data.image = null;
    data.error = false;
  }

  onEditFlightItem(item) {
    this.doShowFastPair({ data: item, mode: 'manual' });
  }

  onRemoveFlightItem(index) {
    $('#deleteFlight').modal('show');

    this.FRemovedIndex = index;
  }

  onSelectProvince(item) {
    this.FProvince = item;
    this.FCounty = {};
  }

  onSelectCounty(item) {
    this.FCounty = item;
  }

  onDeleteFlight() {
    this.FFlightList.splice(this.FRemovedIndex, 1);
    this.FRemovedIndex = -1;
  }

  onSelectPayType(item) {
    this.FPayType = item;
    this.controls.pay_type.setValue(item.value);

    /** 銀行帳戶匯款 */
    if (item.value === '1') {
      this.controls.bank_account_no.enable();

      this.controls.bank_branch_address.setValidators([]);
      // 更新驗證規則
      this.controls.bank_branch_address.updateValueAndValidity();
    } else {
      this.controls.bank_branch_address.setValidators([Validators.required]);
      // 更新驗證規則
      this.controls.bank_branch_address.updateValueAndValidity();
      this.controls.bank_account_no.disable();
    }
  }

  /** 檢核 */
  onCommitApply() {
    /** 解除封印 允許後檢核 */
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });

    /** 檢查判斷是否驗證成功 */
    if (!this.FGroupValid) {
      let resultError;
      for (const key in this.controls) {
        if (!resultError && this.controls[key].invalid) {
          resultError = key;
        }
      }
      console.log(this.group);

      if (this.FFlightList.length > 0) {
        let count = 0;
        let errorStatus = false;
        this.FFlightList.forEach((item) => {
          // 登機證未上傳
          if (item.boarding.error) {
            CommonService.scrollto(
              `file-boarding${count}`,
              $('.header-main').height() + 20
            );
            errorStatus = true;
          }
          // 電子機票未上傳
          if (item['e-ticket'].error) {
            CommonService.scrollto(
              `file-e-ticket${count}`,
              $('.header-main').height() + 20
            );
            errorStatus = true;
          }
          count = count + 1;
        });
        if (errorStatus) {
          return;
        }
      }

      if (resultError) {
        CommonService.scrollto(
          `lyc_${resultError}`,
          $('.header-main').height() + 30
        );
        return;
      }
      if (this.FData.file_id_positive.error) {
        CommonService.scrollto(
          `lyc_file_id_positive`,
          $('.header-main').height() + 30
        );
        return;
      }
      if (this.FData.file_id_negative.error) {
        CommonService.scrollto(
          `lyc_file_id_negative`,
          $('.header-main').height() + 30
        );
        return;
      }
      return;
    }
    if (!this.FGroupValid) {
      return;
    }
    $('#commitHint').modal('show');
  }

  onCommitConfirm() {
    this.group.markAllAsTouched();

    if (this.FGroupValid) {
      this.doSaveClaim();
    } else {
      this.alert.open({
        type: 'warning',
        content: this.FLang.ER.ER012,
      });
    }
  }

  /**
   * @description 子母傳值
   * @params 會員建檔資料
   *
   */
  setUsertoGroup(event) {
    Object.keys(event).forEach((item) => {
      if (this.controls[item]) {
        this.controls[item].setValue(event[item].value);
      }
    });
  }

  controlInvalid(control: AbstractControl) {
    return control.invalid && control.touched;
  }

  getError(control: AbstractControl) {
    const [error] = Object.keys(control.errors);
    return error;
  }

  maxLengthHint(control: AbstractControl) {
    const { requiredLength, actualLength } = control.errors.maxlength;
    return `輸入字數超過限制(${actualLength}/${requiredLength})`;
  }

  /** 選擇銀行 */
  onbankChange(sender) {
    const [, value] = sender.currentTarget.value.split(': ');
    this.FBank = this.FBankList.find((bank) => bank.seqno === value);
  }

  /** 選擇省分 */
  onProvince(sender) {
    this.FProvince = this.FProvinceList.find((province) =>
      sender.currentTarget.value.includes(province.provinceValue)
    );
    this.FCountyList = this.FProvince.countys;
  }

  /** 選擇郡縣 */
  onCounty(sender) {}

  notifyCounty(event) {
    this.controls.county.setValue('');
    this.controls.county.markAsUntouched();
    const index = event.target.selectedIndex - 1;
    this.FCountyList = this.FProvinceList[index].countys;
  }
}
