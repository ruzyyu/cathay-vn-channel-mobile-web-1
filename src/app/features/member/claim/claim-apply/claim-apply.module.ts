import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClaimApplyComponent } from './claim-apply.component';
import { ClaimApplyRoutingModule } from './claim-apply-routing';
import { DeleteFlightComponent } from './components/delete-flight/delete-flight.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CommitHintComponent } from './components/commit-hint/commit-hint.component';

@NgModule({
  declarations: [
    ClaimApplyComponent,
    DeleteFlightComponent,
    CommitHintComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ClaimApplyRoutingModule,
    SharedModule,
  ],
})
export class ClaimApplyModule {}
