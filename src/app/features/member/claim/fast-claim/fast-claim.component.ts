import { UserInfo } from './../../../../core/interface/index';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ClaimService } from 'src/app/core/services/api/claim/claim.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-fast-claim',
  templateUrl: './fast-claim.component.html',
  styleUrls: ['./fast-claim.component.scss'],
})
export class FastClaimComponent implements OnInit {
  constructor(
    private router: Router,
    private api: ClaimService,
    private cds: DatapoolService
  ) {
    this.FLang = this.cds.gLang;
    this.FFlightType = {
      is_delay: { text: this.FLang.F05.F0004, value: 'FD' },
      is_change: { text: this.FLang.F05.F0005, value: 'AC' },
      is_cancel: { text: this.FLang.F05.F0006, value: 'FC' },
    };
  }

  FLang: any = {};

  FActiveIndex = -1;

  FUserInfo: UserInfo;

  /** 理賠清單 */
  FClaimList: any[] = [];

  FFlightData: any[] = [];

  /** 申請種類名稱 */
  FFlightType: any = {};

  async ngOnInit() {
    this.FUserInfo = this.cds.userInfo;
    if (!this.FUserInfo.seq_no) {
      this.router.navigate(['/member/claim']);
    }
    const payload = {
      language: this.cds.gLangType,
      client_target:
        this.cds.userInfo.certificate_number_12 ||
        this.cds.userInfo.certificate_number_9,
      token: this.cds.gToken,
      insured_id9: this.cds.userInfo.certificate_number_9,
      insured_id12: this.cds.userInfo.certificate_number_12,
      birth_day: this.cds.userInfo.birthday,
      insured_name: this.cds.userInfo.customer_name.trim(),
      is_link: this.cds.userInfo.is_validate_policy,
    };

    const mResult = await this.api.getFastFlyClaimList(payload);

    if (mResult.status === 200) {
      this.FClaimList = mResult.data;
    }
  }

  doGoClaimApply(selectedData) {
    this.cds.gIsFastClaim = true;
    this.cds.gFlightDataList = this.FFlightData;
    this.cds.gSelectedFlightDataList = selectedData;

    this.router.navigate(['/member/claim/claim-apply']);
  }

  onSelectClaim(item) {
    let id = 0;
    this.cds.gClaimData = item;
    this.FActiveIndex = this.FClaimList.findIndex((cItem) => item === cItem);

    let flightData = Object.keys(this.FFlightType).reduce((list, key) => {
      item.flight_data.forEach((fItem) => {
        if (fItem[key] === 'Y') {
          list.push({
            ...fItem,
            id: id++,
            clam_kind: this.FFlightType[key].value,
            clam_kind_name: this.FFlightType[key].text,
          });
        }
      });
      return list;
    }, []);

    // 無法理賠所有項目
    if (flightData.length === 0) {
      flightData = item.flight_data.map((item, index) => {
        return {
          ...item,
          id: index,
          clam_kind: this.FFlightType.is_delay.value,
          clam_kind_name: this.FFlightType.is_delay.text,
        };
      });
      this.cds.gNoClaims = true;
    } else {
      this.cds.gNoClaims = false;
    }

    this.FFlightData = flightData;
  }

  onSelectClaimItem(item) {
    this.doGoClaimApply(item);
  }

  onNextStep() {
    if (this.FFlightData.length > 2) {
      $('#selectFlight').modal('show');
    } else {
      this.doGoClaimApply(this.FFlightData);
    }
  }
}
