import { SelectFlightComponent } from './components/select-flight/select-flight.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FastClaimRoutingModule } from './fast-claim-routing.module';
import { FastClaimComponent } from './fast-claim.component';

@NgModule({
  declarations: [FastClaimComponent, SelectFlightComponent],
  imports: [CommonModule, FastClaimRoutingModule, SharedModule],
})
export class FastClaimModule {}
