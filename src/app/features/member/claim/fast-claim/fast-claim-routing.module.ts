import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FastClaimComponent } from './fast-claim.component';

const routes: Routes = [{ path: '', component: FastClaimComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FastClaimRoutingModule {}
