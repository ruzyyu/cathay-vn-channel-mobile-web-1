import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
} from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-select-flight',
  templateUrl: './select-flight.component.html',
  styleUrls: ['./select-flight.component.scss'],
})
export class SelectFlightComponent implements OnChanges, OnDestroy {
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }

  @Input() flightData: any[] = [];

  @Output() selected = new EventEmitter();

  FLang: any = {};

  /** 班機資訊 */
  FFlightData: any[] = [];

  /** 選擇的班機資訊 */
  FSelectedFlightData: any[] = [];

  /** 最大選擇上限 */
  FMaxSelect = 2;

  ngOnChanges() {
    this.FFlightData = this.flightData;
    this.FSelectedFlightData = [];
  }

  ngOnDestroy() {
    $('#selectFlight').modal('hide');
  }

  onFillInClaim() {
    this.selected.emit(this.FSelectedFlightData);

    $('#selectFlight').modal('hide');
  }

  onSelectFlight(item) {
    if (this.FSelectedFlightData.includes(item)) {
      this.FSelectedFlightData.splice(
        this.FSelectedFlightData.indexOf(item),
        1
      );
    } else if (this.FSelectedFlightData.length < this.FMaxSelect) {
      this.FSelectedFlightData.push(item);
    }
  }
}
