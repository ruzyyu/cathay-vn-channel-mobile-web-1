import { UserInfo } from './../../../../core/interface/index';
import { Component, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-claim-result',
  templateUrl: './claim-result.component.html',
  styleUrls: ['./claim-result.component.scss'],
})
export class ClaimResultComponent implements OnInit {
  constructor(private cds: DatapoolService, private router: Router) {
    this.FLang = this.cds.gLang;
  }

  FLang: any = {};

  FUserInfo: UserInfo;

  ngOnInit() {
    this.FUserInfo = this.cds.userInfo;
    if (!this.FUserInfo.seq_no) {
      this.router.navigate(['/member/claim'], { replaceUrl: true });
    }
  }
}
