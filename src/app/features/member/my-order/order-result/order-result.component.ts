import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Lang } from 'src/types';

@Component({
  selector: 'app-order-result',
  templateUrl: './order-result.component.html',
  styleUrls: ['./order-result.component.scss'],
})
export class OrderResultComponent implements OnInit {
  FLang: Lang;

  constructor(private cds: DatapoolService, private route: ActivatedRoute) {
    this.userInfo = this.cds.userInfo;
    this.FLang = this.cds.gLang;
  }
  userInfo: any = {};

  FType: string;

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.FType = params.xType;
    });
  }
}
