import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paymentIcon',
})
export class PaymentIconPipe implements PipeTransform {
  transform(alias: string, type?: 'w'): any {
    switch (type) {
      case 'w':
        return `icons-ic-pay-${alias}-w i-icon pay-w-icon`;
      default:
        return `icons-ic-pay-${alias} i-icon pay-icon`;
    }
  }
}
