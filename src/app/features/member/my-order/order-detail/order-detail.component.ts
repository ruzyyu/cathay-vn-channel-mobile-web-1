import { OnlinecalcService } from './../../../../core/services/onlinecalc/onlinecalc.service';
import { CommonService } from 'src/app/core/services/common.service';
import { PaymentService } from './../../../../core/services/payment/payment.service';
import { PublicService } from './../../../../core/services/api/public/public.service';
import { AlertServiceService } from './../../../../core/services/alert/alert-service.service';
import { Subscription } from 'rxjs';
import { IConfirm, IResponse } from './../../../../core/interface/index';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { OrderService } from './../../../../core/services/api/order/order.service';
import { Router, ActivatedRoute } from '@angular/router';
import {
  Component,
  HostListener,
  OnInit,
  AfterViewInit,
  NgZone,
} from '@angular/core';
import { InsuranceService } from 'src/app/core/services/api/Insurance/insurance.service';
import * as moment from 'moment';
import { Lang } from 'src/types';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss'],
})
export class OrderDetailComponent implements OnInit, AfterViewInit {
  constructor(
    private router: Router,
    private api: OrderService,
    private route: ActivatedRoute,
    private cds: DatapoolService,
    private apiInsurance: InsuranceService,
    private payment: PaymentService,
    private alert: AlertServiceService,
    private apiPublic: PublicService,
    private calcsv: OnlinecalcService,
    private zone: NgZone
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
    this.FMotorPluNoList = this.cds.gMotorPluNoList;
  }
  FLang: Lang;
  FLangType: string;
  FMotorPluNoList: any = [];

  FSelectPayType: any;
  FPayType: Array<any>;
  FReadme = false;
  FOrder_no: number;
  FType: string;
  FAply_no: string;
  FPayNoError: string;
  FAgreeError: string;

  FRep: any = {};
  FOrder_data: any = {};
  FPolicyholder_data: any = {};
  FVehicle_reginfo: any = {};
  FInsr_data: any = [];
  FFlight_data: any = [];

  paymentNumber = -1;
  /** 付款二次確認窗 */
  FConfirm: IConfirm;
  /** 付款緩存單號 */
  FOrderId: string;

  /** 導轉到網路投保付款頁 */
  FsubscribePayAction: Subscription;

  @HostListener('window:resize')
  onResize() {
    this.alignPaymentText();
  }

  async ngOnInit() {
    // 汽車車種
    await this.apiPublic.gCarList();
    // 汽車use類
    await this.apiPublic.gCarTypeList();
    // 機車清單
    await this.apiPublic.gMotorList();
    this.FPayType = [
      {
        id: '1',
        name: this.FLang.F012.L0025,
        selected: false,
        alias: 'creditcard',
        type: 'InternationalCard',
      },
      {
        id: '2',
        name: this.FLang.F012.L0026,
        selected: false,
        alias: 'debitcard',
        type: 'DomesticBank',
      },
      {
        id: '3',
        name: this.FLang.F012.L0027,
        selected: false,
        alias: 'wallet',
        type: 'VTCPay',
      },
    ];
    this.FSelectPayType = this.FPayType[0];

    this.route.queryParams.subscribe(async (queryParams) => {
      this.FOrder_no = queryParams.order_no;
      this.FAply_no = queryParams.aply_no;
      this.FType = queryParams.xtype;

      const apiType = this.FType === '0' ? 'TRAVEL' : 'CAR';

      // 呼叫清單
      const payload = {
        order_no: Number(this.FOrder_no),
        aply_no: this.FAply_no,
      };

      const rep = await this.api.getOrderDetail(payload, apiType);
      this.FOrder_data = rep.data.order_data;
      this.FPolicyholder_data = rep.data.policyholder_data;
      this.FVehicle_reginfo = rep.data.vehicle_reginfo;
      this.FInsr_data = rep.data.insr_data;
      this.FFlight_data = rep.data.flight_data;
    });

    CommonService.autoScrollToAccordionHeader('#accordionService');
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(() => {
      setTimeout(this.alignPaymentText, 500);
    });
  }

  /** 選擇付款方式 */
  doTypeselect(index) {
    this.FPayType.forEach((item) => {
      item.selected = false;
    });

    this.FSelectPayType = this.FPayType[index];
    this.FPayType[index].selected = true;

    this.paymentNumber = index;
    this.FPayNoError = '';
  }

  doTipError() {
    this.FPayNoError = this.paymentNumber < 0 ? this.FLang.PH.PH044 : '';
    this.FAgreeError = !this.FReadme ? this.FLang.PH.PH045 : '';
    return this.paymentNumber < 0 || !this.FReadme;
  }

  /** 訂單儲存 */
  async doInsuredSave() {
    if (this.doTipError()) {
      return;
    }

    const mOrder = this.FOrder_data;
    const orderId = mOrder.aply_no + moment().format('yyMMDDHHmmss');

    // 確認付款(這支其實是儲存訂單)
    const payload: any = {
      member_seq_no: this.cds.userInfo.seq_no,
      pay_refno: orderId,
      ...this.FOrder_data,
      ...this.FPolicyholder_data,
      ...this.FVehicle_reginfo,
    };
    const apiType = this.FType === '0' ? 'TRAVEL' : 'CAR';
    if (this.FType === '0') {
      payload.insr_data = this.FInsr_data;
      payload.flight_data = this.FFlight_data;
    }
    const rep: IResponse = await this.api.saveOrder(payload, apiType);

    if (rep.status === 200) {
      this.doInsuredCheck({ orderId });
    } else {
      this.alert.open({
        type: 'warning',
        title: this.cds.gLang.payment.L0003,
        content: rep.error.data,
      });
    }
  }

  /** 訂單檢核 */
  async doInsuredCheck({ orderId }) {
    const payload: any = {
      language: this.cds.gLangType,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      token: this.cds.gToken,
      member_seq_no: this.cds.userInfo.seq_no,
      pay_refno: orderId,
      ...this.FOrder_data,
      ...this.FPolicyholder_data,
      ...this.FVehicle_reginfo,
    };

    let rep;

    if (this.FType === '0') {
      payload.insr_data = this.FInsr_data;
      payload.flight_data = this.FFlight_data;

      rep = await this.apiInsurance.checkTravelOrderOK(payload);
    } else {
      payload.car_pwr_kd = this.FOrder_data.car_pwr_kd;
      payload.max_capl = this.FOrder_data.max_capl;

      rep = await this.apiInsurance.checkOrderOK(payload);
    }

    if (rep.status === 200) {
      if (CommonService.isIOS()) {
        this.FConfirm = this.calcsv.openPayConfirm();
        this.FOrderId = orderId;
        setTimeout(() => {
          $('#lyConfirm').modal('show');
        }, 300);
      } else {
        this.goOnlineInsured({ orderId });
      }
    } else {
      this.alert.open({
        type: 'warning',
        title: this.cds.gLang.popError.P0001,
        content: rep.error.data,
      });
    }
  }
  async goOnlineInsured({ orderId }) {
    const mOrder = this.FOrder_data;

    if (!this.FsubscribePayAction || this.FsubscribePayAction.closed) {
      this.FsubscribePayAction = this.payment.getPayObserve().subscribe(
        (params) => {
          switch (params.status) {
            case '1':
              this.doInsuredChangeStatus(params);
              break;
            default:
              this.doInsuredChangeStatus(params);
              $('#paymentFailModal').modal('show');
              break;
          }
        },
        (error) => {
          switch (error.status) {
            case -9:
              $('#paymentFailModal').modal('show');
              break;
          }
          console.log(error);
        }
      );
    }

    this.payment.doPayment(
      mOrder.tot_prem_with_vat,
      orderId,
      this.FSelectPayType.type
    );
  }

  ActionYesNo(xresult) {
    if (xresult) {
      this.goOnlineInsured({ orderId: this.FOrderId });
    }
    $('#lyConfirm').modal('hide');
  }

  /** 訂單修改狀態 */
  async doInsuredChangeStatus(param) {
    const {
      order_no,
      aply_no,
      prod_pol_code,
      tot_prem_with_vat,
    } = this.FOrder_data;
    const payload = {
      member_seq_no: this.cds.userInfo.seq_no,
      order_status: param.status === '1' ? '4' : '5',
      order_no,
      aply_no,
      prod_pol_code,
      pay_data: {
        pay_type: this.FSelectPayType.id,
        pay_amt: tot_prem_with_vat,
        pay_date: moment().format('YYYY-MM-DD HH:mm:ss'),
        pay_status: param.status,
        pay_refno: param.reference_number,
        remark: JSON.stringify(param),
      },
    };
    const rep: any = await this.api.editOrderState(payload);

    if (rep.status === 200 && param.status === '1') {
      this.router.navigate(['/member/my-order/result'], {
        replaceUrl: true,
        queryParams: {
          xType: this.FType,
        },
      });
    } else {
      // this.alert.open({
      //   type: 'warning',
      //   title: '付款錯誤',
      //   content: rep.message,
      // });
    }
  }

  /** 確定狀態檢核 */
  checkReadme() {
    this.FReadme = !this.FReadme;
  }

  /** 取得保單總類 */
  getInsureType() {
    const type = this.FOrder_data.aply_no;
    if (!type) {
      return '';
    } else if (/^[A-Z]CA/.exec(type)) {
      return this.FLang.F012.L0031;
    } else if (/^[A-Z]CB/.exec(type)) {
      return this.FLang.F012.L0032;
    } else if (/^[A-Z]T/.exec(type)) {
      return this.FLang.F012.L0033;
    }
  }

  /** 取得被保人姓名項目文字 */
  getInsuredId(index) {
    return `${this.FLang.F012.L0038}${index + 1}`;
  }

  /** 取得航班編號種類 */
  getFlightNoType(type) {
    switch (type) {
      case 0:
        return this.FLang.F012.L0033_.s005;
      case 1:
        return this.FLang.F012.L0033_.s006;
      default:
        return this.FLang.F012.L0033_.s007;
    }
  }

  /** 顯示越南格式總保費 */
  getTotPremWithVat() {
    return (
      this.FOrder_data.tot_prem_with_vat &&
      this.FOrder_data.tot_prem_with_vat.toLocaleString('vi-VN')
    );
  }

  onPaymentFailSubmit(event) {
    console.log(event);
  }

  get displayckd() {
    const ckd = ['21', '31'];
    return !ckd.includes(this.FOrder_data.vehc_kd);
  }

  alignPaymentText() {
    const paymentData = $('.payment-data');
    if (window.innerWidth <= 768) {
      if (paymentData.length > 0) {
        const maxWidth = Math.max(
          ...paymentData.map((key) => paymentData[key].offsetWidth)
        );
        paymentData.width(maxWidth);
      }
    } else {
      paymentData.css('width', 'auto');
    }
  }

  IsCarAddBuy() {
    if (this.FType !== '1') {
      return false;
    }
    return this.FOrder_data && this.FOrder_data.driver_cnt === 1 ? true : false;
  }

  IsMotorAddBuy() {
    if (this.FType !== '2') {
      return false;
    }
    return this.FOrder_data && this.FOrder_data.passenger_cnt === 2
      ? true
      : false;
  }

  /** this.FType === 0 旅遊 1 汽車 2 機車 */
  checkTermsUrl() {
    if (this.FType === '1') {
      return 'assets/pdf/Bao hiem bat buoc o to.pdf';
    }
    if (this.FType === '2') {
      return 'assets/pdf/Bao hiem bat buoc xe may.pdf';
    }
    if (this.FType === '0') {
      if (!(this.FOrder_data && this.FOrder_data.aply_no)) {
        return;
      }
      if (this.FOrder_data.aply_no.indexOf('T1') !== -1) {
        return 'assets/pdf/T1.pdf';
      }
      if (this.FOrder_data.aply_no.indexOf('T3') !== -1) {
        return 'assets/pdf/T3.pdf';
      }
      if (this.FOrder_data.aply_no.indexOf('T4') !== -1) {
        return 'assets/pdf/T4.pdf';
      }
    }
  }
}
