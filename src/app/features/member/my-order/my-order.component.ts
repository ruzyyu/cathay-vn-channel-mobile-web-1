import { IResponse } from 'src/app/core/interface';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { OrderService } from './../../../core/services/api/order/order.service';
import {
  IConfirm,
  IOrdertravel,
  IOrdercar,
} from './../../../core/interface/index';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/core/services/common.service';
declare var $: any;
@Component({
  selector: 'app-my-order',
  templateUrl: './my-order.component.html',
  styleUrls: ['./my-order.component.scss'],
})
export class MyOrderComponent implements OnInit {
  FLang: any;
  FLangType: string;
  constructor(
    private router: Router,
    private api: OrderService,
    private cds: DatapoolService,
    private route: ActivatedRoute
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }
  arrInterval: Array<string>;
  arrAffect: Array<string>;
  arrType: Array<any>;

  FTravellist: Array<IOrdertravel>;
  FCarorderlist: Array<IOrdercar>;
  FScooterorderlist: Array<IOrdercar>;

  FInterval: string;
  FAffect: string;
  FType: string;
  FConfirm: IConfirm;
  FTitleList: Array<string>;
  FTypeIndex = 0;
  FOrderlist: any;

  FNowTraveIitem: IOrdertravel;
  FNowCarorderitem: IOrdercar;
  FNowScooterorderitem: IOrdercar;
  FItem: any;
  // CF02001機車、CF02002汽車

  /** 控制沒有保單 */
  FOnOrder = false;

  // 控制變數
  FControlDropdown = false;
  async ngOnInit() {
    // this.FType = this.arrType[0];
    // 呼叫API 目前未知先使用假的Json即可
    await this.init();

    this.checkNoOrder();
  }

  async init() {
    console.log(this.FTravellist);

    const payload = {
      member_seq_no: this.cds.userInfo.seq_no,
      order_status: ['1', '2', '3', '5', '4', '11'],
    };
    const mResult = await this.api.getOrderList(payload);

    /**
     * 狀態1、2、3、5 尚未付款
     * 狀態4、11  付款完成
     */
    if (mResult.status === 200) {
      this.FTravellist = mResult.data.travel_order_List;
      this.FCarorderlist = mResult.data.car_order_list;
      this.FScooterorderlist = mResult.data.scooter_order_list;
    }
    this.route.queryParams.subscribe((queryParams) => {
      if (queryParams && queryParams.aply_no) {
        setTimeout(() => {
          CommonService.scrollto(queryParams.aply_no, 50);
        }, 100);
      }
    });
  }

  setInterval(type) {
    this.FInterval = type;
  }

  setAffect(type) {
    this.FAffect = type;
  }

  doSearch() {}

  dodropdown(item) {
    item.checked = !item.checked || false;

    // this.FControlDropdown = !this.FControlDropdown;
  }
  doDelorder(item, type) {
    this.FNowTraveIitem = null;
    this.FNowCarorderitem = null;
    this.FNowScooterorderitem = null;

    if (type === 1) {
      this.FNowTraveIitem = item;
    }
    if (type === 2) {
      this.FNowCarorderitem = item;
    }
    if (type === 3) {
      this.FNowScooterorderitem = item;
    }

    this.FItem = {
      type,
      item,
    };

    this.FConfirm = {
      title: this.FLang.F011.L0017,
      content: `${this.FLang.F011.L0007} ${item.aply_no}`,
      type: 'exclamation',
      btnConfirm: this.cds.gLang.F05.F0044,
      btnCancel: this.cds.gLang.F05.F0045,
    };

    $('#lyConfirm').modal('show');
  }

  /** 塞選清單 */
  doFilterdata(e) {
    // CF02001機車、CF02002汽車
    this.FTypeIndex = e;
    this.checkNoOrder(e);
  }

  /** 檢查沒有保單訊息 */
  checkNoOrder(index: number = 0) {
    if (
      !Array.isArray(this.FTravellist) ||
      !Array.isArray(this.FCarorderlist) ||
      !Array.isArray(this.FScooterorderlist)
    ) {
      return;
    }

    if (index === 0) {
      this.FOnOrder = true;
      if (
        this.FTravellist.length === 0 &&
        this.FCarorderlist.length === 0 &&
        this.FScooterorderlist.length === 0
      ) {
        this.FOnOrder = false;
      }
    }
    if (index === 1) {
      this.FOnOrder = true;
      if (this.FCarorderlist.length === 0) {
        this.FOnOrder = false;
      }
    }
    if (index === 2) {
      this.FOnOrder = true;
      if (this.FScooterorderlist.length === 0) {
        this.FOnOrder = false;
      }
    }
    if (index === 3) {
      this.FOnOrder = true;
      if (this.FTravellist.length === 0) {
        this.FOnOrder = false;
      }
    }
  }

  /** 執行訂單刪除 */
  async ActionYesNo(type: boolean) {
    if (type) {
      $('#lyConfirm').modal('hide');
      const { order_no, aply_no, prod_pol_code } = this.FItem.item;
      const payload = {
        member_seq_no: this.cds.userInfo.seq_no,
        pay_data: null,
        order_status: '9',
        order_no,
        aply_no,
        prod_pol_code,
      };

      const rep: IResponse = await this.api.editOrderState(payload);

      if (rep.status === 200) {
        this.init();
      }
      // 假裝刪除XD
    } else {
      $('#lyConfirm').modal('hide');
    }
  }

  goVerify() {
    this.router.navigate(['member/verify']);
  }

  /** 付款 */
  goPayment(item, type) {
    // item 是單筆的資訊
    this.router.navigate(['member/my-order/detail'], {
      queryParams: {
        order_no: item.order_no,
        aply_no: item.aply_no,
        xtype: type,
      },
    });
  }

  /**
   * @description 資料編輯 導轉到網路投保付款頁
   * @param item 是 訂單詳細資料
   * @param xtype 0 旅遊 1汽車 2機車
   */
  async goOnlineInsured(item, xtype) {
    const { order_no, aply_no } = item;
    const payload = {
      order_no,
      aply_no,
    };

    const apiType = xtype === 0 ? 'TRAVEL' : 'CAR';
    const rep: IResponse = await this.api.getOrderCalcDetail(payload, apiType);
    // 將試算資料放到datapool
    if (rep.status === 200) {
      // ** 此流程不可能會有緩存資料 */
      if (this.cds.gTCalc_to_save) {
        this.cds.gTCalc_to_save.type = 'none';
      }
      // 僅續保會是ture ..
      this.cds.gTrafficType = false;
      apiType === 'TRAVEL'
        ? (this.cds.gTrafficTravelInsurance = rep.data)
        : (this.cds.gTrafficicinsurance = rep.data);

      xtype === 0
        ? this.router.navigate([
            'online-insurance/project-trial-travel/calcAmt',
          ])
        : xtype === 1
        ? this.router.navigate(['online-insurance/project-trial-car/calcAmt'])
        : this.router.navigate([
            'online-insurance/project-trial-motor/calcAmt',
          ]);
    } else {
    }
  }

  /**
   * @description 付款狀態判斷
   * 狀態1、2、3、5 尚未付款
   * 狀態4、11  付款完成
   */
  onCheckState(xstate) {
    const statelist = ['1', '2', '3', '5'];
    return statelist.includes(xstate);
  }
}
