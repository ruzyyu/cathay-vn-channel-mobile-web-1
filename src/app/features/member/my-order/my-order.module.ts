import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyOrderRoutingModule } from './my-order-routing.module';
import { MyOrderComponent } from './my-order.component';
import { SharedModule } from 'src/app/shared/shared.module';

import zh from '@angular/common/locales/zh';
import vi from '@angular/common/locales/vi';
import en from '@angular/common/locales/en';
import {registerLocaleData} from '@angular/common';
registerLocaleData(zh);
registerLocaleData(vi);
registerLocaleData(en);


@NgModule({
  declarations: [MyOrderComponent],
  imports: [CommonModule, MyOrderRoutingModule, SharedModule],
})
export class MyOrderModule {}
