import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyPolicyRoutingModule } from './my-policy-routing.module';
import { MyPolicyComponent } from './my-policy.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [MyPolicyComponent],
  imports: [CommonModule, MyPolicyRoutingModule, SharedModule],
})
export class MyPolicyModule {}
