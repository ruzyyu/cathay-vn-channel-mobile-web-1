import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import {
  ITraveInsurance,
  IMotoInsurance,
  ICarInsurance,
  IResponse,
} from 'src/app/core/interface/index';
import { PolicyService } from 'src/app/core/services/api/policy/policy.service';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { CommonService } from 'src/app/core/services/common.service';
import { Lang } from 'src/types';

@Component({
  selector: 'app-my-policy',
  templateUrl: './my-policy.component.html',
  styleUrls: ['./my-policy.component.scss'],
})
export class MyPolicyComponent implements OnInit, AfterViewInit {
  FLang: Lang;
  FLangType: string;

  constructor(
    private router: Router,
    private cds: DatapoolService,
    private api: PolicyService,
    private route: ActivatedRoute
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }

  arrInterval: Array<string>;
  arrType: Array<any>;

  policyList: IResponse;
  traveList: ITraveInsurance;
  motoList: IMotoInsurance;
  carList: ICarInsurance;

  // FCarCards: Array<Carlist>;
  FCards: any = { FCarCards: [], FMotoCards: [], FTravelCards: [] };
  FCarCards: Array<any>;
  FMotoCards: Array<any>;
  FTravelCards: Array<any>;
  FInterval: string;
  FAffect: string;
  FAffectIndex: number;
  FType: string;

  // 大方塊選擇
  FTitleList: Array<string>;

  // 開關元件
  FState = 0;
  FSlice = false;
  FTypeIndex = 0;
  // 汽車下拉清單開關

  // 線下保單開通開關
  FValidatePolicy: boolean;

  // paramMap
  selectItem: string;

  get arrAffect() {
    return this.FTypeIndex === 3
      ? [
          this.FLang.F021.L0036,
          this.FLang.F021.L0014,
          this.FLang.F021.L0015,
          this.FLang.F021.L0016,
        ]
      : [
          this.FLang.F021.L0036,
          this.FLang.F021.L0014,
          this.FLang.F021.L0015,
          this.FLang.F021.L0016,
          this.FLang.F021.L0017,
        ];
  }

  async ngOnInit() {
    this.arrInterval = [
      this.FLang.F021.L0035,
      this.FLang.F021.L0010,
      this.FLang.F021.L0011,
      this.FLang.F021.L0012,
      this.FLang.F021.L0013,
    ];
    this.FTitleList = [
      this.FLang.F021.L0009,
      this.FLang.F021.L0004,
      this.FLang.F021.L0003,
      this.FLang.F021.L0005,
    ];
    this.FInterval = this.arrInterval[0];
    this.FAffect = this.arrAffect[0];
    this.FValidatePolicy =
      this.cds.userInfo.is_validate_policy === 'N' ? true : false;

    await this.initData();

    this.setInterval(0);
  }

  ngAfterViewInit() {
    /** 保留 */
  }

  async initData() {
    // 呼叫API

    if (!this.policyList) {
      const payload = {
        language: this.FLangType,
        token: this.cds.gToken,
        certificate_number_9: this.cds.userInfo.certificate_number_9,
        certificate_number_12: this.cds.userInfo.certificate_number_12,
        customer_name: this.cds.userInfo.customer_name.trim(),
        certificate_type: '1',
        birthday: this.cds.userInfo.birthday.replace(
          /(\d{4})(\d{2})(\d{2})/,
          '$1-$2-$3'
        ),
        policy_page: 'All',
        policy_date: 'All',
        client_target: this.cds.userInfo.certificate_number_12
          ? this.cds.userInfo.certificate_number_12
          : this.cds.userInfo.certificate_number_9,
        // email: this.cds.userInfo.email,
        // is_validate_policy: 'Y',
        is_validate_policy: this.cds.userInfo.is_validate_policy,
      };
      this.policyList = await this.api.getPolicyList(payload);
    }

    // 排列車號、引擎、車身 容器[FCarCards]
    this.FCarCards = [];
    if (this.policyList.data.CarQueryList) {
      this.policyList.data.CarQueryList.forEach((item) => {
        const index = this.FCarCards.findIndex(
          (card) => card.rgst_no === item.rgst_no
        );

        if (index === -1) {
          this.FCarCards.push({
            display_rgst_no: item.rgst_no || item.fram_no || item.engn_no,
            data: [].concat(item),
            pol_dt: item.pol_dt,
            pol_due_dt: item.pol_due_dt,
            slice: true,
            rgst_no: item.rgst_no,
            xid: item.pol_no,
            pol_no_arr: [item.pol_no],
          });
        } else {
          this.FCarCards[index].data.push(item);
          this.FCarCards[index].pol_no_arr.push(item.pol_no);
        }
      });
    }

    this.FMotoCards = [];
    // 排列車號、引擎、車身 容器[FMotoCards] rgst_no > fram_no > engn_no
    if (this.policyList.data.ScooterQueryList) {

      const rgst_noArr = this.policyList.data.ScooterQueryList.filter((data) => {
        return data.rgst_no;
      });
      const noRgst_noDArr = this.policyList.data.ScooterQueryList.filter((data) => {
        return !data.rgst_no;
      });

      rgst_noArr.forEach((item) => {
        const reg = this.FMotoCards.findIndex(
          (card) => {
            if (card.rgst_no){
              return card.rgst_no === item.rgst_no;
            }
            return false;
          }
        );
        if (reg === -1) {
          this.FMotoCards.push({
            display_rgst_no: item.rgst_no,
            data: [].concat(item),
            pol_dt: item.pol_dt,
            pol_due_dt: item.pol_due_dt,
            slice: true,
            rgst_no: item.rgst_no,
            xid: item.pol_no,
            pol_no_arr: [item.pol_no],
            fram_no: item.fram_no,
            engn_no: item.engn_no,
          });
        } else {
          this.FMotoCards[reg].data.push(item);
          this.FMotoCards[reg].pol_no_arr.push(item.pol_no);
        }
      });

      noRgst_noDArr.forEach((item) => {
        if (item.fram_no && item.engn_no) {
          const reg = this.FMotoCards.findIndex(
            (card) => {
              if (!card.rgst_no){
                return card.fram_no === item.fram_no && card.engn_no === item.engn_no;
              }
              return false;
            }
          );

          if (reg === -1) {
            this.FMotoCards.push({
              display_rgst_no: item.fram_no,
              data: [].concat(item),
              pol_dt: item.pol_dt,
              pol_due_dt: item.pol_due_dt,
              slice: true,
              rgst_no: item.rgst_no,
              xid: item.pol_no,
              pol_no_arr: [item.pol_no],
              fram_no: item.fram_no,
              engn_no: item.engn_no,
            });
          } else {
            this.FMotoCards[reg].data.push(item);
            this.FMotoCards[reg].pol_no_arr.push(item.pol_no);
          }
        }
      });
    }
    this.FTravelCards = [];
    // 旅遊 容器[FTravelCards]
    if (this.policyList.data.TravelQueryList) {
      this.policyList.data.TravelQueryList.forEach((item) => {
        this.FTravelCards.push({
          prod_pol_code: item.prod_pol_code,
          prod_pol_name: item.prod_pol_name,
          pol_dt: item.pol_dt,
          pol_due_dt: item.pol_due_dt,
          insr_info: item.insr_info,
          is_apc: item.is_apc,
          endrsmnt_count: item.endrsmnt_count,
          is_group: item.is_group,
          is_insr: item.is_insr,
          data: [].concat(item),
          slice: true,
        });
      });
    }

    ['FCarCards', 'FMotoCards', 'FTravelCards'].forEach((prop) => {
      this[prop] = this[prop].sort(
        (a, b) =>
          this.getStatus(a.pol_dt, a.pol_due_dt) -
          this.getStatus(b.pol_dt, b.pol_due_dt)
      );
    });

    this.route.queryParams.subscribe((queryParams) => {
      if (queryParams && queryParams.pol_no) {
        const marr = ['C1', 'C2', 'C5', 'C6', 'CB', 'CA'];

        /**
         * 汽車  CB   C1  C2
         * 機車  CA   C5  C6
         */
        if (marr.includes(queryParams.pol_no.substr(4, 2))) {
          let mdata = this.FMotoCards.filter((itme) => {
            return itme.pol_no_arr.includes(queryParams.pol_no);
          });

          if (mdata.length === 0) {
            mdata = this.FCarCards.filter((itme) => {
              return itme.pol_no_arr.includes(queryParams.pol_no);
            });
          }

          if (mdata.length > 0) {
            setTimeout(() => {
              CommonService.scrollto(mdata[0].xid, 50);
            }, 100);
          }
        } else {
          setTimeout(() => {
            CommonService.scrollto(queryParams.pol_no, 50);
          }, 100);
        }
      }
    });
  }

  /**
   *  @description 查詢目前的狀態訂單
   */
  get getListEmpty() {
    const keys = Object.keys(this.FCards);
    return this.FTypeIndex === 0
      ? keys.every((key) => this.FCards[key]?.length === 0)
      : this.FCards[keys[this.FTypeIndex - 1]]?.length === 0;
  }

  /**
   * @description Splice資料
   * @param index 車牌index
   */
  cardSplice(index) {
    this.FCarCards[index].slice = !this.FCarCards[index].slice;
  }

  /**
   * @description Splice資料
   * @param index 車牌index
   */
  motoSplice(index) {
    this.FMotoCards[index].slice = !this.FMotoCards[index].slice;
  }

  /**
   * @description Splice 資料
   * @param index 旅遊險index
   */
  travelSplice(index) {
    this.FTravelCards[index].slice = !this.FTravelCards[index].slice;
  }

  getStatus(polDt, polDueDt) {
    const nowDate = moment();
    const startDate = moment(polDt);
    const endDate = moment(polDueDt);

    if (startDate <= nowDate && nowDate <= endDate) {
      return 1;
    } else if (nowDate < startDate) {
      return 2;
    } else if (endDate < nowDate) {
      return 3;
    }
  }

  // 篩選時間區間
  async setInterval(type: number | string) {
    if (typeof type === 'string') {
      type = this.arrInterval.indexOf(type);
    }

    this.FInterval = this.arrInterval[type];

    // await this.initData();

    // 0: 不分日期
    // 1: 三個月內 (90)
    // 2: 半年內 (180)
    // 3: 一年內 (365)
    // 4: 兩年內 (730)
    const mInterval = [0, 90, 180, 365, 730][type] || 0;

    // pol_dt 起始時間 pol_due_dt 截止時間
    /**           今天
     * |-----------|--------------------|
     * |    ^ 過去保單(看過去日期)
     *
     *            今天
     * |-----------|--------------------|
     *                    ^ 未來保單(看起始日期)
     */

    /**
     * 狀態塞選 this.FAffect
     * 0 所有類型
     * 1 生效中 : 生效日 < 系統時間 < 截止日
     * 2 尚未開始 : 系統時間 < 生效日
     * 3 已失效 :  系統時間 >= 截止日
     * 4 可續保 :  系統時間 >= 截止日-60天
     */

    const nowDate = moment();

    ['FCarCards', 'FMotoCards', 'FTravelCards'].forEach((key) => {
      this.FCards[key] = this[key]?.reduce((arr, cur) => {
        const data = cur.data.filter((item) => {
          const endDiff = moment().diff(moment(item.pol_due_dt), 'days');
          return mInterval === 0 || endDiff <= mInterval;
        });
        if (data.length > 0) {
          cur.data = data;
          arr.push(cur);
        }
        return arr;
      }, []);
    });

    // 篩選類型;
    ['FCarCards', 'FMotoCards', 'FTravelCards'].forEach((key) => {
      this.FCards[key] = this.FCards[key]?.reduce((arr, cur) => {
        const data = cur.data.filter((item) => {
          const startDate = moment(item.pol_dt);
          const endDate = moment(item.pol_due_dt);

          switch (this.FAffectIndex) {
            // 生效中
            case 1:
              return startDate <= nowDate && nowDate <= endDate;
            // 尚未生效
            case 2:
              return nowDate < startDate;
            // 已失效
            case 3:
              return endDate < nowDate;
            // 可續保 兩個月
            case 4:
              if (key === 'FTravelCards') {
                return false;
              } else {
                const diff = moment(endDate).diff(
                  moment(nowDate),
                  'days',
                  true
                );
                const isRenewal = 0 <= diff && diff <= 60;
                const onlinePolicyList = [
                  'CF01001',
                  'CF01007',
                  'CF02001',
                  'CF02002',
                ];
                return (
                  // 迄日 60 天前
                  isRenewal &&
                  // 線上保單
                  onlinePolicyList.includes(item.prod_pol_code) &&
                  // 有車牌號
                  item.rgst_no &&
                  // 等於 0 不顯示
                  item.renew_pol_cnt !== '0' &&
                  // 等於 0 顯示
                  item.new_pol_cnt === '0'
                );
              }

            // 不限類型
            default:
              return true;
          }
        });
        if (data.length > 0) {
          cur.data = data;
          arr.push(cur);
        }
        return arr;
      }, []);
    });
  }

  // 塞選類型
  setAffect(type, index) {
    this.FAffect = type;
    this.FAffectIndex = index;
    this.setInterval(this.FInterval);
  }

  doSearch() {}

  /**
   * @description保單明細頁
   * @param type {number} 1. car 2 moto 3 trave
   * @param item 代表的是險種的明細
   */
  goInsuranceDetail(type: number, item: any) {
    this.cds.gPolicyCommon = {
      policylist: item.data.map((item) => ({
        cntr_no: item.cntr_no,
        pol_no: item.pol_no,
        prod_pol_code: item.prod_pol_code,
        rgst_no: item.rgst_no,
        is_apc: item.is_apc,
        is_group: item.is_group,
        is_insr: item.is_insr,
        endrsmnt_count: item.endrsmnt_count,
        renew_pol_cnt: item.renew_pol_cnt,
        new_pol_cnt: item.new_pol_cnt,
      })),
      type,
    };
    this.router.navigate(['member/my-policy/detail']);
  }

  goVerify() {
    this.router.navigate(['member/verify']);
  }

  onFilterIdChange() {
    // 旅遊險
    if (this.FTypeIndex === 3) {
      this.setAffect(this.arrAffect[0], 0);
    }
  }
}
