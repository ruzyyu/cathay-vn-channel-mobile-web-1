import { PublicService } from './../../../../core/services/api/public/public.service';
import { AlertServiceService } from './../../../../core/services/alert/alert-service.service';
import { IResponse } from './../../../../core/interface/index';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { CommonService } from '../../../../core/services/common.service';
import { PolicyService } from '../../../../core/services/api/policy/policy.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { Lang } from 'src/types';
declare const $: any;

@Component({
  selector: 'app-policy-detail',
  templateUrl: './policy-detail.component.html',
  styleUrls: ['./policy-detail.component.scss'],
})
export class PolicyDetailComponent implements OnInit {
  FLang: Lang;

  FLangType: string;

  // queryParams
  FType: number;
  FPolicylist: Array<any>;
  FRgstNo: string;
  FProdPolCode: string;
  FIsApc: boolean;
  FIsGroup: boolean;
  FIsInsr: boolean;
  /** 續保資料狀態 0: 不顯示 */
  FRenewPolCnt: boolean;
  /** 續保狀態 0: 顯示 */
  FNewPolCnt: boolean;
  /** 是否有保單變更 */
  FEndrsmntCount: boolean;
  /** 保單在90天內 */
  FIsPolRange90: boolean;

  // 全域變數
  FCarNo: string;
  FPolicyCount: number;
  FPolicyOnlineCount: number;
  FCheckrenew: boolean;

  amtlist: Array<string> = ['injr_amt', 'aset_amt', 'amt'];

  FDatalist: any;

  FPolicyData: any;
  FInsrData: any;
  FPolicyholderData: any;

  constructor(
    private datePipe: DatePipe,
    private router: Router,
    private api: PolicyService,
    private go: Router,
    private cds: DatapoolService,
    private alert: AlertServiceService,
    private apiPublic: PublicService
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }

  async ngOnInit() {
    // 汽車車種
    await this.apiPublic.gCarList();
    // 汽車use類
    await this.apiPublic.gCarTypeList();
    // 機車清單
    await this.apiPublic.gMotorList();
    // 沒有資料返回上一頁
    if (!this.cds.gPolicyCommon) {
      this.router.navigate(['/member/my-policy']);
      return;
    }
    this.FType = this.cds.gPolicyCommon.type;
    this.FPolicylist = this.cds.gPolicyCommon.policylist;
    this.FRgstNo = this.cds.gPolicyCommon.policylist[0].rgst_no;

    if (this.FType === 3) {
      this.FProdPolCode = this.FPolicylist[0].prod_pol_code;
      this.FIsApc = this.FPolicylist[0].is_apc === 'Y';
      this.FIsGroup = this.FPolicylist[0].is_group === 'Y';
      this.FIsInsr = this.FPolicylist[0].is_insr === 'Y';

      this.FEndrsmntCount = this.FPolicylist[0].endrsmnt_count !== '0';
    } else {
      this.FRenewPolCnt = this.FPolicylist[0].renew_pol_cnt === '0';
      this.FNewPolCnt = this.FPolicylist[0].new_pol_cnt === '0';
    }

    let rep: IResponse;
    let payload: { [key: string]: any } = {
      language: this.FLangType,
      token: this.cds.gToken,
      certificate_number_9: this.cds.userInfo.certificate_number_9,
      certificate_number_12: this.cds.userInfo.certificate_number_12,
      customer_name: this.cds.userInfo.customer_name.trim(),
      certificate_type: 1,
      birthday: this.cds.userInfo.birthday,
      is_validate_policy: this.cds.userInfo.is_validate_policy,
    };

    switch (this.FType) {
      // 汽車險
      case 1:
        payload = {
          ...payload,
          policylist: this.FPolicylist,
          client_target: this.cds.userInfo.certificate_number_12
            ? this.cds.userInfo.certificate_number_12
            : this.cds.userInfo.certificate_number_9,
        };
        rep = await this.api.getCarQueryDetail(payload);
        if (rep.status === 200) {
          this.FDatalist = rep.data.map((item) => {
            item.coverages = item.coverages.map((cgItem) => {
              const coverages: any = {};
              cgItem.coverageItems.forEach((ciItem) => {
                if (!coverages[ciItem.getamt_field]) {
                  coverages[ciItem.getamt_field] = [];
                }
                coverages[ciItem.getamt_field].push(ciItem);
              });
              cgItem.coverages = Object.keys(coverages).map((key) => ({
                group: key,
                coverageItems: coverages[key],
              }));
              return cgItem;
            });
            return item;
          });
        }
        break;
      // 機車險
      case 2:
        payload = {
          ...payload,
          policylist: this.FPolicylist,
          client_target: this.cds.userInfo.certificate_number_12
            ? this.cds.userInfo.certificate_number_12
            : this.cds.userInfo.certificate_number_9,
        };
        rep = await this.api.getMotoQueryDetail(payload);
        if (rep.status === 200) {
          this.FDatalist = rep.data.map((item) => {
            item.coverages = item.coverages.map((cgItem) => {
              const coverages: any = {};
              cgItem.coverageItems.forEach((ciItem) => {
                if (!coverages[ciItem.getamt_field]) {
                  coverages[ciItem.getamt_field] = [];
                }
                coverages[ciItem.getamt_field].push(ciItem);
              });
              cgItem.coverages = Object.keys(coverages).map((key) => ({
                group: key,
                coverageItems: coverages[key],
              }));
              return cgItem;
            });
            return item;
          });
        }
        break;
      case 3:
        payload = {
          ...payload,
          email: this.cds.userInfo.email,
          pol_no: this.FPolicylist[0].pol_no,
          cntr_no: this.FPolicylist[0].cntr_no,
          prod_pol_code: this.FProdPolCode,
          is_apc: this.FIsApc ? 'Y' : 'N',
          is_group: this.FIsGroup ? 'Y' : 'N',
          is_insr: this.FIsInsr ? 'Y' : 'N',
        };

        rep = await this.api.getTravelQueryDetail(payload);
        if (rep.status === 200) {
          this.FDatalist = rep.data;
        }

        break;
    }

    // 查詢保單數量
    if (this.FDatalist && this.FDatalist.length !== 0) {
      this.FPolicyCount = this.FDatalist.length;

      this.FPolicyOnlineCount = this.calcOnlineCount();
      // Renew
      this.FCheckrenew = this.checkRenewal();
      // carNo
      this.FCarNo = this.FDatalist[0].policy_data.pol_no;

      this.FPolicyData = this.FDatalist[0].policy_data;

      const polDt = moment(this.FPolicyData.pol_dt);
      const polDueDt = moment(this.FPolicyData.pol_due_dt);

      this.FIsPolRange90 =
        polDueDt.diff(polDt, 'days', true) <= 90 &&
        polDt.subtract(6, 'hours').diff(moment(), 'days', true) >= 0;

      this.FInsrData = this.FDatalist[0].insr_data;
      this.FPolicyholderData = this.FDatalist[0].policyholder_data;
    }

    CommonService.autoScrollToAccordionHeader('#accordionService');
  }

  dolv3Filter(list, filter) {
    return list.filter((item) => item.getamt_field === filter);
  }

  /**
   * @description 檢核生失效
   */
  checkExpiryDate(item) {
    const mToday = this.datePipe.transform(new Date(), 'yyyyMMdd');
    // 過期
    if (item.policy_data.pol_due_dt < mToday) {
      return 'is-grey';
    }
    // 尚未生效
    else if (item.policy_data.pol_dt > mToday) {
      return 'is-orange';
    }
    // 生效中
    else {
      return 'is-green-middle';
    }
  }

  /**
   * @description 畫面轉換日期
   */
  doDateFormat(item) {
    return CommonService.doDateFormat(item);
  }

  /**
   * @description 確認續保狀態
   */
  checkRenewal() {
    // 旅遊險沒有我要續保
    if (this.FType === 3) {
      return false;
      // B以外皆為線下保單
    }

    return this.FDatalist.every((element) => {
      const item = element.policy_data;
      const diff = moment(item.pol_due_dt).diff(moment(), 'days');
      const isRenewal = 0 <= diff && diff <= 60;
      const onlinePolicyList = ['CF01001', 'CF01007', 'CF02001', 'CF02002'];

      return (
        // 迄日 60 天前
        isRenewal &&
        // 線上保單
        onlinePolicyList.includes(item.prod_pol_code) &&
        // 有車牌號
        this.FRgstNo &&
        // 等於 0 不顯示
        !this.FRenewPolCnt &&
        // 等於 0 顯示
        this.FNewPolCnt
      );
    });
  }

  /** 計算保單數量 */
  calcOnlineCount(): number {
    const items = this.FDatalist;
    return items.filter(
      (item) =>
        item.policy_data.type_pol === 'B' &&
        (item.policy_data.prod_pol_code === 'CF02002' ||
          item.policy_data.prod_pol_code === 'CF01007')
    ).length;
  }

  doShowPdfHint() {
    this.alert.open({
      content: this.FLang.F021.L0033,
      submitTitle: this.FLang.F021.L0034,
      callback: this.openPolicyPdf.bind(this),
    });
  }

  /** 保單下載 */
  openPolicyPdf() {
    this.savePolicy();
    window.open('member/my-policy-download', '_blank');
  }

  savePolicy() {
    const cntrNo = this.FPolicylist[0].cntr_no || '';
    localStorage.setItem('SavePolicy_language', this.FLangType);
    localStorage.setItem(
      'SavePolicy_client_target',
      this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9
    );
    localStorage.setItem('SavePolicy_token', this.cds.gToken);
    localStorage.setItem('SavePolicy_cntr_no', cntrNo);
    localStorage.setItem('SavePolicy_type_pol', 'B');
  }

  /** 跳線上投保 */
  async goOnlineInsurance() {
    const mArr = this.cds.gPolicyCommon.policylist;
    const cntr_arr = [];
    if (mArr && Array.isArray(mArr) && mArr.length > 0) {
      mArr.forEach((item) => {
        cntr_arr.push(item.cntr_no);
      });
    }

    const dueSoonDetail = await this.api.GetDueSoonDetail({
      language: this.FLangType,
      token: this.cds.gToken,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      certificate_type: '1',
      certificate_number_9: this.cds.userInfo.certificate_number_9,
      certificate_number_12: this.cds.userInfo.certificate_number_12,
      customer_name: this.cds.userInfo.customer_name.trim(),
      birthday: this.cds.userInfo.birthday,
      is_validate_policy: this.cds.userInfo.is_validate_policy,
      member_seq_no: this.cds.userInfo.seq_no,
      cntr_no: cntr_arr,
    });

    if (dueSoonDetail.status === 200) {
      const polDt = CommonService.addOneYear(
        dueSoonDetail.data.pol_dt,
        'yyyy-MM-DD'
      );
      const polDueDt = CommonService.addOneYear(
        dueSoonDetail.data.pol_due_dt,
        'yyyy-MM-DD'
      );

      // 僅續保會是 true
      this.cds.gTrafficType = true;
      this.cds.gTrafficicinsurance = {
        ...dueSoonDetail.data,
        pol_dt: polDt,
        pol_due_dt: polDueDt,
      };

      setTimeout(() => {
        this.FType === 2
          ? this.go.navigate(['online-insurance/project-trial-motor/calcAmt'])
          : this.go.navigate(['online-insurance/project-trial-car/calcAmt']);
      });
    }
  }

  /** 顯示保單變更提示 */
  goInsuranceTip() {
    $('app-policy-detail #Modal-21').modal('show');
  }

  /**
   * @description 旅遊險特有 [保單變更]
   *
   */
  goInsuranceChanging() {
    const { prod_pol_code, pol_dt, pol_due_dt, pol_no } = this.FPolicyData;
    const policyCommon = this.cds.gPolicyCommon;
    const policy =
      policyCommon.policylist && policyCommon.policylist.length > 0
        ? policyCommon.policylist[0]
        : null;

    if (policy) {
      this.cds.xinsuranceChangingData = {
        prod_pol_code,
        pol_dt,
        pol_due_dt,
        pol_no,
        cntr_no: policy.cntr_no,
      };

      setTimeout(() => {
        this.go.navigate(['member/my-policy/insurance-changing']);
      }, 200);
    }
    $('app-policy-detail #Modal-21').modal('hide');
  }
}
