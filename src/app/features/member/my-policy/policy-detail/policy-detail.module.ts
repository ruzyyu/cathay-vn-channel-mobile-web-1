import { PolicyDetailTravelComponent } from './components/policy-detail-travel/policy-detail-travel.component';
import { SharedModule } from '../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PolicyDetailRoutingModule } from './policy-detail-routing.module';
import { PolicyDetailComponent } from './policy-detail.component';

import zh from '@angular/common/locales/zh';
import vi from '@angular/common/locales/vi';
import en from '@angular/common/locales/en';
import {registerLocaleData} from '@angular/common';
registerLocaleData(zh);
registerLocaleData(vi);
registerLocaleData(en);
@NgModule({
  declarations: [PolicyDetailComponent, PolicyDetailTravelComponent],
  imports: [CommonModule, PolicyDetailRoutingModule, SharedModule],
})
export class PolicyDetailModule {}
