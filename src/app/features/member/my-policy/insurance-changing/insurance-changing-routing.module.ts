import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InsuranceChangingComponent } from './insurance-changing.component';

const routes: Routes = [{ path: '', component: InsuranceChangingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InsuranceChangingRoutingModule { }
