import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InsuranceChangingRoutingModule } from './insurance-changing-routing.module';
import { InsuranceChangingComponent } from './insurance-changing.component';

@NgModule({
  declarations: [InsuranceChangingComponent],
  imports: [CommonModule, InsuranceChangingRoutingModule, SharedModule],
})
export class InsuranceChangingModule {}
