import { ImportantNoticeDetailComponent } from './components/important-notice-detail/important-notice-detail.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MemberCenterRoutingModule } from './member-center-routing.module';
import { MemberCenterComponent } from './member-center.component';
import { BackgroundComponent } from './components/background/background.component';
import { InsuranceAllComponent } from './components/insurance-all/insurance-all.component';
import { ImportantNoticeComponent } from './components/important-notice/important-notice.component';
import { MyOrderComponent } from './components/my-order/my-order.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    MemberCenterComponent,
    BackgroundComponent,
    InsuranceAllComponent,
    ImportantNoticeComponent,
    MyOrderComponent,
    ImportantNoticeDetailComponent,
  ],
  imports: [CommonModule, MemberCenterRoutingModule, SharedModule],
})
export class MemberCenterModule {}
