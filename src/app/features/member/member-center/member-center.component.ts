import { DatapoolService } from './../../../core/services/datapool/datapool.service';
import { IResponse, UserInfo } from './../../../core/interface/index';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PolicyService } from 'src/app/core/services/api/policy/policy.service';
import { StorageService } from 'src/app/core/services/storage.service';
import { StorageKeys } from 'src/app/core/enums/storage-key.enum';
import { CommonService } from 'src/app/core/services/common.service';

interface IPageinfo {
  car_policy_count: number;
  moto_policy_count: number;
  travel_count: number;
  tmp_count: number;
  renew_count: number;
}
// Member 系列
/** API 保戶服務中心我要續保牌卡 */
interface DueSoonCard {
  cntr_no: string;
  pol_renew_no: string;
  prod_pol_code: string;
  rgst_no: string;
  pol_dt: Date;
  pol_due_dt: Date;
  prod_code: string;
}

/** API 保戶服務中心保單變更牌卡 */
interface EndRsmntCard {
  prod_pol_code: string;
  pol_dt: string;
  pol_due_dt: string;
  pol_no: string;
  cntr_no: string;
  tral_proj: string;
  amt: string;
}
@Component({
  selector: 'app-member-center',
  templateUrl: './member-center.component.html',
  styleUrls: ['./member-center.component.scss'],
})
export class MemberCenterComponent implements OnInit, AfterViewInit {
  constructor(
    private go: Router,
    private api: MemberService,
    private cds: DatapoolService,
    private route: ActivatedRoute,
    private apiPolicy: PolicyService
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
    this.popTitle = this.FLang.popLife.P0001;
    this.popInfo = this.FLang.popLife.P0002;
  }
  FLang: any;
  FLangType: string;
  FNextAction: string;
  FUserInfo: UserInfo;
  FPageinfo: IPageinfo = {
    car_policy_count: 0,
    moto_policy_count: 0,
    travel_count: 0,
    tmp_count: 0,
    renew_count: 0,
  };
  chkMotor = ['CF02001', 'CF01001', 'CF01003'];
  chkCar = ['CF02002', 'CF01002', 'CF01007'];
  FDueSoonCard: DueSoonCard[];
  FEndRsmntCard: EndRsmntCard[];
  /** 變更保單 index */
  FChooseInsuranceIndex: number;

  // POP
  popTitle: string;
  popInfo: string;
  FPop = false;
  async ngOnInit(): Promise<any> {
    /**
     * @description 處理前導頁邏輯判斷
     * 取消router CanActive 由 Router 自訂流程.
     */
    //
    this.route.queryParams.subscribe(async (queryParams) => {
      /**
       * @deprecated
       * 人壽會員跳轉
       * 驗證會員，送給後端解密
       * 成功 - 回應會員資訊，並停留在用戶中心頁
       * 失敗 - 回登入頁
       * @param origin=cathaylife(固定字串)
       * @param param=國壽會員編(需解密)
       * @param LG_CODE=國壽會員編(需解密)
       */
      if (
        queryParams &&
        queryParams.origin &&
        queryParams.origin === 'cathaylife'
      ) {
        // #region  呼叫Mobile
        {
          console.log('checksso memberToken');
          if (queryParams.param) {
            const payload = { data: queryParams.param };
            const rep = await this.api.checksso(payload);
            if (rep.status === 200) {
              // 這邊缺一個token的覆蓋
              this.FUserInfo = this.cds.userInfo;
              await this.init();
              if (rep.data.seq_no === 0) {
                this.cds.userInfoTemp.customer_name = rep.data.customer_name.trim();
                this.cds.userInfoTemp.certificate_number_9 =
                  rep.data.certificate_number_9;
                this.cds.userInfoTemp.certificate_number_12 =
                  rep.data.certificate_number_12;
                this.cds.userInfoTemp.birthday = rep.data.birthday;
                this.cds.userInfoTemp.email = rep.data.email;
                this.cds.userInfoTemp.mobile = rep.data.mobile;
                this.go.navigateByUrl('login/register-data');
              } else {
                this.go.navigateByUrl('member');
              }
              // 做完該做的事情在初始化一次語系
              if (queryParams.LG_CODE !== this.cds.gLangType) {
                let mCode;
                if (queryParams.LG_CODE === 'vi-VN') { mCode = 'VN'; }
                if (queryParams.LG_CODE === 'en-US') { mCode = 'EN'; }
                StorageService.setItem(StorageKeys.lang, mCode);
                // 重導F5
                window.location.reload();
              }
            } else {
              this.go.navigateByUrl('login');
              return;
            }
          } else {
            // 空空，登入
            this.go.navigateByUrl('login');
            return;
          }
        }
        // #endregion
      } else {
        if (!(this.cds.userInfo.seq_no && this.cds.gMobileToken)) {
          // 特殊處理，這頁會有其他導轉規則需自訂
          console.log('spec path');
          this.cds.gPre_url = '/member';
          this.go.navigate(['login'], {
            replaceUrl: true,
          });
          return;
        }
      }
    });
    this.FUserInfo = this.cds.userInfo;
  }

  async init() {
    if (this.cds.userInfo.seq_no) {
      // API GetPolicyCNT
      {
        const payload = {
          language: this.FLangType,
          token: this.cds.gToken,
          client_target: this.FUserInfo.certificate_number_12
            ? this.FUserInfo.certificate_number_12
            : this.FUserInfo.certificate_number_9,
          certificate_type: '1',
          certificate_number_9: this.FUserInfo.certificate_number_9,
          certificate_number_12: this.FUserInfo.certificate_number_12,
          customer_name: this.FUserInfo.customer_name.trim(),
          birthday: this.FUserInfo.birthday,
          is_validate_policy: this.FUserInfo.is_validate_policy,
          member_seq_no: this.FUserInfo.seq_no,
        };
        const rep: IResponse = await this.api.GetPolicyCNT(payload);
        if (rep.status === 200) {
          this.FPageinfo = rep.data;
        }
      }

      const payload = {
        language: this.FLangType,
        token: this.cds.gToken,
        client_target: this.FUserInfo.certificate_number_12
          ? this.FUserInfo.certificate_number_12
          : this.FUserInfo.certificate_number_9,
        certificate_type: '1',
        certificate_number_9: this.FUserInfo.certificate_number_9,
        certificate_number_12: this.FUserInfo.certificate_number_12,
        customer_name: this.FUserInfo.customer_name.trim(),
        birthday: this.FUserInfo.birthday,
        is_validate_policy: this.FUserInfo.is_validate_policy,
        member_seq_no: this.FUserInfo.seq_no,
      };

      let rep: any = await this.apiPolicy.GetEndRsmntCard(payload);
      if (rep.status === 200) {
        this.FEndRsmntCard = rep.data as EndRsmntCard[];
      }

      rep = await this.apiPolicy.GetDueSoonCard(payload);
      if (rep.status === 200) {
        this.FDueSoonCard = rep.data;
        if (Array.isArray(this.FDueSoonCard)) {
          this.FPageinfo.renew_count = this.FDueSoonCard.length;
        }
      }
    }
  }

  async ngAfterViewInit() {
    await this.init();

    /**
     * @deprecated
     * 首頁進入接續動作
     * @param whocall=keep(開啟我要續保窗體)
     *
     */
    this.route.queryParams.subscribe(async (queryParams) => {
      /** 判斷未登入前點選我要續保，登入後可直接開啟pop窗體 */
      const mAction = ['keep', 'policyChange'];
      if (!(this.cds.userInfo.seq_no && this.cds.gMobileToken)) {
        if (mAction.includes(queryParams.whocall)) {
          this.cds.gWhocall = queryParams.whocall;
        } else {
          this.cds.gWhocall = undefined;
        }
        return;
      }
      if (
        (queryParams && mAction.includes(queryParams.whocall)) ||
        this.cds.gWhocall
      ) {
        console.log(queryParams.whocall, this.cds.gWhocall);
        this.FNextAction = this.cds.gWhocall || queryParams.whocall;
        this.cds.gWhocall = undefined;
        this.go.navigate(['/member'], {
          queryParams: { whocall: queryParams.whocall },
        });
      } else {
        this.FNextAction = undefined;
        this.cds.gWhocall = undefined;
      }
    });
  }

  openChooseInsurance(event) {
    if (event === 1) {
      // 續保
      this.cds.gDueSoonCard = this.FDueSoonCard;
      $('#Modal-17').modal('show');
    } else {
      // 變更
      this.cds.gEndRsmntCard = this.FEndRsmntCard;
      $('#Modal-18').modal('show');
    }
  }

  changeInsuranceTip(e) {
    this.cds.xinsuranceChangingData = this.FEndRsmntCard[e];
    $('#Modal-18').modal('hide');
    setTimeout(() => {
      $('#Modal-21').modal('show');
    }, 200);
  }

  goInsuranceChanging() {
    $('#Modal-21').modal('hide');
    setTimeout(() => {
      this.go.navigate(['member/my-policy/insurance-changing']);
    }, 200);
  }

  async goInsuranceRenew(optionCntrNo: string) {
    $('#Modal-17').modal('hide');
    const GetDueSoonDetail = await this.apiPolicy.GetDueSoonDetail({
      language: this.FLangType,
      token: this.cds.gToken,
      client_target: this.FUserInfo.certificate_number_12
        ? this.FUserInfo.certificate_number_12
        : this.FUserInfo.certificate_number_9,
      certificate_type: '1',
      certificate_number_9: this.FUserInfo.certificate_number_9,
      certificate_number_12: this.FUserInfo.certificate_number_12,
      customer_name: this.FUserInfo.customer_name.trim(),
      birthday: this.FUserInfo.birthday,
      is_validate_policy: this.FUserInfo.is_validate_policy,
      member_seq_no: this.FUserInfo.seq_no,
      cntr_no: optionCntrNo,
    });
    if (GetDueSoonDetail.status !== 200) {
    } else {
      // GetDueSoonDetail.data.pol_dt = moment(GetDueSoonDetail.data.pol_dt)
      //   .add(1, 'years')
      //   .format('yyyy-MM-DD hh:mm:ss');
      // GetDueSoonDetail.data.pol_due_dt = moment(
      //   GetDueSoonDetail.data.pol_due_dt
      // )
      //   .add(1, 'years')
      //   .format('yyyy-MM-DD hh:mm:ss');
      GetDueSoonDetail.data.pol_dt = CommonService.addOneYear(
        GetDueSoonDetail.data.pol_dt,
        'yyyy-MM-DD HH:mm:ss'
      );
      GetDueSoonDetail.data.pol_dt = CommonService.addOneYear(
        GetDueSoonDetail.data.pol_due_dt,
        'yyyy-MM-DD HH:mm:ss'
      );

      // 僅續保會是ture ..
      this.cds.gTrafficType = true;
      this.cds.gTrafficicinsurance = GetDueSoonDetail.data;
      setTimeout(() => {
        this.cds.gMotorPluNoList.includes(GetDueSoonDetail.data.prod_pol_code)
          ? this.go.navigate(['online-insurance/project-trial-motor/calcAmt'])
          : this.go.navigate(['online-insurance/project-trial-car/calcAmt']);
      });
    }
  }

  /** 開啟人壽保險pop窗 */
  showLeavePage() {
    $('#popLeavePage').modal('show');
    this.FPop = false;
  }
  /** 點擊開啟人壽保險連結 */
  leavePage(e: boolean) {
    this.FPop = true;
  }
}
