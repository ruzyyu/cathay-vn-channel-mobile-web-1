import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceAllComponent } from './insurance-all.component';

describe('InsuranceAllComponent', () => {
  let component: InsuranceAllComponent;
  let fixture: ComponentFixture<InsuranceAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsuranceAllComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
