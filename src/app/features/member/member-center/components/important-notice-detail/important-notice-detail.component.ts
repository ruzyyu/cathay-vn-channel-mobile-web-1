import { DatapoolService } from './../../../../../core/services/datapool/datapool.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-important-notice-detail',
  templateUrl: './important-notice-detail.component.html',
  styleUrls: ['./important-notice-detail.component.scss'],
})
export class ImportantNoticeDetailComponent implements OnInit {
  constructor(public cds: DatapoolService) {}

  get FLang() {
    return this.cds.gLang;
  }

  get FNoticeDetail() {
    return this.cds.gNoticeDetail;
  }

  ngOnInit() {}
}
