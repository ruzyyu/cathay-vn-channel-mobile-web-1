/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ImportantNoticeDetailComponent } from './important-notice-detail.component';

describe('ImportantNoticeDetailComponent', () => {
  let component: ImportantNoticeDetailComponent;
  let fixture: ComponentFixture<ImportantNoticeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ImportantNoticeDetailComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportantNoticeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
