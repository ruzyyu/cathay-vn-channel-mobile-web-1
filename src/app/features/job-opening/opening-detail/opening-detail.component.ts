import { ActivatedRoute } from '@angular/router';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Component, OnInit } from '@angular/core';
import { jobDetail } from 'src/app/data/job';
import { IJob } from 'src/app/core/interface';
import { Lang } from 'src/types';

@Component({
  selector: 'app-opening-detail',
  templateUrl: './opening-detail.component.html',
  styleUrls: ['./opening-detail.component.scss'],
})
export class OpeningDetailComponent implements OnInit {
  jobId: string;
  jobContent: IJob;
  FData: IJob[];
  FLang: Lang;
  FLangType: string;

  constructor(private cds: DatapoolService, private route: ActivatedRoute) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }
  ngOnInit(): void {
    this.route.queryParams.subscribe((queryParams) => {
      this.jobId = queryParams.jobId;
    });
    this.FData = jobDetail;

    this.jobContent = this.getJobTitle();
  }

  getJobTitle() {
    return this.FData.find((type) => type.id === this.jobId);
  }

  /** 我要應徵
   * job: 職缺類型
   */
  wantToApply(job) {
    switch (job) {
      case 'Indemnify':
        localStorage.setItem(
          'jobContent',
          `${this.FLang.G022.L0001}${this.jobContent.title}`
        );
        break;
      case 'IT':
        localStorage.setItem(
          'jobContent',
          `${this.FLang.G022.L0001}${this.jobContent.title}`
        );
        break;
      case 'ReBao':
        localStorage.setItem(
          'jobContent',
          `${this.FLang.G022.L0001}${this.jobContent.title}`
        );
        break;
      default:
        localStorage.setItem('jobContent', `${this.FLang.G022.L0001}！`);
    }
  }
}
