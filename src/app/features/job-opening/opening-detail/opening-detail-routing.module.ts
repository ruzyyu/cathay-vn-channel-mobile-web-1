import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OpeningDetailComponent } from './opening-detail.component';

const routes: Routes = [{ path: '', component: OpeningDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OpeningDetailRoutingModule {}
