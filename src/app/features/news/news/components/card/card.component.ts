import {
  Component,
  Input,
  OnInit,
  HostListener,
  SimpleChanges,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  constructor(
    private cds: DatapoolService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.FLang = this.cds.gLang;
    // 分類改變時初始化筆數與顯示數量
    this.activatedRoute.queryParamMap.subscribe((params) => {
      if (params.get('type') && this.FType !== params.get('type')) {
        this.FDisplayItem = 10;
        this.FCount = 0;
        this.FType = params.get('type');
      }
    });
  }
  /** 最新消息列表 */
  @Input() newsList: any;

  FLang: any;

  FNewList: any = [];

  windowWidth: number = window.innerWidth;

  // 顯示筆數
  FDisplayItem = 10;
  // 看更多按鈕是否顯示
  FMore: boolean;
  // 記錄點擊次數
  FCount = 0;
  // 存放type狀態
  FType: string;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.windowWidth = event.target.innerWidth;
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.newsList.currentValue.length) {
      // 預設圖片資料轉換
      this.newsList.forEach((item) => {
        if (item.default_image && item.image_url.length === 0) {
          switch (item.default_image) {
            case '1':
              item.image_url.push('assets/img/new/img-latestnews-01.jpg');
              break;
            case '2':
              item.image_url.push('assets/img/new/img-latestnews-02.jpg');
              break;
            case '3':
              item.image_url.push('assets/img/new/img-latestnews-03.jpg');
              break;
            case '4':
              item.image_url.push('assets/img/new/img-latestnews-04.jpg');
              break;
            case '5':
              item.image_url.push('assets/img/new/img-latestnews-05.jpg');
              break;
            default:
              return;
          }
        }
      });

      this.FNewList = this.newsList;

      // 取得資料時判斷是否有看更多按鈕
      if (this.FNewList && this.FNewList.length > this.FDisplayItem) {
        this.FMore = true;
      } else {
        this.FMore = false;
      }
    }
  }

  styleClass(typeId: number) {
    const style = 'label';
    return typeId === 3 ? style + ' is-orange' : style + ' is-green';
  }

  /** To news/news-detail */
  toNewsDetail(tagId, id) {
    this.router.navigate(['/news/news-detail'], {
      queryParams: { type: tagId, option: id },
    });
    // this.router.navigate([`news/news-detail/${tagId}/${id}`]);
  }

  // 每次點擊新增展開數量
  doShowData() {
    this.FCount = this.FCount + 1;
    this.FDisplayItem = this.FDisplayItem + this.FDisplayItem / this.FCount;
    if (this.FNewList.length <= this.FDisplayItem) {
      this.FMore = false;
    }
  }
}
