import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
})
export class DropdownComponent implements OnInit {
  @Input() tagList;

  @Output() tagTitle = new EventEmitter<any>();

  tagId = 'all';

  tagResult: string;

  FLang: any;

  constructor(
    private route: ActivatedRoute,
    private cds: DatapoolService,
    private router: Router
  ) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {
    this.tagId = this.route.snapshot.queryParamMap.get('type');

    this.route.queryParamMap.subscribe((params) => {
      this.tagId = params.get('type');
      this.tagTitle.emit(this.tagId);
      if (!this.tagList) { return; }
      const result: any[] = this.tagList.filter(
        (type) => String(type.tag) === this.tagId
      );
      this.tagResult = result[0]?.label;
      this.getTag(result[0]);
    });
  }
  getTag(item) {
    if (!(this.tagList && item)) { return; }
    const result: any[] = this.tagList.filter((type) => type.tag === item.tag);
    this.tagResult = result[0].label;
    this.tagId = item.tag;
    this.tagTitle.emit(this.tagId);
    const navigationExtras: NavigationExtras = {
      queryParams: { type: item.tag },
    };
    this.router.navigate(['/news'], navigationExtras);
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (!this.tagList) { return; }
    const result: any[] = this.tagList.filter(
      (type) => String(type.tag) === this.tagId
    );
    this.tagResult = result[0]?.label;
    this.getTag(result[0]);
  }
}
