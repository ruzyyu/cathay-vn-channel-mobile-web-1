import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { LocalService } from 'src/app/core/services/api/local/local.service';
import { INews, INewsTag, IResponse } from 'src/app/core/interface';
import moment from 'moment';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent implements OnInit {
  @Input() tagTitle;
  filterTypeId: number | string = 'all';
  type: any;

  FLang: any;
  FtagList: Array<INewsTag>;

  /** 最新消息列表 */
  newsList: Array<INews> = [];

  constructor(
    private cds: DatapoolService,
    private localService: LocalService,
    private go: Router
  ) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {
    this.getNewsList();
  }

  /** 取得最新消息列表 */
  async getNewsList() {
    const rep: IResponse = await this.localService.NewsGetList(0);
    if (rep.status === 200) {
      this.newsList =
        rep.data.sort((a, b) => {
          return (
            moment(b.start_date, 'DD-MM-YYYY').valueOf() -
            moment(a.start_date, 'DD-MM-YYYY').valueOf()
          );
        }) || [];

      this.getTagList();
    }
    // 移除原先try catch .. 底層有攔截
  }

  /** 改變最新消息種類
   * type: 消息類別
   */
  filterTypeIdChange(type) {
    if (type === 'all') {
      this.filterTypeId = type;
    } else {
      this.filterTypeId = Number(type);
    }
  }

  /** 取得公告種類 */
  getTagList() {
    this.FtagList = [
      {
        tag: 'all',
        label: this.FLang.I01.content.I0001,
        queryParams: { type: 'all' },
      },
    ];
    this.newsList.filter((item) => {
      const option = this.FtagList.find(({ tag }) => tag === item.type_id);
      // item.type_id !== 4 排除 "首頁通知"
      if (!option && item.type_id !== 4) {
        this.FtagList.push({
          tag: item.type_id,
          label: item.type,
          queryParams: { type: item.type_id },
        });
      }
    });
  }

  /** 篩選最新消息種類列表 */
  get filterNewsList() {
    if (this.filterTypeId === 'all') {
      return this.newsList;
    }
    return this.newsList.filter((item) => item.type_id === this.filterTypeId);
  }
}
