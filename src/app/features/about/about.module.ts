import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutRoutingModule } from './about-routing.module';
import { AboutComponent } from './about.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { OpeningCardComponent } from './components/opening-card/opening-card.component';
import { MapMarkComponent } from './components/map-mark/map-mark.component';


@NgModule({
  declarations: [AboutComponent, OpeningCardComponent, MapMarkComponent],
  imports: [
    CommonModule,
    AboutRoutingModule,
    SharedModule
  ]
})
export class AboutModule { }
