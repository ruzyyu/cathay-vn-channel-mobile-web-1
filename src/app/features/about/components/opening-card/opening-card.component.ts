import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-opening-card',
  templateUrl: './opening-card.component.html',
  styleUrls: ['./opening-card.component.scss'],
})
export class OpeningCardComponent implements OnInit {
  @Input() cardList: any[];

  slideConfig = {
    dots: false,
    infinite: false,
    variableWidth: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplaySpeed: 1000,
    responsive: [
      {
        breakpoint: 1750,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: false,
          dots: false,
        },
      },
      {
        breakpoint: 1450,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: false,
          dots: false,
        },
      },
      {
        breakpoint: 991.98,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          dots: false,
        },
      },
      {
        breakpoint: 767.98,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
        },
      },
    ],
  };

  constructor(private router: Router) {}

  ngOnInit(): void {}

  clickJob(queryParams: string) {
    this.router.navigate(['about/opening/opening-detail'], {
      queryParams: { jobId: queryParams },
    });
  }
}
