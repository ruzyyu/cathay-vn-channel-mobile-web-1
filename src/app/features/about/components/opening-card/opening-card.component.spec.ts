import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpeningCardComponent } from './opening-card.component';

describe('OpeningCardComponent', () => {
  let component: OpeningCardComponent;
  let fixture: ComponentFixture<OpeningCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpeningCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpeningCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
