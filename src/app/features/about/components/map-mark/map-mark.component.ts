import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { IAboutMap } from 'src/app/core/interface';
import { CommonService } from 'src/app/core/services/common.service';
import { DatapoolService } from '../../../../core/services/datapool/datapool.service';

@Component({
  selector: 'app-map-mark',
  templateUrl: './map-mark.component.html',
  styleUrls: ['./map-mark.component.scss'],
})
export class MapMarkComponent implements OnInit {
  @Input() mapList: IAboutMap[];
  @Input() FTitle: any[];

  index: any = 'a';

  filterTypeId: string;

  mapSrcUrl: SafeResourceUrl | null;

  /** 變數宣告 */
  FLang: any;

  /** 中心座標 */
  center: google.maps.LatLngLiteral = { lat: 16.059547, lng: 108.211224 };

  /** 地點列表 */
  markers: IAboutMap[] = [];

  /** 地圖縮放大小 */
  mapZoom = 1;

  /** 是否顯示 iframe 版本地圖，單一地點時顯示 */
  showIframeMap = false;

  /** Google Map API 是否已讀取 */
  isApiLoaded: Observable<boolean>;

  constructor(
    private cds: DatapoolService,
    private sanitizer: DomSanitizer,
    httpClient: HttpClient
  ) {
    this.FLang = this.cds.gLang;

    // 載入 Google Map Api
    if (!window.google?.maps) {
      const lang = this.getMapLang();

      this.isApiLoaded = httpClient
        .jsonp(
          `https://maps.googleapis.com/maps/api/js?key=AIzaSyDdl1CsQWavil1NutLZhV18237iTGNd8C8&language=${lang}`,
          'callback'
        )
        .pipe(
          map(() => true),
          catchError(() => of(false))
        );
    } else {
      this.isApiLoaded = of(true);
    }
  }

  ngOnInit(): void {
    this.filterTypeIdChange('a');
  }

  /** Change Map */
  filterTypeIdChange(param) {
    this.index = param;
    this.filterTypeId = param;
    this.mapSrcUrl = this.getMapSrcUrl(param);

    if (param === 'a') {
      this.markers = this.mapList;
      this.center = { lat: 16.059547, lng: 108.211224 };
      this.mapZoom = 5;
      this.showIframeMap = false;
    } else {
      const item = this.mapList.find((item) => item.type === param);
      this.markers = item ? [item] : [];
      this.center = item.position;
      this.mapZoom = 18;
      this.showIframeMap = true;
    }
  }

  /** Return Map Card List */
  get filterMapCardList() {
    return this.mapList.filter((item) => item.type === this.filterTypeId);
  }

  /** Get google map iframe src url */
  getMapSrcUrl(filterType) {
    const type = filterType === 'a' ? 'b' : filterType;
    const url = this.mapList.find((item) => item.type === type)?.iframeSrc;

    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  /** RWD 取得 Map 下拉選單選到的名稱 */
  get getMapCardTitle() {
    switch (this.filterTypeId) {
      case 'a':
        return this.FLang.G01.L0007;
      case 'b':
        return this.FLang.G01.L0008;
      case 'c':
        return this.FLang.G01.L0009;
      case 'd':
        return this.FLang.G01.L0010;
      case 'e':
        return this.FLang.G01.L0011;
      case 'f':
        return this.FLang.G01.L0012;
      case 'g':
        return this.FLang.G01.L0013;
      default:
        return '';
    }
  }

  /** 取得 Google Map 語言 */
  getMapLang() {
    switch (CommonService.lang) {
      case 'zh-TW':
        return 'zh-TW';
      case 'EN':
        return 'en';
      default:
        return 'vi';
    }
  }
}
