import { NewsCard } from './../bff/models';

export const newsCard: NewsCard[] = [
  {
    type: 'activity',
    category: 'THÔNG BÁO TIN MỚI',
    img: 'assets/img/img-latestnews-01.jpg',
    date: '15/02/2020',
    title: 'BỆNH MỚI (COVID 19) , CÁC VẤN ĐỀ KHÔNG THƯỜNG GẶP KHI ĐI DU LỊCH',
  },
  {
    type: 'activity',
    category: 'THÔNG BÁO TIN MỚI',
    img: 'assets/img/img-latestnews-02.jpg',
    date: '15/02/2020',
    title: 'CATHAY ĐƯỢC CHỌN LÀM CÔNG TY BẢO HIỂM ƯU TÚ NHẤT TRONG 2019',
  },
  {
    type: 'news',
    category: 'ƯU ĐÃI',
    img: 'assets/img/img-latestnews-03.jpg',
    date: '15/01/2020 - 31/05/2020',
    title: 'MUA BH DU LỊCH, MUA THÊM GÓI CHUYỂN VÙNG DU LỊCH USD 100,000',
  },
  {
    type: 'news',
    category: 'ƯU ĐÃI',
    img: 'assets/img/img-latestnews-04.jpg',
    date: '15/01/2020 - 31/05/2020',
    title: 'Mua bh người trên xe , rút thưởng iphone 11 hàng tháng',
  },
  {
    type: 'activity',
    category: 'THÔNG BÁO TIN MỚI',
    img: 'assets/img/img-latestnews-05.jpg',
    date: '2019/04/20 - 2019/08/20',
    title: ' Bệnh mới (covid 19) , các vấn đề không thường gặp khi đi du lịch',
  },
];
