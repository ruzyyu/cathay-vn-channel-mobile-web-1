import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPath } from 'src/app/core/enums/api-path.enum';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) { }

  getNewsList(params?: any) {
    return this.http.get<any>(ApiPath.news, { params });
  }
}
