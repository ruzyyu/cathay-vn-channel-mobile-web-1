export interface Tags {
  tag: TagType;
  label: string;
  queryParams: { type: TagType };
}

export type TagType = 'all' | 'activity' | 'news';
