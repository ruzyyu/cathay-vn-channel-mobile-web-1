export interface AccountLoginTranrs {
  ACCOUNT_TYPE: string;
  ACCOUNT_TOKEN: string;
  EXPIRE_DATE_TIME: string;
}