export interface MwHeaderRes {
  MSGID: string;
  O360SEQ: string;
  RETURNCODE: string;
  RETURNDESC: string;
  TXNSEQ: string;
}
