export interface PageRes<T> {
  pageSize: number;
  pageIndex: number;
  amount: number;
  result: T[];
}
