import jQuery from 'jquery';

declare global {
  const Waypoint: Waypoint;
  const WOW: WOW;
}

declare module '@angular/core' {
  interface ModuleWithProviders<T = any> {
    ngModule: Type<T>;
    providers?: Provider[];
  }
}

type Waypoint = {
  new (param: {
    element: HTMLElement;
    handler: (direction: 'down' | 'up') => void;
    offset?: string;
  });
};

type WOW = {
  new (param: { mobile: boolean }): {
    init: Function;
  };
};
