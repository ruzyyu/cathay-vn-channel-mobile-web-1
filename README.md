# CathayVnChannelMobileWeb

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

param - 啟用 `Ahead-of-Time` 編譯
output-hashing all - 生成文件的 hash 內容，並將 hash 更新到文件名上，以破壞遊覽器，強制重新載入

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## 專案結構

<!-- - **src/assets/** - 程式使用到的 全域 scss、字型、圖片檔案，需經過 webpack 處理的檔案 -->

- **src/app/componment** - 組件
- **environments/** - 常數設定、區分不同環境設定檔，暫定有 development、stage、production
- **src/app.routing/** - 路由設定
- **src/app/core/services/api** - 共用邏輯服務，例如：呼叫 API 的服務、共用(商業)邏輯
- **src/app/core/services/until** - 全域 plugin、filter、mixin… 設定
- **src/app/core/services/datapool** - 全域狀態存放位置(狀態管理)
- **src/app/shared/components** - 共用 元件
- **src/app/shared/pipes** - 共用 pipes (金額轉換、保險代號轉換...等)
- **src/app/shared/model** - 共用視窗(alert、confirm...等)
- **src/app/shared** - 共用 function 模組，function 需加上 JSDoc 註解說明用途
- **src/app/feature** - 頁面
- **src/app/app.module** - Root
- **src/main.js** - 程式進入點

## 共用函式註解方式(建立 JavaScript IntelliSense 的 JSDoc 註解)

### https://docs.microsoft.com/zh-tw/visualstudio/ide/create-jsdoc-comments-for-javascript-intellisense

| JSDoc 標記     | 語法                           | 備註                                                           |
| -------------- | ------------------------------ | -------------------------------------------------------------- |
| `@deprecated`  | `@deprecated` 描述             | 指定取代函式或方法。                                           |
| `@description` | `@description` 描述            | 指定函式或方法的描述。                                         |
| `@export`      | `@export` moduleName           | 指定函式或方法的輸出模組名稱。                                 |
| `@param`       | `@param` {類型} 參數名稱描述   | 指定函式或方法中之參數的資訊。 TypeScript 也支援 `@paramTag`。 |
| `@property`    | `@property` {類型} 屬性名稱    | 指定欄位或物件上所定義成員的資訊，包括描述。                   |
| `@returns`     | `@returns` {類型}              | 指定傳回值。                                                   |
| `@summary`     | `@summary` 描述                | 指定函式或方法的描述 (與 `@description` 相同)。                |
| `@type`        | `@type` {類型}                 | 指定常數或變數的類型。                                         |
| `@typedef`     | `@typedef` {類型} 自訂類型名稱 | 指定自訂類型。                                                 |

```javascript
/** @description Determines the area of a circle that has the specified radius parameter.
 * @param {number} radius The radius of the circle
 * @return {number}
 */
function getArea(radius) {
  var areaVal;
  areaVal = Math.PI * radius * radius;
  return areaVal;
}
```

## 套件

angularx-social-login - https://www.npmjs.com/package/angularx-social-login
ngx-slick - https://www.npmjs.com/package/ngx-slick
jQuery v3.5.1 - https://jquery.com/
js-sha256 - https://www.npmjs.com/package/js-sha256
crypto-js - https://www.npmjs.com/package/crypto-js
git-describe - https://www.npmjs.com/package/git-describe

scully - https://scully.io/docs/learn/getting-started/installation/
xmlbuilder - https://www.npmjs.com/package/xmlbuilder
fast-xml-parser - https://www.npmjs.com/package/fast-xml-parser

## 變數設定

- apiUrl - api 服務網址
- mode - 執行/打包環境資訊，目前有 development、stage 與 production

## 方法撰寫規範

| 動詞   | 名稱      | 描述                            |
| ------ | --------- | ------------------------------- |
| do     | Something | 做什麼事 ex doSubmit            |
| check  | Something | 檢核 ex checkPassword           |
| calc   | Something | 計算 ex calaAmt                 |
| go     | Something | 跳頁 ex goMember                |
| set    | Something | 設定值 ex setMember             |
| get    | Something | 獲取值 ex getArea               |
| Action | Something | 子傳父事件呼叫 ex ActionMetting |

## 變數撰寫規則

| 前贅詞 | 名稱   | 描述         |
| ------ | ------ | ------------ |
| g      | gPub   | 全局說明     |
| F      | FState | 單頁全域變數 |
| m      | mState | 區域變數     |
| x      | xState | 外部傳入     |

```javascript
let FVal
function getArea(xradius)) {
  var mVal;
  mVal = Math.PI * xradius * radius;
  return mVal;
}

FVal = getArea
```

## git type 說明

> type 只允許使用以下類別

| type     | 說明                                                                              |
| -------- | --------------------------------------------------------------------------------- |
| feat     | 新增 / 修改功能 (feature)。                                                       |
| fix      | 修補 bug (bug fix)。                                                              |
| docs     | 文件 (documentation)。                                                            |
| style    | 格式 (不影響程式碼運行的變動 white-space, formatting, missing semi colons, etc)。 |
| refactor | 重構 (既不是新增功能，也不是修補 bug 的程式碼變動)。                              |
| perf     | 改善效能 (A code change that improves performance)。                              |
| test     | 增加測試 (when adding missing tests)。                                            |
| chore    | 建構程序或輔助工具的變動 (maintain)。                                             |
| revert   | 撤銷回覆先前的 commit 例如：revert: type (scope): subject (回覆版本：xxxx)。      |
