import { ScullyConfig, setPluginConfig } from '@scullyio/scully';
import { baseHrefRewrite } from '@scullyio/scully-plugin-base-href-rewrite';

import { getSitemapPlugin } from '@gammastream/scully-plugin-sitemap';
import { getDelayAngularPlugin } from '@flowaccount/scully-plugin-angular-delay';

// import { ResourceType } from 'puppeteer';

const postRenderers = [
  getDelayAngularPlugin({
    routesBlacklist: [],
    delayMilliseconds: 1500,
  }),
];

const SitemapPlugin = getSitemapPlugin();
setPluginConfig(SitemapPlugin, {
  urlPrefix: 'https://uat.cathay-ins.com.vn',
  sitemapFilename: 'sitemap.xml',
  merge: false,
  trailingSlash: false,
  changeFreq: 'monthly',
  priority: [
    '1.0',
    '0.9',
    '0.8',
    '0.7',
    '0.6',
    '0.5',
    '0.4',
    '0.3',
    '0.2',
    '0.1',
    '0.0',
  ],
  // ignoredRoutes: ['/404'],
  routes: {},
});
/**
 * angular port 4000
 * 靜態渲染頁取代原本的 80
 * */

export const config: ScullyConfig = {
  projectRoot: './src',
  projectName: 'cathay-vn-channel-mobile-web',
  outDir: './dist/static',
  // appPort: 4000,
  staticPort: 80,
  // puppeteerLaunchOptions: { executablePath: '/usr/bin/chromium-browser' },
  // ignoreResourceTypes: ['image', 'media', 'font', 'stylesheet'],
  puppeteerLaunchOptions: {
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
  },
  // routes: {
  //   '/online-insurance/project-trial-car/:step': {
  //     type: 'default',
  //     postRenderers: [baseHrefRewrite],
  //     baseHref: '/online-insurance/project-trial-car/calcAmt',
  //   },
  //   '/online-insurance/project-trial-motor/:step': {
  //     type: 'default',
  //     postRenderers: [baseHrefRewrite],
  //     baseHref: '/online-insurance/project-trial-motor/calcAmt',
  //   },
  //   '/online-insurance/project-trial-travel/:step': {
  //     type: 'default',
  //     postRenderers: [baseHrefRewrite],
  //     baseHref: '/online-insurance/project-trial-travel/calcAmt',
  //   },
  // },
  extraRoutes: new Promise((resolve) => {
    resolve([
      '/online-insurance/project-trial-car/calcAmt',
      '/online-insurance/project-trial-motor/calcAmt',
      '/online-insurance/project-trial-travel/calcAmt',
      '/online-insurance/project-trial-car/personInfo',
      '/online-insurance/project-trial-motor/personInfo',
      '/online-insurance/project-trial-travel/personInfo',
      '/online-insurance/project-trial-car/payment',
      '/online-insurance/project-trial-motor/payment',
      '/online-insurance/project-trial-travel/payment',
      '/404',
    ]);
  }),
  // defaultPostRenderers: postRenderers
};
