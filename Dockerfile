FROM klee310/cfh-node-e2e:latest as BUILD

# install chromium && set ENV
RUN apt update
RUN apt install -y chromium

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true
ENV SCULLY_PUPPETEER_EXECUTABLE_PATH /usr/bin/chromium

# Create app directory
WORKDIR /usr/src/app

# join Dependencies
COPY package*.json ./
RUN npm install
COPY . .

# build all
RUN npm run build:prod

# build static
RUN npm run scully

FROM nginx:1.19.6 as NGINX
RUN rm -f /etc/localtime; ln -s /usr/share/zoneinfo/Asia/Saigon /etc/localtime
# RUN apt-get install nginx-plus-module-cookie-flag
# nginx plugin set cookie

COPY --from=BUILD /usr/src/app/dist/static /usr/share/nginx/html
COPY --from=BUILD /usr/src/app/default.conf /etc/nginx/conf.d/default.conf
COPY --from=BUILD /usr/src/app/start.sh /
CMD ["/start.sh"]

# run service
# CMD ["npm","run","scully:start"]

